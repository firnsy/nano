# nano

nano is a minimal, lightweight starter kit. This minimal CSS framework requires SCSS or CSS knowledge to build large projects.

At a glance:
* Bootstrap compatible layout composition
* Responsive everything
* Elegant and consistent adaptive spacings and typography on all devices.
* Light or Dark mode, automatically enabled according to the user preference.
* No dependencies.

# Limitations

Nano can be used without custom CSS for quick or small projects. However, it’s designed as a starting point, like a “reset CSS on steroids” and as such this minimal CSS framework requires SCSS or CSS knowledge to build large projects.

Must be compiled with the Dart Sass compiler. Due to use of recent SASS preprocessors (eg. @use and @forward) LibSass and Ruby Sass are not compatible compilers.

# Documentation

ROFL

Directory structure:

 - scss/ - theming
 - dist/ - vue components



## Customise Vue configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build-libs
```

### Compile documentation
```sh
npm run build-docs
```
