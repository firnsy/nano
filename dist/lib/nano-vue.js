import { ref as R, toRaw as ne, toValue as W, toRefs as Se, computed as O, openBlock as h, createElementBlock as y, createElementVNode as w, normalizeClass as ce, unref as G, createCommentVNode as k, createTextVNode as x, Fragment as F, renderList as Y, toDisplayString as J, renderSlot as P, withDirectives as H, vModelSelect as ae, normalizeProps as pe, guardReactiveProps as me, vModelDynamic as Re, useSlots as be, createBlock as $e, h as de, Comment as Ue, watch as Ae, readonly as Me, toRef as qe, shallowRef as ge, provide as X, inject as M, vModelText as xe, watchEffect as ze, Transition as Be, withCtx as Fe, withModifiers as Ee, getCurrentScope as Ge, onScopeDispose as Ye, getCurrentInstance as We, onMounted as He, nextTick as je } from "vue";
const le = () => {
};
function z(e) {
  return Array.isArray(e);
}
function Tr(e) {
  return typeof e == "boolean";
}
function T(e) {
  return !we(e);
}
function ve(e) {
  return we(e) || e === "";
}
function ie(e, r) {
  if (e === r)
    return !0;
  if (e === null || r === null)
    return !1;
  if (e !== e && r !== r)
    return !0;
  if (typeof e === typeof r && U(e))
    if (z(e)) {
      if (!z(r))
        return !1;
      const { length: a } = e;
      if (a === r.length) {
        for (let l = 0; l < a; l++)
          if (!ie(e[l], r[l]))
            return !1;
        return !0;
      }
    } else {
      const a = {};
      for (const l in e)
        if (!L(e[l])) {
          if (!ie(e[l], r[l]))
            return !1;
          a[l] = !0;
        }
      for (const l in r)
        if (!(l in a) && T(r[l]) && !L(r[l]))
          return !1;
      return !0;
    }
  return !1;
}
function L(e) {
  return typeof e == "function";
}
function Je(e) {
  return e === null;
}
function Z(e) {
  return typeof e == "number";
}
function U(e) {
  return e !== null && typeof e == "object";
}
function Ke(e) {
  return Object.prototype.toString.call(e) === "[object Object]";
}
function Cr(e) {
  return !we(e) && L(e.then) && L(e.catch);
}
function he(e) {
  return typeof e == "string";
}
function Ze(e) {
  return e === void 0;
}
function we(e) {
  return Ze(e) || Je(e);
}
function ee(...e) {
  function r(a, l) {
    return Object.keys(l).forEach((s) => {
      const c = a[s], f = l[s];
      z(c) && z(f) ? a[s] = c.concat(f) : U(c) && U(f) ? a[s] = r({ ...c }, f) : a[s] = f;
    }), a;
  }
  if (e.length < 2)
    throw new Error("deepMerge: this function expects at least 2 objects to be provided");
  if (e.some((a) => !U(a)))
    throw new Error('deepMerge: all values should be of type "object"');
  const n = e.shift();
  let t;
  for (; t = e.shift(); )
    r(n, t);
  return n;
}
const _e = 0, Ve = 1, Te = 2, B = 1, Q = 2, Er = 1, Or = 2, Nr = 3, Dr = 4, se = {
  GROUP_TYPE_AND: {
    label: "AND",
    shortCode: "$and"
  },
  GROUP_TYPE_NOT_AND: {
    label: "NOT AND",
    shortCode: "$nand"
  },
  GROUP_TYPE_OR: {
    label: "OR",
    shortCode: "$or"
  },
  GROUP_TYPE_NOT_OR: {
    label: "NOT OR",
    shortCode: "$nor"
  }
}, Qe = [
  {
    label: "Equals",
    shortCode: "$eq",
    types: ["string", "number", "date", "datetime"]
  },
  {
    label: "Not Equals",
    shortCode: "$neq",
    types: ["string", "number", "date", "datetime"]
  },
  {
    label: "Greater Than",
    shortCode: "$gt",
    types: ["number", "date", "datetime"]
  },
  {
    label: "Greater Than or Equal To",
    shortCode: "$gte",
    types: ["number", "date", "datetime"]
  },
  {
    label: "Less Than",
    shortCode: "$lt",
    types: ["number", "date", "datetime"]
  },
  {
    label: "Less Than or Equal To",
    shortCode: "$lte",
    types: ["number", "date", "datetime"]
  },
  {
    label: "Contains",
    shortCode: "$c",
    types: ["string"]
  },
  {
    label: "Does Not Contain",
    shortCode: "$nc",
    types: ["string"]
  },
  {
    label: "Starts With",
    shortCode: "$sw",
    types: ["string"]
  },
  {
    label: "Ends With",
    shortCode: "$ew",
    types: ["string"]
  }
], Oe = {
  type: B,
  groupType: se.GROUP_TYPE_AND,
  filters: []
}, te = (e) => e, Xe = {
  string: te,
  number: te,
  date: te,
  datetime: te
}, oe = {};
Qe.forEach((e) => {
  oe[e.shortCode] = {
    filterType: Q,
    entry: e
  };
});
Object.values(se).forEach((e) => {
  oe[e.shortCode] = {
    filterType: B,
    entry: e
  };
});
function Ir(e = {}) {
  const r = e.fetch ?? le, n = e.initialPageSize ?? 10, t = e.initialPage ?? 1, a = R(!1), l = R([]), s = R(rt(e.columns ?? [])), c = R({
    column: e.selectionColumn ?? "id",
    global: !1,
    indexes: {}
  }), f = R({
    pageSize: n,
    page: t,
    pageCount: 0,
    itemCount: 0,
    firstItem: 0,
    lastItem: 0
  }), m = R(structuredClone(Oe)), o = R(0), d = R({});
  function u() {
    return structuredClone(Oe);
  }
  function g(I) {
    const V = re(I), S = re(m);
    ie(V, S) || b({ filter: I.value });
  }
  async function b(I, V = !1) {
    if (a.value)
      return;
    a.value = !0;
    let S = {};
    V || (S = ee(
      {
        sorter: ne(d.value),
        pager: {
          page: f.value.page ?? t,
          pageSize: f.value.pageSize ?? n
        }
      },
      I ?? {}
    ), S.filter = re(S.filter ?? m.value), S.sorter = tt(S.sorter));
    try {
      const $ = await r(S);
      l.value = $.items;
      const q = Pe($.filter ?? []);
      m.value = q, o.value = nt(q), d.value = et($.sorter ?? []), f.value.page = $.pager.page, f.value.pageSize = $.pager.pageSize, f.value.pageCount = $.pager.pageCount, f.value.itemCount = $.pager.itemCount, f.value.firstItem = $.pager.itemCount > 0 ? ($.pager.page - 1) * $.pager.pageSize + 1 : 0, f.value.lastItem = $.pager.itemCount > 0 ? f.value.firstItem + $.items.length - 1 : 0;
    } finally {
      a.value = !1;
    }
  }
  function _(I) {
    const V = ee(
      {
        page: f.value.page,
        pageSize: f.value.pageSize
      },
      I
    );
    (V.page !== f.value.page || V.pageSize !== f.value.pageSize) && b({ pager: V });
  }
  function N(I, V, S) {
    S ^ c.value.global ? (Object.keys(c.value.indexes).forEach(($) => {
      delete c.value.indexes[$];
    }), c.value.global = S) : T(I) && (S ^ V ? c.value.indexes[I] = !0 : delete c.value.indexes[I]);
  }
  function j(I) {
    let V = {};
    if (U(I)) {
      const S = ne(W(d));
      Object.values(S).forEach(($) => {
        $.priority += 1;
      }), V = ee(
        {},
        S,
        I
      );
    }
    ie(V, d.value) || b({ sorter: V });
  }
  return b(), {
    isLoading: a,
    refresh: b,
    columns: s,
    rows: l,
    filter: m,
    filterCount: o,
    filterNew: u,
    filterUpdate: g,
    pager: f,
    pagerUpdate: _,
    selection: c,
    selectionUpdate: N,
    sorter: d,
    sorterUpdate: j
  };
}
function Pe(e) {
  let r = {};
  if (z(e)) {
    if (e.length === 0)
      return r;
    const n = at(e[0]);
    if (n.filterType === B)
      r = {
        type: B,
        groupType: n.entry,
        filters: []
      }, e.slice(1).forEach((t) => {
        r.filters.push(Pe(t));
      });
    else if (n.filterType === Q) {
      if (e.length !== 3)
        throw Error("Invalid filter length");
      r = {
        type: Q,
        method: n.entry,
        columnName: e[1],
        argument: e[2]
      };
    } else
      throw Error("Unsupported filter type");
  }
  return r;
}
function et(e) {
  const r = {};
  return e.forEach((n, t) => {
    const a = n[0] === "-" ? Te : Ve, l = n.substr(1);
    r[l] = { direction: a, priority: t };
  }), r;
}
function re(e, r = 0) {
  let n = [];
  const t = ne(W(e));
  if (t.type === B) {
    const a = t.filters.map((l) => re(l, r + 1)).filter((l) => l.length);
    a.length && (n = [t.groupType.shortCode].concat(a));
  } else
    Ke(t) && t.method && t.columnName && t.argument && (n = [
      t.method.shortCode,
      t.columnName,
      t.argument
    ]);
  return n;
}
function tt(e) {
  return Object.entries(e).map(([r, n]) => ee(
    {
      column: r,
      priority: 0
    },
    n
  )).filter((r) => r.direction !== _e).sort((r, n) => r.priority - n.priority).map((r) => (r.direction === Te ? "-" : "+") + r.column);
}
function rt(e) {
  if (!z(e))
    throw Error("Must specify an array");
  const r = {
    visible: !0,
    sortable: !1,
    filterable: !1,
    type: "string",
    sort: {
      direction: _e,
      priority: 0
    }
  };
  return e.map((t) => {
    let a = {};
    if (he(t))
      a = {
        ...r,
        name: t,
        label: t
      };
    else if (U(t))
      a = {
        ...r,
        ...t
      };
    else
      throw Error("Unsupported column definition");
    return L(a.renderer) || (a.renderer = Xe[a.type ?? "string"]), a;
  });
}
function nt(e) {
  let r = structuredClone((e == null ? void 0 : e.filters) ?? []), n = 0;
  for (; r.length > 0; ) {
    const t = r.pop();
    T(t) && (t.type === B ? r = r.concat(structuredClone(t.filters)) : n += 1);
  }
  return n;
}
function at(e) {
  if (!oe.hasOwnProperty(e))
    throw Error("Unknown filter short code");
  return oe[e];
}
const lt = { class: "n-data-table" }, it = { class: "overflow-auto" }, st = { key: 0 }, ot = ["checked", ".indeterminate"], ut = ["data-index", "onClick"], ct = { key: 0 }, dt = {
  key: 0,
  xmlns: "http://www.w3.org/2000/svg",
  height: "16",
  width: "10",
  viewBox: "0 0 320 512"
}, ft = {
  key: 1,
  xmlns: "http://www.w3.org/2000/svg",
  height: "16",
  width: "10",
  viewBox: "0 0 320 512"
}, pt = {
  key: 2,
  xmlns: "http://www.w3.org/2000/svg",
  height: "16",
  width: "10",
  viewBox: "0 0 320 512"
}, mt = { key: 0 }, gt = ["colspan"], vt = ["data-index"], ht = {
  key: 0,
  scope: "row"
}, yt = ["checked", "onClick"], bt = "n-string", fe = !0, Sr = {
  __name: "NDataTable",
  props: {
    selection: {
      type: Object,
      default: null
    },
    rows: {
      type: Array,
      default() {
        return [];
      }
    },
    columns: {
      type: Array,
      default() {
        return [];
      }
    },
    sorter: {
      type: Object,
      default: null
    }
  },
  emits: ["update", "selection-update", "row-update", "column-update"],
  setup(e, { emit: r }) {
    const n = {
      number: "n-number",
      date: "n-date",
      datetime: "n-datetime",
      time: "n-time",
      string: "n-string"
    }, t = e, a = r, { selection: l, sorter: s } = Se(t), c = O(() => t.rows.length > 0), f = O(() => Object.keys(l.value.indexes).length > 0), m = O(() => t.selection !== null), o = O(() => t.columns.filter((i) => i.visible).map((i) => ee(
      {
        filterable: !0,
        sortable: !0,
        sort: {
          direction: _e
        },
        type: "string"
      },
      {
        ...i,
        classes: u(i),
        sort: s.value[i.name] ?? {}
      }
    ))), d = O(() => o.value.length + (m.value ? 1 : 0));
    function u(i) {
      const E = [];
      return i.sortable && E.push("sortable"), E.push(n[i.type] ?? bt), E;
    }
    function g(i) {
      return !!// eslint-disable-next-line no-bitwise
      (l.value.global ^ l.value.indexes[i[t.selection.column]]);
    }
    function b() {
      a("selection-update", {
        global: !l.value.global
      }), a("update", {
        selection: {
          global: !l.value.global
        }
      });
    }
    function _(i) {
      a("selection-update", {
        index: i[t.selection.column],
        state: !g(i),
        global: l.value.global
      }), a("update", {
        selection: {
          index: i[t.selection.column],
          state: !g(i),
          global: l.value.global
        }
      });
    }
    function N(i) {
      var E;
      if (i.sortable) {
        const A = {
          ...ne(s.value)
        };
        A[i.name] = {
          direction: ((((E = i == null ? void 0 : i.sort) == null ? void 0 : E.direction) ?? 0) + 1) % 3,
          priority: 0
        }, a("update", { sort: A });
      }
    }
    function j(i) {
      i.dataTransfer.setData("text/plain", i.target.dataset.index);
    }
    function I(i) {
      i.currentTarget.classList.add("hover");
    }
    function V(i) {
      i.currentTarget.classList.remove("hover");
    }
    function S(i) {
      i.preventDefault();
    }
    function $(i) {
      i.preventDefault();
      const E = i.dataTransfer.getData("text/plain"), A = i.target.parentNode.dataset.index;
      a("column-update", {
        shift: {
          from: E,
          to: A
        }
      });
    }
    function q(i) {
      i.dataTransfer.setData("text/plain", i.target.dataset.index);
    }
    function p(i) {
      i.currentTarget.classList.add("hover");
    }
    function v(i) {
      i.currentTarget.classList.remove("hover");
    }
    function C(i) {
      i.preventDefault();
    }
    function D(i) {
      i.preventDefault();
      const E = i.dataTransfer.getData("text/plain"), A = i.target.parentNode.dataset.index;
      a("row-update", {
        shift: {
          from: E,
          to: A
        }
      });
    }
    return (i, E) => (h(), y("div", lt, [
      w("div", it, [
        w("table", {
          class: ce({ selectable: m.value, draggable: fe, empty: !c.value })
        }, [
          w("thead", null, [
            w("tr", null, [
              m.value ? (h(), y("th", st, [
                w("input", {
                  type: "checkbox",
                  checked: G(l).global,
                  ".indeterminate": f.value,
                  onClick: b
                }, null, 40, ot)
              ])) : k("", !0),
              E[4] || (E[4] = x()),
              (h(!0), y(F, null, Y(o.value, (A, ue) => (h(), y("th", {
                key: A.name,
                class: ce(A.classes),
                "data-index": ue,
                draggable: fe,
                onClick: (K) => N(A),
                onDragstart: j,
                onDragenter: I,
                onDragleave: V,
                onDragover: S,
                onDrop: $
              }, [
                w("span", null, J(A.label), 1),
                E[3] || (E[3] = x()),
                A.sortable ? (h(), y("span", ct, [
                  A.sort.direction === G(Te) ? (h(), y("svg", dt, E[0] || (E[0] = [
                    w("path", { d: "M41 288h238c21.4 0 32.1 25.9 17 41L177 448c-9.4 9.4-24.6 9.4-33.9 0L24 329c-15.1-15.1-4.4-41 17-41z" }, null, -1)
                  ]))) : A.sort.direction === G(Ve) ? (h(), y("svg", ft, E[1] || (E[1] = [
                    w("path", { d: "M279 224H41c-21.4 0-32.1-25.9-17-41L143 64c9.4-9.4 24.6-9.4 33.9 0l119 119c15.2 15.1 4.5 41-16.9 41z" }, null, -1)
                  ]))) : (h(), y("svg", pt, E[2] || (E[2] = [
                    w("path", { d: "M272 288H48.1c-42.6 0-64.2 51.7-33.9 81.9l111.9 112c18.7 18.7 49.1 18.7 67.9 0l112-112c30-30.1 8.7-81.9-34-81.9zM160 448L48 336h224L160 448zM48 224h223.9c42.6 0 64.2-51.7 33.9-81.9l-111.9-112c-18.7-18.7-49.1-18.7-67.9 0l-112 112C-16 172.2 5.3 224 48 224zM160 64l112 112H48L160 64z" }, null, -1)
                  ])))
                ])) : k("", !0)
              ], 42, ut))), 128))
            ])
          ]),
          E[7] || (E[7] = x()),
          w("tbody", null, [
            c.value ? k("", !0) : (h(), y("tr", mt, [
              w("td", {
                class: "no-data",
                colspan: d.value
              }, `
              No data to show.
            `, 8, gt)
            ])),
            E[6] || (E[6] = x()),
            (h(!0), y(F, null, Y(t.rows, (A, ue) => (h(), y("tr", {
              key: A.id,
              "data-index": ue,
              draggable: fe,
              onDragstart: q,
              onDragenter: p,
              onDragleave: v,
              onDragover: C,
              onDrop: D
            }, [
              m.value ? (h(), y("td", ht, [
                w("input", {
                  type: "checkbox",
                  checked: g(A),
                  onClick: (K) => _(A)
                }, null, 8, yt)
              ])) : k("", !0),
              E[5] || (E[5] = x()),
              (h(!0), y(F, null, Y(o.value, (K) => (h(), y("td", {
                key: K.name,
                class: ce(K.classes)
              }, J(K.renderer(A[K.name])), 3))), 128))
            ], 40, vt))), 128))
          ])
        ], 2)
      ])
    ]));
  }
};
const wt = { class: "n-filter-group" }, _t = { class: "n-filter-group-add" }, Tt = ["value"], Ct = {
  key: 0,
  class: "n-filter-group-children"
}, Et = {
  __name: "NFilterGroup",
  props: {
    group: {
      type: Object,
      required: !0
    },
    groupTypes: {
      type: Array,
      required: !0
    },
    removable: {
      type: Boolean,
      required: !0
    }
  },
  emits: ["addFilter", "deleteGroup"],
  setup(e, { emit: r }) {
    const n = e, t = r;
    function a(l) {
      t("addFilter", n.group.filters, l), document.activeElement.blur();
    }
    return (l, s) => (h(), y("div", wt, [
      w("div", _t, [
        P(l.$slots, "groupTypes", {
          group: e.group,
          groupTypes: e.groupTypes
        }, () => [
          H(w("select", {
            "onUpdate:modelValue": s[0] || (s[0] = (c) => e.group.groupType = c)
          }, [
            (h(!0), y(F, null, Y(e.groupTypes, (c) => (h(), y("option", {
              key: c,
              value: c
            }, J(c.label), 9, Tt))), 128))
          ], 512), [
            [ae, e.group.groupType]
          ])
        ]),
        s[8] || (s[8] = x()),
        P(l.$slots, "filterAdd", pe(me({ addFilter: a })), () => [
          w("button", {
            "data-tooltip": "Add Criterion",
            onClick: s[1] || (s[1] = (c) => a(G(Q)))
          }, s[4] || (s[4] = [
            w("svg", {
              xmlns: "http://www.w3.org/2000/svg",
              height: "16",
              width: "12",
              viewBox: "0 0 384 512"
            }, [
              w("path", { d: "M368 224H224V80c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v144H16c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h144v144c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V288h144c8.8 0 16-7.2 16-16v-32c0-8.8-7.2-16-16-16z" })
            ], -1)
          ])),
          s[6] || (s[6] = x()),
          w("button", {
            "data-tooltip": "Add Group",
            onClick: s[2] || (s[2] = (c) => a(G(B)))
          }, s[5] || (s[5] = [
            w("svg", {
              xmlns: "http://www.w3.org/2000/svg",
              height: "16",
              width: "16",
              viewBox: "0 0 512 512"
            }, [
              w("path", { d: "M492.9 354.6L413.2 320l79.7-34.6c12.2-5.3 17.7-19.4 12.5-31.6-5.3-12.2-19.4-17.7-31.6-12.5l-217.2 94.7L71.9 256l170.5-74c12.2-5.3 17.7-19.4 12.5-31.6-5.3-12.2-19.4-17.7-31.6-12.5L19.2 226.6C7.5 231.6 0 243.2 0 256s7.5 24.4 19.1 29.4L98.8 320l-79.7 34.6C7.5 359.6 0 371.2 0 384c0 12.8 7.5 24.4 19.1 29.4l218.3 94.7a46.5 46.5 0 0 0 18.6 3.9c6.3 0 12.7-1.3 18.6-3.9l218.3-94.7c11.6-5 19.2-16.6 19.2-29.4 0-12.9-7.5-24.4-19.1-29.4zM256.5 464.1L71.9 384l87.2-37.8 78.3 34c5.9 2.6 12.3 3.9 18.6 3.9s12.7-1.3 18.6-3.8l78.3-34 87.3 37.9-183.7 80.1zM496 88h-72V16c0-8.8-7.2-16-16-16h-16c-8.8 0-16 7.2-16 16v72h-72c-8.8 0-16 7.2-16 16v16c0 8.8 7.2 16 16 16h72v72c0 8.8 7.2 16 16 16h16c8.8 0 16-7.2 16-16v-72h72c8.8 0 16-7.2 16-16v-16c0-8.8-7.2-16-16-16z" })
            ], -1)
          ]))
        ]),
        s[9] || (s[9] = x()),
        e.removable ? P(l.$slots, "groupDeletion", {
          key: 0,
          deleteGroup: () => t("deleteGroup", e.group)
        }, () => [
          w("button", {
            onClick: s[3] || (s[3] = (c) => t("deleteGroup", e.group))
          }, s[7] || (s[7] = [
            w("svg", {
              xmlns: "http://www.w3.org/2000/svg",
              height: "16",
              width: "10",
              viewBox: "0 0 320 512"
            }, [
              w("path", { d: "M207.6 256l107.7-107.7c6.2-6.2 6.2-16.3 0-22.6l-25-25c-6.2-6.2-16.3-6.2-22.6 0L160 208.4 52.3 100.7c-6.2-6.2-16.3-6.2-22.6 0L4.7 125.7c-6.2 6.2-6.2 16.3 0 22.6L112.4 256 4.7 363.7c-6.2 6.2-6.2 16.3 0 22.6l25 25c6.2 6.2 16.3 6.2 22.6 0L160 303.6l107.7 107.7c6.2 6.2 16.3 6.2 22.6 0l25-25c6.2-6.2 6.2-16.3 0-22.6L207.6 256z" })
            ], -1)
          ]))
        ]) : k("", !0)
      ]),
      s[10] || (s[10] = x()),
      e.group.filters.length ? (h(), y("div", Ct, [
        P(l.$slots, "groupChildren")
      ])) : k("", !0)
    ]));
  }
};
const Ot = { class: "n-filter-criterion" }, Nt = ["value"], Dt = ["value"], It = ["type"], St = {
  __name: "NFilterCriterion",
  props: {
    criterion: {
      type: Object,
      required: !0
    },
    columns: {
      type: Object,
      required: !0
    },
    comparators: {
      type: Array,
      required: !0
    }
  },
  emits: ["updateCriterion", "deleteCriterion"],
  setup(e, { emit: r }) {
    const n = {
      string: "text",
      number: "number",
      date: "date",
      datetime: "datetime-local"
    }, t = e, a = r, l = O(() => t.columns.filter((m) => m.filterable)), s = O(() => t.comparators.filter((m) => m.types.includes(t.criterion.dataType))), c = O(() => n[t.criterion.dataType] ?? "text");
    function f(m) {
      a("updateCriterion", t.criterion, m);
    }
    return (m, o) => (h(), y("div", Ot, [
      P(m.$slots, "columnUpdate", pe(me({ columns: l.value, criterion: e.criterion, updateCriterion: f })), () => [
        H(w("select", {
          "onUpdate:modelValue": o[0] || (o[0] = (d) => e.criterion.columnName = d),
          class: "n-filter-criterian-column",
          onChange: o[1] || (o[1] = (d) => f(d.target.value))
        }, [
          (h(!0), y(F, null, Y(l.value, (d) => (h(), y("option", {
            key: d,
            value: d.name
          }, J(d.label), 9, Nt))), 128))
        ], 544), [
          [ae, e.criterion.columnName]
        ])
      ]),
      o[6] || (o[6] = x()),
      P(m.$slots, "methodUpdate", pe(me({ methods: s.value, criterion: e.criterion })), () => [
        H(w("select", {
          "onUpdate:modelValue": o[2] || (o[2] = (d) => e.criterion.method = d),
          class: "n-filter-criterian-method"
        }, [
          (h(!0), y(F, null, Y(s.value, (d) => (h(), y("option", {
            key: d,
            value: d
          }, J(d.label), 9, Dt))), 128))
        ], 512), [
          [ae, e.criterion.method]
        ])
      ]),
      o[7] || (o[7] = x()),
      P(m.$slots, "argumentUpdate", { criterion: e.criterion }, () => [
        H(w("input", {
          "onUpdate:modelValue": o[3] || (o[3] = (d) => e.criterion.argument = d),
          type: c.value,
          class: "n-filter-criterian-argument"
        }, null, 8, It), [
          [Re, e.criterion.argument]
        ])
      ]),
      o[8] || (o[8] = x()),
      P(m.$slots, "criterionDelete", {
        deleteCriterion: () => m.$emit("deleteCriterion", e.criterion)
      }, () => [
        w("button", {
          onClick: o[4] || (o[4] = (d) => m.$emit("deleteCriterion", e.criterion))
        }, o[5] || (o[5] = [
          w("svg", {
            xmlns: "http://www.w3.org/2000/svg",
            height: "16",
            width: "10",
            viewBox: "0 0 320 512"
          }, [
            w("path", { d: "M207.6 256l107.7-107.7c6.2-6.2 6.2-16.3 0-22.6l-25-25c-6.2-6.2-16.3-6.2-22.6 0L160 208.4 52.3 100.7c-6.2-6.2-16.3-6.2-22.6 0L4.7 125.7c-6.2 6.2-6.2 16.3 0 22.6L112.4 256 4.7 363.7c-6.2 6.2-6.2 16.3 0 22.6l25 25c6.2 6.2 16.3 6.2 22.6 0L160 303.6l107.7 107.7c6.2 6.2 16.3 6.2 22.6 0l25-25c6.2-6.2 6.2-16.3 0-22.6L207.6 256z" })
          ], -1)
        ]))
      ])
    ]));
  }
}, $r = {
  __name: "NFilter",
  props: {
    filter: {
      type: Object,
      required: !0
    },
    filterOptions: {
      type: Object,
      required: !0
    },
    columns: {
      type: Object,
      required: !0
    }
  },
  setup(e) {
    const r = be(), n = e, { filter: t } = Se(n), a = O(() => n.columns.filter((u) => u.filterable)), l = o();
    function s(u, g) {
      if (g === B)
        u.push({
          type: B,
          groupType: se.GROUP_TYPE_AND,
          filters: []
        });
      else {
        const { name: b, type: _ } = a.value[0], N = m(_)[0];
        u.push({
          type: Q,
          columnName: b,
          dataType: _,
          method: N,
          argument: ""
        });
      }
    }
    function c(u) {
      return u.type === B ? de(
        Et,
        {
          group: u,
          groupTypes: Object.values(se),
          removable: u !== t.value,
          onAddFilter: s,
          onDeleteGroup: f
        },
        {
          groupTypes: r.groupTypes,
          filterAdd: r.filterAdd,
          groupDeletion: r.groupDeletion,
          groupChildren: () => u.filters.map(c)
        }
      ) : u.type === Q ? de(
        St,
        {
          criterion: u,
          columns: n.columns,
          comparators: m(u.dataType),
          onUpdateCriterion: d,
          onDeleteCriterion: f
        },
        {
          fieldUpdate: r.fieldUpdate,
          methodUpdate: r.methodUpdate,
          argumentUpdate: r.argumentUpdate,
          critereDelete: r.critereDelete
        }
      ) : de("div", "Unknown filter type");
    }
    function f(u) {
      function g(b, _, N) {
        b === u ? N.splice(_, 1) : b.type === B && b.filters.map(g);
      }
      u !== t.value && g(t.value);
    }
    function m(u) {
      const { criterionTypes: g = [] } = n.filterOptions;
      return g.filter((b) => b.types.includes(u));
    }
    function o() {
      return c(t.value);
    }
    function d(u, g) {
      const { type: b } = a.value.find((_) => _.name === g);
      if (u.dataType !== b) {
        const _ = m(b)[0];
        u.method = _, u.argument = "", u.dataType = b;
      }
    }
    return (u, g) => (h(), $e(G(l)));
  }
};
function ye(e, r = !0, n = []) {
  return e.forEach((t) => {
    if (t !== null) {
      if (typeof t != "object") {
        (typeof t == "string" || typeof t == "number") && n.push(x(String(t)));
        return;
      }
      if (Array.isArray(t)) {
        ye(t, r, n);
        return;
      }
      if (t.type === F) {
        if (t.children === null)
          return;
        Array.isArray(t.children) && ye(t.children, r, n);
      } else
        t.type !== Ue && n.push(t);
    }
  }), n;
}
function $t(e, r) {
  function n(...t) {
    return new Promise((a, l) => {
      Promise.resolve(
        e(
          () => r.apply(null, t),
          { fn: r, args: t }
        )
      ).then(a).catch(l);
    });
  }
  return n;
}
const ke = (e) => e();
function At(e, r = {}) {
  let n, t, a = le;
  const l = (c) => {
    clearTimeout(c), a(), a = le;
  };
  return (c) => {
    const f = W(e), m = W(r.maxWait);
    return n && l(n), f <= 0 || m !== void 0 && m <= 0 ? (t && (l(t), t = null), Promise.resolve(c())) : new Promise((o, d) => {
      a = r.rejectOnCancel ? d : o, m && !t && (t = setTimeout(() => {
        n && l(n), t = null, o(c());
      }, m)), n = setTimeout(() => {
        t && l(t), t = null, o(c());
      }, f);
    });
  };
}
function xt(e = ke) {
  const r = R(!0);
  function n() {
    r.value = !1;
  }
  function t() {
    r.value = !0;
  }
  const a = (...l) => {
    r.value && e(...l);
  };
  return {
    isActive: Me(r),
    pause: n,
    resume: t,
    eventFilter: a
  };
}
function jt(e, r, n = {}) {
  const {
    debounce: t = 0,
    maxWait: a = void 0,
    ...l
  } = n;
  return Le(
    e,
    r,
    {
      ...l,
      eventFilter: At(t, { maxWait: a })
    }
  );
}
function Vt(e, r, n = {}) {
  const {
    eventFilter: t,
    ...a
  } = n, {
    eventFilter: l,
    pause: s,
    resume: c,
    isActive: f
  } = xt(t);
  return {
    stop: Le(
      e,
      r,
      {
        ...a,
        eventFilter: l
      }
    ),
    pause: s,
    resume: c,
    isActive: f
  };
}
function Le(e, r, n = {}) {
  const {
    eventFilter: t = ke,
    ...a
  } = n;
  return Ae(
    e,
    $t(
      t,
      r
    ),
    a
  );
}
const Pt = {
  validate(e) {
    return /^[a-zA-Z]*$/.test(e);
  },
  message: "Must be alphabetic, only letters allowed"
}, kt = {
  validate(e) {
    return /^[a-zA-Z0-9]*$/.test(e);
  },
  message: "Must be alphanumeric, only letters and numbers allowed"
}, Lt = {
  validate(e) {
    return /^\d*$/.test(e);
  },
  message: "Must be numeric, only digits allowed"
}, Rt = {
  validate(e) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(e);
  },
  message: "Must be a valid email address"
}, Ut = {
  validate(e, r) {
    return Z(e) ? e >= r.value : T(e) ? e.length >= r.value : !1;
  },
  params: ["value"],
  message(e, r, n) {
    return Z(r) ? "Must be greater than {value}" : "Must have at least {value} characters";
  }
}, Mt = {
  validate(e, r) {
    return Z(e) ? e <= r.value : T(e) ? e.length <= r.value : !1;
  },
  params: ["value"],
  message(e, r, n) {
    return Z(r) ? "Must be be less than {value}" : "Must have {value} characters at most";
  }
}, qt = {
  validate(e, r) {
    if (Z(e))
      return e >= r.min && e <= r.max;
    if (!T(e))
      return !1;
    const n = e.length;
    return n >= r.min && n <= r.max;
  },
  params: ["min", "max"],
  message(e, r, n) {
    return Z(r) ? "Must be between {min} and {max}" : "Must have at least {min} characters and {max} characters at most";
  }
}, zt = {
  validate(e) {
    return {
      valid: e != null && e !== "",
      required: !0
    };
  },
  message: "Field is required",
  computesRequired: !0
}, Bt = {
  validate(e, r) {
    const n = ve(e), t = ve(r.target);
    return {
      valid: n,
      required: !t
    };
  },
  params: ["target"],
  message: "Field is required if target field is not empty",
  computesRequired: !0
}, Ne = {
  alpha: Pt,
  alphanum: kt,
  digits: Lt,
  email: Rt,
  max: Mt,
  min: Ut,
  minmax: qt,
  required: zt,
  required_if: Bt
};
function Ft(e, r, n = {}) {
  const {
    bail: t = !0,
    manual: a = !1,
    immediate: l = !0,
    debounce: s = 0,
    maxWait: c = void 0,
    validator: f = Ce
  } = n, m = qe(e), o = ge($(m.value)), d = ge({}), u = R(!0), g = R(!l || a), b = O(() => f.schema(W(r))), _ = O(() => q(o.value, d.value)), N = (p = []) => {
    const v = structuredClone(o.value);
    (z(p) ? p.length > 0 ? p : Object.keys(v) : [p]).forEach((D) => {
      v[D].touched = !0, v[D].untouched = !1;
    }), o.value = v;
  }, j = (p) => {
    const v = structuredClone(o.value);
    (z(p) ? p.length > 0 ? p : Object.keys(v) : [p]).forEach((D) => {
      v[D].pristine = !1, v[D].dirty = !0;
    }), o.value = v;
  }, I = (p) => {
    const v = structuredClone(o.value);
    (z(p) ? p.length > 0 ? p : Object.keys(v) : [p]).forEach((D) => {
      v[D].pristine = !0, v[D].dirty = !1, v[D].changed = !1;
    }), o.value = v;
  }, V = (p) => {
    const v = structuredClone(o.value);
    (z(p) ? p.length > 0 ? p : Object.keys(v) : [p]).forEach((D) => {
      v[D].dirty = !0, v[D].pristine = !1, v[D].changed = m.value[D] !== v[D].initialValue;
    }), o.value = v;
  }, S = async (p, v) => {
    u.value = !1, g.value = !1;
    try {
      const i = await b.value.validate(m.value, { bail: t });
      g.value = !0, d.value = {};
    } catch (i) {
      i instanceof Error ? console.error("[n-validator] ", i) : d.value = i;
    } finally {
      u.value = !0;
    }
    const C = structuredClone(o.value);
    T(v) && T(p) && Object.keys(p).forEach((i) => {
      T(p[i]) && p[i] !== v[i] && (C[i].dirty = !0, C[i].pristine = !1), C[i].changed = p[i] !== C[i].initialValue;
    });
    const D = T(b.value) ? b.value.fieldsRequired() : [];
    return Object.keys(C).forEach((i) => {
      C[i].validated = !0, C[i].passed = !0, C[i].failed = C[i].invalid, C[i].required = D.includes(i);
    }), o.value = C, {
      pass: g.value,
      validity: _.value
    };
  };
  return a || jt(
    () => ({ ...m.value }),
    (p, v) => S(p, v),
    { debounce: s, deep: !0, immediate: l, maxWait: c }
  ), {
    isFinished: u,
    pass: g,
    validity: _,
    blur: N,
    dirty: j,
    pristine: I,
    update: V,
    validate: S
  };
  function $(p) {
    return Object.fromEntries(
      Object.keys(p).map((v) => [v, {
        initialValue: p[v],
        changed: !1,
        dirty: !1,
        pristine: !0,
        required: !1,
        touched: !1,
        untouched: !0,
        validated: !1,
        passed: !1,
        failed: !1,
        ariaInv: ""
      }])
    );
  }
  function q(p = {}, v = {}) {
    return Object.fromEntries(
      Object.keys(m.value).map((C) => {
        const D = v[C] || [], i = p[C] || {}, E = i.dirty || i.touched && i.required ? D.length > 0 ? "true" : "false" : "", A = {
          errors: D,
          valid: D.length === 0,
          invalid: D.length > 0,
          changed: i.changed,
          dirty: i.dirty,
          pristine: i.pristine,
          required: i.required,
          touched: i.touched,
          untouched: i.untouched,
          validated: i.validated,
          passed: i.validated && D.length === 0,
          failed: i.validated && D.length > 0,
          ariaInv: E
        };
        return [C, A];
      })
    );
  }
}
class Gt {
  constructor() {
    this._schema = {}, this._rules = {};
  }
  fields() {
    return Object.keys(this._schema);
  }
  fieldsRequired() {
    return Object.keys(this._schema).filter((r) => this._schema[r].required);
  }
  rule(r, n) {
    let t = {
      message: "{_field_} is not valid",
      params: [],
      computesRequired: !1,
      required: !1,
      transform: (a) => a,
      ...this._rules[r] ?? {}
    };
    if (L(n))
      t.validate = n;
    else if (U(n))
      t = {
        ...t,
        ...n
      };
    else
      throw new Error("The validator must be a function or an object");
    if (!L(t.validate))
      throw new Error("A validate function must be defined");
    return this._rules[r] = t, this;
  }
  schema(r) {
    if (!U(r))
      throw new Error("The schema must be an object");
    return Object.keys(r).forEach((n) => {
      let t = r[n], a = t.rules ?? {};
      if (!U(a))
        throw new Error("The rules must be an object");
      this._schema[n] = t, Object.keys(a).forEach((l) => {
        if (!this._rules[l])
          throw new Error(`Cannot find the validator for rule: ${l}`);
      });
    }), this;
  }
  /**
   * @desc validate the data object against the schema
   *
   * @param {object} data - the data object to validate
   * @param {object} options - the options object
   * @returns {Promise}
   *
   * @throws {Error} - if the schema is not defined
   * @throws {Error} - if the data is not an object
   * @throws {Error} - if the validator for a rule is not found
   * @throws {Error} - if the validator does not return an object when computing required
   */
  async validate(r, n = {}) {
    if (!this._schema)
      throw new Error("Cannot validate without a schema");
    if (!U(r))
      throw new Error("The data must be an object");
    const t = {}, a = Object.keys(this._schema);
    for (let l = 0; l < a.length; l++) {
      const s = a[l], c = this._schema[s], f = c.rules, m = r[s], o = Object.keys(f);
      for (let d = 0; d < o.length; d++) {
        const u = o[d], g = this._rules[u], b = Yt(g, f[u]);
        if (!g)
          throw new Error(`Cannot find the validator for rule: ${g.validator}`);
        let _ = await Promise.resolve(g.validate(m, b));
        if (g.computesRequired) {
          if (!U(_) || !T(_.required) || !T(_.valid))
            throw new Error("The validator must return an object, with required and valid keys, when computing required");
          c.required = _.required, _ = _.valid;
        } else
          g.required === !0 && (c.required = !0);
        if (he(_) || _ === !1) {
          const N = Wt(he(_) ? _ : g.message, f[u], s, m);
          if (t[s] = t[s] || [], t[s].push(N), n.bail)
            break;
        }
      }
    }
    return t.length === 0 ? Promise.resolve() : Promise.reject(t);
  }
}
function Yt(e, r) {
  let n = {};
  if (z(r))
    r.forEach((t, a) => {
      n[e.params[a]] = t;
    });
  else if (U(r))
    n = r;
  else
    return n;
  return Object.keys(n).forEach((t) => {
    e.params.includes(t) || delete n[t];
  }), n;
}
function Wt(e, r, n, t) {
  return L(e) && (e = e(n, t, r)), e.replace(/{([^}]+)}/g, (a, l) => l === "_field_" ? n : l === "_value_" ? t : r[l] ?? "");
}
const Ce = new Gt();
Object.keys(Ne).forEach((e) => {
  Ce.rule(e, Ne[e]);
});
function Ar(e, r) {
  Ce.rule(e, r);
}
const Ht = { class: "n-form" }, xr = {
  __name: "NForm",
  props: {
    source: {
      type: Object,
      required: !0
    },
    schema: {
      type: Object,
      required: !0
    }
  },
  emits: ["update:modelValue"],
  setup(e, { emit: r }) {
    const n = e, a = be().default;
    O(() => a ? ye(a()).filter((c) => {
      var f;
      return ((f = c.type) == null ? void 0 : f.__NANO_INPUT__) ?? !1;
    }) : []);
    const { validity: l, blur: s } = Ft(n.source, n.schema);
    return X("NODE_INPUT_SOURCE", n.source), X("NODE_INPUT_VALIDITY", l), X("NODE_INPUT_BLUR", s), (c, f) => (h(), y("form", Ht, [
      P(c.$slots, "default")
    ]));
  }
}, Jt = ["id", "name", "type", "disabled", "aria-invalid"], jr = /* @__PURE__ */ Object.assign({
  __NANO_INPUT__: !0
  // add marker to aide finding input components
}, {
  __name: "NInput",
  props: {
    name: {
      type: String,
      required: !0
    },
    type: {
      type: String,
      required: !0
    },
    modelValue: {
      type: String,
      default: null
    },
    source: {
      type: Object,
      default: null
    },
    validity: {
      type: Object,
      default: null
    },
    blur: {},
    disabled: {
      type: Boolean
    }
  },
  emits: ["update:modelValue"],
  setup(e, { emit: r }) {
    const n = r, t = e, a = M("NODE_INPUT_SOURCE", null), l = M("NODE_INPUT_VALIDITY", null), s = M("NODE_INPUT_BLUR", null), c = O({
      get: () => {
        if (T(a))
          return a[t.name];
        if (T(t.modelValue))
          return t.modelValue;
      },
      set: (d) => {
        T(a) ? a[t.name] = d : T(t.modelValue) && n("update:modelValue", d);
      }
    }), f = O(() => {
      var d, u;
      return T(l) ? ((d = l.value[t.name]) == null ? void 0 : d.ariaInv) ?? "" : T(t.validity) ? ((u = t.validity[t.name]) == null ? void 0 : u.ariaInv) ?? "" : "";
    }), m = t.name + "-error", o = () => {
      L(s) ? s(t.name) : L(t.blur) && t.blur(t.name);
    };
    return (d, u) => H((h(), y("input", {
      "onUpdate:modelValue": u[0] || (u[0] = (g) => c.value = g),
      id: e.name,
      name: e.name,
      type: e.type,
      disabled: e.disabled,
      "aria-invalid": f.value,
      "aria-describedby": m,
      onBlur: o
    }, null, 40, Jt)), [
      [xe, c.value]
    ]);
  }
}), Vr = /* @__PURE__ */ Object.assign({
  __NANO_INPUT__: !0
  // add marker to aide finding tab components
}, {
  __name: "NInputError",
  props: {
    name: {
      type: String,
      required: !0
    },
    validity: {
      type: Object,
      default: null
    }
  },
  emits: ["update:modelValue"],
  setup(e, { emit: r }) {
    const n = e, t = M("NODE_INPUT_VALIDITY", null), a = O(() => T(t) ? t.value[n.name] : T(n.validity) ? n.validity[n.name] : {}), l = n.name + "-error";
    return (s, c) => a.value.ariaInv ? (h(!0), y(F, { key: 0 }, Y(a.value.errors, (f) => (h(), y("small", {
      class: "n-input-error",
      label: l,
      role: "alert"
    }, J(f), 1))), 256)) : k("", !0);
  }
});
const Kt = {
  key: 0,
  open: ""
}, Zt = { key: 0 }, Qt = { key: 1 }, Pr = {
  __name: "NModal",
  props: {
    open: Boolean,
    notEscapable: {
      type: Boolean,
      default: !1
    },
    persistent: {
      type: Boolean,
      default: !1
    }
  },
  emits: ["close", "click:outside"],
  setup(e, { emit: r }) {
    const n = document.documentElement, t = e;
    ze(() => {
      t.open ? n.classList.add("modal-is-open") : n.classList.remove("modal-is-open");
    });
    const a = r, l = be();
    function s() {
      a("close");
    }
    function c() {
      n.classList.add("modal-is-opening");
    }
    function f() {
      n.classList.add("modal-is-closing");
    }
    function m() {
      n.classList.remove("modal-is-opening");
    }
    function o() {
      n.classList.remove("modal-is-closing");
    }
    return (d, u) => (h(), $e(Be, {
      onBeforeEnter: c,
      onAfterEnter: m,
      onBeforeLeave: f,
      onAfterLeave: o
    }, {
      default: Fe(() => [
        e.open ? (h(), y("dialog", Kt, [
          w("article", null, [
            G(l).headerBody ? (h(), y("header", Zt, [
              w("a", {
                "aria-label": "Close",
                class: "close",
                onClick: s
              }),
              u[0] || (u[0] = x()),
              P(d.$slots, "headerBody")
            ])) : k("", !0),
            u[1] || (u[1] = x()),
            P(d.$slots, "default"),
            u[2] || (u[2] = x()),
            G(l).footerBody ? (h(), y("footer", Qt, [
              P(d.$slots, "footerBody")
            ])) : k("", !0)
          ])
        ])) : k("", !0)
      ]),
      _: 3
    }));
  }
};
const Xt = ["aria-invalid"], er = ["id", "type", "placeholder", "aria-label", "aria-invalid"], tr = {
  key: 0,
  xmlns: "http://www.w3.org/2000/svg",
  width: "18",
  height: "16",
  viewBox: "0 0 576 512",
  fill: "currentColor"
}, rr = {
  key: 1,
  xmlns: "http://www.w3.org/2000/svg",
  width: "20",
  height: "16",
  viewBox: "0 0 640 512",
  fill: "currentColor"
}, kr = /* @__PURE__ */ Object.assign({
  __NANO_INPUT__: !0
  // add marker to aide finding input components
}, {
  __name: "NPassword",
  props: {
    name: {
      type: String,
      required: !0
    },
    modelValue: {
      type: String,
      default: null
    },
    placeholder: {
      type: String,
      default: "Password"
    },
    source: {
      type: Object,
      default: null
    },
    validity: {
      type: Object,
      default: null
    },
    blur: {},
    disabled: {
      type: Boolean
    },
    ariaInvalid: {
      type: Boolean,
      default: null
    },
    momentary: {
      type: Boolean,
      default: !1
    },
    toggle: {
      type: Boolean,
      default: !0
    }
  },
  emits: ["update:modelValue"],
  setup(e, { emit: r }) {
    const n = r, t = e, a = M("NODE_INPUT_SOURCE", null), l = M("NODE_INPUT_VALIDITY", null), s = M("NODE_INPUT_BLUR", null), c = O({
      get: () => {
        if (T(a))
          return a[t.name];
        if (T(t.modelValue))
          return t.modelValue;
      },
      set: (N) => {
        T(a) ? a[t.name] = N : T(t.modelValue) && n("update:modelValue", N);
      }
    }), f = O(() => {
      var j, I;
      let N = null;
      return T(l) ? N = (j = l.value[t.name]) == null ? void 0 : j.ariaInv : T(t.validity) ? N = ((I = t.validity[t.name]) == null ? void 0 : I.ariaInv) ?? "" : T(t.ariaInvalid) && (N = t.ariaInvalid ?? ""), ve(N) ? null : N;
    }), m = t.name + "-error", o = R(!1), d = O(() => o.value ? "text" : "password"), u = () => {
      L(s) ? s(t.name) : L(t.blur) && t.blur(t.name);
    }, g = () => {
      t.momentary && (o.value = !0);
    }, b = () => {
      t.momentary && (o.value = !1);
    }, _ = () => {
      t.momentary || (o.value = !o.value);
    };
    return (N, j) => (h(), y("div", {
      class: "n-password",
      "aria-invalid": f.value
    }, [
      H(w("input", {
        "onUpdate:modelValue": j[0] || (j[0] = (I) => c.value = I),
        id: e.name,
        type: d.value,
        placeholder: e.placeholder,
        "aria-label": e.placeholder,
        "aria-invalid": f.value,
        "aria-describedby": m,
        onBlur: u
      }, null, 40, er), [
        [xe, c.value]
      ]),
      j[3] || (j[3] = x()),
      e.toggle ? (h(), y("div", {
        key: 0,
        class: "toggle",
        onClick: _,
        onMousedown: Ee(g, ["left"]),
        onMouseup: Ee(b, ["left"]),
        onMouseleave: b,
        "aria-label": "Toggle password visibility"
      }, [
        o.value ? (h(), y("svg", tr, j[1] || (j[1] = [
          w("path", { d: "M129.1 361.4C93.6 327.2 67.7 286.9 52.5 256c15.1-30.9 41-71.2 76.6-105.4C171.8 109.5 224.9 80 288 80s116.2 29.5 158.9 70.6c35.6 34.3 61.5 74.5 76.6 105.4c-15.1 30.9-41 71.2-76.6 105.4C404.2 402.5 351.1 432 288 432s-116.2-29.5-158.9-70.6zM288 480c158.4 0 258-149.3 288-224C546 181.3 446.4 32 288 32S30 181.3 0 256c30 74.7 129.6 224 288 224zm0-144c-44.2 0-80-35.8-80-80c0-5.4 .5-10.6 1.5-15.7L288 256l-15.7-78.5c5.1-1 10.3-1.5 15.7-1.5c44.2 0 80 35.8 80 80s-35.8 80-80 80zM160 256c0 70.7 57.3 128 128 128s128-57.3 128-128s-57.3-128-128-128c-8.6 0-17 .8-25.1 2.5c-50.3 10-90 49.5-100.3 99.7l-.1 .7c-1.6 8.1-2.5 16.5-2.5 25.1z" }, null, -1)
        ]))) : (h(), y("svg", rr, j[2] || (j[2] = [
          w("path", { d: "M48.4 14.8L29.4 .1 0 38 19 52.7 591.5 497.2l19 14.7L639.9 474l-19-14.7L524 384.1c41.9-44 70.2-93.9 84-128.1C578 181.3 478.4 32 320 32c-66.9 0-123.2 26.6-168.3 63L48.4 14.8zM190.8 125.4C227.6 98 270.8 80 320 80c63 0 116.2 29.5 158.9 70.6c35.6 34.3 61.5 74.5 76.6 105.4c-14.1 28.9-37.6 65.8-69.6 98.5L434 314.2c8.9-17.5 14-37.2 14-58.2c0-70.7-57.3-128-128-128c-8.6 0-17 .8-25.1 2.5c-22.5 4.5-42.9 14.9-59.5 29.5l-44.6-34.6zM395 283.9l-82.2-63.8-8.5-42.6c5.1-1 10.3-1.5 15.7-1.5c44.2 0 80 35.8 80 80c0 9.8-1.8 19.2-5 27.9zm49.9 162.7l-41.6-32.7C377.9 425.3 350.1 432 320 432c-63.1 0-116.2-29.5-158.9-70.6C125.6 327.2 99.7 286.9 84.5 256c9.1-18.7 22.2-40.7 38.9-62.8L85.7 163.5C60.2 197.1 42.1 230.8 32 256c30 74.7 129.6 224 288 224c46.9 0 88.6-13.1 124.9-33.4zm-86.7-68.3L302 334c-23.5-5.4-43.1-21.2-53.7-42.3l-56.1-44.2c-.2 2.8-.3 5.6-.3 8.5c0 70.7 57.3 128 128 128c13.3 0 26.1-2 38.2-5.8z" }, null, -1)
        ])))
      ], 32)) : k("", !0)
    ], 8, Xt));
  }
}), nr = ["id", "name", "disabled", "aria-invalid"], ar = ["value"], Lr = /* @__PURE__ */ Object.assign({
  __NANO_INPUT__: !0
  // add marker to aide finding input components
}, {
  __name: "NSelect",
  props: {
    name: {
      type: String,
      required: !0
    },
    modelValue: {
      type: String,
      default: null
    },
    options: {
      type: [Array, Object],
      required: !0
    },
    source: {
      type: Object,
      default: null
    },
    validity: {
      type: Object,
      default: null
    },
    blur: {},
    disabled: {
      type: Boolean
    }
  },
  emits: ["update:modelValue"],
  setup(e, { emit: r }) {
    const n = r, t = e, a = M("NODE_INPUT_SOURCE", null), l = M("NODE_INPUT_VALIDITY", null), s = M("NODE_INPUT_BLUR", null), c = O({
      get: () => {
        if (T(a))
          return a[t.name];
        if (T(t.modelValue))
          return t.modelValue;
      },
      set: (u) => {
        T(a) ? a[t.name] = u : T(t.modelValue) && n("update:modelValue", u);
      }
    }), f = O(() => Array.isArray(t.options) ? t.options.reduce((u, g) => (u[g] = g, u), {}) : t.options), m = O(() => {
      var u, g;
      return T(l) ? ((u = l.value[t.name]) == null ? void 0 : u.ariaInv) ?? "" : T(t.validity) ? ((g = t.validity[t.name]) == null ? void 0 : g.ariaInv) ?? "" : "";
    }), o = t.name + "-error", d = () => {
      L(s) ? s(t.name) : L(t.blur) && t.blur(t.name);
    };
    return (u, g) => H((h(), y("select", {
      "onUpdate:modelValue": g[0] || (g[0] = (b) => c.value = b),
      id: e.name,
      name: e.name,
      disabled: e.disabled,
      "aria-invalid": m.value,
      "aria-describedby": o,
      onBlur: d
    }, [
      (h(!0), y(F, null, Y(f.value, (b, _) => (h(), y("option", {
        key: _,
        value: _
      }, J(b), 9, ar))), 128))
    ], 40, nr)), [
      [ae, c.value]
    ]);
  }
});
const lr = { class: "ui-signal" }, ir = {
  key: 0,
  "aria-hidden": "true",
  focusable: "false",
  "data-prefix": "fas",
  "data-icon": "signal-alt-1",
  class: "svg-inline--fa fa-signal-alt-1 fa-w-20",
  role: "img",
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 640 512"
}, sr = {
  key: 1,
  "aria-hidden": "true",
  focusable: "false",
  "data-prefix": "fas",
  "data-icon": "signal-alt-2",
  class: "svg-inline--fa fa-signal-alt-2 fa-w-20",
  role: "img",
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 640 512"
}, or = {
  key: 2,
  "aria-hidden": "true",
  focusable: "false",
  "data-prefix": "fas",
  "data-icon": "signal-alt-3",
  class: "svg-inline--fa fa-signal-alt-3 fa-w-20",
  role: "img",
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 640 512"
}, ur = {
  key: 3,
  "aria-hidden": "true",
  focusable: "false",
  "data-prefix": "fas",
  "data-icon": "signal-alt",
  class: "svg-inline--fa fa-signal-alt fa-w-20",
  role: "img",
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 640 512"
}, Rr = {
  __name: "NSignal",
  props: {
    disabled: Boolean,
    signal: {
      type: Number,
      default: 0
    }
  },
  setup(e) {
    const r = e, n = (t) => r.signal <= -100 ? t === 0 : r.signal >= -50 ? t === 3 : Math.floor(2 * (r.signal + 100) / 25) === t;
    return (t, a) => (h(), y("div", lr, [
      n(0) ? (h(), y("svg", ir, a[0] || (a[0] = [
        w("path", {
          fill: "currentColor",
          d: "M96 384H64c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32z"
        }, null, -1)
      ]))) : k("", !0),
      a[4] || (a[4] = x()),
      n(1) ? (h(), y("svg", sr, a[1] || (a[1] = [
        w("path", {
          fill: "currentColor",
          d: "M96 384H64c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32zm160-128h-32c-17.67 0-32 14.33-32 32v192c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V288c0-17.67-14.33-32-32-32z"
        }, null, -1)
      ]))) : k("", !0),
      a[5] || (a[5] = x()),
      n(2) ? (h(), y("svg", or, a[2] || (a[2] = [
        w("path", {
          fill: "currentColor",
          d: "M96 384H64c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32zm160-128h-32c-17.67 0-32 14.33-32 32v192c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V288c0-17.67-14.33-32-32-32zm160-128h-32c-17.67 0-32 14.33-32 32v320c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V160c0-17.67-14.33-32-32-32z"
        }, null, -1)
      ]))) : k("", !0),
      a[6] || (a[6] = x()),
      n(3) ? (h(), y("svg", ur, a[3] || (a[3] = [
        w("path", {
          fill: "currentColor",
          d: "M96 384H64c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32zm160-128h-32c-17.67 0-32 14.33-32 32v192c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V288c0-17.67-14.33-32-32-32zm160-128h-32c-17.67 0-32 14.33-32 32v320c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V160c0-17.67-14.33-32-32-32zM576 0h-32c-17.67 0-32 14.33-32 32v448c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V32c0-17.67-14.33-32-32-32z"
        }, null, -1)
      ]))) : k("", !0)
    ]));
  }
}, cr = ["disabled"], dr = ["id", "aria-selected", "aria-controls"], Ur = /* @__PURE__ */ Object.assign({
  __NANO_TAB__: !0
  // add marker to aide finding tab components
}, {
  __name: "NTab",
  props: {
    name: {
      type: String,
      required: !0
    },
    disabled: {
      type: Boolean,
      default: !1
    }
  },
  setup(e) {
    const r = e, n = M("NODE_TAB_ACTIVE_IK", ""), t = O(() => n.value === r.name), a = O(() => `n-tab-${r.name}`);
    function l() {
      r.disabled || t.value || (n.value = r.name);
    }
    return (s, c) => (h(), y("li", {
      disabled: r.disabled ? "true" : null
    }, [
      w("a", {
        role: "tab",
        id: a.value,
        "aria-selected": t.value,
        "aria-controls": a.value,
        onClick: l
      }, [
        P(s.$slots, "default")
      ], 8, dr)
    ], 8, cr));
  }
}), fr = {
  role: "tablist",
  "aria-label": "tabs",
  class: "n-tabs"
}, Mr = {
  __name: "NTabs",
  props: {
    modelValue: {
      type: String,
      required: !0
    }
  },
  emits: ["update:modelValue"],
  setup(e, { emit: r }) {
    const n = r, t = e, a = O({
      get: () => t.modelValue,
      set: (l) => n("update:modelValue", l)
    });
    return X("NODE_TAB_ACTIVE_IK", a), (l, s) => (h(), y("nav", fr, [
      w("ul", null, [
        P(l.$slots, "default")
      ])
    ]));
  }
}, pr = ["id", "aria-labelledby", "hidden"], qr = /* @__PURE__ */ Object.assign({
  __NANO_TAB__: !0
  // add marker to aide finding tab components
}, {
  __name: "NTabPanel",
  props: {
    name: {
      type: String,
      required: !0
    },
    disabled: {
      type: Boolean
    }
  },
  setup(e) {
    const r = e, n = M("NODE_TAB_ACTIVE_IK", ""), t = O(() => n.value === r.name), a = O(() => `n-tabpanel-${r.name}`), l = O(() => `n-tab-${r.name}`);
    return (s, c) => (h(), y("div", {
      role: "tabpanel",
      id: a.value,
      "aria-labelledby": l.value,
      hidden: !t.value
    }, [
      P(s.$slots, "default")
    ], 8, pr));
  }
}), mr = { class: "n-tab-panels" }, zr = {
  __name: "NTabPanels",
  props: {
    modelValue: {
      type: String,
      required: !0
    }
  },
  emits: ["update:modelValue"],
  setup(e, { emit: r }) {
    const n = r, t = e, a = O({
      get: () => t.modelValue,
      set: (l) => n("update:modelValue", l)
    });
    return X("NODE_TAB_ACTIVE_IK", a), (l, s) => (h(), y("div", mr, [
      P(l.$slots, "default")
    ]));
  }
};
function gr(e) {
  return Ge() ? (Ye(e), !0) : !1;
}
function vr(e) {
  const r = W(e);
  return (r == null ? void 0 : r.$el) ?? r;
}
function De(...e) {
  let r, n, t, a;
  if (typeof e[0] == "string" || Array.isArray(e[0]) ? ([n, t, a] = e, r = window) : [r, n, t, a] = e, !r)
    return le;
  Array.isArray(n) || (n = [n]), Array.isArray(t) || (t = [t]);
  const l = [], s = () => {
    l.forEach((o) => o()), l.length = 0;
  }, c = (o, d, u, g) => (o.addEventListener(d, u, g), () => o.removeEventListener(d, u, g)), f = Ae(
    () => [vr(r), W(a)],
    ([o, d]) => {
      if (s(), !o)
        return;
      const u = U(d) ? { ...d } : d;
      l.push(
        ...n.flatMap((g) => t.map((b) => c(o, g, b, u)))
      );
    },
    { immediate: !0, flush: "post" }
  ), m = () => {
    f(), s();
  };
  return gr(m), m;
}
function hr(e) {
  return e || We();
}
function yr(e, r, n = !0) {
  const t = hr(r);
  t ? He(e, t) : n ? e() : je(e);
}
const br = {
  boolean: {
    read: (e) => e === "true",
    write: (e) => String(e)
  },
  object: {
    read: (e) => JSON.parse(e),
    write: (e) => JSON.stringify(e)
  },
  number: {
    read: (e) => Number.parseFloat(e),
    write: (e) => String(e)
  },
  any: {
    read: (e) => e,
    write: (e) => String(e)
  },
  string: {
    read: (e) => e,
    write: (e) => String(e)
  },
  map: {
    read: (e) => new Map(JSON.parse(e)),
    write: (e) => JSON.stringify(Array.from(e.entries()))
  },
  set: {
    read: (e) => new Set(JSON.parse(e)),
    write: (e) => JSON.stringify(Array.from(e))
  },
  date: {
    read: (e) => new Date(e),
    write: (e) => e.toISOString()
  }
}, Ie = "ui-storage";
function Br(e, r, n, t = {}) {
  const {
    flush: a = "pre",
    deep: l = !0,
    listenToStorageChanges: s = !0,
    writeDefaults: c = !0,
    mergeDefaults: f = !1,
    shallow: m,
    eventFilter: o,
    onError: d = () => {
    },
    initOnMounted: u
  } = t, g = (m ? ge : R)(typeof r == "function" ? r() : r);
  if (!n)
    try {
      n = window == null ? void 0 : window.localStorage;
    } catch (p) {
      d(p);
    }
  if (!n)
    return g;
  const b = W(r), _ = wr(b), N = t.serializer ?? br[_], { pause: j, resume: I } = Vt(
    g,
    () => V(g.value),
    { flush: a, deep: l, eventFilter: o }
  );
  return window && s && yr(() => {
    De(window, "storage", q), De(window, Ie, $), u && q();
  }), u || q(), g;
  function V(p) {
    try {
      if (p == null)
        n.removeItem(e);
      else {
        const v = N.write(p), C = n.getItem(e);
        C !== v && (n.setItem(e, v), window && window.dispatchEvent(new CustomEvent(Ie, {
          detail: {
            key: e,
            oldValue: C,
            newValue: v,
            storageArea: n
          }
        })));
      }
    } catch (v) {
      d(v);
    }
  }
  function S(p) {
    const v = p ? p.newValue : n.getItem(e);
    if (v == null)
      return c && b != null && n.setItem(e, N.write(b)), b;
    if (!p && f) {
      const C = N.read(v);
      return typeof f == "function" ? f(C, b) : _ === "object" && !Array.isArray(C) ? { ...b, ...C } : C;
    }
    return typeof v != "string" ? v : N.read(v);
  }
  function $(p) {
    q(p.detail);
  }
  function q(p) {
    if (!(p && p.storageArea !== n)) {
      if (p && p.key == null) {
        g.value = b;
        return;
      }
      if (!(p && p.key !== e)) {
        j();
        try {
          (p == null ? void 0 : p.newValue) !== N.write(g.value) && (g.value = S(p));
        } catch (v) {
          d(v);
        } finally {
          p ? je(I) : I();
        }
      }
    }
  }
}
function wr(e) {
  return e == null ? "any" : e instanceof Set ? "set" : e instanceof Map ? "map" : e instanceof Date ? "date" : typeof e == "boolean" ? "boolean" : typeof e == "string" ? "string" : typeof e == "object" ? "object" : Number.isNaN(e) ? "any" : "number";
}
export {
  Qe as CriterionTypes,
  Q as FILTER_TYPE_CRITERION,
  B as FILTER_TYPE_GROUP,
  Er as GROUP_TYPE_AND,
  Or as GROUP_TYPE_NOT_AND,
  Dr as GROUP_TYPE_NOT_OR,
  Nr as GROUP_TYPE_OR,
  se as GroupType,
  Sr as NDataTable,
  $r as NFilter,
  St as NFilterCriterion,
  Et as NFilterGroup,
  xr as NForm,
  jr as NInput,
  Vr as NInputError,
  Pr as NModal,
  kr as NPassword,
  Lr as NSelect,
  Rr as NSignal,
  Ur as NTab,
  qr as NTabPanel,
  zr as NTabPanels,
  Mr as NTabs,
  Ve as SORT_ORDER_ASC,
  Te as SORT_ORDER_DESC,
  _e as SORT_ORDER_NONE,
  br as StorageSerializers,
  Gt as Validator,
  Ie as customStorageEventName,
  jt as debouncedWatch,
  Pe as decodeFilter,
  et as decodeSorter,
  re as encodeFilter,
  tt as encodeSorter,
  Ar as extend,
  ye as flatten,
  hr as getLifeCycleTarget,
  z as isArray,
  Tr as isBoolean,
  T as isDefined,
  ve as isEmpty,
  ie as isEqual,
  L as isFunction,
  Je as isNull,
  Z as isNumber,
  U as isObject,
  Ke as isObjectPlain,
  Cr as isPromise,
  he as isString,
  Ze as isUndefined,
  we as isUndefinedOrNull,
  ee as merge,
  Vt as pausableWatch,
  yr as tryOnMounted,
  gr as tryOnScopeDispose,
  vr as unrefElement,
  Ir as useDataTableState,
  De as useEventListener,
  Br as useStorage,
  Ft as useValidator
};
