var _i=(t,o)=>()=>(o||t((o={exports:{}}).exports,o),o.exports);var GE=_i((fi,Ja)=>{(function(){const o=document.createElement("link").relList;if(o&&o.supports&&o.supports("modulepreload"))return;for(const s of document.querySelectorAll('link[rel="modulepreload"]'))l(s);new MutationObserver(s=>{for(const r of s)if(r.type==="childList")for(const i of r.addedNodes)i.tagName==="LINK"&&i.rel==="modulepreload"&&l(i)}).observe(document,{childList:!0,subtree:!0});function a(s){const r={};return s.integrity&&(r.integrity=s.integrity),s.referrerPolicy&&(r.referrerPolicy=s.referrerPolicy),s.crossOrigin==="use-credentials"?r.credentials="include":s.crossOrigin==="anonymous"?r.credentials="omit":r.credentials="same-origin",r}function l(s){if(s.ep)return;s.ep=!0;const r=a(s);fetch(s.href,r)}})();/**
* @vue/shared v3.4.27
* (c) 2018-present Yuxi (Evan) You and Vue contributors
* @license MIT
**//*! #__NO_SIDE_EFFECTS__ */function Za(t,o){const a=new Set(t.split(","));return o?l=>a.has(l.toLowerCase()):l=>a.has(l)}const Ve={},Mn=[],xt=()=>{},vi=()=>!1,Jo=t=>t.charCodeAt(0)===111&&t.charCodeAt(1)===110&&(t.charCodeAt(2)>122||t.charCodeAt(2)<97),Xa=t=>t.startsWith("onUpdate:"),Ze=Object.assign,Qa=(t,o)=>{const a=t.indexOf(o);a>-1&&t.splice(a,1)},yi=Object.prototype.hasOwnProperty,Ae=(t,o)=>yi.call(t,o),ce=Array.isArray,Pn=t=>io(t)==="[object Map]",Dn=t=>io(t)==="[object Set]",El=t=>io(t)==="[object Date]",ge=t=>typeof t=="function",Ke=t=>typeof t=="string",Ht=t=>typeof t=="symbol",Re=t=>t!==null&&typeof t=="object",zs=t=>(Re(t)||ge(t))&&ge(t.then)&&ge(t.catch),Vs=Object.prototype.toString,io=t=>Vs.call(t),wi=t=>io(t).slice(8,-1),js=t=>io(t)==="[object Object]",el=t=>Ke(t)&&t!=="NaN"&&t[0]!=="-"&&""+parseInt(t,10)===t,qn=Za(",key,ref,ref_for,ref_key,onVnodeBeforeMount,onVnodeMounted,onVnodeBeforeUpdate,onVnodeUpdated,onVnodeBeforeUnmount,onVnodeUnmounted"),Zo=t=>{const o=Object.create(null);return a=>o[a]||(o[a]=t(a))},xi=/-(\w)/g,Lt=Zo(t=>t.replace(xi,(o,a)=>a?a.toUpperCase():"")),ki=/\B([A-Z])/g,zn=Zo(t=>t.replace(ki,"-$1").toLowerCase()),Xo=Zo(t=>t.charAt(0).toUpperCase()+t.slice(1)),ma=Zo(t=>t?`on${Xo(t)}`:""),an=(t,o)=>!Object.is(t,o),Ao=(t,o)=>{for(let a=0;a<t.length;a++)t[a](o)},Fs=(t,o,a,l=!1)=>{Object.defineProperty(t,o,{configurable:!0,enumerable:!1,writable:l,value:a})},Lo=t=>{const o=parseFloat(t);return isNaN(o)?t:o},$i=t=>{const o=Ke(t)?Number(t):NaN;return isNaN(o)?t:o};let Tl;const Hs=()=>Tl||(Tl=typeof globalThis<"u"?globalThis:typeof self<"u"?self:typeof window<"u"?window:typeof global<"u"?global:{});function Qo(t){if(ce(t)){const o={};for(let a=0;a<t.length;a++){const l=t[a],s=Ke(l)?Ti(l):Qo(l);if(s)for(const r in s)o[r]=s[r]}return o}else if(Ke(t)||Re(t))return t}const Ci=/;(?![^(]*\))/g,Si=/:([^]+)/,Ei=/\/\*[^]*?\*\//g;function Ti(t){const o={};return t.replace(Ei,"").split(Ci).forEach(a=>{if(a){const l=a.split(Si);l.length>1&&(o[l[0].trim()]=l[1].trim())}}),o}function Fe(t){let o="";if(Ke(t))o=t;else if(ce(t))for(let a=0;a<t.length;a++){const l=Fe(t[a]);l&&(o+=l+" ")}else if(Re(t))for(const a in t)t[a]&&(o+=a+" ");return o.trim()}function Ma(t){if(!t)return null;let{class:o,style:a}=t;return o&&!Ke(o)&&(t.class=Fe(o)),a&&(t.style=Qo(a)),t}const Ai="itemscope,allowfullscreen,formnovalidate,ismap,nomodule,novalidate,readonly",Mi=Za(Ai);function qs(t){return!!t||t===""}function Pi(t,o){if(t.length!==o.length)return!1;let a=!0;for(let l=0;a&&l<t.length;l++)a=Cn(t[l],o[l]);return a}function Cn(t,o){if(t===o)return!0;let a=El(t),l=El(o);if(a||l)return a&&l?t.getTime()===o.getTime():!1;if(a=Ht(t),l=Ht(o),a||l)return t===o;if(a=ce(t),l=ce(o),a||l)return a&&l?Pi(t,o):!1;if(a=Re(t),l=Re(o),a||l){if(!a||!l)return!1;const s=Object.keys(t).length,r=Object.keys(o).length;if(s!==r)return!1;for(const i in t){const c=t.hasOwnProperty(i),d=o.hasOwnProperty(i);if(c&&!d||!c&&d||!Cn(t[i],o[i]))return!1}}return String(t)===String(o)}function tl(t,o){return t.findIndex(a=>Cn(a,o))}const re=t=>Ke(t)?t:t==null?"":ce(t)||Re(t)&&(t.toString===Vs||!ge(t.toString))?JSON.stringify(t,Us,2):String(t),Us=(t,o)=>o&&o.__v_isRef?Us(t,o.value):Pn(o)?{[`Map(${o.size})`]:[...o.entries()].reduce((a,[l,s],r)=>(a[ba(l,r)+" =>"]=s,a),{})}:Dn(o)?{[`Set(${o.size})`]:[...o.values()].map(a=>ba(a))}:Ht(o)?ba(o):Re(o)&&!ce(o)&&!js(o)?String(o):o,ba=(t,o="")=>{var a;return Ht(t)?`Symbol(${(a=t.description)!=null?a:o})`:t};/**
* @vue/reactivity v3.4.27
* (c) 2018-present Yuxi (Evan) You and Vue contributors
* @license MIT
**/let yt;class Ni{constructor(o=!1){this.detached=o,this._active=!0,this.effects=[],this.cleanups=[],this.parent=yt,!o&&yt&&(this.index=(yt.scopes||(yt.scopes=[])).push(this)-1)}get active(){return this._active}run(o){if(this._active){const a=yt;try{return yt=this,o()}finally{yt=a}}}on(){yt=this}off(){yt=this.parent}stop(o){if(this._active){let a,l;for(a=0,l=this.effects.length;a<l;a++)this.effects[a].stop();for(a=0,l=this.cleanups.length;a<l;a++)this.cleanups[a]();if(this.scopes)for(a=0,l=this.scopes.length;a<l;a++)this.scopes[a].stop(!0);if(!this.detached&&this.parent&&!o){const s=this.parent.scopes.pop();s&&s!==this&&(this.parent.scopes[this.index]=s,s.index=this.index)}this.parent=void 0,this._active=!1}}}function Oi(t,o=yt){o&&o.active&&o.effects.push(t)}function Gs(){return yt}function Ri(t){yt&&yt.cleanups.push(t)}let xn;class nl{constructor(o,a,l,s){this.fn=o,this.trigger=a,this.scheduler=l,this.active=!0,this.deps=[],this._dirtyLevel=4,this._trackId=0,this._runnings=0,this._shouldSchedule=!1,this._depsLength=0,Oi(this,s)}get dirty(){if(this._dirtyLevel===2||this._dirtyLevel===3){this._dirtyLevel=1,cn();for(let o=0;o<this._depsLength;o++){const a=this.deps[o];if(a.computed&&(Ii(a.computed),this._dirtyLevel>=4))break}this._dirtyLevel===1&&(this._dirtyLevel=0),dn()}return this._dirtyLevel>=4}set dirty(o){this._dirtyLevel=o?4:0}run(){if(this._dirtyLevel=0,!this.active)return this.fn();let o=nn,a=xn;try{return nn=!0,xn=this,this._runnings++,Al(this),this.fn()}finally{Ml(this),this._runnings--,xn=a,nn=o}}stop(){this.active&&(Al(this),Ml(this),this.onStop&&this.onStop(),this.active=!1)}}function Ii(t){return t.value}function Al(t){t._trackId++,t._depsLength=0}function Ml(t){if(t.deps.length>t._depsLength){for(let o=t._depsLength;o<t.deps.length;o++)Ws(t.deps[o],t);t.deps.length=t._depsLength}}function Ws(t,o){const a=t.get(o);a!==void 0&&o._trackId!==a&&(t.delete(o),t.size===0&&t.cleanup())}let nn=!0,Pa=0;const Ks=[];function cn(){Ks.push(nn),nn=!1}function dn(){const t=Ks.pop();nn=t===void 0?!0:t}function ol(){Pa++}function al(){for(Pa--;!Pa&&Na.length;)Na.shift()()}function Ys(t,o,a){if(o.get(t)!==t._trackId){o.set(t,t._trackId);const l=t.deps[t._depsLength];l!==o?(l&&Ws(l,t),t.deps[t._depsLength++]=o):t._depsLength++}}const Na=[];function Js(t,o,a){ol();for(const l of t.keys()){let s;l._dirtyLevel<o&&(s??(s=t.get(l)===l._trackId))&&(l._shouldSchedule||(l._shouldSchedule=l._dirtyLevel===0),l._dirtyLevel=o),l._shouldSchedule&&(s??(s=t.get(l)===l._trackId))&&(l.trigger(),(!l._runnings||l.allowRecurse)&&l._dirtyLevel!==2&&(l._shouldSchedule=!1,l.scheduler&&Na.push(l.scheduler)))}al()}const Zs=(t,o)=>{const a=new Map;return a.cleanup=t,a.computed=o,a},Bo=new WeakMap,kn=Symbol(""),Oa=Symbol("");function _t(t,o,a){if(nn&&xn){let l=Bo.get(t);l||Bo.set(t,l=new Map);let s=l.get(a);s||l.set(a,s=Zs(()=>l.delete(a))),Ys(xn,s)}}function jt(t,o,a,l,s,r){const i=Bo.get(t);if(!i)return;let c=[];if(o==="clear")c=[...i.values()];else if(a==="length"&&ce(t)){const d=Number(l);i.forEach((h,u)=>{(u==="length"||!Ht(u)&&u>=d)&&c.push(h)})}else switch(a!==void 0&&c.push(i.get(a)),o){case"add":ce(t)?el(a)&&c.push(i.get("length")):(c.push(i.get(kn)),Pn(t)&&c.push(i.get(Oa)));break;case"delete":ce(t)||(c.push(i.get(kn)),Pn(t)&&c.push(i.get(Oa)));break;case"set":Pn(t)&&c.push(i.get(kn));break}ol();for(const d of c)d&&Js(d,4);al()}function Li(t,o){const a=Bo.get(t);return a&&a.get(o)}const Bi=Za("__proto__,__v_isRef,__isVue"),Xs=new Set(Object.getOwnPropertyNames(Symbol).filter(t=>t!=="arguments"&&t!=="caller").map(t=>Symbol[t]).filter(Ht)),Pl=Di();function Di(){const t={};return["includes","indexOf","lastIndexOf"].forEach(o=>{t[o]=function(...a){const l=Se(this);for(let r=0,i=this.length;r<i;r++)_t(l,"get",r+"");const s=l[o](...a);return s===-1||s===!1?l[o](...a.map(Se)):s}}),["push","pop","shift","unshift","splice"].forEach(o=>{t[o]=function(...a){cn(),ol();const l=Se(this)[o].apply(this,a);return al(),dn(),l}}),t}function zi(t){Ht(t)||(t=String(t));const o=Se(this);return _t(o,"has",t),o.hasOwnProperty(t)}class Qs{constructor(o=!1,a=!1){this._isReadonly=o,this._isShallow=a}get(o,a,l){const s=this._isReadonly,r=this._isShallow;if(a==="__v_isReactive")return!s;if(a==="__v_isReadonly")return s;if(a==="__v_isShallow")return r;if(a==="__v_raw")return l===(s?r?Xi:or:r?nr:tr).get(o)||Object.getPrototypeOf(o)===Object.getPrototypeOf(l)?o:void 0;const i=ce(o);if(!s){if(i&&Ae(Pl,a))return Reflect.get(Pl,a,l);if(a==="hasOwnProperty")return zi}const c=Reflect.get(o,a,l);return(Ht(a)?Xs.has(a):Bi(a))||(s||_t(o,"get",a),r)?c:ut(c)?i&&el(a)?c:c.value:Re(c)?s?rl(c):tn(c):c}}class er extends Qs{constructor(o=!1){super(!1,o)}set(o,a,l,s){let r=o[a];if(!this._isShallow){const d=eo(r);if(!Do(l)&&!eo(l)&&(r=Se(r),l=Se(l)),!ce(o)&&ut(r)&&!ut(l))return d?!1:(r.value=l,!0)}const i=ce(o)&&el(a)?Number(a)<o.length:Ae(o,a),c=Reflect.set(o,a,l,s);return o===Se(s)&&(i?an(l,r)&&jt(o,"set",a,l):jt(o,"add",a,l)),c}deleteProperty(o,a){const l=Ae(o,a);o[a];const s=Reflect.deleteProperty(o,a);return s&&l&&jt(o,"delete",a,void 0),s}has(o,a){const l=Reflect.has(o,a);return(!Ht(a)||!Xs.has(a))&&_t(o,"has",a),l}ownKeys(o){return _t(o,"iterate",ce(o)?"length":kn),Reflect.ownKeys(o)}}class Vi extends Qs{constructor(o=!1){super(!0,o)}set(o,a){return!0}deleteProperty(o,a){return!0}}const ji=new er,Fi=new Vi,Hi=new er(!0),ll=t=>t,ea=t=>Reflect.getPrototypeOf(t);function go(t,o,a=!1,l=!1){t=t.__v_raw;const s=Se(t),r=Se(o);a||(an(o,r)&&_t(s,"get",o),_t(s,"get",r));const{has:i}=ea(s),c=l?ll:a?cl:to;if(i.call(s,o))return c(t.get(o));if(i.call(s,r))return c(t.get(r));t!==s&&t.get(o)}function _o(t,o=!1){const a=this.__v_raw,l=Se(a),s=Se(t);return o||(an(t,s)&&_t(l,"has",t),_t(l,"has",s)),t===s?a.has(t):a.has(t)||a.has(s)}function vo(t,o=!1){return t=t.__v_raw,!o&&_t(Se(t),"iterate",kn),Reflect.get(t,"size",t)}function Nl(t){t=Se(t);const o=Se(this);return ea(o).has.call(o,t)||(o.add(t),jt(o,"add",t,t)),this}function Ol(t,o){o=Se(o);const a=Se(this),{has:l,get:s}=ea(a);let r=l.call(a,t);r||(t=Se(t),r=l.call(a,t));const i=s.call(a,t);return a.set(t,o),r?an(o,i)&&jt(a,"set",t,o):jt(a,"add",t,o),this}function Rl(t){const o=Se(this),{has:a,get:l}=ea(o);let s=a.call(o,t);s||(t=Se(t),s=a.call(o,t)),l&&l.call(o,t);const r=o.delete(t);return s&&jt(o,"delete",t,void 0),r}function Il(){const t=Se(this),o=t.size!==0,a=t.clear();return o&&jt(t,"clear",void 0,void 0),a}function yo(t,o){return function(l,s){const r=this,i=r.__v_raw,c=Se(i),d=o?ll:t?cl:to;return!t&&_t(c,"iterate",kn),i.forEach((h,u)=>l.call(s,d(h),d(u),r))}}function wo(t,o,a){return function(...l){const s=this.__v_raw,r=Se(s),i=Pn(r),c=t==="entries"||t===Symbol.iterator&&i,d=t==="keys"&&i,h=s[t](...l),u=a?ll:o?cl:to;return!o&&_t(r,"iterate",d?Oa:kn),{next(){const{value:p,done:m}=h.next();return m?{value:p,done:m}:{value:c?[u(p[0]),u(p[1])]:u(p),done:m}},[Symbol.iterator](){return this}}}}function Gt(t){return function(...o){return t==="delete"?!1:t==="clear"?void 0:this}}function qi(){const t={get(r){return go(this,r)},get size(){return vo(this)},has:_o,add:Nl,set:Ol,delete:Rl,clear:Il,forEach:yo(!1,!1)},o={get(r){return go(this,r,!1,!0)},get size(){return vo(this)},has:_o,add:Nl,set:Ol,delete:Rl,clear:Il,forEach:yo(!1,!0)},a={get(r){return go(this,r,!0)},get size(){return vo(this,!0)},has(r){return _o.call(this,r,!0)},add:Gt("add"),set:Gt("set"),delete:Gt("delete"),clear:Gt("clear"),forEach:yo(!0,!1)},l={get(r){return go(this,r,!0,!0)},get size(){return vo(this,!0)},has(r){return _o.call(this,r,!0)},add:Gt("add"),set:Gt("set"),delete:Gt("delete"),clear:Gt("clear"),forEach:yo(!0,!0)};return["keys","values","entries",Symbol.iterator].forEach(r=>{t[r]=wo(r,!1,!1),a[r]=wo(r,!0,!1),o[r]=wo(r,!1,!0),l[r]=wo(r,!0,!0)}),[t,a,o,l]}const[Ui,Gi,Wi,Ki]=qi();function sl(t,o){const a=o?t?Ki:Wi:t?Gi:Ui;return(l,s,r)=>s==="__v_isReactive"?!t:s==="__v_isReadonly"?t:s==="__v_raw"?l:Reflect.get(Ae(a,s)&&s in l?a:l,s,r)}const Yi={get:sl(!1,!1)},Ji={get:sl(!1,!0)},Zi={get:sl(!0,!1)},tr=new WeakMap,nr=new WeakMap,or=new WeakMap,Xi=new WeakMap;function Qi(t){switch(t){case"Object":case"Array":return 1;case"Map":case"Set":case"WeakMap":case"WeakSet":return 2;default:return 0}}function ec(t){return t.__v_skip||!Object.isExtensible(t)?0:Qi(wi(t))}function tn(t){return eo(t)?t:il(t,!1,ji,Yi,tr)}function ar(t){return il(t,!1,Hi,Ji,nr)}function rl(t){return il(t,!0,Fi,Zi,or)}function il(t,o,a,l,s){if(!Re(t)||t.__v_raw&&!(o&&t.__v_isReactive))return t;const r=s.get(t);if(r)return r;const i=ec(t);if(i===0)return t;const c=new Proxy(t,i===2?l:a);return s.set(t,c),c}function Un(t){return eo(t)?Un(t.__v_raw):!!(t&&t.__v_isReactive)}function eo(t){return!!(t&&t.__v_isReadonly)}function Do(t){return!!(t&&t.__v_isShallow)}function lr(t){return t?!!t.__v_raw:!1}function Se(t){const o=t&&t.__v_raw;return o?Se(o):t}function tc(t){return Object.isExtensible(t)&&Fs(t,"__v_skip",!0),t}const to=t=>Re(t)?tn(t):t,cl=t=>Re(t)?rl(t):t;class sr{constructor(o,a,l,s){this.getter=o,this._setter=a,this.dep=void 0,this.__v_isRef=!0,this.__v_isReadonly=!1,this.effect=new nl(()=>o(this._value),()=>Mo(this,this.effect._dirtyLevel===2?2:3)),this.effect.computed=this,this.effect.active=this._cacheable=!s,this.__v_isReadonly=l}get value(){const o=Se(this);return(!o._cacheable||o.effect.dirty)&&an(o._value,o._value=o.effect.run())&&Mo(o,4),rr(o),o.effect._dirtyLevel>=2&&Mo(o,2),o._value}set value(o){this._setter(o)}get _dirty(){return this.effect.dirty}set _dirty(o){this.effect.dirty=o}}function nc(t,o,a=!1){let l,s;const r=ge(t);return r?(l=t,s=xt):(l=t.get,s=t.set),new sr(l,s,r||!s,a)}function rr(t){var o;nn&&xn&&(t=Se(t),Ys(xn,(o=t.dep)!=null?o:t.dep=Zs(()=>t.dep=void 0,t instanceof sr?t:void 0)))}function Mo(t,o=4,a){t=Se(t);const l=t.dep;l&&Js(l,o)}function ut(t){return!!(t&&t.__v_isRef===!0)}function Ce(t){return ir(t,!1)}function zo(t){return ir(t,!0)}function ir(t,o){return ut(t)?t:new oc(t,o)}class oc{constructor(o,a){this.__v_isShallow=a,this.dep=void 0,this.__v_isRef=!0,this._rawValue=a?o:Se(o),this._value=a?o:to(o)}get value(){return rr(this),this._value}set value(o){const a=this.__v_isShallow||Do(o)||eo(o);o=a?o:Se(o),an(o,this._rawValue)&&(this._rawValue=o,this._value=a?o:to(o),Mo(this,4))}}function $(t){return ut(t)?t.value:t}function ln(t){return ge(t)?t():$(t)}const ac={get:(t,o,a)=>$(Reflect.get(t,o,a)),set:(t,o,a,l)=>{const s=t[o];return ut(s)&&!ut(a)?(s.value=a,!0):Reflect.set(t,o,a,l)}};function cr(t){return Un(t)?t:new Proxy(t,ac)}function dr(t){const o=ce(t)?new Array(t.length):{};for(const a in t)o[a]=hr(t,a);return o}class lc{constructor(o,a,l){this._object=o,this._key=a,this._defaultValue=l,this.__v_isRef=!0}get value(){const o=this._object[this._key];return o===void 0?this._defaultValue:o}set value(o){this._object[this._key]=o}get dep(){return Li(Se(this._object),this._key)}}class sc{constructor(o){this._getter=o,this.__v_isRef=!0,this.__v_isReadonly=!0}get value(){return this._getter()}}function ur(t,o,a){return ut(t)?t:ge(t)?new sc(t):Re(t)&&arguments.length>1?hr(t,o,a):Ce(t)}function hr(t,o,a){const l=t[o];return ut(l)?l:new lc(t,o,a)}/**
* @vue/runtime-core v3.4.27
* (c) 2018-present Yuxi (Evan) You and Vue contributors
* @license MIT
**/function on(t,o,a,l){try{return l?t(...l):t()}catch(s){ta(s,o,a)}}function Ct(t,o,a,l){if(ge(t)){const s=on(t,o,a,l);return s&&zs(s)&&s.catch(r=>{ta(r,o,a)}),s}if(ce(t)){const s=[];for(let r=0;r<t.length;r++)s.push(Ct(t[r],o,a,l));return s}}function ta(t,o,a,l=!0){const s=o?o.vnode:null;if(o){let r=o.parent;const i=o.proxy,c=`https://vuejs.org/error-reference/#runtime-${a}`;for(;r;){const h=r.ec;if(h){for(let u=0;u<h.length;u++)if(h[u](t,i,c)===!1)return}r=r.parent}const d=o.appContext.config.errorHandler;if(d){cn(),on(d,null,10,[t,i,c]),dn();return}}rc(t,a,s,l)}function rc(t,o,a,l=!0){console.error(t)}let no=!1,Ra=!1;const dt=[];let It=0;const Nn=[];let Jt=null,vn=0;const pr=Promise.resolve();let dl=null;function co(t){const o=dl||pr;return t?o.then(this?t.bind(this):t):o}function ic(t){let o=It+1,a=dt.length;for(;o<a;){const l=o+a>>>1,s=dt[l],r=oo(s);r<t||r===t&&s.pre?o=l+1:a=l}return o}function ul(t){(!dt.length||!dt.includes(t,no&&t.allowRecurse?It+1:It))&&(t.id==null?dt.push(t):dt.splice(ic(t.id),0,t),fr())}function fr(){!no&&!Ra&&(Ra=!0,dl=pr.then(br))}function cc(t){const o=dt.indexOf(t);o>It&&dt.splice(o,1)}function dc(t){ce(t)?Nn.push(...t):(!Jt||!Jt.includes(t,t.allowRecurse?vn+1:vn))&&Nn.push(t),fr()}function Ll(t,o,a=no?It+1:0){for(;a<dt.length;a++){const l=dt[a];if(l&&l.pre){if(t&&l.id!==t.uid)continue;dt.splice(a,1),a--,l()}}}function mr(t){if(Nn.length){const o=[...new Set(Nn)].sort((a,l)=>oo(a)-oo(l));if(Nn.length=0,Jt){Jt.push(...o);return}for(Jt=o,vn=0;vn<Jt.length;vn++)Jt[vn]();Jt=null,vn=0}}const oo=t=>t.id==null?1/0:t.id,uc=(t,o)=>{const a=oo(t)-oo(o);if(a===0){if(t.pre&&!o.pre)return-1;if(o.pre&&!t.pre)return 1}return a};function br(t){Ra=!1,no=!0,dt.sort(uc);const o=xt;try{for(It=0;It<dt.length;It++){const a=dt[It];a&&a.active!==!1&&on(a,null,14)}}finally{It=0,dt.length=0,mr(),no=!1,dl=null,(dt.length||Nn.length)&&br()}}function hc(t,o,...a){if(t.isUnmounted)return;const l=t.vnode.props||Ve;let s=a;const r=o.startsWith("update:"),i=r&&o.slice(7);if(i&&i in l){const u=`${i==="modelValue"?"model":i}Modifiers`,{number:p,trim:m}=l[u]||Ve;m&&(s=a.map(g=>Ke(g)?g.trim():g)),p&&(s=a.map(Lo))}let c,d=l[c=ma(o)]||l[c=ma(Lt(o))];!d&&r&&(d=l[c=ma(zn(o))]),d&&Ct(d,t,6,s);const h=l[c+"Once"];if(h){if(!t.emitted)t.emitted={};else if(t.emitted[c])return;t.emitted[c]=!0,Ct(h,t,6,s)}}function gr(t,o,a=!1){const l=o.emitsCache,s=l.get(t);if(s!==void 0)return s;const r=t.emits;let i={},c=!1;if(!ge(t)){const d=h=>{const u=gr(h,o,!0);u&&(c=!0,Ze(i,u))};!a&&o.mixins.length&&o.mixins.forEach(d),t.extends&&d(t.extends),t.mixins&&t.mixins.forEach(d)}return!r&&!c?(Re(t)&&l.set(t,null),null):(ce(r)?r.forEach(d=>i[d]=null):Ze(i,r),Re(t)&&l.set(t,i),i)}function na(t,o){return!t||!Jo(o)?!1:(o=o.slice(2).replace(/Once$/,""),Ae(t,o[0].toLowerCase()+o.slice(1))||Ae(t,zn(o))||Ae(t,o))}let et=null,_r=null;function Vo(t){const o=et;return et=t,_r=t&&t.type.__scopeId||null,o}function v(t,o=et,a){if(!o||t._n)return t;const l=(...s)=>{l._d&&Kl(-1);const r=Vo(o);let i;try{i=t(...s)}finally{Vo(r),l._d&&Kl(1)}return i};return l._n=!0,l._c=!0,l._d=!0,l}function ga(t){const{type:o,vnode:a,proxy:l,withProxy:s,propsOptions:[r],slots:i,attrs:c,emit:d,render:h,renderCache:u,props:p,data:m,setupState:g,ctx:x,inheritAttrs:B}=t,W=Vo(t);let F,V;try{if(a.shapeFlag&4){const q=s||l,ee=q;F=Rt(h.call(ee,q,u,p,g,m,x)),V=c}else{const q=o;F=Rt(q.length>1?q(p,{attrs:c,slots:i,emit:d}):q(p,null)),V=o.props?c:pc(c)}}catch(q){Yn.length=0,ta(q,t,1),F=f(gt)}let R=F;if(V&&B!==!1){const q=Object.keys(V),{shapeFlag:ee}=R;q.length&&ee&7&&(r&&q.some(Xa)&&(V=fc(V,r)),R=sn(R,V,!1,!0))}return a.dirs&&(R=sn(R,null,!1,!0),R.dirs=R.dirs?R.dirs.concat(a.dirs):a.dirs),a.transition&&(R.transition=a.transition),F=R,Vo(W),F}const pc=t=>{let o;for(const a in t)(a==="class"||a==="style"||Jo(a))&&((o||(o={}))[a]=t[a]);return o},fc=(t,o)=>{const a={};for(const l in t)(!Xa(l)||!(l.slice(9)in o))&&(a[l]=t[l]);return a};function mc(t,o,a){const{props:l,children:s,component:r}=t,{props:i,children:c,patchFlag:d}=o,h=r.emitsOptions;if(o.dirs||o.transition)return!0;if(a&&d>=0){if(d&1024)return!0;if(d&16)return l?Bl(l,i,h):!!i;if(d&8){const u=o.dynamicProps;for(let p=0;p<u.length;p++){const m=u[p];if(i[m]!==l[m]&&!na(h,m))return!0}}}else return(s||c)&&(!c||!c.$stable)?!0:l===i?!1:l?i?Bl(l,i,h):!0:!!i;return!1}function Bl(t,o,a){const l=Object.keys(o);if(l.length!==Object.keys(t).length)return!0;for(let s=0;s<l.length;s++){const r=l[s];if(o[r]!==t[r]&&!na(a,r))return!0}return!1}function bc({vnode:t,parent:o},a){for(;o;){const l=o.subTree;if(l.suspense&&l.suspense.activeBranch===t&&(l.el=t.el),l===t)(t=o.vnode).el=a,o=o.parent;else break}}const gc="components",_c="directives",vc=Symbol.for("v-ndc");function Sn(t){return yc(_c,t)}function yc(t,o,a=!0,l=!1){const s=et||st;if(s){const r=s.type;if(t===gc){const c=fd(r,!1);if(c&&(c===o||c===Lt(o)||c===Xo(Lt(o))))return r}const i=Dl(s[t]||r[t],o)||Dl(s.appContext[t],o);return!i&&l?r:i}}function Dl(t,o){return t&&(t[o]||t[Lt(o)]||t[Xo(Lt(o))])}const wc=t=>t.__isSuspense;function xc(t,o){o&&o.pendingBranch?ce(t)?o.effects.push(...t):o.effects.push(t):dc(t)}const kc=Symbol.for("v-scx"),$c=()=>Je(kc);function Cc(t,o){return hl(t,null,o)}const xo={};function $n(t,o,a){return hl(t,o,a)}function hl(t,o,{immediate:a,deep:l,flush:s,once:r,onTrack:i,onTrigger:c}=Ve){if(o&&r){const Q=o;o=(...P)=>{Q(...P),ee()}}const d=st,h=Q=>l===!0?Q:wn(Q,l===!1?1:void 0);let u,p=!1,m=!1;if(ut(t)?(u=()=>t.value,p=Do(t)):Un(t)?(u=()=>h(t),p=!0):ce(t)?(m=!0,p=t.some(Q=>Un(Q)||Do(Q)),u=()=>t.map(Q=>{if(ut(Q))return Q.value;if(Un(Q))return h(Q);if(ge(Q))return on(Q,d,2)})):ge(t)?o?u=()=>on(t,d,2):u=()=>(g&&g(),Ct(t,d,3,[x])):u=xt,o&&l){const Q=u;u=()=>wn(Q())}let g,x=Q=>{g=R.onStop=()=>{on(Q,d,4),g=R.onStop=void 0}},B;if(sa)if(x=xt,o?a&&Ct(o,d,3,[u(),m?[]:void 0,x]):u(),s==="sync"){const Q=$c();B=Q.__watcherHandles||(Q.__watcherHandles=[])}else return xt;let W=m?new Array(t.length).fill(xo):xo;const F=()=>{if(!(!R.active||!R.dirty))if(o){const Q=R.run();(l||p||(m?Q.some((P,I)=>an(P,W[I])):an(Q,W)))&&(g&&g(),Ct(o,d,3,[Q,W===xo?void 0:m&&W[0]===xo?[]:W,x]),W=Q)}else R.run()};F.allowRecurse=!!o;let V;s==="sync"?V=F:s==="post"?V=()=>bt(F,d&&d.suspense):(F.pre=!0,d&&(F.id=d.uid),V=()=>ul(F));const R=new nl(u,xt,V),q=Gs(),ee=()=>{R.stop(),q&&Qa(q.effects,R)};return o?a?F():W=R.run():s==="post"?bt(R.run.bind(R),d&&d.suspense):R.run(),B&&B.push(ee),ee}function Sc(t,o,a){const l=this.proxy,s=Ke(t)?t.includes(".")?vr(l,t):()=>l[t]:t.bind(l,l);let r;ge(o)?r=o:(r=o.handler,a=o);const i=ho(this),c=hl(s,r.bind(l),a);return i(),c}function vr(t,o){const a=o.split(".");return()=>{let l=t;for(let s=0;s<a.length&&l;s++)l=l[a[s]];return l}}function wn(t,o=1/0,a){if(o<=0||!Re(t)||t.__v_skip||(a=a||new Set,a.has(t)))return t;if(a.add(t),o--,ut(t))wn(t.value,o,a);else if(ce(t))for(let l=0;l<t.length;l++)wn(t[l],o,a);else if(Dn(t)||Pn(t))t.forEach(l=>{wn(l,o,a)});else if(js(t))for(const l in t)wn(t[l],o,a);return t}function $e(t,o){if(et===null)return t;const a=ra(et)||et.proxy,l=t.dirs||(t.dirs=[]);for(let s=0;s<o.length;s++){let[r,i,c,d=Ve]=o[s];r&&(ge(r)&&(r={mounted:r,updated:r}),r.deep&&wn(i),l.push({dir:r,instance:a,value:i,oldValue:void 0,arg:c,modifiers:d}))}return t}function fn(t,o,a,l){const s=t.dirs,r=o&&o.dirs;for(let i=0;i<s.length;i++){const c=s[i];r&&(c.oldValue=r[i].value);let d=c.dir[l];d&&(cn(),Ct(d,a,8,[t.el,c,t,o]),dn())}}const Zt=Symbol("_leaveCb"),ko=Symbol("_enterCb");function Ec(){const t={isMounted:!1,isLeaving:!1,isUnmounting:!1,leavingVNodes:new Map};return uo(()=>{t.isMounted=!0}),Cr(()=>{t.isUnmounting=!0}),t}const $t=[Function,Array],yr={mode:String,appear:Boolean,persisted:Boolean,onBeforeEnter:$t,onEnter:$t,onAfterEnter:$t,onEnterCancelled:$t,onBeforeLeave:$t,onLeave:$t,onAfterLeave:$t,onLeaveCancelled:$t,onBeforeAppear:$t,onAppear:$t,onAfterAppear:$t,onAppearCancelled:$t},Tc={name:"BaseTransition",props:yr,setup(t,{slots:o}){const a=gl(),l=Ec();return()=>{const s=o.default&&xr(o.default(),!0);if(!s||!s.length)return;let r=s[0];if(s.length>1){for(const m of s)if(m.type!==gt){r=m;break}}const i=Se(t),{mode:c}=i;if(l.isLeaving)return _a(r);const d=zl(r);if(!d)return _a(r);const h=Ia(d,i,l,a);La(d,h);const u=a.subTree,p=u&&zl(u);if(p&&p.type!==gt&&!yn(d,p)){const m=Ia(p,i,l,a);if(La(p,m),c==="out-in"&&d.type!==gt)return l.isLeaving=!0,m.afterLeave=()=>{l.isLeaving=!1,a.update.active!==!1&&(a.effect.dirty=!0,a.update())},_a(r);c==="in-out"&&d.type!==gt&&(m.delayLeave=(g,x,B)=>{const W=wr(l,p);W[String(p.key)]=p,g[Zt]=()=>{x(),g[Zt]=void 0,delete h.delayedLeave},h.delayedLeave=B})}return r}}},Ac=Tc;function wr(t,o){const{leavingVNodes:a}=t;let l=a.get(o.type);return l||(l=Object.create(null),a.set(o.type,l)),l}function Ia(t,o,a,l){const{appear:s,mode:r,persisted:i=!1,onBeforeEnter:c,onEnter:d,onAfterEnter:h,onEnterCancelled:u,onBeforeLeave:p,onLeave:m,onAfterLeave:g,onLeaveCancelled:x,onBeforeAppear:B,onAppear:W,onAfterAppear:F,onAppearCancelled:V}=o,R=String(t.key),q=wr(a,t),ee=(I,te)=>{I&&Ct(I,l,9,te)},Q=(I,te)=>{const oe=te[1];ee(I,te),ce(I)?I.every(A=>A.length<=1)&&oe():I.length<=1&&oe()},P={mode:r,persisted:i,beforeEnter(I){let te=c;if(!a.isMounted)if(s)te=B||c;else return;I[Zt]&&I[Zt](!0);const oe=q[R];oe&&yn(t,oe)&&oe.el[Zt]&&oe.el[Zt](),ee(te,[I])},enter(I){let te=d,oe=h,A=u;if(!a.isMounted)if(s)te=W||d,oe=F||h,A=V||u;else return;let D=!1;const j=I[ko]=be=>{D||(D=!0,be?ee(A,[I]):ee(oe,[I]),P.delayedLeave&&P.delayedLeave(),I[ko]=void 0)};te?Q(te,[I,j]):j()},leave(I,te){const oe=String(t.key);if(I[ko]&&I[ko](!0),a.isUnmounting)return te();ee(p,[I]);let A=!1;const D=I[Zt]=j=>{A||(A=!0,te(),j?ee(x,[I]):ee(g,[I]),I[Zt]=void 0,q[oe]===t&&delete q[oe])};q[oe]=t,m?Q(m,[I,D]):D()},clone(I){return Ia(I,o,a,l)}};return P}function _a(t){if(oa(t))return t=sn(t),t.children=null,t}function zl(t){if(!oa(t))return t;const{shapeFlag:o,children:a}=t;if(a){if(o&16)return a[0];if(o&32&&ge(a.default))return a.default()}}function La(t,o){t.shapeFlag&6&&t.component?La(t.component.subTree,o):t.shapeFlag&128?(t.ssContent.transition=o.clone(t.ssContent),t.ssFallback.transition=o.clone(t.ssFallback)):t.transition=o}function xr(t,o=!1,a){let l=[],s=0;for(let r=0;r<t.length;r++){let i=t[r];const c=a==null?i.key:String(a)+String(i.key!=null?i.key:r);i.type===J?(i.patchFlag&128&&s++,l=l.concat(xr(i.children,o,c))):(o||i.type!==gt)&&l.push(c!=null?sn(i,{key:c}):i)}if(s>1)for(let r=0;r<l.length;r++)l[r].patchFlag=-2;return l}/*! #__NO_SIDE_EFFECTS__ */function kr(t,o){return ge(t)?(()=>Ze({name:t.name},o,{setup:t}))():t}const Gn=t=>!!t.type.__asyncLoader,oa=t=>t.type.__isKeepAlive;function Mc(t,o){$r(t,"a",o)}function Pc(t,o){$r(t,"da",o)}function $r(t,o,a=st){const l=t.__wdc||(t.__wdc=()=>{let s=a;for(;s;){if(s.isDeactivated)return;s=s.parent}return t()});if(aa(o,l,a),a){let s=a.parent;for(;s&&s.parent;)oa(s.parent.vnode)&&Nc(l,o,a,s),s=s.parent}}function Nc(t,o,a,l){const s=aa(o,t,l,!0);Sr(()=>{Qa(l[o],s)},a)}function aa(t,o,a=st,l=!1){if(a){const s=a[t]||(a[t]=[]),r=o.__weh||(o.__weh=(...i)=>{if(a.isUnmounted)return;cn();const c=ho(a),d=Ct(o,a,t,i);return c(),dn(),d});return l?s.unshift(r):s.push(r),r}}const qt=t=>(o,a=st)=>(!sa||t==="sp")&&aa(t,(...l)=>o(...l),a),Oc=qt("bm"),uo=qt("m"),Rc=qt("bu"),Ic=qt("u"),Cr=qt("bum"),Sr=qt("um"),Lc=qt("sp"),Bc=qt("rtg"),Dc=qt("rtc");function zc(t,o=st){aa("ec",t,o)}function Oe(t,o,a,l){let s;const r=a&&a[l];if(ce(t)||Ke(t)){s=new Array(t.length);for(let i=0,c=t.length;i<c;i++)s[i]=o(t[i],i,void 0,r&&r[i])}else if(typeof t=="number"){s=new Array(t);for(let i=0;i<t;i++)s[i]=o(i+1,i,void 0,r&&r[i])}else if(Re(t))if(t[Symbol.iterator])s=Array.from(t,(i,c)=>o(i,c,void 0,r&&r[c]));else{const i=Object.keys(t);s=new Array(i.length);for(let c=0,d=i.length;c<d;c++){const h=i[c];s[c]=o(t[h],h,c,r&&r[c])}}else s=[];return a&&(a[l]=s),s}function rt(t,o,a={},l,s){if(et.isCE||et.parent&&Gn(et.parent)&&et.parent.isCE)return o!=="default"&&(a.name=o),f("slot",a,l&&l());let r=t[o];r&&r._c&&(r._d=!1),C();const i=r&&Er(r(a)),c=En(J,{key:a.key||i&&i.key||`_${o}`},i||(l?l():[]),i&&t._===1?64:-2);return!s&&c.scopeId&&(c.slotScopeIds=[c.scopeId+"-s"]),r&&r._c&&(r._d=!0),c}function Er(t){return t.some(o=>Fo(o)?!(o.type===gt||o.type===J&&!Er(o.children)):!0)?t:null}const Ba=t=>t?Fr(t)?ra(t)||t.proxy:Ba(t.parent):null,Wn=Ze(Object.create(null),{$:t=>t,$el:t=>t.vnode.el,$data:t=>t.data,$props:t=>t.props,$attrs:t=>t.attrs,$slots:t=>t.slots,$refs:t=>t.refs,$parent:t=>Ba(t.parent),$root:t=>Ba(t.root),$emit:t=>t.emit,$options:t=>fl(t),$forceUpdate:t=>t.f||(t.f=()=>{t.effect.dirty=!0,ul(t.update)}),$nextTick:t=>t.n||(t.n=co.bind(t.proxy)),$watch:t=>Sc.bind(t)}),va=(t,o)=>t!==Ve&&!t.__isScriptSetup&&Ae(t,o),Vc={get({_:t},o){if(o==="__v_skip")return!0;const{ctx:a,setupState:l,data:s,props:r,accessCache:i,type:c,appContext:d}=t;let h;if(o[0]!=="$"){const g=i[o];if(g!==void 0)switch(g){case 1:return l[o];case 2:return s[o];case 4:return a[o];case 3:return r[o]}else{if(va(l,o))return i[o]=1,l[o];if(s!==Ve&&Ae(s,o))return i[o]=2,s[o];if((h=t.propsOptions[0])&&Ae(h,o))return i[o]=3,r[o];if(a!==Ve&&Ae(a,o))return i[o]=4,a[o];Da&&(i[o]=0)}}const u=Wn[o];let p,m;if(u)return o==="$attrs"&&_t(t.attrs,"get",""),u(t);if((p=c.__cssModules)&&(p=p[o]))return p;if(a!==Ve&&Ae(a,o))return i[o]=4,a[o];if(m=d.config.globalProperties,Ae(m,o))return m[o]},set({_:t},o,a){const{data:l,setupState:s,ctx:r}=t;return va(s,o)?(s[o]=a,!0):l!==Ve&&Ae(l,o)?(l[o]=a,!0):Ae(t.props,o)||o[0]==="$"&&o.slice(1)in t?!1:(r[o]=a,!0)},has({_:{data:t,setupState:o,accessCache:a,ctx:l,appContext:s,propsOptions:r}},i){let c;return!!a[i]||t!==Ve&&Ae(t,i)||va(o,i)||(c=r[0])&&Ae(c,i)||Ae(l,i)||Ae(Wn,i)||Ae(s.config.globalProperties,i)},defineProperty(t,o,a){return a.get!=null?t._.accessCache[o]=0:Ae(a,"value")&&this.set(t,o,a.value,null),Reflect.defineProperty(t,o,a)}};function pl(){return jc().slots}function jc(){const t=gl();return t.setupContext||(t.setupContext=qr(t))}function Vl(t){return ce(t)?t.reduce((o,a)=>(o[a]=null,o),{}):t}let Da=!0;function Fc(t){const o=fl(t),a=t.proxy,l=t.ctx;Da=!1,o.beforeCreate&&jl(o.beforeCreate,t,"bc");const{data:s,computed:r,methods:i,watch:c,provide:d,inject:h,created:u,beforeMount:p,mounted:m,beforeUpdate:g,updated:x,activated:B,deactivated:W,beforeDestroy:F,beforeUnmount:V,destroyed:R,unmounted:q,render:ee,renderTracked:Q,renderTriggered:P,errorCaptured:I,serverPrefetch:te,expose:oe,inheritAttrs:A,components:D,directives:j,filters:be}=o;if(h&&Hc(h,l,null),i)for(const he in i){const de=i[he];ge(de)&&(l[he]=de.bind(a))}if(s){const he=s.call(a,a);Re(he)&&(t.data=tn(he))}if(Da=!0,r)for(const he in r){const de=r[he],Be=ge(de)?de.bind(a,a):ge(de.get)?de.get.bind(a,a):xt,Ye=!ge(de)&&ge(de.set)?de.set.bind(a):xt,qe=ue({get:Be,set:Ye});Object.defineProperty(l,he,{enumerable:!0,configurable:!0,get:()=>qe.value,set:je=>qe.value=je})}if(c)for(const he in c)Tr(c[he],l,a,he);if(d){const he=ge(d)?d.call(a):d;Reflect.ownKeys(he).forEach(de=>{Ft(de,he[de])})}u&&jl(u,t,"c");function fe(he,de){ce(de)?de.forEach(Be=>he(Be.bind(a))):de&&he(de.bind(a))}if(fe(Oc,p),fe(uo,m),fe(Rc,g),fe(Ic,x),fe(Mc,B),fe(Pc,W),fe(zc,I),fe(Dc,Q),fe(Bc,P),fe(Cr,V),fe(Sr,q),fe(Lc,te),ce(oe))if(oe.length){const he=t.exposed||(t.exposed={});oe.forEach(de=>{Object.defineProperty(he,de,{get:()=>a[de],set:Be=>a[de]=Be})})}else t.exposed||(t.exposed={});ee&&t.render===xt&&(t.render=ee),A!=null&&(t.inheritAttrs=A),D&&(t.components=D),j&&(t.directives=j)}function Hc(t,o,a=xt){ce(t)&&(t=za(t));for(const l in t){const s=t[l];let r;Re(s)?"default"in s?r=Je(s.from||l,s.default,!0):r=Je(s.from||l):r=Je(s),ut(r)?Object.defineProperty(o,l,{enumerable:!0,configurable:!0,get:()=>r.value,set:i=>r.value=i}):o[l]=r}}function jl(t,o,a){Ct(ce(t)?t.map(l=>l.bind(o.proxy)):t.bind(o.proxy),o,a)}function Tr(t,o,a,l){const s=l.includes(".")?vr(a,l):()=>a[l];if(Ke(t)){const r=o[t];ge(r)&&$n(s,r)}else if(ge(t))$n(s,t.bind(a));else if(Re(t))if(ce(t))t.forEach(r=>Tr(r,o,a,l));else{const r=ge(t.handler)?t.handler.bind(a):o[t.handler];ge(r)&&$n(s,r,t)}}function fl(t){const o=t.type,{mixins:a,extends:l}=o,{mixins:s,optionsCache:r,config:{optionMergeStrategies:i}}=t.appContext,c=r.get(o);let d;return c?d=c:!s.length&&!a&&!l?d=o:(d={},s.length&&s.forEach(h=>jo(d,h,i,!0)),jo(d,o,i)),Re(o)&&r.set(o,d),d}function jo(t,o,a,l=!1){const{mixins:s,extends:r}=o;r&&jo(t,r,a,!0),s&&s.forEach(i=>jo(t,i,a,!0));for(const i in o)if(!(l&&i==="expose")){const c=qc[i]||a&&a[i];t[i]=c?c(t[i],o[i]):o[i]}return t}const qc={data:Fl,props:Hl,emits:Hl,methods:Hn,computed:Hn,beforeCreate:pt,created:pt,beforeMount:pt,mounted:pt,beforeUpdate:pt,updated:pt,beforeDestroy:pt,beforeUnmount:pt,destroyed:pt,unmounted:pt,activated:pt,deactivated:pt,errorCaptured:pt,serverPrefetch:pt,components:Hn,directives:Hn,watch:Gc,provide:Fl,inject:Uc};function Fl(t,o){return o?t?function(){return Ze(ge(t)?t.call(this,this):t,ge(o)?o.call(this,this):o)}:o:t}function Uc(t,o){return Hn(za(t),za(o))}function za(t){if(ce(t)){const o={};for(let a=0;a<t.length;a++)o[t[a]]=t[a];return o}return t}function pt(t,o){return t?[...new Set([].concat(t,o))]:o}function Hn(t,o){return t?Ze(Object.create(null),t,o):o}function Hl(t,o){return t?ce(t)&&ce(o)?[...new Set([...t,...o])]:Ze(Object.create(null),Vl(t),Vl(o??{})):o}function Gc(t,o){if(!t)return o;if(!o)return t;const a=Ze(Object.create(null),t);for(const l in o)a[l]=pt(t[l],o[l]);return a}function Ar(){return{app:null,config:{isNativeTag:vi,performance:!1,globalProperties:{},optionMergeStrategies:{},errorHandler:void 0,warnHandler:void 0,compilerOptions:{}},mixins:[],components:{},directives:{},provides:Object.create(null),optionsCache:new WeakMap,propsCache:new WeakMap,emitsCache:new WeakMap}}let Wc=0;function Kc(t,o){return function(l,s=null){ge(l)||(l=Ze({},l)),s!=null&&!Re(s)&&(s=null);const r=Ar(),i=new WeakSet;let c=!1;const d=r.app={_uid:Wc++,_component:l,_props:s,_container:null,_context:r,_instance:null,version:bd,get config(){return r.config},set config(h){},use(h,...u){return i.has(h)||(h&&ge(h.install)?(i.add(h),h.install(d,...u)):ge(h)&&(i.add(h),h(d,...u))),d},mixin(h){return r.mixins.includes(h)||r.mixins.push(h),d},component(h,u){return u?(r.components[h]=u,d):r.components[h]},directive(h,u){return u?(r.directives[h]=u,d):r.directives[h]},mount(h,u,p){if(!c){const m=f(l,s);return m.appContext=r,p===!0?p="svg":p===!1&&(p=void 0),u&&o?o(m,h):t(m,h,p),c=!0,d._container=h,h.__vue_app__=d,ra(m.component)||m.component.proxy}},unmount(){c&&(t(null,d._container),delete d._container.__vue_app__)},provide(h,u){return r.provides[h]=u,d},runWithContext(h){const u=Kn;Kn=d;try{return h()}finally{Kn=u}}};return d}}let Kn=null;function Ft(t,o){if(st){let a=st.provides;const l=st.parent&&st.parent.provides;l===a&&(a=st.provides=Object.create(l)),a[t]=o}}function Je(t,o,a=!1){const l=st||et;if(l||Kn){const s=l?l.parent==null?l.vnode.appContext&&l.vnode.appContext.provides:l.parent.provides:Kn._context.provides;if(s&&t in s)return s[t];if(arguments.length>1)return a&&ge(o)?o.call(l&&l.proxy):o}}const Mr={},Pr=()=>Object.create(Mr),Nr=t=>Object.getPrototypeOf(t)===Mr;function Yc(t,o,a,l=!1){const s={},r=Pr();t.propsDefaults=Object.create(null),Or(t,o,s,r);for(const i in t.propsOptions[0])i in s||(s[i]=void 0);a?t.props=l?s:ar(s):t.type.props?t.props=s:t.props=r,t.attrs=r}function Jc(t,o,a,l){const{props:s,attrs:r,vnode:{patchFlag:i}}=t,c=Se(s),[d]=t.propsOptions;let h=!1;if((l||i>0)&&!(i&16)){if(i&8){const u=t.vnode.dynamicProps;for(let p=0;p<u.length;p++){let m=u[p];if(na(t.emitsOptions,m))continue;const g=o[m];if(d)if(Ae(r,m))g!==r[m]&&(r[m]=g,h=!0);else{const x=Lt(m);s[x]=Va(d,c,x,g,t,!1)}else g!==r[m]&&(r[m]=g,h=!0)}}}else{Or(t,o,s,r)&&(h=!0);let u;for(const p in c)(!o||!Ae(o,p)&&((u=zn(p))===p||!Ae(o,u)))&&(d?a&&(a[p]!==void 0||a[u]!==void 0)&&(s[p]=Va(d,c,p,void 0,t,!0)):delete s[p]);if(r!==c)for(const p in r)(!o||!Ae(o,p))&&(delete r[p],h=!0)}h&&jt(t.attrs,"set","")}function Or(t,o,a,l){const[s,r]=t.propsOptions;let i=!1,c;if(o)for(let d in o){if(qn(d))continue;const h=o[d];let u;s&&Ae(s,u=Lt(d))?!r||!r.includes(u)?a[u]=h:(c||(c={}))[u]=h:na(t.emitsOptions,d)||(!(d in l)||h!==l[d])&&(l[d]=h,i=!0)}if(r){const d=Se(a),h=c||Ve;for(let u=0;u<r.length;u++){const p=r[u];a[p]=Va(s,d,p,h[p],t,!Ae(h,p))}}return i}function Va(t,o,a,l,s,r){const i=t[a];if(i!=null){const c=Ae(i,"default");if(c&&l===void 0){const d=i.default;if(i.type!==Function&&!i.skipFactory&&ge(d)){const{propsDefaults:h}=s;if(a in h)l=h[a];else{const u=ho(s);l=h[a]=d.call(null,o),u()}}else l=d}i[0]&&(r&&!c?l=!1:i[1]&&(l===""||l===zn(a))&&(l=!0))}return l}function Rr(t,o,a=!1){const l=o.propsCache,s=l.get(t);if(s)return s;const r=t.props,i={},c=[];let d=!1;if(!ge(t)){const u=p=>{d=!0;const[m,g]=Rr(p,o,!0);Ze(i,m),g&&c.push(...g)};!a&&o.mixins.length&&o.mixins.forEach(u),t.extends&&u(t.extends),t.mixins&&t.mixins.forEach(u)}if(!r&&!d)return Re(t)&&l.set(t,Mn),Mn;if(ce(r))for(let u=0;u<r.length;u++){const p=Lt(r[u]);ql(p)&&(i[p]=Ve)}else if(r)for(const u in r){const p=Lt(u);if(ql(p)){const m=r[u],g=i[p]=ce(m)||ge(m)?{type:m}:Ze({},m);if(g){const x=Wl(Boolean,g.type),B=Wl(String,g.type);g[0]=x>-1,g[1]=B<0||x<B,(x>-1||Ae(g,"default"))&&c.push(p)}}}const h=[i,c];return Re(t)&&l.set(t,h),h}function ql(t){return t[0]!=="$"&&!qn(t)}function Ul(t){return t===null?"null":typeof t=="function"?t.name||"":typeof t=="object"&&t.constructor&&t.constructor.name||""}function Gl(t,o){return Ul(t)===Ul(o)}function Wl(t,o){return ce(o)?o.findIndex(a=>Gl(a,t)):ge(o)&&Gl(o,t)?0:-1}const Ir=t=>t[0]==="_"||t==="$stable",ml=t=>ce(t)?t.map(Rt):[Rt(t)],Zc=(t,o,a)=>{if(o._n)return o;const l=v((...s)=>ml(o(...s)),a);return l._c=!1,l},Lr=(t,o,a)=>{const l=t._ctx;for(const s in t){if(Ir(s))continue;const r=t[s];if(ge(r))o[s]=Zc(s,r,l);else if(r!=null){const i=ml(r);o[s]=()=>i}}},Br=(t,o)=>{const a=ml(o);t.slots.default=()=>a},Xc=(t,o)=>{const a=t.slots=Pr();if(t.vnode.shapeFlag&32){const l=o._;l?(Ze(a,o),Fs(a,"_",l,!0)):Lr(o,a)}else o&&Br(t,o)},Qc=(t,o,a)=>{const{vnode:l,slots:s}=t;let r=!0,i=Ve;if(l.shapeFlag&32){const c=o._;c?a&&c===1?r=!1:(Ze(s,o),!a&&c===1&&delete s._):(r=!o.$stable,Lr(o,s)),i=o}else o&&(Br(t,o),i={default:1});if(r)for(const c in s)!Ir(c)&&i[c]==null&&delete s[c]};function ja(t,o,a,l,s=!1){if(ce(t)){t.forEach((m,g)=>ja(m,o&&(ce(o)?o[g]:o),a,l,s));return}if(Gn(l)&&!s)return;const r=l.shapeFlag&4?ra(l.component)||l.component.proxy:l.el,i=s?null:r,{i:c,r:d}=t,h=o&&o.r,u=c.refs===Ve?c.refs={}:c.refs,p=c.setupState;if(h!=null&&h!==d&&(Ke(h)?(u[h]=null,Ae(p,h)&&(p[h]=null)):ut(h)&&(h.value=null)),ge(d))on(d,c,12,[i,u]);else{const m=Ke(d),g=ut(d);if(m||g){const x=()=>{if(t.f){const B=m?Ae(p,d)?p[d]:u[d]:d.value;s?ce(B)&&Qa(B,r):ce(B)?B.includes(r)||B.push(r):m?(u[d]=[r],Ae(p,d)&&(p[d]=u[d])):(d.value=[r],t.k&&(u[t.k]=d.value))}else m?(u[d]=i,Ae(p,d)&&(p[d]=i)):g&&(d.value=i,t.k&&(u[t.k]=i))};i?(x.id=-1,bt(x,a)):x()}}}const bt=xc;function ed(t){return td(t)}function td(t,o){const a=Hs();a.__VUE__=!0;const{insert:l,remove:s,patchProp:r,createElement:i,createText:c,createComment:d,setText:h,setElementText:u,parentNode:p,nextSibling:m,setScopeId:g=xt,insertStaticContent:x}=t,B=(b,w,E,L=null,O=null,K=null,ne=void 0,U=null,_=!!w.dynamicChildren)=>{if(b===w)return;b&&!yn(b,w)&&(L=M(b),je(b,O,K,!0),b=null),w.patchFlag===-2&&(_=!1,w.dynamicChildren=null);const{type:y,ref:T,shapeFlag:z}=w;switch(y){case la:W(b,w,E,L);break;case gt:F(b,w,E,L);break;case Po:b==null&&V(w,E,L,ne);break;case J:D(b,w,E,L,O,K,ne,U,_);break;default:z&1?ee(b,w,E,L,O,K,ne,U,_):z&6?j(b,w,E,L,O,K,ne,U,_):(z&64||z&128)&&y.process(b,w,E,L,O,K,ne,U,_,ie)}T!=null&&O&&ja(T,b&&b.ref,K,w||b,!w)},W=(b,w,E,L)=>{if(b==null)l(w.el=c(w.children),E,L);else{const O=w.el=b.el;w.children!==b.children&&h(O,w.children)}},F=(b,w,E,L)=>{b==null?l(w.el=d(w.children||""),E,L):w.el=b.el},V=(b,w,E,L)=>{[b.el,b.anchor]=x(b.children,w,E,L,b.el,b.anchor)},R=({el:b,anchor:w},E,L)=>{let O;for(;b&&b!==w;)O=m(b),l(b,E,L),b=O;l(w,E,L)},q=({el:b,anchor:w})=>{let E;for(;b&&b!==w;)E=m(b),s(b),b=E;s(w)},ee=(b,w,E,L,O,K,ne,U,_)=>{w.type==="svg"?ne="svg":w.type==="math"&&(ne="mathml"),b==null?Q(w,E,L,O,K,ne,U,_):te(b,w,O,K,ne,U,_)},Q=(b,w,E,L,O,K,ne,U)=>{let _,y;const{props:T,shapeFlag:z,transition:Z,dirs:N}=b;if(_=b.el=i(b.type,K,T&&T.is,T),z&8?u(_,b.children):z&16&&I(b.children,_,null,L,O,ya(b,K),ne,U),N&&fn(b,null,L,"created"),P(_,b,b.scopeId,ne,L),T){for(const H in T)H!=="value"&&!qn(H)&&r(_,H,null,T[H],K,b.children,L,O,Ue);"value"in T&&r(_,"value",null,T.value,K),(y=T.onVnodeBeforeMount)&&Ot(y,L,b)}N&&fn(b,null,L,"beforeMount");const Y=nd(O,Z);Y&&Z.beforeEnter(_),l(_,w,E),((y=T&&T.onVnodeMounted)||Y||N)&&bt(()=>{y&&Ot(y,L,b),Y&&Z.enter(_),N&&fn(b,null,L,"mounted")},O)},P=(b,w,E,L,O)=>{if(E&&g(b,E),L)for(let K=0;K<L.length;K++)g(b,L[K]);if(O){let K=O.subTree;if(w===K){const ne=O.vnode;P(b,ne,ne.scopeId,ne.slotScopeIds,O.parent)}}},I=(b,w,E,L,O,K,ne,U,_=0)=>{for(let y=_;y<b.length;y++){const T=b[y]=U?Xt(b[y]):Rt(b[y]);B(null,T,w,E,L,O,K,ne,U)}},te=(b,w,E,L,O,K,ne)=>{const U=w.el=b.el;let{patchFlag:_,dynamicChildren:y,dirs:T}=w;_|=b.patchFlag&16;const z=b.props||Ve,Z=w.props||Ve;let N;if(E&&mn(E,!1),(N=Z.onVnodeBeforeUpdate)&&Ot(N,E,w,b),T&&fn(w,b,E,"beforeUpdate"),E&&mn(E,!0),y?oe(b.dynamicChildren,y,U,E,L,ya(w,O),K):ne||de(b,w,U,null,E,L,ya(w,O),K,!1),_>0){if(_&16)A(U,w,z,Z,E,L,O);else if(_&2&&z.class!==Z.class&&r(U,"class",null,Z.class,O),_&4&&r(U,"style",z.style,Z.style,O),_&8){const Y=w.dynamicProps;for(let H=0;H<Y.length;H++){const ve=Y[H],we=z[ve],ke=Z[ve];(ke!==we||ve==="value")&&r(U,ve,we,ke,O,b.children,E,L,Ue)}}_&1&&b.children!==w.children&&u(U,w.children)}else!ne&&y==null&&A(U,w,z,Z,E,L,O);((N=Z.onVnodeUpdated)||T)&&bt(()=>{N&&Ot(N,E,w,b),T&&fn(w,b,E,"updated")},L)},oe=(b,w,E,L,O,K,ne)=>{for(let U=0;U<w.length;U++){const _=b[U],y=w[U],T=_.el&&(_.type===J||!yn(_,y)||_.shapeFlag&70)?p(_.el):E;B(_,y,T,null,L,O,K,ne,!0)}},A=(b,w,E,L,O,K,ne)=>{if(E!==L){if(E!==Ve)for(const U in E)!qn(U)&&!(U in L)&&r(b,U,E[U],null,ne,w.children,O,K,Ue);for(const U in L){if(qn(U))continue;const _=L[U],y=E[U];_!==y&&U!=="value"&&r(b,U,y,_,ne,w.children,O,K,Ue)}"value"in L&&r(b,"value",E.value,L.value,ne)}},D=(b,w,E,L,O,K,ne,U,_)=>{const y=w.el=b?b.el:c(""),T=w.anchor=b?b.anchor:c("");let{patchFlag:z,dynamicChildren:Z,slotScopeIds:N}=w;N&&(U=U?U.concat(N):N),b==null?(l(y,E,L),l(T,E,L),I(w.children||[],E,T,O,K,ne,U,_)):z>0&&z&64&&Z&&b.dynamicChildren?(oe(b.dynamicChildren,Z,E,O,K,ne,U),(w.key!=null||O&&w===O.subTree)&&Dr(b,w,!0)):de(b,w,E,T,O,K,ne,U,_)},j=(b,w,E,L,O,K,ne,U,_)=>{w.slotScopeIds=U,b==null?w.shapeFlag&512?O.ctx.activate(w,E,L,ne,_):be(w,E,L,O,K,ne,_):xe(b,w,_)},be=(b,w,E,L,O,K,ne)=>{const U=b.component=dd(b,L,O);if(oa(b)&&(U.ctx.renderer=ie),ud(U),U.asyncDep){if(O&&O.registerDep(U,fe),!b.el){const _=U.subTree=f(gt);F(null,_,w,E)}}else fe(U,b,w,E,O,K,ne)},xe=(b,w,E)=>{const L=w.component=b.component;if(mc(b,w,E))if(L.asyncDep&&!L.asyncResolved){he(L,w,E);return}else L.next=w,cc(L.update),L.effect.dirty=!0,L.update();else w.el=b.el,L.vnode=w},fe=(b,w,E,L,O,K,ne)=>{const U=()=>{if(b.isMounted){let{next:T,bu:z,u:Z,parent:N,vnode:Y}=b;{const ht=zr(b);if(ht){T&&(T.el=Y.el,he(b,T,ne)),ht.asyncDep.then(()=>{b.isUnmounted||U()});return}}let H=T,ve;mn(b,!1),T?(T.el=Y.el,he(b,T,ne)):T=Y,z&&Ao(z),(ve=T.props&&T.props.onVnodeBeforeUpdate)&&Ot(ve,N,T,Y),mn(b,!0);const we=ga(b),ke=b.subTree;b.subTree=we,B(ke,we,p(ke.el),M(ke),b,O,K),T.el=we.el,H===null&&bc(b,we.el),Z&&bt(Z,O),(ve=T.props&&T.props.onVnodeUpdated)&&bt(()=>Ot(ve,N,T,Y),O)}else{let T;const{el:z,props:Z}=w,{bm:N,m:Y,parent:H}=b,ve=Gn(w);if(mn(b,!1),N&&Ao(N),!ve&&(T=Z&&Z.onVnodeBeforeMount)&&Ot(T,H,w),mn(b,!0),z&&Le){const we=()=>{b.subTree=ga(b),Le(z,b.subTree,b,O,null)};ve?w.type.__asyncLoader().then(()=>!b.isUnmounted&&we()):we()}else{const we=b.subTree=ga(b);B(null,we,E,L,b,O,K),w.el=we.el}if(Y&&bt(Y,O),!ve&&(T=Z&&Z.onVnodeMounted)){const we=w;bt(()=>Ot(T,H,we),O)}(w.shapeFlag&256||H&&Gn(H.vnode)&&H.vnode.shapeFlag&256)&&b.a&&bt(b.a,O),b.isMounted=!0,w=E=L=null}},_=b.effect=new nl(U,xt,()=>ul(y),b.scope),y=b.update=()=>{_.dirty&&_.run()};y.id=b.uid,mn(b,!0),y()},he=(b,w,E)=>{w.component=b;const L=b.vnode.props;b.vnode=w,b.next=null,Jc(b,w.props,L,E),Qc(b,w.children,E),cn(),Ll(b),dn()},de=(b,w,E,L,O,K,ne,U,_=!1)=>{const y=b&&b.children,T=b?b.shapeFlag:0,z=w.children,{patchFlag:Z,shapeFlag:N}=w;if(Z>0){if(Z&128){Ye(y,z,E,L,O,K,ne,U,_);return}else if(Z&256){Be(y,z,E,L,O,K,ne,U,_);return}}N&8?(T&16&&Ue(y,O,K),z!==y&&u(E,z)):T&16?N&16?Ye(y,z,E,L,O,K,ne,U,_):Ue(y,O,K,!0):(T&8&&u(E,""),N&16&&I(z,E,L,O,K,ne,U,_))},Be=(b,w,E,L,O,K,ne,U,_)=>{b=b||Mn,w=w||Mn;const y=b.length,T=w.length,z=Math.min(y,T);let Z;for(Z=0;Z<z;Z++){const N=w[Z]=_?Xt(w[Z]):Rt(w[Z]);B(b[Z],N,E,null,O,K,ne,U,_)}y>T?Ue(b,O,K,!0,!1,z):I(w,E,L,O,K,ne,U,_,z)},Ye=(b,w,E,L,O,K,ne,U,_)=>{let y=0;const T=w.length;let z=b.length-1,Z=T-1;for(;y<=z&&y<=Z;){const N=b[y],Y=w[y]=_?Xt(w[y]):Rt(w[y]);if(yn(N,Y))B(N,Y,E,null,O,K,ne,U,_);else break;y++}for(;y<=z&&y<=Z;){const N=b[z],Y=w[Z]=_?Xt(w[Z]):Rt(w[Z]);if(yn(N,Y))B(N,Y,E,null,O,K,ne,U,_);else break;z--,Z--}if(y>z){if(y<=Z){const N=Z+1,Y=N<T?w[N].el:L;for(;y<=Z;)B(null,w[y]=_?Xt(w[y]):Rt(w[y]),E,Y,O,K,ne,U,_),y++}}else if(y>Z)for(;y<=z;)je(b[y],O,K,!0),y++;else{const N=y,Y=y,H=new Map;for(y=Y;y<=Z;y++){const Ge=w[y]=_?Xt(w[y]):Rt(w[y]);Ge.key!=null&&H.set(Ge.key,y)}let ve,we=0;const ke=Z-Y+1;let ht=!1,Tn=0;const Ut=new Array(ke);for(y=0;y<ke;y++)Ut[y]=0;for(y=N;y<=z;y++){const Ge=b[y];if(we>=ke){je(Ge,O,K,!0);continue}let vt;if(Ge.key!=null)vt=H.get(Ge.key);else for(ve=Y;ve<=Z;ve++)if(Ut[ve-Y]===0&&yn(Ge,w[ve])){vt=ve;break}vt===void 0?je(Ge,O,K,!0):(Ut[vt-Y]=y+1,vt>=Tn?Tn=vt:ht=!0,B(Ge,w[vt],E,null,O,K,ne,U,_),we++)}const hn=ht?od(Ut):Mn;for(ve=hn.length-1,y=ke-1;y>=0;y--){const Ge=Y+y,vt=w[Ge],Vn=Ge+1<T?w[Ge+1].el:L;Ut[y]===0?B(null,vt,E,Vn,O,K,ne,U,_):ht&&(ve<0||y!==hn[ve]?qe(vt,E,Vn,2):ve--)}}},qe=(b,w,E,L,O=null)=>{const{el:K,type:ne,transition:U,children:_,shapeFlag:y}=b;if(y&6){qe(b.component.subTree,w,E,L);return}if(y&128){b.suspense.move(w,E,L);return}if(y&64){ne.move(b,w,E,ie);return}if(ne===J){l(K,w,E);for(let z=0;z<_.length;z++)qe(_[z],w,E,L);l(b.anchor,w,E);return}if(ne===Po){R(b,w,E);return}if(L!==2&&y&1&&U)if(L===0)U.beforeEnter(K),l(K,w,E),bt(()=>U.enter(K),O);else{const{leave:z,delayLeave:Z,afterLeave:N}=U,Y=()=>l(K,w,E),H=()=>{z(K,()=>{Y(),N&&N()})};Z?Z(K,Y,H):H()}else l(K,w,E)},je=(b,w,E,L=!1,O=!1)=>{const{type:K,props:ne,ref:U,children:_,dynamicChildren:y,shapeFlag:T,patchFlag:z,dirs:Z}=b;if(U!=null&&ja(U,null,E,b,!0),T&256){w.ctx.deactivate(b);return}const N=T&1&&Z,Y=!Gn(b);let H;if(Y&&(H=ne&&ne.onVnodeBeforeUnmount)&&Ot(H,w,b),T&6)kt(b.component,E,L);else{if(T&128){b.suspense.unmount(E,L);return}N&&fn(b,null,w,"beforeUnmount"),T&64?b.type.remove(b,w,E,O,ie,L):y&&(K!==J||z>0&&z&64)?Ue(y,w,E,!1,!0):(K===J&&z&384||!O&&T&16)&&Ue(_,w,E),L&&it(b)}(Y&&(H=ne&&ne.onVnodeUnmounted)||N)&&bt(()=>{H&&Ot(H,w,b),N&&fn(b,null,w,"unmounted")},E)},it=b=>{const{type:w,el:E,anchor:L,transition:O}=b;if(w===J){ot(E,L);return}if(w===Po){q(b);return}const K=()=>{s(E),O&&!O.persisted&&O.afterLeave&&O.afterLeave()};if(b.shapeFlag&1&&O&&!O.persisted){const{leave:ne,delayLeave:U}=O,_=()=>ne(E,K);U?U(b.el,K,_):_()}else K()},ot=(b,w)=>{let E;for(;b!==w;)E=m(b),s(b),b=E;s(w)},kt=(b,w,E)=>{const{bum:L,scope:O,update:K,subTree:ne,um:U}=b;L&&Ao(L),O.stop(),K&&(K.active=!1,je(ne,b,w,E)),U&&bt(U,w),bt(()=>{b.isUnmounted=!0},w),w&&w.pendingBranch&&!w.isUnmounted&&b.asyncDep&&!b.asyncResolved&&b.suspenseId===w.pendingId&&(w.deps--,w.deps===0&&w.resolve())},Ue=(b,w,E,L=!1,O=!1,K=0)=>{for(let ne=K;ne<b.length;ne++)je(b[ne],w,E,L,O)},M=b=>b.shapeFlag&6?M(b.component.subTree):b.shapeFlag&128?b.suspense.next():m(b.anchor||b.el);let ae=!1;const X=(b,w,E)=>{b==null?w._vnode&&je(w._vnode,null,null,!0):B(w._vnode||null,b,w,null,null,null,E),ae||(ae=!0,Ll(),mr(),ae=!1),w._vnode=b},ie={p:B,um:je,m:qe,r:it,mt:be,mc:I,pc:de,pbc:oe,n:M,o:t};let Te,Le;return o&&([Te,Le]=o(ie)),{render:X,hydrate:Te,createApp:Kc(X,Te)}}function ya({type:t,props:o},a){return a==="svg"&&t==="foreignObject"||a==="mathml"&&t==="annotation-xml"&&o&&o.encoding&&o.encoding.includes("html")?void 0:a}function mn({effect:t,update:o},a){t.allowRecurse=o.allowRecurse=a}function nd(t,o){return(!t||t&&!t.pendingBranch)&&o&&!o.persisted}function Dr(t,o,a=!1){const l=t.children,s=o.children;if(ce(l)&&ce(s))for(let r=0;r<l.length;r++){const i=l[r];let c=s[r];c.shapeFlag&1&&!c.dynamicChildren&&((c.patchFlag<=0||c.patchFlag===32)&&(c=s[r]=Xt(s[r]),c.el=i.el),a||Dr(i,c)),c.type===la&&(c.el=i.el)}}function od(t){const o=t.slice(),a=[0];let l,s,r,i,c;const d=t.length;for(l=0;l<d;l++){const h=t[l];if(h!==0){if(s=a[a.length-1],t[s]<h){o[l]=s,a.push(l);continue}for(r=0,i=a.length-1;r<i;)c=r+i>>1,t[a[c]]<h?r=c+1:i=c;h<t[a[r]]&&(r>0&&(o[l]=a[r-1]),a[r]=l)}}for(r=a.length,i=a[r-1];r-- >0;)a[r]=i,i=o[i];return a}function zr(t){const o=t.subTree.component;if(o)return o.asyncDep&&!o.asyncResolved?o:zr(o)}const ad=t=>t.__isTeleport,J=Symbol.for("v-fgt"),la=Symbol.for("v-txt"),gt=Symbol.for("v-cmt"),Po=Symbol.for("v-stc"),Yn=[];let Tt=null;function C(t=!1){Yn.push(Tt=t?null:[])}function ld(){Yn.pop(),Tt=Yn[Yn.length-1]||null}let ao=1;function Kl(t){ao+=t}function Vr(t){return t.dynamicChildren=ao>0?Tt||Mn:null,ld(),ao>0&&Tt&&Tt.push(t),t}function S(t,o,a,l,s,r){return Vr(e(t,o,a,l,s,r,!0))}function En(t,o,a,l,s){return Vr(f(t,o,a,l,s,!0))}function Fo(t){return t?t.__v_isVNode===!0:!1}function yn(t,o){return t.type===o.type&&t.key===o.key}const jr=({key:t})=>t??null,No=({ref:t,ref_key:o,ref_for:a})=>(typeof t=="number"&&(t=""+t),t!=null?Ke(t)||ut(t)||ge(t)?{i:et,r:t,k:o,f:!!a}:t:null);function e(t,o=null,a=null,l=0,s=null,r=t===J?0:1,i=!1,c=!1){const d={__v_isVNode:!0,__v_skip:!0,type:t,props:o,key:o&&jr(o),ref:o&&No(o),scopeId:_r,slotScopeIds:null,children:a,component:null,suspense:null,ssContent:null,ssFallback:null,dirs:null,transition:null,el:null,anchor:null,target:null,targetAnchor:null,staticCount:0,shapeFlag:r,patchFlag:l,dynamicProps:s,dynamicChildren:null,appContext:null,ctx:et};return c?(bl(d,a),r&128&&t.normalize(d)):a&&(d.shapeFlag|=Ke(a)?8:16),ao>0&&!i&&Tt&&(d.patchFlag>0||r&6)&&d.patchFlag!==32&&Tt.push(d),d}const f=sd;function sd(t,o=null,a=null,l=0,s=null,r=!1){if((!t||t===vc)&&(t=gt),Fo(t)){const c=sn(t,o,!0);return a&&bl(c,a),ao>0&&!r&&Tt&&(c.shapeFlag&6?Tt[Tt.indexOf(t)]=c:Tt.push(c)),c.patchFlag|=-2,c}if(md(t)&&(t=t.__vccOpts),o){o=Ho(o);let{class:c,style:d}=o;c&&!Ke(c)&&(o.class=Fe(c)),Re(d)&&(lr(d)&&!ce(d)&&(d=Ze({},d)),o.style=Qo(d))}const i=Ke(t)?1:wc(t)?128:ad(t)?64:Re(t)?4:ge(t)?2:0;return e(t,o,a,l,s,i,r,!0)}function Ho(t){return t?lr(t)||Nr(t)?Ze({},t):t:null}function sn(t,o,a=!1,l=!1){const{props:s,ref:r,patchFlag:i,children:c,transition:d}=t,h=o?rd(s||{},o):s,u={__v_isVNode:!0,__v_skip:!0,type:t.type,props:h,key:h&&jr(h),ref:o&&o.ref?a&&r?ce(r)?r.concat(No(o)):[r,No(o)]:No(o):r,scopeId:t.scopeId,slotScopeIds:t.slotScopeIds,children:c,target:t.target,targetAnchor:t.targetAnchor,staticCount:t.staticCount,shapeFlag:t.shapeFlag,patchFlag:o&&t.type!==J?i===-1?16:i|16:i,dynamicProps:t.dynamicProps,dynamicChildren:t.dynamicChildren,appContext:t.appContext,dirs:t.dirs,transition:d,component:t.component,suspense:t.suspense,ssContent:t.ssContent&&sn(t.ssContent),ssFallback:t.ssFallback&&sn(t.ssFallback),el:t.el,anchor:t.anchor,ctx:t.ctx,ce:t.ce};return d&&l&&(u.transition=d.clone(u)),u}function n(t=" ",o=0){return f(la,null,t,o)}function G(t,o){const a=f(Po,null,t);return a.staticCount=o,a}function He(t="",o=!1){return o?(C(),En(gt,null,t)):f(gt,null,t)}function Rt(t){return t==null||typeof t=="boolean"?f(gt):ce(t)?f(J,null,t.slice()):typeof t=="object"?Xt(t):f(la,null,String(t))}function Xt(t){return t.el===null&&t.patchFlag!==-1||t.memo?t:sn(t)}function bl(t,o){let a=0;const{shapeFlag:l}=t;if(o==null)o=null;else if(ce(o))a=16;else if(typeof o=="object")if(l&65){const s=o.default;s&&(s._c&&(s._d=!1),bl(t,s()),s._c&&(s._d=!0));return}else{a=32;const s=o._;!s&&!Nr(o)?o._ctx=et:s===3&&et&&(et.slots._===1?o._=1:(o._=2,t.patchFlag|=1024))}else ge(o)?(o={default:o,_ctx:et},a=32):(o=String(o),l&64?(a=16,o=[n(o)]):a=8);t.children=o,t.shapeFlag|=a}function rd(...t){const o={};for(let a=0;a<t.length;a++){const l=t[a];for(const s in l)if(s==="class")o.class!==l.class&&(o.class=Fe([o.class,l.class]));else if(s==="style")o.style=Qo([o.style,l.style]);else if(Jo(s)){const r=o[s],i=l[s];i&&r!==i&&!(ce(r)&&r.includes(i))&&(o[s]=r?[].concat(r,i):i)}else s!==""&&(o[s]=l[s])}return o}function Ot(t,o,a,l=null){Ct(t,o,7,[a,l])}const id=Ar();let cd=0;function dd(t,o,a){const l=t.type,s=(o?o.appContext:t.appContext)||id,r={uid:cd++,vnode:t,type:l,parent:o,appContext:s,root:null,next:null,subTree:null,effect:null,update:null,scope:new Ni(!0),render:null,proxy:null,exposed:null,exposeProxy:null,withProxy:null,provides:o?o.provides:Object.create(s.provides),accessCache:null,renderCache:[],components:null,directives:null,propsOptions:Rr(l,s),emitsOptions:gr(l,s),emit:null,emitted:null,propsDefaults:Ve,inheritAttrs:l.inheritAttrs,ctx:Ve,data:Ve,props:Ve,attrs:Ve,slots:Ve,refs:Ve,setupState:Ve,setupContext:null,attrsProxy:null,slotsProxy:null,suspense:a,suspenseId:a?a.pendingId:0,asyncDep:null,asyncResolved:!1,isMounted:!1,isUnmounted:!1,isDeactivated:!1,bc:null,c:null,bm:null,m:null,bu:null,u:null,um:null,bum:null,da:null,a:null,rtg:null,rtc:null,ec:null,sp:null};return r.ctx={_:r},r.root=o?o.root:r,r.emit=hc.bind(null,r),t.ce&&t.ce(r),r}let st=null;const gl=()=>st||et;let qo,Fa;{const t=Hs(),o=(a,l)=>{let s;return(s=t[a])||(s=t[a]=[]),s.push(l),r=>{s.length>1?s.forEach(i=>i(r)):s[0](r)}};qo=o("__VUE_INSTANCE_SETTERS__",a=>st=a),Fa=o("__VUE_SSR_SETTERS__",a=>sa=a)}const ho=t=>{const o=st;return qo(t),t.scope.on(),()=>{t.scope.off(),qo(o)}},Yl=()=>{st&&st.scope.off(),qo(null)};function Fr(t){return t.vnode.shapeFlag&4}let sa=!1;function ud(t,o=!1){o&&Fa(o);const{props:a,children:l}=t.vnode,s=Fr(t);Yc(t,a,s,o),Xc(t,l);const r=s?hd(t,o):void 0;return o&&Fa(!1),r}function hd(t,o){const a=t.type;t.accessCache=Object.create(null),t.proxy=new Proxy(t.ctx,Vc);const{setup:l}=a;if(l){const s=t.setupContext=l.length>1?qr(t):null,r=ho(t);cn();const i=on(l,t,0,[t.props,s]);if(dn(),r(),zs(i)){if(i.then(Yl,Yl),o)return i.then(c=>{Jl(t,c,o)}).catch(c=>{ta(c,t,0)});t.asyncDep=i}else Jl(t,i,o)}else Hr(t,o)}function Jl(t,o,a){ge(o)?t.type.__ssrInlineRender?t.ssrRender=o:t.render=o:Re(o)&&(t.setupState=cr(o)),Hr(t,a)}let Zl;function Hr(t,o,a){const l=t.type;if(!t.render){if(!o&&Zl&&!l.render){const s=l.template||fl(t).template;if(s){const{isCustomElement:r,compilerOptions:i}=t.appContext.config,{delimiters:c,compilerOptions:d}=l,h=Ze(Ze({isCustomElement:r,delimiters:c},i),d);l.render=Zl(s,h)}}t.render=l.render||xt}{const s=ho(t);cn();try{Fc(t)}finally{dn(),s()}}}const pd={get(t,o){return _t(t,"get",""),t[o]}};function qr(t){const o=a=>{t.exposed=a||{}};return{attrs:new Proxy(t.attrs,pd),slots:t.slots,emit:t.emit,expose:o}}function ra(t){if(t.exposed)return t.exposeProxy||(t.exposeProxy=new Proxy(cr(tc(t.exposed)),{get(o,a){if(a in o)return o[a];if(a in Wn)return Wn[a](t)},has(o,a){return a in o||a in Wn}}))}function fd(t,o=!0){return ge(t)?t.displayName||t.name:t.name||o&&t.__name}function md(t){return ge(t)&&"__vccOpts"in t}const ue=(t,o)=>nc(t,o,sa);function ft(t,o,a){const l=arguments.length;return l===2?Re(o)&&!ce(o)?Fo(o)?f(t,null,[o]):f(t,o):f(t,null,o):(l>3?a=Array.prototype.slice.call(arguments,2):l===3&&Fo(a)&&(a=[a]),f(t,o,a))}const bd="3.4.27";/**
* @vue/runtime-dom v3.4.27
* (c) 2018-present Yuxi (Evan) You and Vue contributors
* @license MIT
**/const gd="http://www.w3.org/2000/svg",_d="http://www.w3.org/1998/Math/MathML",Qt=typeof document<"u"?document:null,Xl=Qt&&Qt.createElement("template"),vd={insert:(t,o,a)=>{o.insertBefore(t,a||null)},remove:t=>{const o=t.parentNode;o&&o.removeChild(t)},createElement:(t,o,a,l)=>{const s=o==="svg"?Qt.createElementNS(gd,t):o==="mathml"?Qt.createElementNS(_d,t):Qt.createElement(t,a?{is:a}:void 0);return t==="select"&&l&&l.multiple!=null&&s.setAttribute("multiple",l.multiple),s},createText:t=>Qt.createTextNode(t),createComment:t=>Qt.createComment(t),setText:(t,o)=>{t.nodeValue=o},setElementText:(t,o)=>{t.textContent=o},parentNode:t=>t.parentNode,nextSibling:t=>t.nextSibling,querySelector:t=>Qt.querySelector(t),setScopeId(t,o){t.setAttribute(o,"")},insertStaticContent(t,o,a,l,s,r){const i=a?a.previousSibling:o.lastChild;if(s&&(s===r||s.nextSibling))for(;o.insertBefore(s.cloneNode(!0),a),!(s===r||!(s=s.nextSibling)););else{Xl.innerHTML=l==="svg"?`<svg>${t}</svg>`:l==="mathml"?`<math>${t}</math>`:t;const c=Xl.content;if(l==="svg"||l==="mathml"){const d=c.firstChild;for(;d.firstChild;)c.appendChild(d.firstChild);c.removeChild(d)}o.insertBefore(c,a)}return[i?i.nextSibling:o.firstChild,a?a.previousSibling:o.lastChild]}},Wt="transition",jn="animation",lo=Symbol("_vtc"),_l=(t,{slots:o})=>ft(Ac,yd(t),o);_l.displayName="Transition";const Ur={name:String,type:String,css:{type:Boolean,default:!0},duration:[String,Number,Object],enterFromClass:String,enterActiveClass:String,enterToClass:String,appearFromClass:String,appearActiveClass:String,appearToClass:String,leaveFromClass:String,leaveActiveClass:String,leaveToClass:String};_l.props=Ze({},yr,Ur);const bn=(t,o=[])=>{ce(t)?t.forEach(a=>a(...o)):t&&t(...o)},Ql=t=>t?ce(t)?t.some(o=>o.length>1):t.length>1:!1;function yd(t){const o={};for(const D in t)D in Ur||(o[D]=t[D]);if(t.css===!1)return o;const{name:a="v",type:l,duration:s,enterFromClass:r=`${a}-enter-from`,enterActiveClass:i=`${a}-enter-active`,enterToClass:c=`${a}-enter-to`,appearFromClass:d=r,appearActiveClass:h=i,appearToClass:u=c,leaveFromClass:p=`${a}-leave-from`,leaveActiveClass:m=`${a}-leave-active`,leaveToClass:g=`${a}-leave-to`}=t,x=wd(s),B=x&&x[0],W=x&&x[1],{onBeforeEnter:F,onEnter:V,onEnterCancelled:R,onLeave:q,onLeaveCancelled:ee,onBeforeAppear:Q=F,onAppear:P=V,onAppearCancelled:I=R}=o,te=(D,j,be)=>{gn(D,j?u:c),gn(D,j?h:i),be&&be()},oe=(D,j)=>{D._isLeaving=!1,gn(D,p),gn(D,g),gn(D,m),j&&j()},A=D=>(j,be)=>{const xe=D?P:V,fe=()=>te(j,D,be);bn(xe,[j,fe]),es(()=>{gn(j,D?d:r),Kt(j,D?u:c),Ql(xe)||ts(j,l,B,fe)})};return Ze(o,{onBeforeEnter(D){bn(F,[D]),Kt(D,r),Kt(D,i)},onBeforeAppear(D){bn(Q,[D]),Kt(D,d),Kt(D,h)},onEnter:A(!1),onAppear:A(!0),onLeave(D,j){D._isLeaving=!0;const be=()=>oe(D,j);Kt(D,p),Kt(D,m),$d(),es(()=>{D._isLeaving&&(gn(D,p),Kt(D,g),Ql(q)||ts(D,l,W,be))}),bn(q,[D,be])},onEnterCancelled(D){te(D,!1),bn(R,[D])},onAppearCancelled(D){te(D,!0),bn(I,[D])},onLeaveCancelled(D){oe(D),bn(ee,[D])}})}function wd(t){if(t==null)return null;if(Re(t))return[wa(t.enter),wa(t.leave)];{const o=wa(t);return[o,o]}}function wa(t){return $i(t)}function Kt(t,o){o.split(/\s+/).forEach(a=>a&&t.classList.add(a)),(t[lo]||(t[lo]=new Set)).add(o)}function gn(t,o){o.split(/\s+/).forEach(l=>l&&t.classList.remove(l));const a=t[lo];a&&(a.delete(o),a.size||(t[lo]=void 0))}function es(t){requestAnimationFrame(()=>{requestAnimationFrame(t)})}let xd=0;function ts(t,o,a,l){const s=t._endId=++xd,r=()=>{s===t._endId&&l()};if(a)return setTimeout(r,a);const{type:i,timeout:c,propCount:d}=kd(t,o);if(!i)return l();const h=i+"end";let u=0;const p=()=>{t.removeEventListener(h,m),r()},m=g=>{g.target===t&&++u>=d&&p()};setTimeout(()=>{u<d&&p()},c+1),t.addEventListener(h,m)}function kd(t,o){const a=window.getComputedStyle(t),l=x=>(a[x]||"").split(", "),s=l(`${Wt}Delay`),r=l(`${Wt}Duration`),i=ns(s,r),c=l(`${jn}Delay`),d=l(`${jn}Duration`),h=ns(c,d);let u=null,p=0,m=0;o===Wt?i>0&&(u=Wt,p=i,m=r.length):o===jn?h>0&&(u=jn,p=h,m=d.length):(p=Math.max(i,h),u=p>0?i>h?Wt:jn:null,m=u?u===Wt?r.length:d.length:0);const g=u===Wt&&/\b(transform|all)(,|$)/.test(l(`${Wt}Property`).toString());return{type:u,timeout:p,propCount:m,hasTransform:g}}function ns(t,o){for(;t.length<o.length;)t=t.concat(t);return Math.max(...o.map((a,l)=>os(a)+os(t[l])))}function os(t){return t==="auto"?0:Number(t.slice(0,-1).replace(",","."))*1e3}function $d(){return document.body.offsetHeight}function Cd(t,o,a){const l=t[lo];l&&(o=(o?[o,...l]:[...l]).join(" ")),o==null?t.removeAttribute("class"):a?t.setAttribute("class",o):t.className=o}const as=Symbol("_vod"),Sd=Symbol("_vsh"),Ed=Symbol(""),Td=/(^|;)\s*display\s*:/;function Ad(t,o,a){const l=t.style,s=Ke(a);let r=!1;if(a&&!s){if(o)if(Ke(o))for(const i of o.split(";")){const c=i.slice(0,i.indexOf(":")).trim();a[c]==null&&Oo(l,c,"")}else for(const i in o)a[i]==null&&Oo(l,i,"");for(const i in a)i==="display"&&(r=!0),Oo(l,i,a[i])}else if(s){if(o!==a){const i=l[Ed];i&&(a+=";"+i),l.cssText=a,r=Td.test(a)}}else o&&t.removeAttribute("style");as in t&&(t[as]=r?l.display:"",t[Sd]&&(l.display="none"))}const ls=/\s*!important$/;function Oo(t,o,a){if(ce(a))a.forEach(l=>Oo(t,o,l));else if(a==null&&(a=""),o.startsWith("--"))t.setProperty(o,a);else{const l=Md(t,o);ls.test(a)?t.setProperty(zn(l),a.replace(ls,""),"important"):t[l]=a}}const ss=["Webkit","Moz","ms"],xa={};function Md(t,o){const a=xa[o];if(a)return a;let l=Lt(o);if(l!=="filter"&&l in t)return xa[o]=l;l=Xo(l);for(let s=0;s<ss.length;s++){const r=ss[s]+l;if(r in t)return xa[o]=r}return o}const rs="http://www.w3.org/1999/xlink";function Pd(t,o,a,l,s){if(l&&o.startsWith("xlink:"))a==null?t.removeAttributeNS(rs,o.slice(6,o.length)):t.setAttributeNS(rs,o,a);else{const r=Mi(o);a==null||r&&!qs(a)?t.removeAttribute(o):t.setAttribute(o,r?"":a)}}function Nd(t,o,a,l,s,r,i){if(o==="innerHTML"||o==="textContent"){l&&i(l,s,r),t[o]=a??"";return}const c=t.tagName;if(o==="value"&&c!=="PROGRESS"&&!c.includes("-")){const h=c==="OPTION"?t.getAttribute("value")||"":t.value,u=a??"";(h!==u||!("_value"in t))&&(t.value=u),a==null&&t.removeAttribute(o),t._value=a;return}let d=!1;if(a===""||a==null){const h=typeof t[o];h==="boolean"?a=qs(a):a==null&&h==="string"?(a="",d=!0):h==="number"&&(a=0,d=!0)}try{t[o]=a}catch{}d&&t.removeAttribute(o)}function Vt(t,o,a,l){t.addEventListener(o,a,l)}function Od(t,o,a,l){t.removeEventListener(o,a,l)}const is=Symbol("_vei");function Rd(t,o,a,l,s=null){const r=t[is]||(t[is]={}),i=r[o];if(l&&i)i.value=l;else{const[c,d]=Id(o);if(l){const h=r[o]=Dd(l,s);Vt(t,c,h,d)}else i&&(Od(t,c,i,d),r[o]=void 0)}}const cs=/(?:Once|Passive|Capture)$/;function Id(t){let o;if(cs.test(t)){o={};let l;for(;l=t.match(cs);)t=t.slice(0,t.length-l[0].length),o[l[0].toLowerCase()]=!0}return[t[2]===":"?t.slice(3):zn(t.slice(2)),o]}let ka=0;const Ld=Promise.resolve(),Bd=()=>ka||(Ld.then(()=>ka=0),ka=Date.now());function Dd(t,o){const a=l=>{if(!l._vts)l._vts=Date.now();else if(l._vts<=a.attached)return;Ct(zd(l,a.value),o,5,[l])};return a.value=t,a.attached=Bd(),a}function zd(t,o){if(ce(o)){const a=t.stopImmediatePropagation;return t.stopImmediatePropagation=()=>{a.call(t),t._stopped=!0},o.map(l=>s=>!s._stopped&&l&&l(s))}else return o}const ds=t=>t.charCodeAt(0)===111&&t.charCodeAt(1)===110&&t.charCodeAt(2)>96&&t.charCodeAt(2)<123,Vd=(t,o,a,l,s,r,i,c,d)=>{const h=s==="svg";o==="class"?Cd(t,l,h):o==="style"?Ad(t,a,l):Jo(o)?Xa(o)||Rd(t,o,a,l,i):(o[0]==="."?(o=o.slice(1),!0):o[0]==="^"?(o=o.slice(1),!1):jd(t,o,l,h))?Nd(t,o,l,r,i,c,d):(o==="true-value"?t._trueValue=l:o==="false-value"&&(t._falseValue=l),Pd(t,o,l,h))};function jd(t,o,a,l){if(l)return!!(o==="innerHTML"||o==="textContent"||o in t&&ds(o)&&ge(a));if(o==="spellcheck"||o==="draggable"||o==="translate"||o==="form"||o==="list"&&t.tagName==="INPUT"||o==="type"&&t.tagName==="TEXTAREA")return!1;if(o==="width"||o==="height"){const s=t.tagName;if(s==="IMG"||s==="VIDEO"||s==="CANVAS"||s==="SOURCE")return!1}return ds(o)&&Ke(a)?!1:o in t}const rn=t=>{const o=t.props["onUpdate:modelValue"]||!1;return ce(o)?a=>Ao(o,a):o};function Fd(t){t.target.composing=!0}function us(t){const o=t.target;o.composing&&(o.composing=!1,o.dispatchEvent(new Event("input")))}const St=Symbol("_assign"),ct={created(t,{modifiers:{lazy:o,trim:a,number:l}},s){t[St]=rn(s);const r=l||s.props&&s.props.type==="number";Vt(t,o?"change":"input",i=>{if(i.target.composing)return;let c=t.value;a&&(c=c.trim()),r&&(c=Lo(c)),t[St](c)}),a&&Vt(t,"change",()=>{t.value=t.value.trim()}),o||(Vt(t,"compositionstart",Fd),Vt(t,"compositionend",us),Vt(t,"change",us))},mounted(t,{value:o}){t.value=o??""},beforeUpdate(t,{value:o,modifiers:{lazy:a,trim:l,number:s}},r){if(t[St]=rn(r),t.composing)return;const i=(s||t.type==="number")&&!/^0\d/.test(t.value)?Lo(t.value):t.value,c=o??"";i!==c&&(document.activeElement===t&&t.type!=="range"&&(a||l&&t.value.trim()===c)||(t.value=c))}},Ro={deep:!0,created(t,o,a){t[St]=rn(a),Vt(t,"change",()=>{const l=t._modelValue,s=On(t),r=t.checked,i=t[St];if(ce(l)){const c=tl(l,s),d=c!==-1;if(r&&!d)i(l.concat(s));else if(!r&&d){const h=[...l];h.splice(c,1),i(h)}}else if(Dn(l)){const c=new Set(l);r?c.add(s):c.delete(s),i(c)}else i(Gr(t,r))})},mounted:hs,beforeUpdate(t,o,a){t[St]=rn(a),hs(t,o,a)}};function hs(t,{value:o,oldValue:a},l){t._modelValue=o,ce(o)?t.checked=tl(o,l.props.value)>-1:Dn(o)?t.checked=o.has(l.props.value):o!==a&&(t.checked=Cn(o,Gr(t,!0)))}const Hd={created(t,{value:o},a){t.checked=Cn(o,a.props.value),t[St]=rn(a),Vt(t,"change",()=>{t[St](On(t))})},beforeUpdate(t,{value:o,oldValue:a},l){t[St]=rn(l),o!==a&&(t.checked=Cn(o,l.props.value))}},Uo={deep:!0,created(t,{value:o,modifiers:{number:a}},l){const s=Dn(o);Vt(t,"change",()=>{const r=Array.prototype.filter.call(t.options,i=>i.selected).map(i=>a?Lo(On(i)):On(i));t[St](t.multiple?s?new Set(r):r:r[0]),t._assigning=!0,co(()=>{t._assigning=!1})}),t[St]=rn(l)},mounted(t,{value:o,modifiers:{number:a}}){ps(t,o)},beforeUpdate(t,o,a){t[St]=rn(a)},updated(t,{value:o,modifiers:{number:a}}){t._assigning||ps(t,o)}};function ps(t,o,a){const l=t.multiple,s=ce(o);if(!(l&&!s&&!Dn(o))){for(let r=0,i=t.options.length;r<i;r++){const c=t.options[r],d=On(c);if(l)if(s){const h=typeof d;h==="string"||h==="number"?c.selected=o.some(u=>String(u)===String(d)):c.selected=tl(o,d)>-1}else c.selected=o.has(d);else if(Cn(On(c),o)){t.selectedIndex!==r&&(t.selectedIndex=r);return}}!l&&t.selectedIndex!==-1&&(t.selectedIndex=-1)}}function On(t){return"_value"in t?t._value:t.value}function Gr(t,o){const a=o?"_trueValue":"_falseValue";return a in t?t[a]:o}const qd={created(t,o,a){$o(t,o,a,null,"created")},mounted(t,o,a){$o(t,o,a,null,"mounted")},beforeUpdate(t,o,a,l){$o(t,o,a,l,"beforeUpdate")},updated(t,o,a,l){$o(t,o,a,l,"updated")}};function Ud(t,o){switch(t){case"SELECT":return Uo;case"TEXTAREA":return ct;default:switch(o){case"checkbox":return Ro;case"radio":return Hd;default:return ct}}}function $o(t,o,a,l,s){const i=Ud(t.tagName,a.props&&a.props.type)[s];i&&i(t,o,a,l)}const Gd=["ctrl","shift","alt","meta"],Wd={stop:t=>t.stopPropagation(),prevent:t=>t.preventDefault(),self:t=>t.target!==t.currentTarget,ctrl:t=>!t.ctrlKey,shift:t=>!t.shiftKey,alt:t=>!t.altKey,meta:t=>!t.metaKey,left:t=>"button"in t&&t.button!==0,middle:t=>"button"in t&&t.button!==1,right:t=>"button"in t&&t.button!==2,exact:(t,o)=>Gd.some(a=>t[`${a}Key`]&&!o.includes(a))},Ha=(t,o)=>{const a=t._withMods||(t._withMods={}),l=o.join(".");return a[l]||(a[l]=(s,...r)=>{for(let i=0;i<o.length;i++){const c=Wd[o[i]];if(c&&c(s,o))return}return t(s,...r)})},Kd=Ze({patchProp:Vd},vd);let fs;function Yd(){return fs||(fs=ed(Kd))}const Jd=(...t)=>{const o=Yd().createApp(...t),{mount:a}=o;return o.mount=l=>{const s=Xd(l);if(!s)return;const r=o._component;!ge(r)&&!r.render&&!r.template&&(r.template=s.innerHTML),s.innerHTML="";const i=a(s,!1,Zd(s));return s instanceof Element&&(s.removeAttribute("v-cloak"),s.setAttribute("data-v-app","")),i},o};function Zd(t){if(t instanceof SVGElement)return"svg";if(typeof MathMLElement=="function"&&t instanceof MathMLElement)return"mathml"}function Xd(t){return Ke(t)?document.querySelector(t):t}/*!
  * vue-router v4.3.2
  * (c) 2024 Eduardo San Martin Morote
  * @license MIT
  */const An=typeof document<"u";function Qd(t){return t.__esModule||t[Symbol.toStringTag]==="Module"}const Me=Object.assign;function $a(t,o){const a={};for(const l in o){const s=o[l];a[l]=Pt(s)?s.map(t):t(s)}return a}const Jn=()=>{},Pt=Array.isArray,Wr=/#/g,eu=/&/g,tu=/\//g,nu=/=/g,ou=/\?/g,Kr=/\+/g,au=/%5B/g,lu=/%5D/g,Yr=/%5E/g,su=/%60/g,Jr=/%7B/g,ru=/%7C/g,Zr=/%7D/g,iu=/%20/g;function vl(t){return encodeURI(""+t).replace(ru,"|").replace(au,"[").replace(lu,"]")}function cu(t){return vl(t).replace(Jr,"{").replace(Zr,"}").replace(Yr,"^")}function qa(t){return vl(t).replace(Kr,"%2B").replace(iu,"+").replace(Wr,"%23").replace(eu,"%26").replace(su,"`").replace(Jr,"{").replace(Zr,"}").replace(Yr,"^")}function du(t){return qa(t).replace(nu,"%3D")}function uu(t){return vl(t).replace(Wr,"%23").replace(ou,"%3F")}function hu(t){return t==null?"":uu(t).replace(tu,"%2F")}function so(t){try{return decodeURIComponent(""+t)}catch{}return""+t}const pu=/\/$/,fu=t=>t.replace(pu,"");function Ca(t,o,a="/"){let l,s={},r="",i="";const c=o.indexOf("#");let d=o.indexOf("?");return c<d&&c>=0&&(d=-1),d>-1&&(l=o.slice(0,d),r=o.slice(d+1,c>-1?c:o.length),s=t(r)),c>-1&&(l=l||o.slice(0,c),i=o.slice(c,o.length)),l=_u(l??o,a),{fullPath:l+(r&&"?")+r+i,path:l,query:s,hash:so(i)}}function mu(t,o){const a=o.query?t(o.query):"";return o.path+(a&&"?")+a+(o.hash||"")}function ms(t,o){return!o||!t.toLowerCase().startsWith(o.toLowerCase())?t:t.slice(o.length)||"/"}function bu(t,o,a){const l=o.matched.length-1,s=a.matched.length-1;return l>-1&&l===s&&Rn(o.matched[l],a.matched[s])&&Xr(o.params,a.params)&&t(o.query)===t(a.query)&&o.hash===a.hash}function Rn(t,o){return(t.aliasOf||t)===(o.aliasOf||o)}function Xr(t,o){if(Object.keys(t).length!==Object.keys(o).length)return!1;for(const a in t)if(!gu(t[a],o[a]))return!1;return!0}function gu(t,o){return Pt(t)?bs(t,o):Pt(o)?bs(o,t):t===o}function bs(t,o){return Pt(o)?t.length===o.length&&t.every((a,l)=>a===o[l]):t.length===1&&t[0]===o}function _u(t,o){if(t.startsWith("/"))return t;if(!t)return o;const a=o.split("/"),l=t.split("/"),s=l[l.length-1];(s===".."||s===".")&&l.push("");let r=a.length-1,i,c;for(i=0;i<l.length;i++)if(c=l[i],c!==".")if(c==="..")r>1&&r--;else break;return a.slice(0,r).join("/")+"/"+l.slice(i).join("/")}var ro;(function(t){t.pop="pop",t.push="push"})(ro||(ro={}));var Zn;(function(t){t.back="back",t.forward="forward",t.unknown=""})(Zn||(Zn={}));function vu(t){if(!t)if(An){const o=document.querySelector("base");t=o&&o.getAttribute("href")||"/",t=t.replace(/^\w+:\/\/[^\/]+/,"")}else t="/";return t[0]!=="/"&&t[0]!=="#"&&(t="/"+t),fu(t)}const yu=/^[^#]+#/;function wu(t,o){return t.replace(yu,"#")+o}function xu(t,o){const a=document.documentElement.getBoundingClientRect(),l=t.getBoundingClientRect();return{behavior:o.behavior,left:l.left-a.left-(o.left||0),top:l.top-a.top-(o.top||0)}}const ia=()=>({left:window.scrollX,top:window.scrollY});function ku(t){let o;if("el"in t){const a=t.el,l=typeof a=="string"&&a.startsWith("#"),s=typeof a=="string"?l?document.getElementById(a.slice(1)):document.querySelector(a):a;if(!s)return;o=xu(s,t)}else o=t;"scrollBehavior"in document.documentElement.style?window.scrollTo(o):window.scrollTo(o.left!=null?o.left:window.scrollX,o.top!=null?o.top:window.scrollY)}function gs(t,o){return(history.state?history.state.position-o:-1)+t}const Ua=new Map;function $u(t,o){Ua.set(t,o)}function Cu(t){const o=Ua.get(t);return Ua.delete(t),o}let Su=()=>location.protocol+"//"+location.host;function Qr(t,o){const{pathname:a,search:l,hash:s}=o,r=t.indexOf("#");if(r>-1){let c=s.includes(t.slice(r))?t.slice(r).length:1,d=s.slice(c);return d[0]!=="/"&&(d="/"+d),ms(d,"")}return ms(a,t)+l+s}function Eu(t,o,a,l){let s=[],r=[],i=null;const c=({state:m})=>{const g=Qr(t,location),x=a.value,B=o.value;let W=0;if(m){if(a.value=g,o.value=m,i&&i===x){i=null;return}W=B?m.position-B.position:0}else l(g);s.forEach(F=>{F(a.value,x,{delta:W,type:ro.pop,direction:W?W>0?Zn.forward:Zn.back:Zn.unknown})})};function d(){i=a.value}function h(m){s.push(m);const g=()=>{const x=s.indexOf(m);x>-1&&s.splice(x,1)};return r.push(g),g}function u(){const{history:m}=window;m.state&&m.replaceState(Me({},m.state,{scroll:ia()}),"")}function p(){for(const m of r)m();r=[],window.removeEventListener("popstate",c),window.removeEventListener("beforeunload",u)}return window.addEventListener("popstate",c),window.addEventListener("beforeunload",u,{passive:!0}),{pauseListeners:d,listen:h,destroy:p}}function _s(t,o,a,l=!1,s=!1){return{back:t,current:o,forward:a,replaced:l,position:window.history.length,scroll:s?ia():null}}function Tu(t){const{history:o,location:a}=window,l={value:Qr(t,a)},s={value:o.state};s.value||r(l.value,{back:null,current:l.value,forward:null,position:o.length-1,replaced:!0,scroll:null},!0);function r(d,h,u){const p=t.indexOf("#"),m=p>-1?(a.host&&document.querySelector("base")?t:t.slice(p))+d:Su()+t+d;try{o[u?"replaceState":"pushState"](h,"",m),s.value=h}catch(g){console.error(g),a[u?"replace":"assign"](m)}}function i(d,h){const u=Me({},o.state,_s(s.value.back,d,s.value.forward,!0),h,{position:s.value.position});r(d,u,!0),l.value=d}function c(d,h){const u=Me({},s.value,o.state,{forward:d,scroll:ia()});r(u.current,u,!0);const p=Me({},_s(l.value,d,null),{position:u.position+1},h);r(d,p,!1),l.value=d}return{location:l,state:s,push:c,replace:i}}function Au(t){t=vu(t);const o=Tu(t),a=Eu(t,o.state,o.location,o.replace);function l(r,i=!0){i||a.pauseListeners(),history.go(r)}const s=Me({location:"",base:t,go:l,createHref:wu.bind(null,t)},o,a);return Object.defineProperty(s,"location",{enumerable:!0,get:()=>o.location.value}),Object.defineProperty(s,"state",{enumerable:!0,get:()=>o.state.value}),s}function Mu(t){return typeof t=="string"||t&&typeof t=="object"}function ei(t){return typeof t=="string"||typeof t=="symbol"}const Yt={path:"/",name:void 0,params:{},query:{},hash:"",fullPath:"/",matched:[],meta:{},redirectedFrom:void 0},ti=Symbol("");var vs;(function(t){t[t.aborted=4]="aborted",t[t.cancelled=8]="cancelled",t[t.duplicated=16]="duplicated"})(vs||(vs={}));function In(t,o){return Me(new Error,{type:t,[ti]:!0},o)}function zt(t,o){return t instanceof Error&&ti in t&&(o==null||!!(t.type&o))}const ys="[^/]+?",Pu={sensitive:!1,strict:!1,start:!0,end:!0},Nu=/[.+*?^${}()[\]/\\]/g;function Ou(t,o){const a=Me({},Pu,o),l=[];let s=a.start?"^":"";const r=[];for(const h of t){const u=h.length?[]:[90];a.strict&&!h.length&&(s+="/");for(let p=0;p<h.length;p++){const m=h[p];let g=40+(a.sensitive?.25:0);if(m.type===0)p||(s+="/"),s+=m.value.replace(Nu,"\\$&"),g+=40;else if(m.type===1){const{value:x,repeatable:B,optional:W,regexp:F}=m;r.push({name:x,repeatable:B,optional:W});const V=F||ys;if(V!==ys){g+=10;try{new RegExp(`(${V})`)}catch(q){throw new Error(`Invalid custom RegExp for param "${x}" (${V}): `+q.message)}}let R=B?`((?:${V})(?:/(?:${V}))*)`:`(${V})`;p||(R=W&&h.length<2?`(?:/${R})`:"/"+R),W&&(R+="?"),s+=R,g+=20,W&&(g+=-8),B&&(g+=-20),V===".*"&&(g+=-50)}u.push(g)}l.push(u)}if(a.strict&&a.end){const h=l.length-1;l[h][l[h].length-1]+=.7000000000000001}a.strict||(s+="/?"),a.end?s+="$":a.strict&&(s+="(?:/|$)");const i=new RegExp(s,a.sensitive?"":"i");function c(h){const u=h.match(i),p={};if(!u)return null;for(let m=1;m<u.length;m++){const g=u[m]||"",x=r[m-1];p[x.name]=g&&x.repeatable?g.split("/"):g}return p}function d(h){let u="",p=!1;for(const m of t){(!p||!u.endsWith("/"))&&(u+="/"),p=!1;for(const g of m)if(g.type===0)u+=g.value;else if(g.type===1){const{value:x,repeatable:B,optional:W}=g,F=x in h?h[x]:"";if(Pt(F)&&!B)throw new Error(`Provided param "${x}" is an array but it is not repeatable (* or + modifiers)`);const V=Pt(F)?F.join("/"):F;if(!V)if(W)m.length<2&&(u.endsWith("/")?u=u.slice(0,-1):p=!0);else throw new Error(`Missing required param "${x}"`);u+=V}}return u||"/"}return{re:i,score:l,keys:r,parse:c,stringify:d}}function Ru(t,o){let a=0;for(;a<t.length&&a<o.length;){const l=o[a]-t[a];if(l)return l;a++}return t.length<o.length?t.length===1&&t[0]===40+40?-1:1:t.length>o.length?o.length===1&&o[0]===40+40?1:-1:0}function Iu(t,o){let a=0;const l=t.score,s=o.score;for(;a<l.length&&a<s.length;){const r=Ru(l[a],s[a]);if(r)return r;a++}if(Math.abs(s.length-l.length)===1){if(ws(l))return 1;if(ws(s))return-1}return s.length-l.length}function ws(t){const o=t[t.length-1];return t.length>0&&o[o.length-1]<0}const Lu={type:0,value:""},Bu=/[a-zA-Z0-9_]/;function Du(t){if(!t)return[[]];if(t==="/")return[[Lu]];if(!t.startsWith("/"))throw new Error(`Invalid path "${t}"`);function o(g){throw new Error(`ERR (${a})/"${h}": ${g}`)}let a=0,l=a;const s=[];let r;function i(){r&&s.push(r),r=[]}let c=0,d,h="",u="";function p(){h&&(a===0?r.push({type:0,value:h}):a===1||a===2||a===3?(r.length>1&&(d==="*"||d==="+")&&o(`A repeatable param (${h}) must be alone in its segment. eg: '/:ids+.`),r.push({type:1,value:h,regexp:u,repeatable:d==="*"||d==="+",optional:d==="*"||d==="?"})):o("Invalid state to consume buffer"),h="")}function m(){h+=d}for(;c<t.length;){if(d=t[c++],d==="\\"&&a!==2){l=a,a=4;continue}switch(a){case 0:d==="/"?(h&&p(),i()):d===":"?(p(),a=1):m();break;case 4:m(),a=l;break;case 1:d==="("?a=2:Bu.test(d)?m():(p(),a=0,d!=="*"&&d!=="?"&&d!=="+"&&c--);break;case 2:d===")"?u[u.length-1]=="\\"?u=u.slice(0,-1)+d:a=3:u+=d;break;case 3:p(),a=0,d!=="*"&&d!=="?"&&d!=="+"&&c--,u="";break;default:o("Unknown state");break}}return a===2&&o(`Unfinished custom RegExp for param "${h}"`),p(),i(),s}function zu(t,o,a){const l=Ou(Du(t.path),a),s=Me(l,{record:t,parent:o,children:[],alias:[]});return o&&!s.record.aliasOf==!o.record.aliasOf&&o.children.push(s),s}function Vu(t,o){const a=[],l=new Map;o=$s({strict:!1,end:!0,sensitive:!1},o);function s(u){return l.get(u)}function r(u,p,m){const g=!m,x=ju(u);x.aliasOf=m&&m.record;const B=$s(o,u),W=[x];if("alias"in u){const R=typeof u.alias=="string"?[u.alias]:u.alias;for(const q of R)W.push(Me({},x,{components:m?m.record.components:x.components,path:q,aliasOf:m?m.record:x}))}let F,V;for(const R of W){const{path:q}=R;if(p&&q[0]!=="/"){const ee=p.record.path,Q=ee[ee.length-1]==="/"?"":"/";R.path=p.record.path+(q&&Q+q)}if(F=zu(R,p,B),m?m.alias.push(F):(V=V||F,V!==F&&V.alias.push(F),g&&u.name&&!ks(F)&&i(u.name)),x.children){const ee=x.children;for(let Q=0;Q<ee.length;Q++)r(ee[Q],F,m&&m.children[Q])}m=m||F,(F.record.components&&Object.keys(F.record.components).length||F.record.name||F.record.redirect)&&d(F)}return V?()=>{i(V)}:Jn}function i(u){if(ei(u)){const p=l.get(u);p&&(l.delete(u),a.splice(a.indexOf(p),1),p.children.forEach(i),p.alias.forEach(i))}else{const p=a.indexOf(u);p>-1&&(a.splice(p,1),u.record.name&&l.delete(u.record.name),u.children.forEach(i),u.alias.forEach(i))}}function c(){return a}function d(u){let p=0;for(;p<a.length&&Iu(u,a[p])>=0&&(u.record.path!==a[p].record.path||!ni(u,a[p]));)p++;a.splice(p,0,u),u.record.name&&!ks(u)&&l.set(u.record.name,u)}function h(u,p){let m,g={},x,B;if("name"in u&&u.name){if(m=l.get(u.name),!m)throw In(1,{location:u});B=m.record.name,g=Me(xs(p.params,m.keys.filter(V=>!V.optional).concat(m.parent?m.parent.keys.filter(V=>V.optional):[]).map(V=>V.name)),u.params&&xs(u.params,m.keys.map(V=>V.name))),x=m.stringify(g)}else if(u.path!=null)x=u.path,m=a.find(V=>V.re.test(x)),m&&(g=m.parse(x),B=m.record.name);else{if(m=p.name?l.get(p.name):a.find(V=>V.re.test(p.path)),!m)throw In(1,{location:u,currentLocation:p});B=m.record.name,g=Me({},p.params,u.params),x=m.stringify(g)}const W=[];let F=m;for(;F;)W.unshift(F.record),F=F.parent;return{name:B,path:x,params:g,matched:W,meta:Hu(W)}}return t.forEach(u=>r(u)),{addRoute:r,resolve:h,removeRoute:i,getRoutes:c,getRecordMatcher:s}}function xs(t,o){const a={};for(const l of o)l in t&&(a[l]=t[l]);return a}function ju(t){return{path:t.path,redirect:t.redirect,name:t.name,meta:t.meta||{},aliasOf:void 0,beforeEnter:t.beforeEnter,props:Fu(t),children:t.children||[],instances:{},leaveGuards:new Set,updateGuards:new Set,enterCallbacks:{},components:"components"in t?t.components||null:t.component&&{default:t.component}}}function Fu(t){const o={},a=t.props||!1;if("component"in t)o.default=a;else for(const l in t.components)o[l]=typeof a=="object"?a[l]:a;return o}function ks(t){for(;t;){if(t.record.aliasOf)return!0;t=t.parent}return!1}function Hu(t){return t.reduce((o,a)=>Me(o,a.meta),{})}function $s(t,o){const a={};for(const l in t)a[l]=l in o?o[l]:t[l];return a}function ni(t,o){return o.children.some(a=>a===t||ni(t,a))}function qu(t){const o={};if(t===""||t==="?")return o;const l=(t[0]==="?"?t.slice(1):t).split("&");for(let s=0;s<l.length;++s){const r=l[s].replace(Kr," "),i=r.indexOf("="),c=so(i<0?r:r.slice(0,i)),d=i<0?null:so(r.slice(i+1));if(c in o){let h=o[c];Pt(h)||(h=o[c]=[h]),h.push(d)}else o[c]=d}return o}function Cs(t){let o="";for(let a in t){const l=t[a];if(a=du(a),l==null){l!==void 0&&(o+=(o.length?"&":"")+a);continue}(Pt(l)?l.map(r=>r&&qa(r)):[l&&qa(l)]).forEach(r=>{r!==void 0&&(o+=(o.length?"&":"")+a,r!=null&&(o+="="+r))})}return o}function Uu(t){const o={};for(const a in t){const l=t[a];l!==void 0&&(o[a]=Pt(l)?l.map(s=>s==null?null:""+s):l==null?l:""+l)}return o}const Gu=Symbol(""),Ss=Symbol(""),ca=Symbol(""),yl=Symbol(""),Ga=Symbol("");function Fn(){let t=[];function o(l){return t.push(l),()=>{const s=t.indexOf(l);s>-1&&t.splice(s,1)}}function a(){t=[]}return{add:o,list:()=>t.slice(),reset:a}}function en(t,o,a,l,s,r=i=>i()){const i=l&&(l.enterCallbacks[s]=l.enterCallbacks[s]||[]);return()=>new Promise((c,d)=>{const h=m=>{m===!1?d(In(4,{from:a,to:o})):m instanceof Error?d(m):Mu(m)?d(In(2,{from:o,to:m})):(i&&l.enterCallbacks[s]===i&&typeof m=="function"&&i.push(m),c())},u=r(()=>t.call(l&&l.instances[s],o,a,h));let p=Promise.resolve(u);t.length<3&&(p=p.then(h)),p.catch(m=>d(m))})}function Sa(t,o,a,l,s=r=>r()){const r=[];for(const i of t)for(const c in i.components){let d=i.components[c];if(!(o!=="beforeRouteEnter"&&!i.instances[c]))if(Wu(d)){const u=(d.__vccOpts||d)[o];u&&r.push(en(u,a,l,i,c,s))}else{let h=d();r.push(()=>h.then(u=>{if(!u)return Promise.reject(new Error(`Couldn't resolve component "${c}" at "${i.path}"`));const p=Qd(u)?u.default:u;i.components[c]=p;const g=(p.__vccOpts||p)[o];return g&&en(g,a,l,i,c,s)()}))}}return r}function Wu(t){return typeof t=="object"||"displayName"in t||"props"in t||"__vccOpts"in t}function Es(t){const o=Je(ca),a=Je(yl),l=ue(()=>{const d=$(t.to);return o.resolve(d)}),s=ue(()=>{const{matched:d}=l.value,{length:h}=d,u=d[h-1],p=a.matched;if(!u||!p.length)return-1;const m=p.findIndex(Rn.bind(null,u));if(m>-1)return m;const g=Ts(d[h-2]);return h>1&&Ts(u)===g&&p[p.length-1].path!==g?p.findIndex(Rn.bind(null,d[h-2])):m}),r=ue(()=>s.value>-1&&Ju(a.params,l.value.params)),i=ue(()=>s.value>-1&&s.value===a.matched.length-1&&Xr(a.params,l.value.params));function c(d={}){return Yu(d)?o[$(t.replace)?"replace":"push"]($(t.to)).catch(Jn):Promise.resolve()}return{route:l,href:ue(()=>l.value.href),isActive:r,isExactActive:i,navigate:c}}const Ku=kr({name:"RouterLink",compatConfig:{MODE:3},props:{to:{type:[String,Object],required:!0},replace:Boolean,activeClass:String,exactActiveClass:String,custom:Boolean,ariaCurrentValue:{type:String,default:"page"}},useLink:Es,setup(t,{slots:o}){const a=tn(Es(t)),{options:l}=Je(ca),s=ue(()=>({[As(t.activeClass,l.linkActiveClass,"router-link-active")]:a.isActive,[As(t.exactActiveClass,l.linkExactActiveClass,"router-link-exact-active")]:a.isExactActive}));return()=>{const r=o.default&&o.default(a);return t.custom?r:ft("a",{"aria-current":a.isExactActive?t.ariaCurrentValue:null,href:a.href,onClick:a.navigate,class:s.value},r)}}}),_e=Ku;function Yu(t){if(!(t.metaKey||t.altKey||t.ctrlKey||t.shiftKey)&&!t.defaultPrevented&&!(t.button!==void 0&&t.button!==0)){if(t.currentTarget&&t.currentTarget.getAttribute){const o=t.currentTarget.getAttribute("target");if(/\b_blank\b/i.test(o))return}return t.preventDefault&&t.preventDefault(),!0}}function Ju(t,o){for(const a in o){const l=o[a],s=t[a];if(typeof l=="string"){if(l!==s)return!1}else if(!Pt(s)||s.length!==l.length||l.some((r,i)=>r!==s[i]))return!1}return!0}function Ts(t){return t?t.aliasOf?t.aliasOf.path:t.path:""}const As=(t,o,a)=>t??o??a,Zu=kr({name:"RouterView",inheritAttrs:!1,props:{name:{type:String,default:"default"},route:Object},compatConfig:{MODE:3},setup(t,{attrs:o,slots:a}){const l=Je(Ga),s=ue(()=>t.route||l.value),r=Je(Ss,0),i=ue(()=>{let h=$(r);const{matched:u}=s.value;let p;for(;(p=u[h])&&!p.components;)h++;return h}),c=ue(()=>s.value.matched[i.value]);Ft(Ss,ue(()=>i.value+1)),Ft(Gu,c),Ft(Ga,s);const d=Ce();return $n(()=>[d.value,c.value,t.name],([h,u,p],[m,g,x])=>{u&&(u.instances[p]=h,g&&g!==u&&h&&h===m&&(u.leaveGuards.size||(u.leaveGuards=g.leaveGuards),u.updateGuards.size||(u.updateGuards=g.updateGuards))),h&&u&&(!g||!Rn(u,g)||!m)&&(u.enterCallbacks[p]||[]).forEach(B=>B(h))},{flush:"post"}),()=>{const h=s.value,u=t.name,p=c.value,m=p&&p.components[u];if(!m)return Ms(a.default,{Component:m,route:h});const g=p.props[u],x=g?g===!0?h.params:typeof g=="function"?g(h):g:null,W=ft(m,Me({},x,o,{onVnodeUnmounted:F=>{F.component.isUnmounted&&(p.instances[u]=null)},ref:d}));return Ms(a.default,{Component:W,route:h})||W}}});function Ms(t,o){if(!t)return null;const a=t(o);return a.length===1?a[0]:a}const oi=Zu;function Xu(t){const o=Vu(t.routes,t),a=t.parseQuery||qu,l=t.stringifyQuery||Cs,s=t.history,r=Fn(),i=Fn(),c=Fn(),d=zo(Yt);let h=Yt;An&&t.scrollBehavior&&"scrollRestoration"in history&&(history.scrollRestoration="manual");const u=$a.bind(null,M=>""+M),p=$a.bind(null,hu),m=$a.bind(null,so);function g(M,ae){let X,ie;return ei(M)?(X=o.getRecordMatcher(M),ie=ae):ie=M,o.addRoute(ie,X)}function x(M){const ae=o.getRecordMatcher(M);ae&&o.removeRoute(ae)}function B(){return o.getRoutes().map(M=>M.record)}function W(M){return!!o.getRecordMatcher(M)}function F(M,ae){if(ae=Me({},ae||d.value),typeof M=="string"){const w=Ca(a,M,ae.path),E=o.resolve({path:w.path},ae),L=s.createHref(w.fullPath);return Me(w,E,{params:m(E.params),hash:so(w.hash),redirectedFrom:void 0,href:L})}let X;if(M.path!=null)X=Me({},M,{path:Ca(a,M.path,ae.path).path});else{const w=Me({},M.params);for(const E in w)w[E]==null&&delete w[E];X=Me({},M,{params:p(w)}),ae.params=p(ae.params)}const ie=o.resolve(X,ae),Te=M.hash||"";ie.params=u(m(ie.params));const Le=mu(l,Me({},M,{hash:cu(Te),path:ie.path})),b=s.createHref(Le);return Me({fullPath:Le,hash:Te,query:l===Cs?Uu(M.query):M.query||{}},ie,{redirectedFrom:void 0,href:b})}function V(M){return typeof M=="string"?Ca(a,M,d.value.path):Me({},M)}function R(M,ae){if(h!==M)return In(8,{from:ae,to:M})}function q(M){return P(M)}function ee(M){return q(Me(V(M),{replace:!0}))}function Q(M){const ae=M.matched[M.matched.length-1];if(ae&&ae.redirect){const{redirect:X}=ae;let ie=typeof X=="function"?X(M):X;return typeof ie=="string"&&(ie=ie.includes("?")||ie.includes("#")?ie=V(ie):{path:ie},ie.params={}),Me({query:M.query,hash:M.hash,params:ie.path!=null?{}:M.params},ie)}}function P(M,ae){const X=h=F(M),ie=d.value,Te=M.state,Le=M.force,b=M.replace===!0,w=Q(X);if(w)return P(Me(V(w),{state:typeof w=="object"?Me({},Te,w.state):Te,force:Le,replace:b}),ae||X);const E=X;E.redirectedFrom=ae;let L;return!Le&&bu(l,ie,X)&&(L=In(16,{to:E,from:ie}),qe(ie,ie,!0,!1)),(L?Promise.resolve(L):oe(E,ie)).catch(O=>zt(O)?zt(O,2)?O:Ye(O):de(O,E,ie)).then(O=>{if(O){if(zt(O,2))return P(Me({replace:b},V(O.to),{state:typeof O.to=="object"?Me({},Te,O.to.state):Te,force:Le}),ae||E)}else O=D(E,ie,!0,b,Te);return A(E,ie,O),O})}function I(M,ae){const X=R(M,ae);return X?Promise.reject(X):Promise.resolve()}function te(M){const ae=ot.values().next().value;return ae&&typeof ae.runWithContext=="function"?ae.runWithContext(M):M()}function oe(M,ae){let X;const[ie,Te,Le]=Qu(M,ae);X=Sa(ie.reverse(),"beforeRouteLeave",M,ae);for(const w of ie)w.leaveGuards.forEach(E=>{X.push(en(E,M,ae))});const b=I.bind(null,M,ae);return X.push(b),Ue(X).then(()=>{X=[];for(const w of r.list())X.push(en(w,M,ae));return X.push(b),Ue(X)}).then(()=>{X=Sa(Te,"beforeRouteUpdate",M,ae);for(const w of Te)w.updateGuards.forEach(E=>{X.push(en(E,M,ae))});return X.push(b),Ue(X)}).then(()=>{X=[];for(const w of Le)if(w.beforeEnter)if(Pt(w.beforeEnter))for(const E of w.beforeEnter)X.push(en(E,M,ae));else X.push(en(w.beforeEnter,M,ae));return X.push(b),Ue(X)}).then(()=>(M.matched.forEach(w=>w.enterCallbacks={}),X=Sa(Le,"beforeRouteEnter",M,ae,te),X.push(b),Ue(X))).then(()=>{X=[];for(const w of i.list())X.push(en(w,M,ae));return X.push(b),Ue(X)}).catch(w=>zt(w,8)?w:Promise.reject(w))}function A(M,ae,X){c.list().forEach(ie=>te(()=>ie(M,ae,X)))}function D(M,ae,X,ie,Te){const Le=R(M,ae);if(Le)return Le;const b=ae===Yt,w=An?history.state:{};X&&(ie||b?s.replace(M.fullPath,Me({scroll:b&&w&&w.scroll},Te)):s.push(M.fullPath,Te)),d.value=M,qe(M,ae,X,b),Ye()}let j;function be(){j||(j=s.listen((M,ae,X)=>{if(!kt.listening)return;const ie=F(M),Te=Q(ie);if(Te){P(Me(Te,{replace:!0}),ie).catch(Jn);return}h=ie;const Le=d.value;An&&$u(gs(Le.fullPath,X.delta),ia()),oe(ie,Le).catch(b=>zt(b,12)?b:zt(b,2)?(P(b.to,ie).then(w=>{zt(w,20)&&!X.delta&&X.type===ro.pop&&s.go(-1,!1)}).catch(Jn),Promise.reject()):(X.delta&&s.go(-X.delta,!1),de(b,ie,Le))).then(b=>{b=b||D(ie,Le,!1),b&&(X.delta&&!zt(b,8)?s.go(-X.delta,!1):X.type===ro.pop&&zt(b,20)&&s.go(-1,!1)),A(ie,Le,b)}).catch(Jn)}))}let xe=Fn(),fe=Fn(),he;function de(M,ae,X){Ye(M);const ie=fe.list();return ie.length?ie.forEach(Te=>Te(M,ae,X)):console.error(M),Promise.reject(M)}function Be(){return he&&d.value!==Yt?Promise.resolve():new Promise((M,ae)=>{xe.add([M,ae])})}function Ye(M){return he||(he=!M,be(),xe.list().forEach(([ae,X])=>M?X(M):ae()),xe.reset()),M}function qe(M,ae,X,ie){const{scrollBehavior:Te}=t;if(!An||!Te)return Promise.resolve();const Le=!X&&Cu(gs(M.fullPath,0))||(ie||!X)&&history.state&&history.state.scroll||null;return co().then(()=>Te(M,ae,Le)).then(b=>b&&ku(b)).catch(b=>de(b,M,ae))}const je=M=>s.go(M);let it;const ot=new Set,kt={currentRoute:d,listening:!0,addRoute:g,removeRoute:x,hasRoute:W,getRoutes:B,resolve:F,options:t,push:q,replace:ee,go:je,back:()=>je(-1),forward:()=>je(1),beforeEach:r.add,beforeResolve:i.add,afterEach:c.add,onError:fe.add,isReady:Be,install(M){const ae=this;M.component("RouterLink",_e),M.component("RouterView",oi),M.config.globalProperties.$router=ae,Object.defineProperty(M.config.globalProperties,"$route",{enumerable:!0,get:()=>$(d)}),An&&!it&&d.value===Yt&&(it=!0,q(s.location).catch(Te=>{}));const X={};for(const Te in Yt)Object.defineProperty(X,Te,{get:()=>d.value[Te],enumerable:!0});M.provide(ca,ae),M.provide(yl,ar(X)),M.provide(Ga,d);const ie=M.unmount;ot.add(M),M.unmount=function(){ot.delete(M),ot.size<1&&(h=Yt,j&&j(),j=null,d.value=Yt,it=!1,he=!1),ie()}}};function Ue(M){return M.reduce((ae,X)=>ae.then(()=>te(X)),Promise.resolve())}return kt}function Qu(t,o){const a=[],l=[],s=[],r=Math.max(o.matched.length,t.matched.length);for(let i=0;i<r;i++){const c=o.matched[i];c&&(t.matched.find(h=>Rn(h,c))?l.push(c):a.push(c));const d=t.matched[i];d&&(o.matched.find(h=>Rn(h,d))||s.push(d))}return[a,l,s]}function eh(){return Je(ca)}function th(){return Je(yl)}const Go=()=>{};function nh(t,o){function a(...l){return new Promise((s,r)=>{Promise.resolve(t(()=>o.apply(null,l),{fn:o,args:l})).then(s).catch(r)})}return a}const ai=t=>t();function oh(t,o={}){let a,l,s=Go;const r=c=>{clearTimeout(c),s(),s=Go};return c=>{const d=ln(t),h=ln(o.maxWait);return a&&r(a),d<=0||h!==void 0&&h<=0?(l&&(r(l),l=null),Promise.resolve(c())):new Promise((u,p)=>{s=o.rejectOnCancel?p:u,h&&!l&&(l=setTimeout(()=>{a&&r(a),l=null,u(c())},h)),a=setTimeout(()=>{l&&r(l),l=null,u(c())},d)})}}function ah(t=ai){const o=Ce(!0);function a(){o.value=!1}function l(){o.value=!0}const s=(...r)=>{o.value&&t(...r)};return{isActive:rl(o),pause:a,resume:l,eventFilter:s}}function lh(t,o,a={}){const{debounce:l=0,maxWait:s=void 0,...r}=a;return li(t,o,{...r,eventFilter:oh(l,{maxWait:s})})}function sh(t,o,a={}){const{eventFilter:l,...s}=a,{eventFilter:r,pause:i,resume:c,isActive:d}=ah(l);return{stop:li(t,o,{...s,eventFilter:r}),pause:i,resume:c,isActive:d}}function li(t,o,a={}){const{eventFilter:l=ai,...s}=a;return $n(t,nh(l,o),s)}function At(t){return Array.isArray(t)}function Pe(t){return!si(t)}function Wa(t){return si(t)||t===""}function Wo(t,o){if(t===o)return!0;if(t===null||o===null)return!1;if(t!==t&&o!==o)return!0;if(typeof t===typeof o&&wt(t))if(At(t)){if(!At(o))return!1;const{length:s}=t;if(s===o.length){for(let r=0;r<s;r++)if(!Wo(t[r],o[r]))return!1;return!0}}else{const s={};for(const r in t)if(!Bt(t[r])){if(!Wo(t[r],o[r]))return!1;s[r]=!0}for(const r in o)if(!(r in s)&&Pe(o[r])&&!Bt(o[r]))return!1;return!0}return!1}function Bt(t){return typeof t=="function"}function rh(t){return t===null}function Ln(t){return typeof t=="number"}function wt(t){return t!==null&&typeof t=="object"}function ih(t){return Object.prototype.toString.call(t)==="[object Object]"}function Ka(t){return typeof t=="string"}function ch(t){return t===void 0}function si(t){return ch(t)||rh(t)}function dh(t){return Gs()?(Ri(t),!0):!1}function uh(t){const o=ln(t);return(o==null?void 0:o.$el)??o}function Ps(...t){let o,a,l,s;if(typeof t[0]=="string"||Array.isArray(t[0])?([a,l,s]=t,o=window):[o,a,l,s]=t,!o)return Go;Array.isArray(a)||(a=[a]),Array.isArray(l)||(l=[l]);const r=[],i=()=>{r.forEach(u=>u()),r.length=0},c=(u,p,m,g)=>(u.addEventListener(p,m,g),()=>u.removeEventListener(p,m,g)),d=$n(()=>[uh(o),ln(s)],([u,p])=>{if(i(),!u)return;const m=wt(p)?{...p}:p;r.push(...a.flatMap(g=>l.map(x=>c(u,g,x,m))))},{immediate:!0,flush:"post"}),h=()=>{d(),i()};return dh(h),h}function hh(t){return t||gl()}function ph(t,o,a=!0){const l=hh(o);l?uo(t,l):a?t():co(t)}const fh={boolean:{read:t=>t==="true",write:t=>String(t)},object:{read:t=>JSON.parse(t),write:t=>JSON.stringify(t)},number:{read:t=>Number.parseFloat(t),write:t=>String(t)},any:{read:t=>t,write:t=>String(t)},string:{read:t=>t,write:t=>String(t)},map:{read:t=>new Map(JSON.parse(t)),write:t=>JSON.stringify(Array.from(t.entries()))},set:{read:t=>new Set(JSON.parse(t)),write:t=>JSON.stringify(Array.from(t))},date:{read:t=>new Date(t),write:t=>t.toISOString()}},Ns="ui-storage";function ri(t,o,a,l={}){const{flush:s="pre",deep:r=!0,listenToStorageChanges:i=!0,writeDefaults:c=!0,mergeDefaults:d=!1,shallow:h,eventFilter:u,onError:p=()=>{},initOnMounted:m}=l,g=(h?zo:Ce)(typeof o=="function"?o():o);if(!a)try{a=window==null?void 0:window.localStorage}catch(P){p(P)}if(!a)return g;const x=ln(o),B=mh(x),W=l.serializer??fh[B],{pause:F,resume:V}=sh(g,()=>R(g.value),{flush:s,deep:r,eventFilter:u});return window&&i&&ph(()=>{Ps(window,"storage",Q),Ps(window,Ns,ee),m&&Q()}),m||Q(),g;function R(P){try{if(P==null)a.removeItem(t);else{const I=W.write(P),te=a.getItem(t);te!==I&&(a.setItem(t,I),window&&window.dispatchEvent(new CustomEvent(Ns,{detail:{key:t,oldValue:te,newValue:I,storageArea:a}})))}}catch(I){p(I)}}function q(P){const I=P?P.newValue:a.getItem(t);if(I==null)return c&&x!=null&&a.setItem(t,W.write(x)),x;if(!P&&d){const te=W.read(I);return typeof d=="function"?d(te,x):B==="object"&&!Array.isArray(te)?{...x,...te}:te}return typeof I!="string"?I:W.read(I)}function ee(P){Q(P.detail)}function Q(P){if(!(P&&P.storageArea!==a)){if(P&&P.key==null){g.value=x;return}if(!(P&&P.key!==t)){F();try{(P==null?void 0:P.newValue)!==W.write(g.value)&&(g.value=q(P))}catch(I){p(I)}finally{P?co(V):V()}}}}}function mh(t){return t==null?"any":t instanceof Set?"set":t instanceof Map?"map":t instanceof Date?"date":typeof t=="boolean"?"boolean":typeof t=="string"?"string":typeof t=="object"?"object":Number.isNaN(t)?"any":"number"}const bh=e("clipPath",{id:"theme-toggle-cutout"},[e("path",{d:"M0-11h25a1 1 0 0017 13v30H0Z"})],-1),gh=e("g",{"clip-path":"url(#theme-toggle-cutout)"},[e("circle",{cx:"16",cy:"16",r:"8.4"}),n(),e("path",{d:"M18.3 3.2c0 1.3-1 2.3-2.3 2.3s-2.3-1-2.3-2.3S14.7.9 16 .9s2.3 1 2.3 2.3zm-4.6 25.6c0-1.3 1-2.3 2.3-2.3s2.3 1 2.3 2.3-1 2.3-2.3 2.3-2.3-1-2.3-2.3zm15.1-10.5c-1.3 0-2.3-1-2.3-2.3s1-2.3 2.3-2.3 2.3 1 2.3 2.3-1 2.3-2.3 2.3zM3.2 13.7c1.3 0 2.3 1 2.3 2.3s-1 2.3-2.3 2.3S.9 17.3.9 16s1-2.3 2.3-2.3zm5.8-7C9 7.9 7.9 9 6.7 9S4.4 8 4.4 6.7s1-2.3 2.3-2.3S9 5.4 9 6.7zm16.3 21c-1.3 0-2.3-1-2.3-2.3s1-2.3 2.3-2.3 2.3 1 2.3 2.3-1 2.3-2.3 2.3zm2.4-21c0 1.3-1 2.3-2.3 2.3S23 7.9 23 6.7s1-2.3 2.3-2.3 2.4 1 2.4 2.3zM6.7 23C8 23 9 24 9 25.3s-1 2.3-2.3 2.3-2.3-1-2.3-2.3 1-2.3 2.3-2.3z"})],-1),ii={__name:"ThemeToggleIcon",props:{theme:{type:String,default:"light"}},setup(t){const o=t,a=ue(()=>o.theme==="dark");return(l,s)=>(C(),S("svg",{xmlns:"http://www.w3.org/2000/svg",width:"24",height:"24",viewBox:"0 0 32 32",fill:"currentColor",class:Fe(["theme-toggle",{moon:a.value}])},[bh,n(),gh],2))}},_h={class:"is-fixed-above-lg"},vh={class:"container"},yh=G('<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" x="0" y="0" version="1.1" viewBox="0 0 128 48" width="128"><g fill-rule="evenodd" clip-rule="evenodd"><path d="m12.211 8.3-9.063 5.234 7.88 4.555.045-.045z" fill="#710698"></path><path d="m12.211 8.3 9.075 3.828V3.065z" fill="#7944bd"></path><path d="M21.286 3.065v9.063L30.349 8.3z" fill="#034185"></path><path d="m31.487 18.044.056.045 7.869-4.555L30.349 8.3z" fill="#03b5df"></path><path d="m39.412 13.534-7.869 4.555 7.869 5.915z" fill="#4caa1c"></path><path d="M31.487 29.875v.022l7.925 4.576V24.004z" fill="#b8fb2a"></path><path d="m39.412 34.473-7.925-4.576-1.138 9.811z" fill="#f0cc26"></path><path d="m30.349 39.708-9.063-3.918v9.153z" fill="#f13e03"></path><path d="M21.286 44.943V35.79l-9.075 3.918z" fill="#a90000"></path><path d="m12.211 39.708-1.138-9.81-7.925 4.575z" fill="#ab0000"></path><path d="M11.073 29.897v-.022l-7.925-5.87v10.468z" fill="#e50380"></path><path d="m3.148 24.004 7.88-5.915-7.88-4.555z" fill="#720353"></path><path d="m11.073 18.044 5.1-2.958 5.113-2.958L12.21 8.3z" fill="#5924bd"></path><path d="M11.073 23.96v-5.85l-.045-.021-7.88 5.915 7.925 5.871z" fill="#c2037b"></path><path d="m12.211 39.708 9.075-3.918-5.112-2.957-5.079-2.947-.022.011z" fill="#c10000"></path><path d="m31.487 29.897-.011-.01-5.09 2.946-5.1 2.957 9.063 3.918z" fill="#f37b22" stroke="#fc5c06" stroke-miterlimit="10" stroke-width=".053"></path><path d="m31.543 18.089-.056.022v11.764l7.925-5.87z" fill="#8be232"></path><path d="M31.487 18.044 30.35 8.3l-9.063 3.828 5.1 2.958z" fill="#0375af"></path></g><path d="M7.389 2.605V4.02h-.383V2.689q0-.173-.084-.314-.084-.141-.225-.223-.141-.084-.314-.084-.17 0-.314.084-.141.082-.225.223-.084.141-.084.314v1.33h-.383V1.746h.383v.284q.12-.16.305-.252.184-.096.4-.096.255 0 .464.125.212.123.334.332.126.21.126.467zm2.238-.86h.382V4.02h-.382l-.016-.385q-.107.198-.296.321-.186.123-.445.123-.25 0-.47-.093-.217-.096-.386-.262-.166-.168-.26-.387-.092-.218-.092-.47 0-.244.09-.46.092-.216.253-.378.164-.164.378-.255.214-.093.46-.093.268 0 .47.127.203.126.328.326zm-.764 1.963q.225 0 .389-.11.166-.11.255-.3.09-.188.09-.416 0-.232-.09-.418-.091-.19-.257-.298-.164-.112-.387-.112-.225 0-.41.112-.184.111-.293.3-.107.187-.107.416 0 .23.111.419.112.186.296.298.184.11.403.11zm3.612-1.103V4.02h-.382V2.689q0-.173-.084-.314-.084-.141-.225-.223-.141-.084-.314-.084-.17 0-.314.084-.141.082-.225.223-.084.141-.084.314v1.33h-.383V1.746h.383v.284q.12-.16.304-.252.185-.096.4-.096.256 0 .465.125.211.123.334.332.125.21.125.467zm1.41 1.474q-.313 0-.572-.162-.257-.161-.412-.432-.153-.273-.153-.603 0-.25.09-.466.088-.218.243-.382.157-.166.364-.26.207-.093.44-.093.315 0 .572.162.26.161.412.434.154.273.154.605 0 .248-.088.464-.089.216-.246.383-.155.163-.362.257-.204.093-.441.093zm0-.382q.215 0 .385-.112.173-.114.271-.298.1-.186.1-.405 0-.223-.102-.41-.1-.186-.27-.297-.172-.112-.383-.112-.214 0-.385.114-.17.112-.27.298-.1.187-.1.407 0 .228.102.414.102.185.275.294.173.107.378.107z" aria-label="nano" transform="translate(.354 .133) scale(8.28729)"></path></svg>',1),wh={class:"container"},xh={id:"documentation-menu"},kh=G('<header><h2>Documentation</h2> <a aria-label="Close" class="secondary" href="/docs"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke-width="3" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path> <path d="M18 6l-12 12"></path> <path d="M6 6l12 12"></path></svg></a></header>',1),$h=["open"],Ch=e("summary",{"aria-current":"true"},`
            Getting Started
          `,-1),Sh=["open"],Eh=e("summary",null,"Customisation",-1),Th=["open"],Ah=e("summary",null,"Layout",-1),Mh=["open"],Ph=e("summary",null,"Content",-1),Nh=["open"],Oh=e("summary",null,"Forms",-1),Rh=["open"],Ih=e("summary",null,"Components",-1),Lh=["open"],Bh=e("summary",null,"Vue Components",-1),Dh=["open"],zh=e("summary",null,"About",-1),Vh=G(`<footer><div class="container"><div><p class="text-center">
          v0.2.0 – MIT Licence – Standing on the shoulders of <a rel="noopener noreferrer" href="https://vuecomponenets.com" target="_blank">VueComponents</a>, <a rel="noopener noreferrer" href="https://getbootstrap.com" target="_blank">Bootstrap</a> and <a rel="noopener noreferrer" href="https://github.com/picocss/pico" target="_blank">PicoCSS</a>.
        </p></div></div></footer>`,1),jh={__name:"App",setup(t){const o=ri("theme","light"),a=th();function l(i){var c;return((c=a.meta)==null?void 0:c.section)===i}function s(i){document.querySelector("html").setAttribute("data-theme",i)}function r(){o.value=o.value==="light"?"dark":"light"}return $n(o,i=>{s(i)}),uo(()=>{s(o.value??"light")}),(i,c)=>(C(),S(J,null,[e("header",_h,[e("div",vh,[yh,n(),e("nav",null,[e("ul",null,[e("li",null,[e("a",{href:"#",class:"contrast","aria-label":"Turn on dark mode",onClick:Ha(r,["prevent"])},[f(ii,{theme:$(o)},null,8,["theme"])])])])])])]),n(),e("main",wh,[e("aside",xh,[kh,n(),e("nav",null,[e("details",{open:l("getting-started")},[Ch,n(),e("ul",null,[e("li",null,[f($(_e),{class:"secondary",to:"/docs"},{default:v(()=>[n(`
                Quick Start
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/color-schemes"},{default:v(()=>[n(`
                Colour Schemes
              `)]),_:1})])])],8,$h),n(),e("details",{open:l("customisation")},[Eh,n(),e("ul",null,[e("li",null,[f($(_e),{class:"secondary",to:"/docs/css-variables"},{default:v(()=>[n(`
                CSS Variables
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/sass"},{default:v(()=>[n(`
                Sass
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/colors"},{default:v(()=>[n(`
                Colors
              `)]),_:1})])])],8,Sh),n(),e("details",{open:l("layout")},[Ah,n(),e("ul",null,[e("li",null,[f($(_e),{class:"secondary",to:"/docs/container"},{default:v(()=>[n(`
                Container
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/landmark-section"},{default:v(()=>[n(`
                Landmark & Section
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/grid"},{default:v(()=>[n(`
                Grid
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/overflow-auto"},{default:v(()=>[n(`
                Overflow Auto
              `)]),_:1})])])],8,Th),n(),e("details",{open:l("content")},[Ph,n(),e("ul",null,[e("li",null,[f($(_e),{class:"secondary",to:"/docs/typography"},{default:v(()=>[n(`
                Typography
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/link"},{default:v(()=>[n(`
                Link
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/button"},{default:v(()=>[n(`
                Button
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/table"},{default:v(()=>[n(`
                Table
              `)]),_:1})])])],8,Mh),n(),e("details",{open:l("forms")},[Oh,n(),e("ul",null,[e("li",null,[f($(_e),{class:"secondary",to:"/docs/forms/overview"},{default:v(()=>[n(`
                Overview
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/forms/input"},{default:v(()=>[n(`
                Input
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/forms/textarea"},{default:v(()=>[n(`
                Textarea
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/forms/select"},{default:v(()=>[n(`
                Select
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/forms/checkboxes"},{default:v(()=>[n(`
                Checkboxes
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/forms/radios"},{default:v(()=>[n(`
                Radios
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/forms/switch"},{default:v(()=>[n(`
                Switch
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/forms/range"},{default:v(()=>[n(`
                Range
              `)]),_:1})])])],8,Nh),n(),e("details",{open:l("components")},[Ih,n(),e("ul",null,[e("li",null,[f($(_e),{class:"secondary",to:"/docs/accordion"},{default:v(()=>[n(`
                Accordion
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/alert"},{default:v(()=>[n(`
                Alert
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/card"},{default:v(()=>[n(`
                Card
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/dropdown"},{default:v(()=>[n(`
                Dropdown
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/group"},{default:v(()=>[n(`
                Group
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/loading"},{default:v(()=>[n(`
                Loading
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/modal"},{default:v(()=>[n(`
                Modal
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/nav"},{default:v(()=>[n(`
                Nav
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/note"},{default:v(()=>[n(`
                Note
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/progress"},{default:v(()=>[n(`
                Progress
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/tabs"},{default:v(()=>[n(`
                Tabs
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/tooltip"},{default:v(()=>[n(`
                Tooltip
              `)]),_:1})])])],8,Rh),n(),e("details",{open:l("vue-components")},[Bh,n(),e("ul",null,[e("li",null,[f($(_e),{class:"secondary",to:"/docs/vue/data-table"},{default:v(()=>[n(`
                Data Table
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/vue/modal"},{default:v(()=>[n(`
                Modal
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/vue/password"},{default:v(()=>[n(`
                Password
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/vue/sparkline"},{default:v(()=>[n(`
                Sparkline
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/vue/tabs"},{default:v(()=>[n(`
                Tabs
              `)]),_:1})]),n(),e("li",null,[f($(_e),{class:"secondary",to:"/docs/vue/validation"},{default:v(()=>[n(`
                Validation
              `)]),_:1})])])],8,Lh),n(),e("details",{open:l("about")},[zh,n(),e("ul",null,[e("li",null,[f($(_e),{class:"secondary",to:"/docs/mission"},{default:v(()=>[n(`
                Mission
              `)]),_:1})])])],8,Dh)])]),n(),f($(oi))]),n(),Vh],64))}},Fh={__name:"HomeView",setup(t){return eh().push("/docs"),(a,l)=>(C(),S("span"))}},Hh=["data-tooltip"],qh={key:0,xmlns:"http://www.w3.org/2000/svg",height:"16",width:"16",viewBox:"0 0 512 512",fill:"currentColor",class:"check"},Uh=e("path",{id:"check-icon",d:"M435.8 83.5L172.8 346.5l-96.7-96.7c-4.7-4.7-12.3-4.7-17 0l-28.3 28.3c-4.7 4.7-4.7 12.3 0 17l133.4 133.4c4.7 4.7 12.3 4.7 17 0l299.8-299.8c4.7-4.7 4.7-12.3 0-17l-28.3-28.3c-4.7-4.7-12.3-4.7-17 0z"},null,-1),Gh=[Uh],Wh={key:1,xmlns:"http://www.w3.org/2000/svg",height:"16",width:"14",viewBox:"0 0 448 512"},Kh=e("path",{d:"M400 336l-224 0 0-288 156.1 0L400 115.9 400 336zM448 96L352 0 176 0 128 0l0 48 0 288 0 48 48 0 224 0 48 0 0-48 0-240zM48 128L0 128l0 48L0 464l0 48 48 0 224 0 48 0 0-48 0-48-48 0 0 48L48 464l0-288 48 0 0-48-48 0z"},null,-1),Yh=[Kh],k={__name:"CodeBlock",props:{language:{type:String,default:""},noHighlight:{type:Boolean,default:!0}},setup(t){const o=t,a=Ce(null),l=Ce(!1),s=ue(()=>o.language?`language-${o.language}`:""),r=ue(()=>l.value?"Copied!":"Copy to clipboard");function i(){navigator.clipboard.writeText(a.value.innerText).then(()=>{l.value=!0,document.activeElement.blur(),setTimeout(()=>{l.value=!1},1500)})}return(c,d)=>{const h=Sn("highlight");return C(),S(J,null,[e("a",{class:"copy-to-clipboard",tabindex:"-1","aria-label":"Copy code","data-tooltip":r.value,"data-placement":"left",onClick:d[0]||(d[0]=u=>i())},[l.value?(C(),S("svg",qh,Gh)):(C(),S("svg",Wh,Yh))],8,Hh),n(),e("pre",null,[$e((C(),S("code",{ref_key:"codeSnippet",ref:a,class:Fe(s.value)},[rt(c.$slots,"default")],2)),[[h]])])],64)}}},Jh=e("hgroup",null,[e("p",{class:"chapter"},`
      Components
    `),n(),e("h1",null,"Accordions"),n(),e("p",null,"Toggle sections of content in pure HTML, without JavaScript, using minimal and semantic markup.")],-1),Zh=e("aside",{id:"table-of-contents"},[e("nav",null,[e("details",{open:""},[e("summary",null,"Content"),n(),e("ul",null,[e("li",null,[e("a",{class:"secondary",href:"/docs/accordion","aria-current":"page"},"Overview")]),n(),e("li",null,[e("a",{class:"secondary",href:"/docs/accordion#button-variants"},"Button variants")])])])])],-1),Xh={role:"document"},Qh={"aria-label":"Accordions example",class:"component"},ep=e("details",{open:""},[e("summary",null,"Accordion 1"),n(),e("p",null,`
            Flamingos are known for their bright pink feathers and distinctive long necks. These birds are social
            creatures that live in large groups, and a group of flamingos is called a flamboyance. They can often be
            seen standing on one leg, which helps them conserve body heat.
          `)],-1),tp=e("hr",null,null,-1),np=G(`<details><summary>Accordion 2</summary> <article><ul><li>Kangaroos are marsupials that are native to Australia.</li> <li>They are known for their powerful hind legs, which they use to hop around.</li> <li>Kangaroos can&#39;t walk backwards due to the shape of their legs and tail.</li> <li>
                Baby kangaroos, called joeys, are born very small and undeveloped and must crawl into their mother&#39;s
                pouch to continue developing.
              </li> <li>Some species of kangaroos can leap up to 30 feet in a single bound.</li></ul></article></details>`,1),op={class:"code","data-theme":"dark"},ap=e("pre",null,`<details open>
  <summary>Accordion 1</summary>
  <p>...</p>
</details>

<hr />

<details>
  <summary>Accordion 2</summary>
  <article>
    <ul>
      <li>...</li>
      <li>...</li>
    </ul>
  </article>
</details>`,-1),lp=e("h2",null,[n(`
        Button variants`),e("a",{id:"button-variants",href:"#button-variants",class:"secondary",tabindex:"-1"},"#")],-1),sp=e("p",null,[e("code",null,'role="button"'),n(" can be used to turn "),e("code",null,"<summary>"),n(" into a button.")],-1),rp={"aria-label":"Accordions buttons example",class:"component"},ip=e("details",null,[e("summary",{role:"button"},`
            Button
          `),n(),e("p",null,`
            Owls are nocturnal birds of prey that are known for their distinctive hooting calls. A group of owls is
            called a parliament, and these birds are often associated with wisdom and intelligence. Owls have excellent
            hearing and vision, which helps them hunt prey in the dark.
          `)],-1),cp={class:"code","data-theme":"dark"},dp=e("pre",null,`<details>
  <summary role="button">Owls</summary>
  <p>...</p>
</details>`,-1),up=e("p",null,[n(`
        Like regular buttons, they come with `),e("code",null,".secondary"),n(", "),e("code",null,".contrast"),n(", and "),e("code",null,".outline"),n(`.
      `)],-1),hp={"aria-label":"Accordions buttons example",class:"component"},pp=e("details",null,[e("summary",{role:"button",class:"secondary"},`
            Secondary
          `),n(),e("p",null,`
            Ostriches are the largest birds in the world and are native to Africa. They have long, powerful legs that
            they use to run at high speeds, and they can reach up to 45 miles per hour. An ostrich's eye is bigger than
            its brain, which is unusual for birds.
          `)],-1),fp=e("details",null,[e("summary",{role:"button",class:"contrast"},`
            Contrast
          `),n(),e("p",null,`
            Koalas are arboreal marsupials that are native to Australia. They are known for their cute and cuddly
            appearance, but they can be quite aggressive if provoked. The fingerprints of koalas are so similar to those
            of humans that they have been mistaken for crime scene prints.
          `)],-1),mp=e("details",null,[e("summary",{role:"button",class:"outline"},`
            Primary outline
          `),n(),e("p",null,`
            Elephants are the largest land animals and highly intelligent with intricate communication systems. They use
            infrasonic sounds to talk and have long memories. They create and maintain habitats for other species, and
            can eat up to 300 pounds of vegetation per day. Their elongated incisor teeth, called tusks, serve various
            purposes, including digging and defense.
          `)],-1),bp=e("details",null,[e("summary",{role:"button",class:"outline secondary"},`
            Secondary outline
          `),n(),e("p",null,`
            Crows are intelligent birds that are known for their problem-solving abilities. A group of crows is called a
            murder, and these birds have a reputation for being mischievous and sometimes even aggressive. Despite their
            negative image in some cultures, crows are important for their role in controlling pest populations and
            maintaining ecological balance.
          `)],-1),gp=e("details",null,[e("summary",{role:"button",class:"outline contrast"},`
            Contrast outline
          `),n(),e("p",null,`
            Penguins are flightless birds with a tuxedo-like appearance. They swim well and can hold their breath for up
            to 20 minutes. Penguins are social, forming tight-knit communities, and some mate for life. They have
            adaptations to survive in cold climates, including thick feathers and a layer of fat for insulation.
          `)],-1),_p={class:"code","data-theme":"dark"},vp=e("pre",null,`<!-- Secondary -->
<details>
  <summary role="button" class="secondary">Secondary</summary>
  <p>...</p>
</details>

<!-- Contrast -->
<details>
  <summary role="button" class="contrast">Contrast</summary>
  <p>...</p>
</details>

<!-- Primary outline -->
<details>
  <summary role="button" class="outline">Primary outline</summary>
  <p>...</p>
</details>

<!-- Secondary outline -->
<details>
  <summary role="button" class="outline secondary">Secondary outline</summary>
  <p>...</p>
</details>

<!-- Contrast outline -->
<details>
  <summary role="button" class="outline contrast">Contrast outline</summary>
  <p>...</p>
</details>`,-1),yp={__name:"AccordionView",setup(t){return(o,a)=>(C(),S(J,null,[Jh,n(),Zh,n(),e("div",Xh,[e("section",null,[e("article",Qh,[ep,n(),tp,n(),np,n(),e("footer",op,[f(k,null,{default:v(()=>[ap]),_:1})])])]),n(),e("section",null,[lp,n(),sp,n(),e("article",rp,[ip,n(),e("footer",cp,[f(k,null,{default:v(()=>[dp]),_:1})])]),n(),up,n(),e("article",hp,[pp,n(),fp,n(),mp,n(),bp,n(),gp,n(),e("footer",_p,[f(k,null,{default:v(()=>[vp]),_:1})])])])])],64))}},wp=e("hgroup",null,[e("p",{class:"chapter"},`
      Components
    `),n(),e("h1",null,"Alert"),n(),e("p",null,"Provide contextual feedback messages for typical user actions with the handful of available and flexible alert messages.")],-1),xp=e("aside",{id:"table-of-contents"},[e("nav",null,[e("details",{open:""},[e("summary",null,"Content"),n(),e("ul",null,[e("li",null,[e("a",{class:"secondary",href:"/docs/card","aria-current":"page"},"Syntax")]),n(),e("li",null,[e("a",{class:"secondary",href:"/docs/card#sectioning"},"Sectioning")])])])])],-1),kp={role:"document"},$p=G("<p>Alerts are constructed using the <code>role=&quot;alert&quot;</code> attribute and can be further styled with the <code>primary</code>, <code>secondary</code>, <code>contrast</code>, <code>danger</code>, <code>info</code>, <code>success</code> and <code>warning</code> classes.</p>",1),Cp=e("div",{class:"primary",role:"alert"},[n("A simple "),e("strong",null,"primary"),n(" alert.")],-1),Sp=e("div",{class:"secondary",role:"alert"},[n("A simple "),e("strong",null,"secondary"),n(" alert.")],-1),Ep=e("div",{class:"contrast",role:"alert"},[n("A simple "),e("strong",null,"contrast"),n(" alert.")],-1),Tp=e("div",{class:"danger",role:"alert"},[n("A simple "),e("strong",null,"danger"),n(" alert.")],-1),Ap=e("div",{class:"info",role:"alert"},[n("A simple "),e("strong",null,"info"),n(" alert.")],-1),Mp=e("div",{class:"success",role:"alert"},[n("A simple "),e("strong",null,"success"),n(" alert.")],-1),Pp=e("div",{class:"warning",role:"alert"},[n("A simple "),e("strong",null,"warning"),n(" alert.")],-1),Np={class:"code","data-theme":"dark"},Op=e("pre",null,`<div class="primary" role="alert">
  A simple <strong>primary</strong> alert.
</div>
<div class="secondary" role="alert">
  A simple <strong>secondary</strong> alert.
</div>
<div class="contrast" role="alert">
  A simple <strong>contrast</strong> alert.
</div>
<div class="danger" role="alert">
  A simple <strong>danger</strong> alert.
</div>
<div class="info" role="alert">
  A simple <strong>info</strong> alert.
</div>
<div class="success" role="alert">
  A simple <strong>success</strong> alert.
</div>
<div class="warning" role="alert">
  A simple <strong>warning</strong> alert.
</div>
`,-1),Rp={__name:"AlertView",setup(t){return(o,a)=>(C(),S(J,null,[wp,n(),xp,n(),e("div",kp,[e("section",null,[$p,n(),Cp,n(),Sp,n(),Ep,n(),Tp,n(),Ap,n(),Mp,n(),Pp,n(),e("div",Np,[f(k,null,{default:v(()=>[Op]),_:1})])])])],64))}},Ip=(t,o)=>{const a=t.__vccOpts||t;for(const[l,s]of o)a[l]=s;return a},Lp={},Bp=e("hgroup",null,[e("p",{class:"chapter"},`
      Content
    `),n(),e("h1",null,"Button"),n(),e("p",null,[n(`
      Buttons are using the native `),e("code",{class:"language-xml"},"<button>"),n(` tag, without
      `),e("code",{class:"language-css"},".classes"),n(`. for the default style.
    `)])],-1),Dp=G('<aside id="table-of-contents"><nav><details open><summary>Content</summary> <ul><li><a class="secondary" href="/docs/button">Syntax</a></li> <li><a class="secondary" href="/docs/button#variants">Variants</a></li> <li><a class="secondary" href="/docs/button#form-buttons">Form buttons</a></li> <li><a class="secondary" href="/docs/button#disabled">Disabled</a></li> <li><a class="secondary" href="/docs/button#role-button">Role button</a></li> <li><a class="secondary" href="/docs/button#usage-with-group">Usage with group</a></li></ul></details></nav></aside>',1),zp={role:"document"},Vp={"aria-label":"Overview"},jp={"aria-label":"Button example",class:"component"},Fp=e("button",null,"Button",-1),Hp={class:"code small","data-theme":"dark"},qp=G('<a class="copy-to-clipboard" tabindex="-1" aria-label="Copy code" data-tooltip="Copy to clipboard" data-placement="left" href="/docs/button"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round" class="clipboard"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path> <path d="M8 8m0 2a2 2 0 0 1 2 -2h8a2 2 0 0 1 2 2v8a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2z"></path> <path d="M16 8v-2a2 2 0 0 0 -2 -2h-8a2 2 0 0 0 -2 2v8a2 2 0 0 0 2 2h2"></path></svg></a>',1),Up={class:"language-xml"},Gp=e("h2",null,[n(`
        Variants`),e("a",{id:"variants",href:"#variants",class:"secondary",tabindex:"-1"},"#")],-1),Wp=e("p",null,[n(`
        Buttons come with `),e("code",{class:"language-css"},".secondary"),n(" and "),e("code",{class:"language-css"},".contrast"),n(`
        styles.
      `)],-1),Kp={"aria-label":"Button colors example",class:"component"},Yp=e("div",{class:"grid"},[e("button",{class:"secondary"},`
            Secondary
          `),n(),e("button",{class:"contrast"},`
            Contrast
          `)],-1),Jp={class:"code","data-theme":"dark"},Zp=G('<a class="copy-to-clipboard" tabindex="-1" aria-label="Copy code" data-tooltip="Copy to clipboard" data-placement="left" href="/docs/button"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round" class="clipboard"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path> <path d="M8 8m0 2a2 2 0 0 1 2 -2h8a2 2 0 0 1 2 2v8a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2z"></path> <path d="M16 8v-2a2 2 0 0 0 -2 -2h-8a2 2 0 0 0 -2 2v8a2 2 0 0 0 2 2h2"></path></svg></a>',1),Xp=e("p",null,"They also come with a classic outline style.",-1),Qp={"aria-label":"Buttons outline example",class:"component"},ef=e("div",{class:"grid"},[e("button",{class:"outline"},`
            Primary
          `),e("button",{class:"outline secondary"},`
            Secondary
          `),e("button",{class:"outline contrast"},`
            Contrast
          `)],-1),tf={class:"code","data-theme":"dark"},nf=G('<a class="copy-to-clipboard" tabindex="-1" aria-label="Copy code" data-tooltip="Copy to clipboard" data-placement="left" href="/docs/button"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round" class="clipboard"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path> <path d="M8 8m0 2a2 2 0 0 1 2 -2h8a2 2 0 0 1 2 2v8a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2z"></path> <path d="M16 8v-2a2 2 0 0 0 -2 -2h-8a2 2 0 0 0 -2 2v8a2 2 0 0 0 2 2h2"></path></svg></a>',1),of=e("h2",null,[n(`
        Form buttons`),e("a",{id:"form-buttons",href:"#form-buttons",class:"secondary",tabindex:"-1"},"#")],-1),af=e("p",null,[e("code",null,'type="submit"'),n(" and "),e("code",null,'type="button"'),n(` inputs are also displayed as buttons. All form buttons
        are `),e("code",null,"width: 100%;"),n(` by default, to match with the other form elements.
      `)],-1),lf={"aria-label":"Input buttons example",class:"component"},sf=e("input",{type:"submit"},null,-1),rf=e("input",{type:"button",value:"Input"},null,-1),cf={class:"code","data-theme":"dark"},df=G('<a class="copy-to-clipboard" tabindex="-1" aria-label="Copy code" data-tooltip="Copy to clipboard" data-placement="left" href="/docs/button"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round" class="clipboard"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path> <path d="M8 8m0 2a2 2 0 0 1 2 -2h8a2 2 0 0 1 2 2v8a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2z"></path> <path d="M16 8v-2a2 2 0 0 0 -2 -2h-8a2 2 0 0 0 -2 2v8a2 2 0 0 0 2 2h2"></path></svg></a>',1),uf=e("p",null,"Reset inputs have the secondary style by default.",-1),hf={"aria-label":"Reset input example",class:"component"},pf=e("input",{type:"reset"},null,-1),ff={class:"code small","data-theme":"dark"},mf=G('<a class="copy-to-clipboard" tabindex="-1" aria-label="Copy code" data-tooltip="Copy to clipboard" data-placement="left" href="/docs/button"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round" class="clipboard"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path> <path d="M8 8m0 2a2 2 0 0 1 2 -2h8a2 2 0 0 1 2 2v8a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2z"></path> <path d="M16 8v-2a2 2 0 0 0 -2 -2h-8a2 2 0 0 0 -2 2v8a2 2 0 0 0 2 2h2"></path></svg></a>',1),bf=e("h2",null,[n(`
        Disabled`),e("a",{id:"disabled",href:"#disabled",class:"secondary",tabindex:"-1"},"#")],-1),gf=e("p",null,"Buttons come with a disabled style.",-1),_f={"aria-label":"Disabled buttons example",class:"component"},vf=e("div",{class:"grid"},[e("button",{disabled:""},`
            Disabled
          `),e("button",{class:"secondary",disabled:""},`
            Disabled
          `),e("button",{class:"contrast",disabled:""},`
            Disabled
          `)],-1),yf={class:"code","data-theme":"dark"},wf=G('<a class="copy-to-clipboard" tabindex="-1" aria-label="Copy code" data-tooltip="Copy to clipboard" data-placement="left" href="/docs/button"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round" class="clipboard"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path> <path d="M8 8m0 2a2 2 0 0 1 2 -2h8a2 2 0 0 1 2 2v8a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2z"></path> <path d="M16 8v-2a2 2 0 0 0 -2 -2h-8a2 2 0 0 0 -2 2v8a2 2 0 0 0 2 2h2"></path></svg></a>',1),xf=e("section",null,[e("h2",null,[n(`
        Role button`),e("a",{id:"role-button",href:"#role-button",class:"secondary",tabindex:"-1"},"#")]),n(),e("p",null,[n("Clickable elements with "),e("code",null,[e("span",{class:""},'role="button"')]),n(" are rendered as buttons.")]),n(),e("article",{"aria-label":"Role button example",class:"component"},[e("div",{role:"button",tabindex:"0"},`
          Div as button
        `),n(),e("footer",{class:"code small","data-theme":"dark"},[e("a",{class:"copy-to-clipboard",tabindex:"-1","aria-label":"Copy code","data-tooltip":"Copy to clipboard","data-placement":"left",href:"/docs/button"},[e("svg",{xmlns:"http://www.w3.org/2000/svg",width:"24",height:"24",viewBox:"0 0 24 24","stroke-width":"2",stroke:"currentColor",fill:"none","stroke-linecap":"round","stroke-linejoin":"round",class:"clipboard"},[e("path",{stroke:"none",d:"M0 0h24v24H0z",fill:"none"}),n(),e("path",{d:"M8 8m0 2a2 2 0 0 1 2 -2h8a2 2 0 0 1 2 2v8a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2z"}),n(),e("path",{d:"M16 8v-2a2 2 0 0 0 -2 -2h-8a2 2 0 0 0 -2 2v8a2 2 0 0 0 2 2h2"})])]),n(),e("pre",null,[e("code",null,`
<div role="button" tabindex="0">Div as a button</div>
          `)])])])],-1),kf=e("h2",null,[n(`
        Usage with group`),e("a",{id:"usage-with-group",href:"#usage-with-group",class:"secondary",tabindex:"-1"},"#")],-1),$f=e("p",null,[n(`
        You can use `),e("code",null,[e("span",{class:""},'role="group"')]),n(" with buttons. See "),e("a",{href:"/docs/group"},"Group"),n(`.
      `)],-1),Cf={class:"component","aria-label":"Group example"},Sf=e("div",{role:"group"},[e("button",null,"Button"),e("button",null,"Button"),e("button",null,"Button")],-1),Ef={class:"code","data-theme":"dark"},Tf=G('<a class="copy-to-clipboard" tabindex="-1" aria-label="Copy code" data-tooltip="Copy to clipboard" data-placement="left" href="/docs/button"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round" class="clipboard"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path> <path d="M8 8m0 2a2 2 0 0 1 2 -2h8a2 2 0 0 1 2 2v8a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2z"></path> <path d="M16 8v-2a2 2 0 0 0 -2 -2h-8a2 2 0 0 0 -2 2v8a2 2 0 0 0 2 2h2"></path></svg></a>',1);function Af(t,o){const a=Sn("highlight");return C(),S(J,null,[Bp,n(),Dp,n(),e("div",zp,[e("section",Vp,[e("article",jp,[Fp,n(),e("footer",Hp,[qp,n(),e("pre",null,[$e((C(),S("code",Up,[n(`
<button>Button</button>
          `)])),[[a]])])])])]),n(),e("section",null,[Gp,n(),Wp,n(),e("article",Kp,[Yp,n(),e("footer",Jp,[Zp,n(),e("pre",null,[$e((C(),S("code",null,[n(`
<button class="secondary">Secondary</button>
<button class="contrast">Contrast</button>
          `)])),[[a]])])])]),n(),Xp,n(),e("article",Qp,[ef,n(),e("footer",tf,[nf,n(),e("pre",null,[$e((C(),S("code",null,[n(`
<button class="outline">Primary</button>
<button class="outline secondary">Secondary</button>
<button class="outline contrast">Contrast</button>
          `)])),[[a]])])])])]),n(),e("section",null,[of,n(),af,n(),e("article",lf,[sf,rf,n(),e("footer",cf,[df,n(),e("pre",null,[$e((C(),S("code",null,[n(`
<input type="submit" />
<input type="button" value="Input" />
          `)])),[[a]])])])]),n(),uf,n(),e("article",hf,[pf,n(),e("footer",ff,[mf,n(),e("pre",null,[$e((C(),S("code",null,[n(`
<input type="reset" />
          `)])),[[a]])])])])]),n(),e("section",null,[bf,n(),gf,n(),e("article",_f,[vf,n(),e("footer",yf,[wf,n(),e("pre",null,[$e((C(),S("code",null,[n(`
<button disabled>Disabled</button>
<button class="secondary" disabled>Disabled</button>
<button class="contrast" disabled>Disabled</button>
          `)])),[[a]])])])])]),n(),xf,n(),e("section",null,[kf,n(),$f,n(),e("article",Cf,[Sf,n(),e("footer",Ef,[Tf,n(),e("pre",null,[$e((C(),S("code",null,[n(`
<div role="group">
  <button>Button</button>
  <button>Button</button>
  <button>Button</button>
</div>
          `)])),[[a]])])])])])])],64)}const Mf=Ip(Lp,[["render",Af]]),Pf=e("hgroup",null,[e("p",{class:"chapter"},`
      Components
    `),n(),e("h1",null,"Card"),n(),e("p",null,`
      Create flexible cards with a semantic markup that provides graceful spacings across various devices and viewports.
    `)],-1),Nf=e("aside",{id:"table-of-contents"},[e("nav",null,[e("details",{open:""},[e("summary",null,"Content"),n(),e("ul",null,[e("li",null,[e("a",{class:"secondary",href:"/docs/card","aria-current":"page"},"Syntax")]),n(),e("li",null,[e("a",{class:"secondary",href:"/docs/card#sectioning"},"Sectioning")])])])])],-1),Of={role:"document"},Rf=e("article",{"aria-label":"Card example"},`
        I'm a card!
      `,-1),If={class:"code small","data-theme":"dark"},Lf=e("pre",null,"<article>I'm a card!</article>",-1),Bf=e("p",null,[n(`
        You can use `),e("code",null,"<header>"),n(" and "),e("code",null,"<footer>"),n(" inside "),e("code",null,"<article>"),n(`.
      `)],-1),Df=e("h2",null,[n(`
        Sectioning`),e("a",{id:"sectioning",href:"#sectioning",class:"secondary",tabindex:"-1"},"#")],-1),zf=e("article",{"aria-label":"Card sectioning example"},[e("header",null,"Header"),n(`
        Body
        `),e("footer",null,"Footer")],-1),Vf={class:"code","data-theme":"dark"},jf=e("pre",null,`<article>
  <header>Header</header>
  Body
  <footer>Footer</footer>
</article>`,-1),Ff=e("p",null,[n("Optionally, you can use "),e("code",null,"<main>"),n(" to wrap the body content.")],-1),Hf={class:"code","data-theme":"dark"},qf=e("pre",null,`<article>
  <header>Header</header>
  <main>Body</main>
  <footer>Footer</footer>
</article>`,-1),Uf={__name:"CardView",setup(t){return(o,a)=>(C(),S(J,null,[Pf,n(),Nf,n(),e("div",Of,[e("section",null,[Rf,n(),e("div",If,[f(k,null,{default:v(()=>[Lf]),_:1})])]),n(),e("section",null,[Bf,n(),Df,n(),zf,n(),e("div",Vf,[f(k,null,{default:v(()=>[jf]),_:1})]),n(),Ff,n(),e("div",Hf,[f(k,null,{default:v(()=>[qf]),_:1})])])])],64))}},Gf=e("hgroup",null,[e("p",{class:"chapter"},`
      Getting started
    `),n(),e("h1",null,"Color schemes"),n(),e("p",null,"Choose from two consistent color schemes that can be automatically enabled based on users' preferences.")],-1),Wf={role:"document"},Kf={id:"theme-switcher","aria-label":"Theme switcher"},Yf=["aria-label"],Jf={"data-theme":"light","aria-label":"Forced light theme example"},Zf=e("h2",null,"Light card",-1),Xf=e("form",null,[e("fieldset",{class:"grid"},[e("input",{type:"text",name:"login",placeholder:"Login","aria-label":"Login",autoComplete:"nickname"}),e("input",{type:"password",name:"password",placeholder:"Password","aria-label":"Password",autoComplete:"current-password"}),e("button",{type:"submit","aria-label":"Example button"},`
            Login
          `)]),n(),e("fieldset",null,[e("label",null,[e("input",{type:"checkbox",role:"switch",name:"switch",checked:""}),n(" Remember me")])])],-1),Qf={class:"code","data-theme":"light"},em=e("pre",null,`<article data-theme="light">
  ...
</article>`,-1),tm={"data-theme":"dark","aria-label":"Forced dark theme example"},nm=e("h2",null,"Dark card",-1),om=e("form",null,[e("fieldset",{class:"grid"},[e("input",{type:"text",name:"login",placeholder:"Login","aria-label":"Login",autoComplete:"nickname"}),e("input",{type:"password",name:"password",placeholder:"Password","aria-label":"Password",autoComplete:"current-password"}),e("button",{type:"submit"},`
            Login
          `)]),n(),e("fieldset",null,[e("label",null,[e("input",{type:"checkbox",role:"switch",name:"switch",checked:""}),n(" Remember me")])])],-1),am={class:"code","data-theme":"dark"},lm=e("pre",null,`<article data-theme="dark">
  ...
</article>`,-1),sm={__name:"ColorSchemesView",setup(t){const o=ri("theme"),a=ue(()=>o.value==="light"?"Turn on dark mode":"Turn off dark mode");function l(){o.value=o.value==="light"?"dark":"light"}return(s,r)=>{const i=Sn("highlight");return C(),S(J,null,[Gf,n(),e("div",Wf,[e("p",null,[n(`
      The default color scheme is Light. The Dark scheme is automatically enabled if the user has dark mode enabled
      `),$e((C(),S("code",null,[n("prefers-color-scheme: dark;")])),[[i]]),n(`.
    `)]),n(),e("article",Kf,[e("button",{class:"theme-toggler contrast","aria-label":a.value,onClick:l},[f(ii,{theme:$(o)},null,8,["theme"]),n(),e("span",null,re(a.value),1)],8,Yf)]),n(),e("p",null,[n(`
      Color schemes can be forced on document level `),$e((C(),S("code",null,[n('<html data-theme="light">')])),[[i]]),n(` or on any
      HTML element `),$e((C(),S("code",null,[n('<article data-theme="dark">')])),[[i]])]),n(),e("article",Jf,[Zf,n(),Xf,n(),e("footer",Qf,[f(k,null,{default:v(()=>[em]),_:1})])]),n(),e("article",tm,[nm,n(),om,n(),e("footer",am,[f(k,null,{default:v(()=>[lm]),_:1})])])])],64)}}},rm={key:0,open:""},im={key:0},cm={key:1},da={__name:"NModal",props:{open:Boolean,notEscapable:{type:Boolean,default:!1},persistent:{type:Boolean,default:!1}},emits:["close","click:outside"],setup(t,{emit:o}){const a=document.documentElement,l=t;Cc(()=>{l.open?a.classList.add("modal-is-open"):a.classList.remove("modal-is-open")});const s=o,r=pl();function i(){s("close")}function c(){a.classList.add("modal-is-opening")}function d(){a.classList.add("modal-is-closing")}function h(){a.classList.remove("modal-is-opening")}function u(){a.classList.remove("modal-is-closing")}return(p,m)=>(C(),En(_l,{onBeforeEnter:c,onAfterEnter:h,onBeforeLeave:d,onAfterLeave:u},{default:v(()=>[t.open?(C(),S("dialog",rm,[e("article",null,[$(r).headerBody?(C(),S("header",im,[e("a",{"aria-label":"Close",class:"close",onClick:i}),n(),rt(p.$slots,"headerBody")])):He("",!0),n(),rt(p.$slots,"default"),n(),$(r).footerBody?(C(),S("footer",cm,[rt(p.$slots,"footerBody")])):He("",!0)])])):He("",!0)]),_:3}))}},dm=e("svg",{xmlns:"http://www.w3.org/2000/svg",width:"24",height:"24",viewBox:"0 0 24 24","stroke-width":"3",stroke:"currentColor",fill:"none","stroke-linecap":"round","stroke-linejoin":"round"},[e("path",{stroke:"none",d:"M0 0h24v24H0z",fill:"none"}),e("path",{d:"M18 6l-12 12"}),e("path",{d:"M6 6l12 12"})],-1),um=[dm],hm={class:"color-family"},pm={class:"color-shade"},fm={class:"grid"},mm=e("p",null,"Hexadecimal Color",-1),bm={class:"code small","data-theme":"dark"},gm=e("p",null,"RGB Color",-1),_m={class:"code small","data-theme":"dark"},vm=e("a",{class:"copy-to-clipboard",tabindex:"-1","aria-label":"Copy code","data-tooltip":"Copy to clipboard","data-placement":"left",href:"/docs/colors"},[e("svg",{xmlns:"http://www.w3.org/2000/svg",width:"24",height:"24",viewBox:"0 0 24 24","stroke-width":"2",stroke:"currentColor",fill:"none","stroke-linecap":"round","stroke-linejoin":"round",class:"clipboard"},[e("path",{stroke:"none",d:"M0 0h24v24H0z",fill:"none"}),e("path",{d:"M8 8m0 2a2 2 0 0 1 2 -2h8a2 2 0 0 1 2 2v8a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2z"}),e("path",{d:"M16 8v-2a2 2 0 0 0 -2 -2h-8a2 2 0 0 0 -2 2v8a2 2 0 0 0 2 2h2"})])],-1),ym={class:""},wm=e("p",null,"Sass Variable",-1),xm={class:"code small","data-theme":"dark"},km=e("a",{class:"copy-to-clipboard",tabindex:"-1","aria-label":"Copy code","data-tooltip":"Copy to clipboard","data-placement":"left",href:"/docs/colors"},[e("svg",{xmlns:"http://www.w3.org/2000/svg",width:"24",height:"24",viewBox:"0 0 24 24","stroke-width":"2",stroke:"currentColor",fill:"none","stroke-linecap":"round","stroke-linejoin":"round",class:"clipboard"},[e("path",{stroke:"none",d:"M0 0h24v24H0z",fill:"none"}),e("path",{d:"M8 8m0 2a2 2 0 0 1 2 -2h8a2 2 0 0 1 2 2v8a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2z"}),e("path",{d:"M16 8v-2a2 2 0 0 0 -2 -2h-8a2 2 0 0 0 -2 2v8a2 2 0 0 0 2 2h2"})])],-1),$m={class:""},Cm=e("p",null,"Color Utility Class",-1),Sm={class:"code small","data-theme":"dark"},Em=e("a",{class:"copy-to-clipboard",tabindex:"-1","aria-label":"Copy code","data-tooltip":"Copy to clipboard","data-placement":"left",href:"/docs/colors"},[e("svg",{xmlns:"http://www.w3.org/2000/svg",width:"24",height:"24",viewBox:"0 0 24 24","stroke-width":"2",stroke:"currentColor",fill:"none","stroke-linecap":"round","stroke-linejoin":"round",class:"clipboard"},[e("path",{stroke:"none",d:"M0 0h24v24H0z",fill:"none"}),e("path",{d:"M8 8m0 2a2 2 0 0 1 2 -2h8a2 2 0 0 1 2 2v8a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2z"}),e("path",{d:"M16 8v-2a2 2 0 0 0 -2 -2h-8a2 2 0 0 0 -2 2v8a2 2 0 0 0 2 2h2"})])],-1),Tm={class:""},Am=e("p",null,"Background Utility Class",-1),Mm={class:"code small","data-theme":"dark"},Pm=e("a",{class:"copy-to-clipboard",tabindex:"-1","aria-label":"Copy code","data-tooltip":"Copy to clipboard","data-placement":"left",href:"/docs/colors"},[e("svg",{xmlns:"http://www.w3.org/2000/svg",width:"24",height:"24",viewBox:"0 0 24 24","stroke-width":"2",stroke:"currentColor",fill:"none","stroke-linecap":"round","stroke-linejoin":"round",class:"clipboard"},[e("path",{stroke:"none",d:"M0 0h24v24H0z",fill:"none"}),e("path",{d:"M8 8m0 2a2 2 0 0 1 2 -2h8a2 2 0 0 1 2 2v8a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2z"}),e("path",{d:"M16 8v-2a2 2 0 0 0 -2 -2h-8a2 2 0 0 0 -2 2v8a2 2 0 0 0 2 2h2"})])],-1),Nm=e("p",null,"CSS Variable",-1),Om={class:"code small","data-theme":"dark"},Rm=e("a",{class:"copy-to-clipboard",tabindex:"-1","aria-label":"Copy code","data-tooltip":"Copy to clipboard","data-placement":"left",href:"/docs/colors"},[e("svg",{xmlns:"http://www.w3.org/2000/svg",width:"24",height:"24",viewBox:"0 0 24 24","stroke-width":"2",stroke:"currentColor",fill:"none","stroke-linecap":"round","stroke-linejoin":"round",class:"clipboard"},[e("path",{stroke:"none",d:"M0 0h24v24H0z",fill:"none"}),e("path",{d:"M8 8m0 2a2 2 0 0 1 2 -2h8a2 2 0 0 1 2 2v8a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2z"}),e("path",{d:"M16 8v-2a2 2 0 0 0 -2 -2h-8a2 2 0 0 0 -2 2v8a2 2 0 0 0 2 2h2"})])],-1),Im={__name:"ColorModal",props:{open:Boolean,family:{type:String,default:""},familyMap:{type:Object,default:null},shade:{type:String,default:""}},emits:["close"],setup(t,{emit:o}){const a=t,l=ue(()=>`nano-background-${a.family}-${a.shade}`),s=ue(()=>a.familyMap[a.shade].name),r=ue(()=>a.familyMap[a.shade].hex),i=ue(()=>a.familyMap[a.shade].rgb),c=o;function d(){c("close")}return(h,u)=>(C(),En(da,{open:t.open,class:"color"},{default:v(()=>[e("header",{class:Fe(l.value)},[e("a",{"aria-label":"Close",class:"close",onClick:u[0]||(u[0]=p=>d())},um),n(),e("hgroup",null,[e("h3",hm,re(a.family)+" "+re(a.shade),1),e("p",pm,re(s.value),1)])],2),n(),e("div",fm,[mm,n(),e("div",bm,[f(k,null,{default:v(()=>[n(re(r.value),1)]),_:1})]),n(),gm,n(),e("div",_m,[vm,n(),f(k,null,{default:v(()=>[e("span",ym,re(i.value),1)]),_:1})]),n(),wm,n(),e("div",xm,[km,n(),f(k,null,{default:v(()=>[e("span",$m,"$"+re(a.family)+"-"+re(a.shade),1)]),_:1})]),n(),Cm,e("div",Sm,[Em,n(),f(k,null,{default:v(()=>[e("span",Tm,"nano-color-"+re(a.family)+"-"+re(a.shade),1)]),_:1})]),n(),Am,n(),e("div",Mm,[Pm,n(),f(k,null,{default:v(()=>[n(re(l.value),1)]),_:1})]),n(),Nm,n(),e("div",Om,[Rm,n(),f(k,null,{default:v(()=>[n("var(--nano-color-"+re(a.family)+"-"+re(a.shade)+")",1)]),_:1})])])]),_:1},8,["open"]))}},Lm={class:"family"},Bm=["onClick"],Dm={__name:"SwatchCard",props:{family:{type:String,default:"primary"},familyMap:{type:Object,default:()=>({main:{50:"#f5f5f5",100:"#eeeeee",200:"#e0e0e0",300:"#bdbdbd",400:"#9e9e9e",500:"#757575",600:"#616161",700:"#424242",800:"#212121",900:"#212121"}})}},setup(t){const o=t,a=Ce(!1),l=Ce("main");function s(){return Object.keys(o.familyMap).filter(d=>d!=="main")}function r(d){const{family:h}=o;return d==="main"?`nano-background-${h}`:`nano-background-${h}-${d}`}function i(d){l.value=d==="main"?"500":d,a.value=!0}function c(){a.value=!1}return(d,h)=>(C(),S(J,null,[e("article",Lm,[e("header",{class:Fe(r("main")),role:"button",onClick:h[0]||(h[0]=u=>i("main"))},re(t.family),3),n(),e("main",null,[(C(!0),S(J,null,Oe(s(),u=>(C(),S("button",{key:u,class:Fe(["secondary",r(u)]),onClick:p=>i(u)},re(u),11,Bm))),128))])]),n(),f(Im,{open:a.value,family:t.family,shade:l.value,"family-map":o.familyMap,onClose:h[1]||(h[1]=u=>c())},null,8,["open","family","shade","family-map"])],64))}},ci={red:{50:{name:"Salt Pebble",hex:"#faeeeb",rgb:"rgb(250, 238, 235)"},100:{name:"Pale Shrimp",hex:"#f8dcd6",rgb:"rgb(248, 220, 214)"},150:{name:"Sandblast",hex:"#f6cabf",rgb:"rgb(246, 202, 191)"},200:{name:"Pink Mimosa",hex:"#f5b7a8",rgb:"rgb(245, 183, 168)"},250:{name:"Poppy Petal",hex:"#f5a390",rgb:"rgb(245, 163, 144)"},300:{name:"Corralize",hex:"#f38f79",rgb:"rgb(243, 143, 121)"},350:{name:"Akebono Dawn",hex:"#f17961",rgb:"rgb(241, 121, 97)"},400:{name:"Connect Red",hex:"#f06048",rgb:"rgb(240, 96, 72)"},450:{name:"Cascara",hex:"#ee402e",rgb:"rgb(238, 64, 46)"},500:{name:"Shōjōhi Red",hex:"#d93526",rgb:"rgb(217, 53, 38)"},550:{name:"Bloody Red",hex:"#c52f21",rgb:"rgb(197, 47, 33)"},600:{name:"Carnelian",hex:"#af291d",rgb:"rgb(175, 41, 29)"},650:{name:"Antique Port Wine",hex:"#9b2318",rgb:"rgb(155, 35, 24)"},700:{name:"Netherworld",hex:"#861d13",rgb:"rgb(134, 29, 19)"},750:{name:"Prune",hex:"#72170f",rgb:"rgb(114, 23, 15)"},800:{name:"Cherrywood",hex:"#5c160d",rgb:"rgb(92, 22, 13)"},850:{name:"Bean",hex:"#45150c",rgb:"rgb(69, 21, 12)"},900:{name:"Root Brew",hex:"#30130a",rgb:"rgb(48, 19, 10)"},950:{name:"Bighorn Sheep",hex:"#1c0d06",rgb:"rgb(28, 13, 6)"},main:{name:"Shōjōhi Red",hex:"#d93526",rgb:"rgb(217, 53, 38)"}},pink:{50:{name:"Pale Cherry Blossom",hex:"#fbedef",rgb:"rgb(251, 237, 239)"},100:{name:"Carousel Pink",hex:"#f9dbdf",rgb:"rgb(249, 219, 223)"},150:{name:"Rosey Afterglow",hex:"#f9c8ce",rgb:"rgb(249, 200, 206)"},200:{name:"Powder Rose",hex:"#f9b4be",rgb:"rgb(249, 180, 190)"},250:{name:"Sweet 60",hex:"#f99eae",rgb:"rgb(249, 158, 174)"},300:{name:"Posy Petal",hex:"#f8889e",rgb:"rgb(248, 136, 158)"},350:{name:"Pink Explosion",hex:"#f7708e",rgb:"rgb(247, 112, 142)"},400:{name:"Macaroon Rose",hex:"#f6547e",rgb:"rgb(246, 84, 126)"},450:{name:"Vivid Raspberry",hex:"#f42c6f",rgb:"rgb(244, 44, 111)"},500:{name:"Qing Dynasty Fire",hex:"#d92662",rgb:"rgb(217, 38, 98)"},550:{name:"Bright Rose",hex:"#c72259",rgb:"rgb(199, 34, 89)"},600:{name:"Minted Blueberry Lemonade",hex:"#b21e4f",rgb:"rgb(178, 30, 79)"},650:{name:"Berry Rossi",hex:"#9d1945",rgb:"rgb(157, 25, 69)"},700:{name:"Atlas Red",hex:"#88143b",rgb:"rgb(136, 20, 59)"},750:{name:"Deep Claret",hex:"#740f31",rgb:"rgb(116, 15, 49)"},800:{name:"Plum Cheese",hex:"#5f0e28",rgb:"rgb(95, 14, 40)"},850:{name:"What We Do in the Shadows",hex:"#4b0c1f",rgb:"rgb(75, 12, 31)"},900:{name:"Ilvaite Black",hex:"#380916",rgb:"rgb(56, 9, 22)"},950:{name:"Ponzu Brown",hex:"#25060c",rgb:"rgb(37, 6, 12)"},main:{name:"Qing Dynasty Fire",hex:"#d92662",rgb:"rgb(217, 38, 98)"}},fuchsia:{50:{name:"Lavender Blush",hex:"#fbedf4",rgb:"rgb(251, 237, 244)"},100:{name:"Cherry Flower",hex:"#f9daea",rgb:"rgb(249, 218, 234)"},150:{name:"Sugarwinkle",hex:"#f9c6e1",rgb:"rgb(249, 198, 225)"},200:{name:"Hot Aquarelle Pink",hex:"#f9b1d8",rgb:"rgb(249, 177, 216)"},250:{name:"Himalayan Balsam",hex:"#fa9acf",rgb:"rgb(250, 154, 207)"},300:{name:"Aries Hot Pink",hex:"#f983c7",rgb:"rgb(249, 131, 199)"},350:{name:"Kisses",hex:"#f869bf",rgb:"rgb(248, 105, 191)"},400:{name:"Major Magenta",hex:"#f748b7",rgb:"rgb(247, 72, 183)"},450:{name:"Magnificent Magenta",hex:"#ed2aac",rgb:"rgb(237, 42, 172)"},500:{name:"Melodramatic Magenta",hex:"#d9269d",rgb:"rgb(217, 38, 157)"},550:{name:"Loulou's Purple",hex:"#c1208b",rgb:"rgb(193, 32, 139)"},600:{name:"Fabric of Love",hex:"#ac1c7c",rgb:"rgb(172, 28, 124)"},650:{name:"Romantic Vampire",hex:"#98176d",rgb:"rgb(152, 23, 109)"},700:{name:"Cardinal Pink",hex:"#84135e",rgb:"rgb(132, 19, 94)"},750:{name:"Velvet",hex:"#700e4f",rgb:"rgb(112, 14, 79)"},800:{name:"Plum Purple",hex:"#5c0d41",rgb:"rgb(92, 13, 65)"},850:{name:"Dark Sanctuary",hex:"#480b33",rgb:"rgb(72, 11, 51)"},900:{name:"Vienna Roast",hex:"#360925",rgb:"rgb(54, 9, 37)"},950:{name:"Belladonna",hex:"#230518",rgb:"rgb(35, 5, 24)"},main:{name:"Melodramatic Magenta",hex:"#d9269d",rgb:"rgb(217, 38, 157)"}},purple:{50:{name:"Perfume Haze",hex:"#f8eef9",rgb:"rgb(248, 238, 249)"},100:{name:"Grape Taffy",hex:"#f2dcf4",rgb:"rgb(242, 220, 244)"},150:{name:"Sugar High",hex:"#edc9f1",rgb:"rgb(237, 201, 241)"},200:{name:"Lovecloud",hex:"#e7b6ee",rgb:"rgb(231, 182, 238)"},250:{name:"Herb Robert",hex:"#e2a3eb",rgb:"rgb(226, 163, 235)"},300:{name:"Bright Lilac",hex:"#db90e8",rgb:"rgb(219, 144, 232)"},350:{name:"Lavender Perceptions",hex:"#d47de4",rgb:"rgb(212, 125, 228)"},400:{name:"Sensual Fumes",hex:"#cd68e0",rgb:"rgb(205, 104, 224)"},450:{name:"Thick Pink",hex:"#c652dc",rgb:"rgb(198, 82, 220)"},500:{name:"Ultraviolet Berl",hex:"#b645cd",rgb:"rgb(182, 69, 205)"},550:{name:"Fuchsia Pheromone",hex:"#aa40bf",rgb:"rgb(170, 64, 191)"},600:{name:"Purple Ink",hex:"#9236a4",rgb:"rgb(146, 54, 164)"},650:{name:"Druchii Violet",hex:"#802e90",rgb:"rgb(128, 46, 144)"},700:{name:"Berry Blackmail",hex:"#6f277d",rgb:"rgb(111, 39, 125)"},750:{name:"Violethargic",hex:"#5e206b",rgb:"rgb(94, 32, 107)"},800:{name:"Shani Purple",hex:"#4d1a57",rgb:"rgb(77, 26, 87)"},850:{name:"Velvet Cosmos",hex:"#3d1545",rgb:"rgb(61, 21, 69)"},900:{name:"Synallactida",hex:"#2d0f33",rgb:"rgb(45, 15, 51)"},950:{name:"Meški Black",hex:"#1e0820",rgb:"rgb(30, 8, 32)"},main:{name:"Purple Ink",hex:"#9236a4",rgb:"rgb(146, 54, 164)"}},violet:{50:{name:"Greek Goddess",hex:"#f3eff7",rgb:"rgb(243, 239, 247)"},100:{name:"Lingering Lilac",hex:"#e8dff2",rgb:"rgb(232, 223, 242)"},150:{name:"Plum's the Word",hex:"#decfed",rgb:"rgb(222, 207, 237)"},200:{name:"York Plum",hex:"#d3bfe8",rgb:"rgb(211, 191, 232)"},250:{name:"Perfume",hex:"#c9afe4",rgb:"rgb(201, 175, 228)"},300:{name:"Enchanted Lavender",hex:"#bd9fdf",rgb:"rgb(189, 159, 223)"},350:{name:"Middy's Purple",hex:"#b290d9",rgb:"rgb(178, 144, 217)"},400:{name:"Lavender Blossom",hex:"#a780d4",rgb:"rgb(167, 128, 212)"},450:{name:"Iridescent Purple",hex:"#9b71cf",rgb:"rgb(155, 113, 207)"},500:{name:"Amethyst",hex:"#9062ca",rgb:"rgb(144, 98, 202)"},550:{name:"Fuchsia Blue",hex:"#8352c5",rgb:"rgb(131, 82, 197)"},600:{name:"Purple Rain",hex:"#7540bf",rgb:"rgb(117, 64, 191)"},650:{name:"Purple Emperor",hex:"#6935b3",rgb:"rgb(105, 53, 179)"},700:{name:"Royalty",hex:"#5b2d9c",rgb:"rgb(91, 45, 156)"},750:{name:"Spanish Violet",hex:"#4d2585",rgb:"rgb(77, 37, 133)"},800:{name:"Explorer of the Galaxies",hex:"#3f1e6d",rgb:"rgb(63, 30, 109)"},850:{name:"Fabric of Space",hex:"#321856",rgb:"rgb(50, 24, 86)"},900:{name:"Shadow Planet",hex:"#251140",rgb:"rgb(37, 17, 64)"},950:{name:"Space Grey",hex:"#190928",rgb:"rgb(25, 9, 40)"},main:{name:"Purple Rain",hex:"#7540bf",rgb:"rgb(117, 64, 191)"}},indigo:{50:{name:"Sugar Crystal",hex:"#f2f0f9",rgb:"rgb(242, 240, 249)"},100:{name:"Lingering Lilac",hex:"#e5e0f4",rgb:"rgb(229, 224, 244)"},150:{name:"Japan Blush",hex:"#d8d0f1",rgb:"rgb(216, 208, 241)"},200:{name:"Violet Gems",hex:"#cac1ee",rgb:"rgb(202, 193, 238)"},250:{name:"Purple Sand",hex:"#bdb2ec",rgb:"rgb(189, 178, 236)"},300:{name:"Dull Lavender",hex:"#b0a3e8",rgb:"rgb(176, 163, 232)"},350:{name:"Dull Lavender",hex:"#a294e5",rgb:"rgb(162, 148, 229)"},400:{name:"Perrywinkle",hex:"#9486e1",rgb:"rgb(148, 134, 225)"},450:{name:"Kickstart Purple",hex:"#8577dd",rgb:"rgb(133, 119, 221)"},500:{name:"C64 Purple",hex:"#7569da",rgb:"rgb(117, 105, 218)"},550:{name:"Dark Periwinkle",hex:"#655cd6",rgb:"rgb(101, 92, 214)"},600:{name:"Iris",hex:"#524ed2",rgb:"rgb(82, 78, 210)"},650:{name:"Early Spring Night",hex:"#4040bf",rgb:"rgb(64, 64, 191)"},700:{name:"Purple Cabbage",hex:"#3838ab",rgb:"rgb(56, 56, 171)"},750:{name:"Royal Night",hex:"#2f2f92",rgb:"rgb(47, 47, 146)"},800:{name:"Snorlax",hex:"#272678",rgb:"rgb(39, 38, 120)"},850:{name:"Intergalactic Cowboy",hex:"#1f1e5e",rgb:"rgb(31, 30, 94)"},900:{name:"Tetsu-Kon Blue",hex:"#181546",rgb:"rgb(24, 21, 70)"},950:{name:"Spanish Roast",hex:"#110b31",rgb:"rgb(17, 11, 49)"},main:{name:"Iris",hex:"#524ed2",rgb:"rgb(82, 78, 210)"}},blue:{50:{name:"Silver Phoenix",hex:"#f0f0fb",rgb:"rgb(240, 240, 251)"},100:{name:"Lavender Mist",hex:"#e0e1fa",rgb:"rgb(224, 225, 250)"},150:{name:"Miami White",hex:"#d0d2fa",rgb:"rgb(208, 210, 250)"},200:{name:"Light Periwinkle",hex:"#bfc3fa",rgb:"rgb(191, 195, 250)"},250:{name:"Greyish Lavender",hex:"#aeb5fb",rgb:"rgb(174, 181, 251)"},300:{name:"Widowmaker",hex:"#9ca7fa",rgb:"rgb(156, 167, 250)"},350:{name:"Periwinkle Blue",hex:"#8999f9",rgb:"rgb(137, 153, 249)"},400:{name:"Punch Out Glove",hex:"#748bf8",rgb:"rgb(116, 139, 248)"},450:{name:"C64 NTSC",hex:"#5c7ef8",rgb:"rgb(92, 126, 248)"},500:{name:"Hailey Blue",hex:"#3c71f7",rgb:"rgb(60, 113, 247)"},550:{name:"Old Gungeon Red",hex:"#2060df",rgb:"rgb(32, 96, 223)"},600:{name:"Bnei Brak Bay",hex:"#1d59d0",rgb:"rgb(29, 89, 208)"},650:{name:"Mourning Blue",hex:"#184eb8",rgb:"rgb(24, 78, 184)"},700:{name:"Space Explorer",hex:"#1343a0",rgb:"rgb(19, 67, 160)"},750:{name:"Magnus Blue",hex:"#0f3888",rgb:"rgb(15, 56, 136)"},800:{name:"Old Glory Blue",hex:"#0f2d70",rgb:"rgb(15, 45, 112)"},850:{name:"Fibonacci Blue",hex:"#0e2358",rgb:"rgb(14, 35, 88)"},900:{name:"Navy",hex:"#0c1a41",rgb:"rgb(12, 26, 65)"},950:{name:"Mysterious Depths",hex:"#080f2d",rgb:"rgb(8, 15, 45)"},main:{name:"Hailey Blue",hex:"#3c71f7",rgb:"rgb(60, 113, 247)"}},azure:{50:{name:"First Snow",hex:"#e9f2fc",rgb:"rgb(233, 242, 252)"},100:{name:"Veiling Waterfalls",hex:"#d1e5fb",rgb:"rgb(209, 229, 251)"},150:{name:"Broad Daylight",hex:"#b7d9fc",rgb:"rgb(183, 217, 252)"},200:{name:"Apocyan",hex:"#9bccfd",rgb:"rgb(155, 204, 253)"},250:{name:"Broken Blue",hex:"#79c0ff",rgb:"rgb(121, 192, 255)"},300:{name:"Tiān Lán Sky",hex:"#51b4ff",rgb:"rgb(81, 180, 255)"},350:{name:"Protoss Pylon",hex:"#01aaff",rgb:"rgb(1, 170, 255)"},400:{name:"Pervenche",hex:"#029ae8",rgb:"rgb(2, 154, 232)"},450:{name:"Zürich Blue",hex:"#018cd4",rgb:"rgb(1, 140, 212)"},500:{name:"Honolulu Blue",hex:"#017fc0",rgb:"rgb(1, 127, 192)"},550:{name:"Cloisonné",hex:"#0172ad",rgb:"rgb(1, 114, 173)"},600:{name:"Wavy Navy",hex:"#02659a",rgb:"rgb(2, 101, 154)"},650:{name:"Baby Shoes",hex:"#015887",rgb:"rgb(1, 88, 135)"},700:{name:"Blue Meridian",hex:"#014c75",rgb:"rgb(1, 76, 117)"},750:{name:"Lush Aqua",hex:"#014063",rgb:"rgb(1, 64, 99)"},800:{name:"Dark Denim Blue",hex:"#033452",rgb:"rgb(3, 52, 82)"},850:{name:"Nato Blue",hex:"#052940",rgb:"rgb(5, 41, 64)"},900:{name:"The Count's Black",hex:"#061e2f",rgb:"rgb(6, 30, 47)"},950:{name:"Kuretake Black Manga",hex:"#04121d",rgb:"rgb(4, 18, 29)"},main:{name:"Honolulu Blue",hex:"#017fc0",rgb:"rgb(1, 127, 192)"}},cyan:{50:{name:"VIC 20 Blue",hex:"#c3fcfa",rgb:"rgb(195, 252, 250)"},100:{name:"CGA Blue",hex:"#58faf9",rgb:"rgb(88, 250, 249)"},150:{name:"Phosphorescent Blue",hex:"#3deceb",rgb:"rgb(61, 236, 235)"},200:{name:"Fish Boy",hex:"#25dddd",rgb:"rgb(37, 221, 221)"},250:{name:"Tilla Kari Mosque",hex:"#0ccece",rgb:"rgb(12, 206, 206)"},300:{name:"Cala Benirrás Blue",hex:"#0ac2c2",rgb:"rgb(10, 194, 194)"},350:{name:"Swagger",hex:"#0ab1b1",rgb:"rgb(10, 177, 177)"},400:{name:"Pleasant Stream",hex:"#05a2a2",rgb:"rgb(5, 162, 162)"},450:{name:"Paradise Landscape",hex:"#059494",rgb:"rgb(5, 148, 148)"},500:{name:"Green Moblin",hex:"#058686",rgb:"rgb(5, 134, 134)"},550:{name:"Prismarine",hex:"#047878",rgb:"rgb(4, 120, 120)"},600:{name:"Mayan Blue",hex:"#046a6a",rgb:"rgb(4, 106, 106)"},650:{name:"Dark Turquoise",hex:"#025d5d",rgb:"rgb(2, 93, 93)"},700:{name:"Wool Turquoise",hex:"#015050",rgb:"rgb(1, 80, 80)"},750:{name:"Warm Black",hex:"#014343",rgb:"rgb(1, 67, 67)"},800:{name:"Shrub Green",hex:"#043737",rgb:"rgb(4, 55, 55)"},850:{name:"Roof Tile Green",hex:"#052b2b",rgb:"rgb(5, 43, 43)"},900:{name:"Stellar Explorer",hex:"#051f1f",rgb:"rgb(5, 31, 31)"},950:{name:"Black Glaze",hex:"#041413",rgb:"rgb(4, 20, 19)"},main:{name:"Green Moblin",hex:"#058686",rgb:"rgb(5, 134, 134)"}},jade:{50:{name:"Out of Blue",hex:"#cbfce1",rgb:"rgb(203, 252, 225)"},100:{name:"Ineffable Green",hex:"#70fcba",rgb:"rgb(112, 252, 186)"},150:{name:"Hyperpop Green",hex:"#39f1a6",rgb:"rgb(57, 241, 166)"},200:{name:"Aqua Green",hex:"#21e299",rgb:"rgb(33, 226, 153)"},250:{name:"Retro Lime",hex:"#00cc88",rgb:"rgb(0, 204, 136)"},300:{name:"Píng Gǔo Lǜ Green",hex:"#00c482",rgb:"rgb(0, 196, 130)"},350:{name:"Green Teal",hex:"#00b478",rgb:"rgb(0, 180, 120)"},400:{name:"Sesame Street Green",hex:"#00a66e",rgb:"rgb(0, 166, 110)"},450:{name:"Green Cyan",hex:"#029764",rgb:"rgb(2, 151, 100)"},500:{name:"Vital Green",hex:"#00895a",rgb:"rgb(0, 137, 90)"},550:{name:"Poplar Forest",hex:"#007a50",rgb:"rgb(0, 122, 80)"},600:{name:"Clover Green",hex:"#006d46",rgb:"rgb(0, 109, 70)"},650:{name:"Aotake Bamboo",hex:"#005f3d",rgb:"rgb(0, 95, 61)"},700:{name:"Permanent Green",hex:"#015234",rgb:"rgb(1, 82, 52)"},750:{name:"Alaskan Moss",hex:"#00452b",rgb:"rgb(0, 69, 43)"},800:{name:"Off-Road Green",hex:"#033823",rgb:"rgb(3, 56, 35)"},850:{name:"Bush",hex:"#042c1b",rgb:"rgb(4, 44, 27)"},900:{name:"Clock Chimes Thirteen",hex:"#052014",rgb:"rgb(5, 32, 20)"},950:{name:"Jedi Night",hex:"#04140c",rgb:"rgb(4, 20, 12)"},main:{name:"Vital Green",hex:"#00895a",rgb:"rgb(0, 137, 90)"}},green:{50:{name:"Herbal Vapors",hex:"#d7fbc1",rgb:"rgb(215, 251, 193)"},100:{name:"Stadium Lawn",hex:"#95fb62",rgb:"rgb(149, 251, 98)"},150:{name:"Lush Greenery",hex:"#77ef3d",rgb:"rgb(119, 239, 61)"},200:{name:"Green Apple",hex:"#62d926",rgb:"rgb(98, 217, 38)"},250:{name:"Verminal",hex:"#5dd121",rgb:"rgb(93, 209, 33)"},300:{name:"Frog",hex:"#55c21e",rgb:"rgb(85, 194, 30)"},350:{name:"Luigi",hex:"#4eb31b",rgb:"rgb(78, 179, 27)"},400:{name:"Lawn Green",hex:"#47a417",rgb:"rgb(71, 164, 23)"},450:{name:"Planter",hex:"#409614",rgb:"rgb(64, 150, 20)"},500:{name:"Chlorophyll",hex:"#398712",rgb:"rgb(57, 135, 18)"},550:{name:"Odd Pea Pod",hex:"#33790f",rgb:"rgb(51, 121, 15)"},600:{name:"Tatzelwurm Green",hex:"#2c6c0c",rgb:"rgb(44, 108, 12)"},650:{name:"Lincoln Green",hex:"#265e09",rgb:"rgb(38, 94, 9)"},700:{name:"Patch of Land",hex:"#205107",rgb:"rgb(32, 81, 7)"},750:{name:"Forest Green",hex:"#1a4405",rgb:"rgb(26, 68, 5)"},800:{name:"Elite Green",hex:"#173806",rgb:"rgb(23, 56, 6)"},850:{name:"Lindworm Green",hex:"#152b07",rgb:"rgb(21, 43, 7)"},900:{name:"Midnight Pines",hex:"#131f07",rgb:"rgb(19, 31, 7)"},950:{name:"Blackn't",hex:"#0b1305",rgb:"rgb(11, 19, 5)"},main:{name:"Chlorophyll",hex:"#398712",rgb:"rgb(57, 135, 18)"}},lime:{50:{name:"Mystic Green",hex:"#defc85",rgb:"rgb(222, 252, 133)"},100:{name:"Yellowy Green",hex:"#c1f335",rgb:"rgb(193, 243, 53)"},150:{name:"Akhdhar Green",hex:"#b2e51a",rgb:"rgb(178, 229, 26)"},200:{name:"Vivid Lime Green",hex:"#a5d601",rgb:"rgb(165, 214, 1)"},250:{name:"Bergamot",hex:"#99c801",rgb:"rgb(153, 200, 1)"},300:{name:"Seasoned Apple Green",hex:"#8eb901",rgb:"rgb(142, 185, 1)"},350:{name:"UFO Defense Green",hex:"#82ab00",rgb:"rgb(130, 171, 0)"},400:{name:"Green Thumb",hex:"#779c00",rgb:"rgb(119, 156, 0)"},450:{name:"Canopy",hex:"#6c8f00",rgb:"rgb(108, 143, 0)"},500:{name:"Jungle Vibes",hex:"#628100",rgb:"rgb(98, 129, 0)"},550:{name:"Over the Hills",hex:"#577400",rgb:"rgb(87, 116, 0)"},600:{name:"Serrano Pepper",hex:"#4d6600",rgb:"rgb(77, 102, 0)"},650:{name:"Lustrian Undergrowth",hex:"#435a00",rgb:"rgb(67, 90, 0)"},700:{name:"Godzilla",hex:"#394d00",rgb:"rgb(57, 77, 0)"},750:{name:"Garden Shadow",hex:"#304100",rgb:"rgb(48, 65, 0)"},800:{name:"Blind Forest",hex:"#273500",rgb:"rgb(39, 53, 0)"},850:{name:"Lindworm Green",hex:"#202902",rgb:"rgb(32, 41, 2)"},900:{name:"Black Mesa",hex:"#191d03",rgb:"rgb(25, 29, 3)"},950:{name:"Chinese Black",hex:"#101203",rgb:"rgb(16, 18, 3)"},main:{name:"Vivid Lime Green",hex:"#a5d601",rgb:"rgb(165, 214, 1)"}},yellow:{50:{name:"Swiss Cheese",hex:"#fdf1b4",rgb:"rgb(253, 241, 180)"},100:{name:"Xanthic",hex:"#f2df0d",rgb:"rgb(242, 223, 13)"},150:{name:"Holy Grail",hex:"#e8d600",rgb:"rgb(232, 214, 0)"},200:{name:"Ryoku-Ou-Shoku Yellow",hex:"#d9c800",rgb:"rgb(217, 200, 0)"},250:{name:"Olive Ocher",hex:"#caba01",rgb:"rgb(202, 186, 1)"},300:{name:"Muddy Yellow",hex:"#bbac00",rgb:"rgb(187, 172, 0)"},350:{name:"Nuclear Fallout",hex:"#ad9f00",rgb:"rgb(173, 159, 0)"},400:{name:"Smoothie Green",hex:"#9e9200",rgb:"rgb(158, 146, 0)"},450:{name:"Thicket Green",hex:"#908501",rgb:"rgb(144, 133, 1)"},500:{name:"Soft Fig",hex:"#827800",rgb:"rgb(130, 120, 0)"},550:{name:"Brown Green",hex:"#756b00",rgb:"rgb(117, 107, 0)"},600:{name:"Green Brown",hex:"#685f00",rgb:"rgb(104, 95, 0)"},650:{name:"Scorzonera Brown",hex:"#5b5300",rgb:"rgb(91, 83, 0)"},700:{name:"Scorzonera Brown",hex:"#4e4700",rgb:"rgb(78, 71, 0)"},750:{name:"Storm",hex:"#423c00",rgb:"rgb(66, 60, 0)"},800:{name:"Wasabi Nori",hex:"#363100",rgb:"rgb(54, 49, 0)"},850:{name:"Swamp of Sorrows",hex:"#2b2600",rgb:"rgb(43, 38, 0)"},900:{name:"Black Mesa",hex:"#1f1c02",rgb:"rgb(31, 28, 2)"},950:{name:"Darkness",hex:"#141103",rgb:"rgb(20, 17, 3)"},main:{name:"Xanthic",hex:"#f2df0d",rgb:"rgb(242, 223, 13)"}},amber:{50:{name:"Desert Powder",hex:"#fcefd9",rgb:"rgb(252, 239, 217)"},100:{name:"Bachimitsu Gold",hex:"#fddea6",rgb:"rgb(253, 222, 166)"},150:{name:"Qing Yellow",hex:"#fecc63",rgb:"rgb(254, 204, 99)"},200:{name:"Amber",hex:"#ffbf00",rgb:"rgb(255, 191, 0)"},250:{name:"Sun Drops",hex:"#e8ae01",rgb:"rgb(232, 174, 1)"},300:{name:"Golden Beryl Yellow",hex:"#d8a100",rgb:"rgb(216, 161, 0)"},350:{name:"Retro Vibe",hex:"#c79400",rgb:"rgb(199, 148, 0)"},400:{name:"Kenyan Sand",hex:"#b78800",rgb:"rgb(183, 136, 0)"},450:{name:"Hornet Yellow",hex:"#a77c00",rgb:"rgb(167, 124, 0)"},500:{name:"Sandy Taupe",hex:"#977000",rgb:"rgb(151, 112, 0)"},550:{name:"Alligator",hex:"#876400",rgb:"rgb(135, 100, 0)"},600:{name:"Blackfire Earth",hex:"#785800",rgb:"rgb(120, 88, 0)"},650:{name:"Pickled Grape Leaves",hex:"#694d00",rgb:"rgb(105, 77, 0)"},700:{name:"Mud Brown",hex:"#5b4200",rgb:"rgb(91, 66, 0)"},750:{name:"Night in the Woods",hex:"#4d3700",rgb:"rgb(77, 55, 0)"},800:{name:"Night in the Woods",hex:"#3f2d00",rgb:"rgb(63, 45, 0)"},850:{name:"Black Swan",hex:"#312302",rgb:"rgb(49, 35, 2)"},900:{name:"Cannon Black",hex:"#231a03",rgb:"rgb(35, 26, 3)"},950:{name:"Soulless",hex:"#161003",rgb:"rgb(22, 16, 3)"},main:{name:"Amber",hex:"#ffbf00",rgb:"rgb(255, 191, 0)"}},pumpkin:{50:{name:"Natural White",hex:"#fceee3",rgb:"rgb(252, 238, 227)"},100:{name:"Karry",hex:"#fcdcc1",rgb:"rgb(252, 220, 193)"},150:{name:"Orpiment Yellow",hex:"#fcca9b",rgb:"rgb(252, 202, 155)"},200:{name:"Durazno Maduro",hex:"#feb670",rgb:"rgb(254, 182, 112)"},250:{name:"Blazing Bonfire",hex:"#ffa23a",rgb:"rgb(255, 162, 58)"},300:{name:"Barcelona Orange",hex:"#ff9500",rgb:"rgb(255, 149, 0)"},350:{name:"Fulvous",hex:"#e48500",rgb:"rgb(228, 133, 0)"},400:{name:"Autumn Leaf Orange",hex:"#d27a01",rgb:"rgb(210, 122, 1)"},450:{name:"Indocile Tiger",hex:"#bf6e00",rgb:"rgb(191, 110, 0)"},500:{name:"Ginger",hex:"#ad6400",rgb:"rgb(173, 100, 0)"},550:{name:"Charlie Brown",hex:"#9c5900",rgb:"rgb(156, 89, 0)"},600:{name:"Plain Old Brown",hex:"#8b4f00",rgb:"rgb(139, 79, 0)"},650:{name:"Dusky Brown",hex:"#7a4400",rgb:"rgb(122, 68, 0)"},700:{name:"Bestial Brown",hex:"#693a00",rgb:"rgb(105, 58, 0)"},750:{name:"Eyelash Camel",hex:"#593100",rgb:"rgb(89, 49, 0)"},800:{name:"Stirland Mud",hex:"#482802",rgb:"rgb(72, 40, 2)"},850:{name:"Powdered Cocoa",hex:"#372004",rgb:"rgb(55, 32, 4)"},900:{name:"Cannon Black",hex:"#271805",rgb:"rgb(39, 24, 5)"},950:{name:"Crowshead",hex:"#180f04",rgb:"rgb(24, 15, 4)"},main:{name:"Barcelona Orange",hex:"#ff9500",rgb:"rgb(255, 149, 0)"}},orange:{50:{name:"Rose White",hex:"#faeeea",rgb:"rgb(250, 238, 234)"},100:{name:"Peony Prize",hex:"#f9dcd2",rgb:"rgb(249, 220, 210)"},150:{name:"Delicate Sweet Apricot",hex:"#f8cab9",rgb:"rgb(248, 202, 185)"},200:{name:"Pallid Orange",hex:"#f8b79f",rgb:"rgb(248, 183, 159)"},250:{name:"Creamy Peach",hex:"#f8a283",rgb:"rgb(248, 162, 131)"},300:{name:"Melon Red",hex:"#f68e68",rgb:"rgb(246, 142, 104)"},350:{name:"Chinese Orange",hex:"#f56b3d",rgb:"rgb(245, 107, 61)"},400:{name:"Raw Sunset",hex:"#f45d2c",rgb:"rgb(244, 93, 44)"},450:{name:"Nasturtium Flower",hex:"#e74b1a",rgb:"rgb(231, 75, 26)"},500:{name:"Fire Bolt",hex:"#d24317",rgb:"rgb(210, 67, 23)"},550:{name:"Devil’s Butterfly",hex:"#bd3c13",rgb:"rgb(189, 60, 19)"},600:{name:"Shakshuka",hex:"#a83410",rgb:"rgb(168, 52, 16)"},650:{name:"Dark Umber",hex:"#942d0d",rgb:"rgb(148, 45, 13)"},700:{name:"Mahogany Brown",hex:"#7f270b",rgb:"rgb(127, 39, 11)"},750:{name:"Moussaka",hex:"#6b220a",rgb:"rgb(107, 34, 10)"},800:{name:"Peaty Brown",hex:"#561e0a",rgb:"rgb(86, 30, 10)"},850:{name:"Chocolate Brown",hex:"#411a0a",rgb:"rgb(65, 26, 10)"},900:{name:"Darkout",hex:"#2d1509",rgb:"rgb(45, 21, 9)"},950:{name:"Bighorn Sheep",hex:"#1b0d06",rgb:"rgb(27, 13, 6)"},main:{name:"Fire Bolt",hex:"#d24317",rgb:"rgb(210, 67, 23)"}},sand:{50:{name:"White Shoulders",hex:"#f2f0ec",rgb:"rgb(242, 240, 236)"},100:{name:"Bud's Sails",hex:"#e8e2d2",rgb:"rgb(232, 226, 210)"},150:{name:"Stonewashed",hex:"#dad4c2",rgb:"rgb(218, 212, 194)"},200:{name:"Sapphire Light Yellow",hex:"#ccc6b4",rgb:"rgb(204, 198, 180)"},250:{name:"Historical Ruins",hex:"#beb8a7",rgb:"rgb(190, 184, 167)"},300:{name:"Chatroom",hex:"#b0ab9b",rgb:"rgb(176, 171, 155)"},350:{name:"Wooster Smoke",hex:"#a39e8f",rgb:"rgb(163, 158, 143)"},400:{name:"Stony Creek",hex:"#959082",rgb:"rgb(149, 144, 130)"},450:{name:"Roller Coaster",hex:"#888377",rgb:"rgb(136, 131, 119)"},500:{name:"Slippery Shale",hex:"#7b776b",rgb:"rgb(123, 119, 107)"},550:{name:"Secret Passageway",hex:"#6e6a60",rgb:"rgb(110, 106, 96)"},600:{name:"Office Grey",hex:"#615e55",rgb:"rgb(97, 94, 85)"},650:{name:"Masala",hex:"#55524a",rgb:"rgb(85, 82, 74)"},700:{name:"Whiskey and Wine",hex:"#49463f",rgb:"rgb(73, 70, 63)"},750:{name:"Black Olive",hex:"#3d3b35",rgb:"rgb(61, 59, 53)"},800:{name:"Dryad Bark",hex:"#32302b",rgb:"rgb(50, 48, 43)"},850:{name:"Petroleum",hex:"#272622",rgb:"rgb(39, 38, 34)"},900:{name:"Siyâh Black",hex:"#1c1b19",rgb:"rgb(28, 27, 25)"},950:{name:"Dreamless Sleep",hex:"#111110",rgb:"rgb(17, 17, 16)"},main:{name:"Sapphire Light Yellow",hex:"#ccc6b4",rgb:"rgb(204, 198, 180)"}},grey:{50:{name:"Snowflake",hex:"#f1f1f1",rgb:"rgb(241, 241, 241)"},100:{name:"Titanium White",hex:"#e2e2e2",rgb:"rgb(226, 226, 226)"},150:{name:"Disco Ball",hex:"#d4d4d4",rgb:"rgb(212, 212, 212)"},200:{name:"Silver Polish",hex:"#c6c6c6",rgb:"rgb(198, 198, 198)"},250:{name:"Baby Talk Grey",hex:"#b9b9b9",rgb:"rgb(185, 185, 185)"},300:{name:"Dhūsar Grey",hex:"#ababab",rgb:"rgb(171, 171, 171)"},350:{name:"Cold Grey",hex:"#9e9e9e",rgb:"rgb(158, 158, 158)"},400:{name:"Tin",hex:"#919191",rgb:"rgb(145, 145, 145)"},450:{name:"Grey",hex:"#808080",rgb:"rgb(128, 128, 128)"},500:{name:"Lucky Grey",hex:"#777777",rgb:"rgb(119, 119, 119)"},550:{name:"Dim Grey",hex:"#6a6a6a",rgb:"rgb(106, 106, 106)"},600:{name:"Iron",hex:"#5e5e5e",rgb:"rgb(94, 94, 94)"},650:{name:"Cavernous",hex:"#525252",rgb:"rgb(82, 82, 82)"},700:{name:"Wood Charcoal",hex:"#474747",rgb:"rgb(71, 71, 71)"},750:{name:"Black Liquorice",hex:"#3b3b3b",rgb:"rgb(59, 59, 59)"},800:{name:"Off Black",hex:"#303030",rgb:"rgb(48, 48, 48)"},850:{name:"Nero",hex:"#262626",rgb:"rgb(38, 38, 38)"},900:{name:"Eerie Black",hex:"#1b1b1b",rgb:"rgb(27, 27, 27)"},950:{name:"Dreamless Sleep",hex:"#111111",rgb:"rgb(17, 17, 17)"},main:{name:"Dhūsar Grey",hex:"#ababab",rgb:"rgb(171, 171, 171)"}},zinc:{50:{name:"Errigal White",hex:"#f0f1f3",rgb:"rgb(240, 241, 243)"},100:{name:"White Iris",hex:"#e0e3e7",rgb:"rgb(224, 227, 231)"},150:{name:"Angel Hair Silver",hex:"#d1d5db",rgb:"rgb(209, 213, 219)"},200:{name:"Cemetery Ash",hex:"#c2c7d0",rgb:"rgb(194, 199, 208)"},250:{name:"Blunt",hex:"#b3b9c5",rgb:"rgb(179, 185, 197)"},300:{name:"Atelier",hex:"#a4acba",rgb:"rgb(164, 172, 186)"},350:{name:"Vesper Violet",hex:"#969eaf",rgb:"rgb(150, 158, 175)"},400:{name:"Dusty Heather",hex:"#8891a4",rgb:"rgb(136, 145, 164)"},450:{name:"Stowaway",hex:"#7b8495",rgb:"rgb(123, 132, 149)"},500:{name:"Lunar Shadow",hex:"#6f7887",rgb:"rgb(111, 120, 135)"},550:{name:"Magic",hex:"#646b79",rgb:"rgb(100, 107, 121)"},600:{name:"Blue Metal",hex:"#5c6370",rgb:"rgb(92, 99, 112)"},650:{name:"Extinct Volcano",hex:"#4d535e",rgb:"rgb(77, 83, 94)"},700:{name:"Midnight Sky",hex:"#424751",rgb:"rgb(66, 71, 81)"},750:{name:"Antarctic Deep",hex:"#373c44",rgb:"rgb(55, 60, 68)"},800:{name:"Suit Blue",hex:"#2d3138",rgb:"rgb(45, 49, 56)"},850:{name:"Black Beauty",hex:"#23262c",rgb:"rgb(35, 38, 44)"},900:{name:"Satin Deep Black",hex:"#191c20",rgb:"rgb(25, 28, 32)"},950:{name:"Ruined Smores",hex:"#0f1114",rgb:"rgb(15, 17, 20)"},main:{name:"Lunar Shadow",hex:"#6f7887",rgb:"rgb(111, 120, 135)"}},slate:{50:{name:"Paper White",hex:"#eff1f4",rgb:"rgb(239, 241, 244)"},100:{name:"December Dawn",hex:"#dfe3eb",rgb:"rgb(223, 227, 235)"},150:{name:"Morning Chill",hex:"#cfd5e2",rgb:"rgb(207, 213, 226)"},200:{name:"Himalayan Poppy",hex:"#bfc7d9",rgb:"rgb(191, 199, 217)"},250:{name:"Cold Winter's Morn",hex:"#b0b9d0",rgb:"rgb(176, 185, 208)"},300:{name:"Foxflower Viola",hex:"#a0acc7",rgb:"rgb(160, 172, 199)"},350:{name:"Periwinkle Blossom",hex:"#909ebe",rgb:"rgb(144, 158, 190)"},400:{name:"Luxury",hex:"#8191b5",rgb:"rgb(129, 145, 181)"},450:{name:"Berwick Berry",hex:"#7385a9",rgb:"rgb(115, 133, 169)"},500:{name:"Chicory Flower",hex:"#687899",rgb:"rgb(104, 120, 153)"},550:{name:"Allegiance",hex:"#5d6b89",rgb:"rgb(93, 107, 137)"},600:{name:"Award Night",hex:"#525f7a",rgb:"rgb(82, 95, 122)"},650:{name:"Snoop",hex:"#48536b",rgb:"rgb(72, 83, 107)"},700:{name:"Anthracite Blue",hex:"#3d475c",rgb:"rgb(61, 71, 92)"},750:{name:"Burka Black",hex:"#333c4e",rgb:"rgb(51, 60, 78)"},800:{name:"Diamond Black",hex:"#2a3140",rgb:"rgb(42, 49, 64)"},850:{name:"Sky Captain",hex:"#202632",rgb:"rgb(32, 38, 50)"},900:{name:"River Styx",hex:"#181c25",rgb:"rgb(24, 28, 37)"},950:{name:"Dark Rift",hex:"#0e1118",rgb:"rgb(14, 17, 24)"},main:{name:"Award Night",hex:"#525f7a",rgb:"rgb(82, 95, 122)"}}},zm=()=>Object.keys(ci),Vm=t=>ci[t],jm=e("hgroup",null,[e("p",{class:"chapter"},`
      Customisation
    `),n(),e("h1",null,"Colors"),n(),e("p",null,"Nano comes with 380 manually crafted colors to help you personalise your brand design system.")],-1),Fm=G('<aside id="table-of-contents"><nav><details open=""><summary>Content</summary> <ul><li><a class="secondary" href="/docs/colors" aria-current="page">Colors</a></li> <li><a class="secondary" href="/docs/colors#usage-with-css">Usage with CSS</a></li> <li><a class="secondary" href="/docs/colors#usage-with-sass">Usage with SASS</a></li></ul></details></nav></aside>',1),Hm={role:"document"},qm={class:"color-families"},Um=e("h2",null,[n(`
        Usage with CSS`),e("a",{id:"usage-with-css",href:"#usage-with-css",class:"secondary",tabindex:"-1"},"#")],-1),Gm=e("p",null,"No color utilities are in the main nano stylesheet.",-1),Wm={class:"code small","data-theme":"dark"},Km=e("pre",null,'<link rel="stylesheet" href="css/nano.colors.min.css" />',-1),Ym=e("p",null,"This stylesheet is almost the same size as the entire nano library.",-1),Jm=e("p",null,`
        We do not recommend including all colors on a production site. You should include only the color families
        and shades that you use.
      `,-1),Zm=e("p",null,`
        After linking the color utilities, you can style any element with the utility classes. Click on any color above
        to see details.
      `,-1),Xm={"aria-label":"Color example",class:"component"},Qm=e("h2",{class:"nano-color-pink-500"},`
          Pink title
        `,-1),eb={class:"code small","data-theme":"dark"},tb=e("pre",null,'<h2 className="nano-color-pink-500">Pink title</h2>',-1),nb=e("article",{"aria-label":"Background color example",class:"nano-background-pink-600"},`
        Pink card
      `,-1),ob={class:"code","data-theme":"dark"},ab=e("pre",null,`<article class="nano-background-pink-600">
  Pink card
</article>`,-1),lb=e("h2",null,[n(`
        Usage with SASS`),e("a",{id:"usage-with-sass",href:"#usage-with-sass",class:"secondary",tabindex:"-1"},"#")],-1),sb=e("p",null,[n("You can import all colors as SASS variables in any "),e("code",null,".scss"),n(" file with:")],-1),rb={class:"code","data-theme":"dark"},ib=e("pre",null,'@use "colors" as *;',-1),cb=e("p",null,"The colors can then be used like this:",-1),db={class:"code","data-theme":"dark"},ub=e("pre",null,`h2 {
  color: $pink-500;
}`,-1),hb=e("p",null,[n(`
        You can also generate the utility classes with `),e("a",{rel:"noopener noreferrer",href:"https://sass-lang.com/documentation/at-rules/use",target:"_blank"},"@use"),n(`:
      `)],-1),pb={class:"code","data-theme":"dark"},fb=e("pre",null,'@use "colors/utilities";',-1),mb=e("p",null,"There are many settings available.",-1),bb=e("p",null,`
        Here is, for example, how to generate only the color utilities (and not the background utilities) and only for
        red, pink, fuchsia, and purple color families.
      `,-1),gb={class:"code","data-theme":"dark"},_b=e("pre",null,`@use "colors/utilities" with (
  $palette: (
    "color-families": (
      red,
      pink,
      fuchsia,
      purple,
    ),
  ),
  $utilities: (
    "background-colors": false,
  )
);`,-1),vb=e("summary",{role:"button",class:"secondary"},`
          All default settings
        `,-1),yb={class:"code","data-theme":"dark"},wb=e("pre",null,`// prefix for css variables
$css-var-prefix: nano-settings.$css-var-prefix !default;
$css-class-prefix: nano-settings.$css-var-prefix !default;

// Palette
$palette: () !default;
$palette: map.merge(
  (
    // Color families
    "color-families":
      (
        red,
        pink,
        fuchsia,
        purple,
        violet,
        indigo,
        blue,
        azure,
        cyan,
        jade,
        green,
        lime,
        yellow,
        amber,
        pumpkin,
        orange,
        sand,
        grey,
        zinc,
        slate
      ),
    // Shades
    "shades":
      (
        50,
        100,
        150,
        200,
        250,
        300,
        350,
        400,
        450,
        500,
        550,
        600,
        650,
        700,
        750,
        800,
        850,
        900,
        950
      ),
    // Export main color for each family
    "enable-main-color": true,

    // Export shades for each family
    "enable-shades": true,

    // Export black and white
    "enable-black-and-white": false,

    // Light color used for dark backgrounds
    "light-color": #fff,

    // Dark color used for light backgrounds
    "dark-color": #000,

    // Export as HEX, RGB or HSL values
    "export-as": "hex" // hex | rgb | hsl
  ),
  $palette
);

// Properties names used for CSS variables and classes
// Useful if you want to shorten the names
$properties: () !default;
$properties: map.merge(
  (
    "color": "color",
    "background-color": "background",
  ),
  $properties
);

// Utilities to export
$utilities: () !default;
$utilities: map.merge(
  (
    // CSS Vars
    "css-vars": true,

    // Colors utility classes
    "colors": true,

    // Background color utility classes
    "background-colors": true,

    // Color value for background color utility classes
    "color-for-background-colors": true
  ),
  $utilities
);`,-1),xb={__name:"ColorsView",setup(t){return(o,a)=>{const l=Sn("highlight");return C(),S(J,null,[jm,n(),Fm,n(),e("div",Hm,[e("section",qm,[(C(!0),S(J,null,Oe($(zm)(),s=>(C(),En(Dm,{key:s,family:s,"family-map":$(Vm)(s)},null,8,["family","family-map"]))),128))]),n(),e("section",null,[Um,n(),Gm,n(),e("p",null,[n(`
        There is a separate stylesheet with all the color utilities that you can link in the
        `),$e((C(),S("code",null,[n("<head>")])),[[l]]),n(` of your website.
      `)]),n(),e("div",Wm,[f(k,null,{default:v(()=>[Km]),_:1})]),n(),Ym,n(),Jm,n(),Zm,n(),e("article",Xm,[Qm,n(),e("footer",eb,[f(k,null,{default:v(()=>[tb]),_:1})])]),n(),nb,n(),e("div",ob,[f(k,null,{default:v(()=>[ab]),_:1})])]),n(),e("section",null,[lb,n(),sb,n(),e("div",rb,[f(k,null,{default:v(()=>[ib]),_:1})]),n(),cb,n(),e("div",db,[f(k,null,{default:v(()=>[ub]),_:1})]),n(),hb,n(),e("div",pb,[f(k,null,{default:v(()=>[fb]),_:1})]),n(),mb,n(),bb,n(),e("div",gb,[f(k,null,{default:v(()=>[_b]),_:1})]),n(),e("details",null,[vb,n(),e("div",yb,[f(k,null,{default:v(()=>[wb]),_:1})])])])])],64)}}},kb=e("hgroup",null,[e("p",{class:"chapter"},`
      Layout
    `),n(),e("h1",null,"Container"),n(),e("p",null,[n("Use "),e("code",null,".container"),n(" for a centered viewport or "),e("code",null,".container-fluid"),n(" for a full-width layout.")])],-1),$b=G('<aside id="table-of-contents"><nav><details open><summary>Content</summary> <ul><li><a class="secondary" href="/docs/container">Breakpoints</a></li> <li><a class="secondary" href="/docs/container#fixed-width">Fixed Width</a></li> <li><a class="secondary" href="/docs/container#fluid-width">Fluid Width</a></li> <li><a class="secondary" href="/docs/container#auto-layout-columns">Auto-layout Columns</a></li> <li><a class="secondary" href="/docs/container#equal-width">Equal-width</a></li> <li><a class="secondary" href="/docs/container#setting-one-column-width">Setting one column width</a></li> <li><a class="secondary" href="/docs/container#responsive-classes">Responsive Classes</a></li> <li><a class="secondary" href="/docs/container#all-breakpoints">All Breakpoints</a></li> <li><a class="secondary" href="/docs/container#stacked-to-horizontal">Stacked to Horizontal</a></li> <li><a class="secondary" href="/docs/container#mix-and-match">Mix and Match</a></li> <li><a class="secondary" href="/docs/container#row-columns">Row Columns</a></li> <li><a class="secondary" href="/docs/container#nesting">Nesting</a></li></ul></details></nav></aside>',1),Cb={role:"document"},Sb=e("p",null,[n(`
        Nano's grid system is largely compatible with
        `),e("a",{href:"https://getbootstrap.com/docs/4.1/layout/grid/"},"Bootstrap"),n(`. It uses a series of containers, rows,
        and columns to layout and align content. It’s built with flexbox and is fully responsive. Below is an example
        and an in-depth look at how the grid comes together.
      `)],-1),Eb={"aria-label":"Grid example"},Tb=G(`<div class="container-example"><div class="container"><div class="row"><div class="col-sm">
                1 of 3 columns
              </div> <div class="col-sm">
                1 of 3 columns
              </div> <div class="col-sm">
                1 of 3 columns
              </div></div></div></div>`,1),Ab={class:"code","data-theme":"dark"},Mb=e("pre",null,`<div class="container">
  <div class="row">
    <div class="col-sm">
      1 of 3 columns
    </div>
    <div class="col-sm">
      1 of 3 columns
    </div>
    <div class="col-sm">
      1 of 3 columns
    </div>
  </div>
</div>`,-1),Pb=G(`<section><p>
        Nano includes six default breakpoints. These breakpoints can be customised with <a href="/docs/sass">Sass</a>.
      </p> <table class="striped"><thead><tr><th>Device</th> <th>Breakpoint</th> <th>Viewport</th></tr></thead> <tbody><tr><td>Extra small</td> <td>&lt;576px</td> <td>100%</td></tr> <tr><td>Small</td> <td>≥576px</td> <td>540px</td></tr> <tr><td>Medium</td> <td>≥768px</td> <td>720px</td></tr> <tr><td>Large</td> <td>≥992px</td> <td>960px</td></tr> <tr><td>Extra large</td> <td>≥1280px</td> <td>1200px</td></tr> <tr><td>Extra extra large</td> <td>≥1536px</td> <td>1440px</td></tr></tbody></table></section>`,1),Nb=e("h2",null,[n(`
        Fixed Width`),e("a",{id:"fixed-width",href:"#fixed-width",class:"secondary",tabindex:"-1"},"#")],-1),Ob=e("p",null,[e("code",null,".container"),n(" provides a centered container with a fixed width.")],-1),Rb={class:"code","data-theme":"dark"},Ib=e("pre",null,`<body>
  <main class="container">
    ...
  </main>
</body>`,-1),Lb=e("h2",null,[n(`
        Fluid Width`),e("a",{id:"fluid-width",href:"#fluid-width",class:"secondary",tabindex:"-1"},"#")],-1),Bb=e("p",null,[e("code",null,".container-fluid"),n(" provides a full-width container.")],-1),Db={class:"code","data-theme":"dark"},zb=e("pre",null,`<body>
  <main class="container-fluid">
    ...
  </main>
</body>`,-1),Vb=e("h2",null,[n(`
        Auto-layout Columns`),e("a",{id:"auto-layout-columns",href:"#auto-layout-columns",class:"secondar",tabindex:"-1"},"#")],-1),jb=e("p",null,[n(`
        Utilise breakpoint-specific column classes for easy column sizing without an explicit numbered class like
        `),e("code",null,".col-sm-6"),n(`.
      `)],-1),Fb=e("h3",null,[n(`
        Equal-width`),e("a",{id:"equal-width",href:"#equal-width",class:"secondar",tabindex:"-1"},"#")],-1),Hb=e("p",null,[n(`
        For example, here are two grid layouts that apply to every device and viewport, from `),e("code",null,"xs"),n(` to
        `),e("code",null,"xxl"),n(`. Add any number of unit-less classes for each breakpoint you need and every column will be the
        same width.
      `)],-1),qb=G(`<div class="container-example container-example-row"><div class="container"><div class="row"><div class="col">
                1 of 2
              </div> <div class="col">
                2 of 2
              </div></div> <div class="row"><div class="col">
                1 of 3
              </div> <div class="col">
                2 of 3
              </div> <div class="col">
                3 of 3
              </div></div></div></div>`,1),Ub={class:"code","data-theme":"dark"},Gb=e("pre",null,`<div class="container">
  <div class="row">
    <div class="col">
      1 of 2
    </div>
    <div class="col">
      2 of 2
    </div>
  </div>
  <div class="row">
    <div class="col">
      1 of 3
    </div>
    <div class="col">
      2 of 3
    </div>
    <div class="col">
      3 of 3
    </div>
  </div>
</div>`,-1),Wb=e("h3",null,[n(`
        Setting one column width`),e("a",{id:"setting-one-column-width",href:"#setting-one-column-width",class:"secondar",tabindex:"-1"},"#")],-1),Kb=e("p",null,`
        Auto-layout for flexbox grid columns also means you can set the width of one column and have the sibling columns
        automatically resize around it. You may use predefined grid classes (as shown below), grid mixins, or inline
        widths. Note that the other columns will resize no matter the width of the center column.
      `,-1),Yb=G(`<div class="container-example container-example-row"><div class="container"><div class="row"><div class="col">
                1 of 3
              </div> <div class="col-6">
                2 of 3 (wider)
              </div> <div class="col">
                3 of 3
              </div></div> <div class="row"><div class="col">
                1 of 3
              </div> <div class="col-5">
                2 of 3 (wider)
              </div> <div class="col">
                3 of 3
              </div></div></div></div>`,1),Jb={class:"code","data-theme":"dark"},Zb=e("pre",null,`<div class="container">
  <div class="row">
    <div class="col">
      1 of 3
    </div>
    <div class="col-6">
      2 of 3 (wider)
    </div>
    <div class="col">
      3 of 3
    </div>
  </div>
  <div class="row">
    <div class="col">
      1 of 3
    </div>
    <div class="col-5">
      2 of 3 (wider)
    </div>
    <div class="col">
      3 of 3
    </div>
  </div>
</div>`,-1),Xb=e("h3",null,[n(`
        Variable width content`),e("a",{id:"variable-width-content",href:"#variable-width-content",class:"secondar",tabindex:"-1"},"#")],-1),Qb=e("p",null,[n("Use "),e("code",null,"col-{breakpoint}-auto"),n(" classes to size columns based on the natural width of their content.")],-1),eg=G(`<div class="container-example container-example-row"><div class="container"><div class="row justify-content-md-center"><div class="col col-lg-2">
                1 of 3
              </div> <div class="col-md-auto">
                Variable width content
              </div> <div class="col col-lg-2">
                3 of 3
              </div></div> <div class="row"><div class="col">
                1 of 3
              </div> <div class="col-md-auto">
                Variable width content
              </div> <div class="col col-lg-2">
                3 of 3
              </div></div></div></div>`,1),tg={class:"code","data-theme":"dark"},ng=e("pre",null,`<div class="container">
  <div class="row justify-content-md-center">
    <div class="col col-lg-2">
      1 of 3
    </div>
    <div class="col-md-auto">
      Variable width content
    </div>
    <div class="col col-lg-2">
      3 of 3
    </div>
  </div>
  <div class="row">
    <div class="col">
      1 of 3
    </div>
    <div class="col-md-auto">
      Variable width content
    </div>
    <div class="col col-lg-2">
      3 of 3
    </div>
  </div>
</div>`,-1),og=e("h2",null,[n(`
        Responsive Classes`),e("a",{id:"responsive-classes",href:"#responsive-classes",class:"secondar",tabindex:"-1"},"#")],-1),ag=e("p",null,`
        Bootstrap's grid includes six tiers of predefined classes for building complex responsive layouts. Customise the
        size of your columns on extra small, small, medium, large, or extra large devices however you see fit.
      `,-1),lg=e("h3",null,[n(`
        All Breakpoints`),e("a",{id:"all-breakpoints",href:"#all-breakpoints",class:"secondar",tabindex:"-1"},"#")],-1),sg=e("p",null,[n(`
        For grids that are the same from the smallest of devices to the largest, use the `),e("code",null,".col"),n(` and
        `),e("code",null,".col-*"),n(` classes. Specify a numbered class when you need a particularly sized column; otherwise, feel
        free to stick to `),e("code",null,".col"),n(`.
      `)],-1),rg=G(`<div class="container-example container-example-row"><div class="container"><div class="row"><div class="col">
                col
              </div> <div class="col">
                col
              </div> <div class="col">
                col
              </div> <div class="col">
                col
              </div></div> <div class="row"><div class="col-8">
                col-8
              </div> <div class="col-4">
                col-4
              </div></div></div></div>`,1),ig={class:"code","data-theme":"dark"},cg=e("pre",null,`<div class="container">
  <div class="row">
    <div class="col">col</div>
    <div class="col">col</div>
    <div class="col">col</div>
    <div class="col">col</div>
  </div>
  <div class="row">
    <div class="col-8">col-8</div>
    <div class="col-4">col-4</div>
  </div>
</div>`,-1),dg=e("h3",null,[n(`
        Stacked to Horizontal`),e("a",{id:"stacked-to-horizontal",href:"#stacked-to-horizontal",class:"secondar",tabindex:"-1"},"#")],-1),ug=e("p",null,[n(`
        Using a single set of `),e("code",null,".col-sm-*"),n(` classes, you can create a basic grid system that starts out stacked
        and becomes horizontal at the small breakpoint (`),e("code",null,"sm"),n(`).
      `)],-1),hg=G(`<div class="container-example container-example-row"><div class="container"><div class="row"><div class="col-sm-8">
                col-sm-8
              </div> <div class="col-sm-4">
                col-sm-4
              </div></div> <div class="row"><div class="col-sm">
                col-sm
              </div> <div class="col-sm">
                col-sm
              </div> <div class="col-sm">
                col-sm
              </div></div></div></div>`,1),pg={class:"code","data-theme":"dark"},fg=e("pre",null,`<div class="container">
  <div class="row">
    <div class="col-sm-8">col-sm-8</div>
    <div class="col-sm-4">col-sm-4</div>
  </div>
  <div class="row">
    <div class="col-sm">col-sm</div>
    <div class="col-sm">col-sm</div>
    <div class="col-sm">col-sm</div>
  </div>
</div>`,-1),mg=e("h3",null,[n(`
        Mix and Match`),e("a",{id:"mix-and-match",href:"#mix-and-match",class:"secondar",tabindex:"-1"},"#")],-1),bg=e("p",null,`
        Don't want your columns to simply stack in some grid tiers? Use a combination of different classes for each tier
        as needed. See the example below for a better idea of how it all works.
      `,-1),gg=G(`<div class="container-example container-example-row"><div class="container"><div class="row"><div class="col-md-8">
                .col-md-8
              </div> <div class="col-6 col-md-4">
                .col-6 .col-md-4
              </div></div> <div class="row"><div class="col-6 col-md-4">
                .col-6 .col-md-4
              </div> <div class="col-6 col-md-4">
                .col-6 .col-md-4
              </div> <div class="col-6 col-md-4">
                .col-6 .col-md-4
              </div></div> <div class="row"><div class="col-6">
                .col-6
              </div> <div class="col-6">
                .col-6
              </div></div></div></div>`,1),_g={class:"code","data-theme":"dark"},vg=e("pre",null,`<div class="container">
  <!-- Stack the columns on mobile by making one full-width and the other half-width -->
  <div class="row">
    <div class="col-md-8">.col-md-8</div>
    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
  </div>

  <!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
  <div class="row">
    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
  </div>

  <!-- Columns are always 50% wide, on mobile and desktop -->
  <div class="row">
    <div class="col-6">.col-6</div>
    <div class="col-6">.col-6</div>
  </div>
</div>`,-1),yg=e("h3",null,[n(`
        Row Columns`),e("a",{id:"row-columns",href:"#row-columns",class:"secondar",tabindex:"-1"},"#")],-1),wg=e("p",null,[n(`
        Use the responsive `),e("code",null,".row-cols-*"),n(` classes to quickly set the number of columns that best render your
        content and layout. Whereas normal `),e("code",null,".col-*"),n(` classes apply to the individual columns (e.g.,
        `),e("code",null,".col-md-4"),n("), the row columns classes are set on the parent "),e("code",null,".row"),n(` as a default for
        contained columns. With `),e("code",null,".row-cols-auto"),n(` you can give the columns their natural width.
      `)],-1),xg=e("p",null,`
        Use these row columns classes to quickly create basic grid layouts or to control your card layouts and override
        when needed at the column level.
      `,-1),kg=G(`<div class="container-example container-example-row"><div class="container"><div class="row row-cols-2"><div class="col">
                Column
              </div> <div class="col">
                Column
              </div> <div class="col">
                Column
              </div> <div class="col">
                Column
              </div></div></div></div>`,1),$g={class:"code","data-theme":"dark"},Cg=e("pre",null,`<div class="container">
  <div class="row row-cols-2">
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
  </div>
</div>`,-1),Sg=G(`<div class="container-example container-example-row"><div class="container"><div class="row row-cols-3"><div class="col">
                Column
              </div> <div class="col">
                Column
              </div> <div class="col">
                Column
              </div> <div class="col">
                Column
              </div></div></div></div>`,1),Eg={class:"code","data-theme":"dark"},Tg=e("pre",null,`<div class="container">
  <div class="row row-cols-3">
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
  </div>
</div>`,-1),Ag=G(`<div class="container-example container-example-row"><div class="container"><div class="row row-cols-auto"><div class="col">
                Column
              </div> <div class="col">
                Column
              </div> <div class="col">
                Column
              </div> <div class="col">
                Column
              </div></div></div></div>`,1),Mg={class:"code","data-theme":"dark"},Pg=e("pre",null,`<div class="container">
  <div class="row row-cols-auto">
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
  </div>
</div>`,-1),Ng=G(`<div class="container-example container-example-row"><div class="container"><div class="row row-cols-4"><div class="col">
                Column
              </div> <div class="col">
                Column
              </div> <div class="col">
                Column
              </div> <div class="col">
                Column
              </div></div></div></div>`,1),Og={class:"code","data-theme":"dark"},Rg=e("pre",null,`<div class="container">
  <div class="row row-cols-4">
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
  </div>
</div>`,-1),Ig=G(`<div class="container-example container-example-row"><div class="container"><div class="row row-cols-4"><div class="col">
                Column
              </div> <div class="col">
                Column
              </div> <div class="col-6">
                Column
              </div> <div class="col">
                Column
              </div></div></div></div>`,1),Lg={class:"code","data-theme":"dark"},Bg=e("pre",null,`<div class="container">
  <div class="row row-cols-4">
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col-6">Column</div>
    <div class="col">Column</div>
  </div>
</div>`,-1),Dg=G(`<div class="container-example container-example-row"><div class="container"><div class="row row-cols-1 row-cols-sm-2 row-cols-md-4"><div class="col">
                Column
              </div> <div class="col">
                Column
              </div> <div class="col">
                Column
              </div> <div class="col">
                Column
              </div></div></div></div>`,1),zg={class:"code","data-theme":"dark"},Vg=e("pre",null,`<div class="container">
  <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4">
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
  </div>
</div>`,-1),jg=G(`<div class="container-example container-example-row"><div class="container"><div class="row row-cols-2 row-cols-lg-3"><div class="col">
                Column
              </div> <div class="col">
                Column
              </div> <div class="col">
                Column
              </div> <div class="col">
                Column
              </div> <div class="col">
                Column
              </div> <div class="col">
                Column
              </div> <div class="col-4 col-lg-2">
                Column
              </div> <div class="col-4 col-lg-2">
                Column
              </div> <div class="col-4 col-lg-2">
                Column
              </div> <div class="col-4 col-lg-2">
                Column
              </div> <div class="col-4 col-lg-2">
                Column
              </div> <div class="col-4 col-lg-2">
                Column
              </div></div></div></div>`,1),Fg={class:"code","data-theme":"dark"},Hg=e("pre",null,`<div class="container">
  <div class="row row-cols-2 row-cols-lg-3">
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col-4 col-lg-2">Column</div>
    <div class="col-4 col-lg-2">Column</div>
    <div class="col-4 col-lg-2">Column</div>
    <div class="col-4 col-lg-2">Column</div>
    <div class="col-4 col-lg-2">Column</div>
    <div class="col-4 col-lg-2">Column</div>
  </div>
</div>`,-1),qg=e("p",null,[n("You can also use the accompanying Sass mixin, "),e("code",null,"row-cols()"),n(":")],-1),Ug={class:"code","data-theme":"dark"},Gg=e("pre",null,`.element {
  // Three columns to start
  @include row-cols(3);

  // Five columns from medium breakpoint up
  @include media-breakpoint-up(md) {
    @include row-cols(5);
  }
}`,-1),Wg=e("h2",null,[n(`
        Nesting`),e("a",{id:"nesting",href:"#nesting",class:"secondar",tabindex:"-1"},"#")],-1),Kg=e("p",null,[n(`
        To nest your content with the default grid, add a new `),e("code",null,".row"),n(" and set of "),e("code",null,".col-sm-*"),n(`
        columns within an existing `),e("code",null,".col-sm-*"),n(` column. Nested rows should include a set of columns that add
        up to 12 or fewer (it is not required that you use all 12 available columns).
      `)],-1),Yg=G(`<div class="container-example container-example-row"><div class="container"><div class="row"><div class="col-sm-3">
                Level 1: .col-sm-3
              </div> <div class="col-sm-9"><div class="row"><div class="col-8 col-sm-6">
                    Level 2: .col-8 .col-sm-6
                  </div> <div class="col-4 col-sm-6">
                    Level 2: .col-4 .col-sm-6
                  </div></div></div></div></div></div>`,1),Jg={class:"code","data-theme":"dark"},Zg=e("pre",null,`<div class="container">
  <div class="row">
    <div class="col-sm-3">
      Level 1: .col-sm-3
    </div>
    <div class="col-sm-9">
      <div class="row">
        <div class="col-8 col-sm-6">
          Level 2: .col-8 .col-sm-6
        </div>
        <div class="col-4 col-sm-6">
          Level 2: .col-4 .col-sm-6
        </div>
      </div>
    </div>
  </div>
</div>`,-1),Xg={__name:"ContainerView",setup(t){return(o,a)=>(C(),S(J,null,[kb,n(),$b,n(),e("div",Cb,[e("section",null,[Sb,n(),e("article",Eb,[Tb,n(),e("footer",Ab,[f(k,null,{default:v(()=>[Mb]),_:1})])])]),n(),Pb,n(),e("section",null,[Nb,n(),Ob,n(),e("div",Rb,[f(k,null,{default:v(()=>[Ib]),_:1})])]),n(),e("section",null,[Lb,n(),Bb,n(),e("div",Db,[f(k,null,{default:v(()=>[zb]),_:1})])]),n(),e("section",null,[Vb,n(),jb,n(),Fb,n(),Hb,n(),e("article",null,[qb,n(),e("footer",Ub,[f(k,null,{default:v(()=>[Gb]),_:1})])]),n(),Wb,n(),Kb,n(),e("article",null,[Yb,n(),e("footer",Jb,[f(k,null,{default:v(()=>[Zb]),_:1})])]),n(),Xb,n(),Qb,n(),e("article",null,[eg,n(),e("footer",tg,[f(k,null,{default:v(()=>[ng]),_:1})])])]),n(),e("section",null,[og,n(),ag,n(),lg,n(),sg,n(),e("article",null,[rg,n(),e("footer",ig,[f(k,null,{default:v(()=>[cg]),_:1})])])]),n(),e("section",null,[dg,n(),ug,n(),e("article",null,[hg,n(),e("footer",pg,[f(k,null,{default:v(()=>[fg]),_:1})])]),n(),mg,n(),bg,n(),e("article",null,[gg,n(),e("footer",_g,[f(k,null,{default:v(()=>[vg]),_:1})])]),n(),yg,n(),wg,n(),xg,n(),e("article",null,[kg,n(),e("footer",$g,[f(k,null,{default:v(()=>[Cg]),_:1})])]),n(),e("article",null,[Sg,n(),e("footer",Eg,[f(k,null,{default:v(()=>[Tg]),_:1})])]),n(),e("article",null,[Ag,n(),e("footer",Mg,[f(k,null,{default:v(()=>[Pg]),_:1})])]),n(),e("article",null,[Ng,n(),e("footer",Og,[f(k,null,{default:v(()=>[Rg]),_:1})])]),n(),e("article",null,[Ig,n(),e("footer",Lg,[f(k,null,{default:v(()=>[Bg]),_:1})])]),n(),e("article",null,[Dg,n(),e("footer",zg,[f(k,null,{default:v(()=>[Vg]),_:1})])]),n(),e("article",null,[jg,n(),e("footer",Fg,[f(k,null,{default:v(()=>[Hg]),_:1})])]),n(),qg,n(),e("footer",Ug,[f(k,null,{default:v(()=>[Gg]),_:1})])]),n(),e("section",null,[Wg,n(),Kg,n(),e("article",null,[Yg,n(),e("footer",Jg,[f(k,null,{default:v(()=>[Zg]),_:1})])])])])],64))}},Qg=e("hgroup",null,[e("p",{class:"chapter"},`
      Customisation
    `),n(),e("h1",null,"CSS Variables"),n(),e("p",null,"Customise Nano's design system with over 130 CSS variables to create a unique look and feel.")],-1),e1=G('<aside id="table-of-contents"><nav><details open><summary>Content</summary> <ul><li><a class="secondary" href="/docs/css-variables">Introduction</a></li> <li><a class="secondary" href="/docs/css-variables#example">Example</a></li> <li><a class="secondary" href="/docs/css-variables#css-variables-for-color-schemes">Color schemes</a></li> <li><a class="secondary" href="/docs/css-variables#all-css-variables">All CSS variables</a></li></ul></details></nav></aside>',1),t1={role:"document"},n1=G(`<section><p>
        Nano includes many custom properties (variables) that allow easy access to frequently used values such as
        <code>font-family</code>, <code>font-size</code>,<code>border-radius</code>, <code>margin</code>,
        <code>padding</code>, and more.
      </p> <p>
        All CSS variables are prefixed with <code>nano-</code> to avoid collisions with other CSS frameworks or your own
        vars. You can remove or customise this prefix by recompiling the CSS files with <a href="/docs/sass">Sass</a>.
      </p> <p>
        You can define the CSS variables within the <code>:root</code> selector to apply the changes globally or
        overwrite the CSS variables on specific selectors to apply the changes locally.
      </p></section>`,1),o1=e("h2",null,[n(`
        Example`),e("a",{id:"example",href:"#example",class:"secondary",tabindex:"-1"},"#")],-1),a1={id:"css-var-example","aria-label":"Button colors example",class:"component"},l1=e("h1",null,"Music fest mania",-1),s1=e("p",null,`
          Get ready to dance and sing your heart out at our Music Fest Mania. Join the crowd, jam to your favorite band,
          and discover new artists.
        `,-1),r1=e("button",null,"Let's rock out!",-1),i1={class:"code","data-theme":"dark"},c1=e("pre",null,`<style>
  :root {
    --nano-border-radius: 2rem;
    --nano-typography-spacing-vertical: 1.5rem;
    --nano-form-element-spacing-vertical: 1rem;
    --nano-form-element-spacing-horizontal: 1.25rem;
  }
  h1 {
    --nano-font-family: Pacifico, cursive;
    --nano-font-weight: 400;
    --nano-typography-spacing-vertical: 0.5rem;
  }
  button {
    --nano-font-weight: 700;
  }
</style>

<h1>Music fest mania</h1>
<p>
  Get ready to dance and sing your heart out at
  our Music Fest Mania. Join the crowd, jam to
  your favorite band, and discover new artists.
</p>
<button>Let's rock out!</button>`,-1),d1=e("h2",null,[n(`
        CSS variables for color schemes`),e("a",{id:"css-variables-for-color-schemes",href:"#css-variables-for-color-schemes",class:"secondary",tabindex:"-1"},"#")],-1),u1=e("p",null,"To add or edit CSS variables for light mode only (the default mode), define them inside:",-1),h1={class:"code","data-theme":"dark"},p1=e("pre",null,`/* light color scheme (default) */
/* can be forced with data-theme="light" */
[data-theme="light"],
:root:not([data-theme="dark"]) {
 ...
} `,-1),f1=e("p",null,"To add or edit CSS variables for dark mode, you need to define them twice.",-1),m1=e("p",null,[n(`
        The first inclusion is in the `),e("code",null,"@media"),n(`query that checks if the user has dark mode enabled through
        their device settings with `),e("code",null,"prefers-color-scheme: dark"),n(`. In this case, the dark mode styling is
        applied to the `),e("code",null,":root"),n(" element if there is no explicit "),e("code",null,"data-theme"),n(` attribute set.
      `)],-1),b1=e("p",null,[n(`
        The second inclusion is when you force the dark mode with `),e("code",null,'data-theme="dark"'),n(`. This allows you to
        manually toggle between the light and dark themes regardless of the user's device settings.
      `)],-1),g1={class:"code","data-theme":"dark"},_1=e("pre",null,`/* dark color scheme (auto) */
/* automatically enabled if user has dark mode enabled */
 @media only screen and (prefers-color-scheme: dark) {
  :root:not([data-theme]) {
    ...
  }
}

/* dark color scheme (forced) */
/* enabled if forced with data-theme="dark" */
[data-theme="dark"] {
  ...
}`,-1),v1=e("p",null,[n("Try our "),e("a",{href:"/docs/theme-generator"},"Minimal theme generator"),n(" for a detailed example.")],-1),y1=e("h2",null,[n(`
        All CSS variables`),e("a",{id:"all-css-variables",href:"#all-css-variables",class:"secondary",tabindex:"-1"},"#")],-1),w1=e("p",null,"There are two categories of CSS variables:",-1),x1=e("ol",null,[e("li",null,[e("strong",null,"Style variables"),n(", which do not depend on the color scheme,")]),n(),e("li",null,[e("strong",null,"Color variables"),n(", which depend on the color scheme.")])],-1),k1=e("p",{style:{"margin-bottom":"2rem"}},`
        Here is the list of all CSS variables used in Nano:
      `,-1),$1=e("summary",{role:"button",class:"secondary"},`
          Default styles CSS variables
        `,-1),C1={class:"code","data-theme":"dark"},S1=e("pre",null,`:root {
  --nano-font-family-emoji: "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
  --nano-font-family-sans-serif: system-ui, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, var(--nano-font-family-emoji);
  --nano-font-family-monospace: ui-monospace, SFMono-Regular, "SF Mono", Menlo, Consolas, "Liberation Mono", monospace, var(--nano-font-family-emoji);
  --nano-font-family: var(--nano-font-family-sans-serif);
  --nano-line-height: 1.5;
  --nano-font-weight: 400;
  --nano-font-size: 100%;
  --nano-text-underline-offset: 0.1rem;
  --nano-border-radius: 0.25rem;
  --nano-border-width: 0.0625rem;
  --nano-outline-width: 0.1875rem;
  --nano-transition: 0.2s ease-in-out;
  --nano-spacing: 1rem;
  --nano-typography-spacing-vertical: 1rem;
  --nano-block-spacing-vertical: calc(var(--nano-spacing) * 2);
  --nano-block-spacing-horizontal: var(--nano-spacing);
  --nano-grid-column-gap: var(--nano-spacing);
  --nano-grid-row-gap: var(--nano-spacing);
  --nano-form-element-spacing-vertical: 0.75rem;
  --nano-form-element-spacing-horizontal: 1rem;
  --nano-group-box-shadow: 0 0 0 rgba(0, 0, 0, 0);
  --nano-group-box-shadow-focus-with-button: 0 0 0 var(--nano-outline-width) var(--nano-primary-focus);
  --nano-group-box-shadow-focus-with-input: 0 0 0 0.0625rem var(--nano-form-element-border-color);
  --nano-modal-overlay-backdrop-filter: blur(0.375rem);
  --nano-nav-element-spacing-vertical: 1rem;
  --nano-nav-element-spacing-horizontal: 0.5rem;
  --nano-nav-link-spacing-vertical: 0.5rem;
  --nano-nav-link-spacing-horizontal: 0.5rem;
  --nano-nav-breadcrumb-divider: ">";
  --nano-icon-checkbox: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(255, 255, 255)' stroke-width='4' stroke-linecap='round' stroke-linejoin='round'%3E%3Cpolyline points='20 6 9 17 4 12'%3E%3C/polyline%3E%3C/svg%3E");
  --nano-icon-minus: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(255, 255, 255)' stroke-width='4' stroke-linecap='round' stroke-linejoin='round'%3E%3Cline x1='5' y1='12' x2='19' y2='12'%3E%3C/line%3E%3C/svg%3E");
  --nano-icon-chevron: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(136, 145, 164)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Cpolyline points='6 9 12 15 18 9'%3E%3C/polyline%3E%3C/svg%3E");
  --nano-icon-date: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(136, 145, 164)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Crect x='3' y='4' width='18' height='18' rx='2' ry='2'%3E%3C/rect%3E%3Cline x1='16' y1='2' x2='16' y2='6'%3E%3C/line%3E%3Cline x1='8' y1='2' x2='8' y2='6'%3E%3C/line%3E%3Cline x1='3' y1='10' x2='21' y2='10'%3E%3C/line%3E%3C/svg%3E");
  --nano-icon-time: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(136, 145, 164)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Ccircle cx='12' cy='12' r='10'%3E%3C/circle%3E%3Cpolyline points='12 6 12 12 16 14'%3E%3C/polyline%3E%3C/svg%3E");
  --nano-icon-search: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(136, 145, 164)' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round'%3E%3Ccircle cx='11' cy='11' r='8'%3E%3C/circle%3E%3Cline x1='21' y1='21' x2='16.65' y2='16.65'%3E%3C/line%3E%3C/svg%3E");
  --nano-icon-close: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(136, 145, 164)' stroke-width='3' stroke-linecap='round' stroke-linejoin='round'%3E%3Cline x1='18' y1='6' x2='6' y2='18'%3E%3C/line%3E%3Cline x1='6' y1='6' x2='18' y2='18'%3E%3C/line%3E%3C/svg%3E");
  --nano-icon-loading: url("data:image/svg+xml,%3Csvg fill='none' height='24' width='24' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg' %3E%3Cstyle%3E g %7B animation: rotate 2s linear infinite; transform-origin: center center; %7D circle %7B stroke-dasharray: 75,100; stroke-dashoffset: -5; animation: dash 1.5s ease-in-out infinite; stroke-linecap: round; %7D @keyframes rotate %7B 0%25 %7B transform: rotate(0deg); %7D 100%25 %7B transform: rotate(360deg); %7D %7D @keyframes dash %7B 0%25 %7B stroke-dasharray: 1,100; stroke-dashoffset: 0; %7D 50%25 %7B stroke-dasharray: 44.5,100; stroke-dashoffset: -17.5; %7D 100%25 %7B stroke-dasharray: 44.5,100; stroke-dashoffset: -62; %7D %7D %3C/style%3E%3Cg%3E%3Ccircle cx='12' cy='12' r='10' fill='none' stroke='rgb(136, 145, 164)' stroke-width='4' /%3E%3C/g%3E%3C/svg%3E");
}
@media (min-width: 576px) {
  :root {
    --nano-font-size: 106.25%;
  }
}
@media (min-width: 768px) {
  :root {
    --nano-font-size: 112.5%;
  }
}
@media (min-width: 1024px) {
  :root {
    --nano-font-size: 118.75%;
  }
}
@media (min-width: 1280px) {
  :root {
    --nano-font-size: 125%;
  }
}
@media (min-width: 1536px) {
  :root {
    --nano-font-size: 131.25%;
  }
}
:root details summary[role=button]:not(.outline)::after {
  filter: brightness(0) invert(1);
}
:root [aria-busy=true]:not(input, select, textarea):is(button, [type=submit], [type=button], [type=reset], [role=button]):not(.outline)::before {
  filter: brightness(0) invert(1);
}

@media (min-width: 576px) {
  body > header,
  body > main,
  body > footer,
  section {
    --nano-block-spacing-vertical: calc(var(--nano-spacing) * 2.5);
  }
}
@media (min-width: 768px) {
  body > header,
  body > main,
  body > footer,
  section {
    --nano-block-spacing-vertical: calc(var(--nano-spacing) * 3);
  }
}
@media (min-width: 1024px) {
  body > header,
  body > main,
  body > footer,
  section {
    --nano-block-spacing-vertical: calc(var(--nano-spacing) * 3.5);
  }
}
@media (min-width: 1280px) {
  body > header,
  body > main,
  body > footer,
  section {
    --nano-block-spacing-vertical: calc(var(--nano-spacing) * 4);
  }
}
@media (min-width: 1536px) {
  body > header,
  body > main,
  body > footer,
  section {
    --nano-block-spacing-vertical: calc(var(--nano-spacing) * 4.5);
  }
}

@media (min-width: 576px) {
  article {
    --nano-block-spacing-horizontal: calc(var(--nano-spacing) * 1.25);
  }
}
@media (min-width: 768px) {
  article {
    --nano-block-spacing-horizontal: calc(var(--nano-spacing) * 1.5);
  }
}
@media (min-width: 1024px) {
  article {
    --nano-block-spacing-horizontal: calc(var(--nano-spacing) * 1.75);
  }
}
@media (min-width: 1280px) {
  article {
    --nano-block-spacing-horizontal: calc(var(--nano-spacing) * 2);
  }
}
@media (min-width: 1536px) {
  article {
    --nano-block-spacing-horizontal: calc(var(--nano-spacing) * 2.25);
  }
}

dialog > article {
  --nano-block-spacing-vertical: calc(var(--nano-spacing) * 2);
  --nano-block-spacing-horizontal: var(--nano-spacing);
}
@media (min-width: 576px) {
  dialog > article {
    --nano-block-spacing-vertical: calc(var(--nano-spacing) * 2.5);
    --nano-block-spacing-horizontal: calc(var(--nano-spacing) * 1.25);
  }
}
@media (min-width: 768px) {
  dialog > article {
    --nano-block-spacing-vertical: calc(var(--nano-spacing) * 3);
    --nano-block-spacing-horizontal: calc(var(--nano-spacing) * 1.5);
  }
}

a {
  --nano-text-decoration: underline;
}
a.secondary, a.contrast {
  --nano-text-decoration: underline;
}

small {
  --nano-font-size: 0.875em;
}

h1,
h2,
h3,
h4,
h5,
h6 {
  --nano-font-weight: 700;
}

h1 {
  --nano-font-size: 2rem;
  --nano-line-height: 1.25;
  --nano-typography-spacing-top: 3rem;
}

h2 {
  --nano-font-size: 1.75rem;
  --nano-line-height: 1.3;
  --nano-typography-spacing-top: 2.625rem;
}

h3 {
  --nano-font-size: 1.5rem;
  --nano-line-height: 1.35;
  --nano-typography-spacing-top: 2.25rem;
}

h4 {
  --nano-font-size: 1.25rem;
  --nano-line-height: 1.4;
  --nano-typography-spacing-top: 1.874rem;
}

h5 {
  --nano-font-size: 1.125rem;
  --nano-line-height: 1.45;
  --nano-typography-spacing-top: 1.6875rem;
}

h6 {
  --nano-font-size: 1rem;
  --nano-typography-spacing-top: 1.5rem;
}

thead th,
thead td,
tfoot th,
tfoot td {
  --nano-font-weight: 600;
  --nano-border-width: 0.1875rem;
}

pre,
code,
kbd,
samp {
  --nano-font-family: var(--nano-font-family-monospace);
}

kbd {
  --nano-font-weight: bolder;
}

input:not([type=submit],
[type=button],
[type=reset],
[type=checkbox],
[type=radio],
[type=file]),
:where(select, textarea) {
  --nano-outline-width: 0.0625rem;
}

[type=search] {
  --nano-border-radius: 5rem;
}

[type=checkbox],
[type=radio] {
  --nano-border-width: 0.125rem;
}

[type=checkbox][role=switch] {
  --nano-border-width: 0.1875rem;
}

details.dropdown summary:not([role=button]) {
  --nano-outline-width: 0.0625rem;
}

nav details.dropdown summary:focus-visible {
  --nano-outline-width: 0.1875rem;
}

[role=search] {
  --nano-border-radius: 5rem;
}

[role=search]:has(button.secondary:focus,
[type=submit].secondary:focus,
[type=button].secondary:focus,
[role=button].secondary:focus),
[role=group]:has(button.secondary:focus,
[type=submit].secondary:focus,
[type=button].secondary:focus,
[role=button].secondary:focus) {
  --nano-group-box-shadow-focus-with-button: 0 0 0 var(--nano-outline-width) var(--nano-secondary-focus);
}
[role=search]:has(button.contrast:focus,
[type=submit].contrast:focus,
[type=button].contrast:focus,
[role=button].contrast:focus),
[role=group]:has(button.contrast:focus,
[type=submit].contrast:focus,
[type=button].contrast:focus,
[role=button].contrast:focus) {
  --nano-group-box-shadow-focus-with-button: 0 0 0 var(--nano-outline-width) var(--nano-contrast-focus);
}
[role=search] button,
[role=search] [type=submit],
[role=search] [type=button],
[role=search] [role=button],
[role=group] button,
[role=group] [type=submit],
[role=group] [type=button],
[role=group] [role=button] {
  --nano-form-element-spacing-horizontal: 2rem;
}`,-1),E1=e("summary",{role:"button",class:"secondary"},`
          Default colors CSS variables
        `,-1),T1={class:"code","data-theme":"dark"},A1=e("pre",null,`[data-theme=light],
:root:not([data-theme=dark]) {
  --nano-background-color: #fff;
  --nano-color: #373c44;
  --nano-text-selection-color: rgba(2, 154, 232, 0.25);
  --nano-muted-color: #646b79;
  --nano-muted-border-color: #e7eaf0;
  --nano-primary: #0172ad;
  --nano-primary-background: #0172ad;
  --nano-primary-border: var(--nano-primary-background);
  --nano-primary-underline: rgba(1, 114, 173, 0.5);
  --nano-primary-hover: #015887;
  --nano-primary-hover-background: #02659a;
  --nano-primary-hover-border: var(--nano-primary-hover-background);
  --nano-primary-hover-underline: var(--nano-primary-hover);
  --nano-primary-focus: rgba(2, 154, 232, 0.5);
  --nano-primary-inverse: #fff;
  --nano-secondary: #5d6b89;
  --nano-secondary-background: #525f7a;
  --nano-secondary-border: var(--nano-secondary-background);
  --nano-secondary-underline: rgba(93, 107, 137, 0.5);
  --nano-secondary-hover: #48536b;
  --nano-secondary-hover-background: #48536b;
  --nano-secondary-hover-border: var(--nano-secondary-hover-background);
  --nano-secondary-hover-underline: var(--nano-secondary-hover);
  --nano-secondary-focus: rgba(93, 107, 137, 0.25);
  --nano-secondary-inverse: #fff;
  --nano-contrast: #181c25;
  --nano-contrast-background: #181c25;
  --nano-contrast-border: var(--nano-contrast-background);
  --nano-contrast-underline: rgba(24, 28, 37, 0.5);
  --nano-contrast-hover: #000;
  --nano-contrast-hover-background: #000;
  --nano-contrast-hover-border: var(--nano-contrast-hover-background);
  --nano-contrast-hover-underline: var(--nano-secondary-hover);
  --nano-contrast-focus: rgba(93, 107, 137, 0.25);
  --nano-contrast-inverse: #fff;
  --nano-box-shadow: 0.0145rem 0.029rem 0.174rem rgba(104, 120, 153, 0.01698), 0.0335rem 0.067rem 0.402rem rgba(104, 120, 153, 0.024), 0.0625rem 0.125rem 0.75rem rgba(104, 120, 153, 0.03), 0.1125rem 0.225rem 1.35rem rgba(104, 120, 153, 0.036), 0.2085rem 0.417rem 2.502rem rgba(104, 120, 153, 0.04302), 0.5rem 1rem 6rem rgba(104, 120, 153, 0.06), 0 0 0 0.0625rem rgba(104, 120, 153, 0.015);
  --nano-h1-color: #2d3138;
  --nano-h2-color: #373c44;
  --nano-h3-color: #424751;
  --nano-h4-color: #4d535e;
  --nano-h5-color: #5c6370;
  --nano-h6-color: #646b79;
  --nano-mark-background-color: #fde7c0;
  --nano-mark-color: #0f1114;
  --nano-ins-color: #1d6a54;
  --nano-del-color: #883935;
  --nano-blockquote-border-color: var(--nano-muted-border-color);
  --nano-blockquote-footer-color: var(--nano-muted-color);
  --nano-button-box-shadow: 0 0 0 rgba(0, 0, 0, 0);
  --nano-button-hover-box-shadow: 0 0 0 rgba(0, 0, 0, 0);
  --nano-table-border-color: var(--nano-muted-border-color);
  --nano-table-row-stripped-background-color: rgba(111, 120, 135, 0.0375);
  --nano-code-background-color: #f3f5f7;
  --nano-code-color: #646b79;
  --nano-code-kbd-background-color: var(--nano-color);
  --nano-code-kbd-color: var(--nano-background-color);
  --nano-form-element-background-color: #fbfcfc;
  --nano-form-element-selected-background-color: #dfe3eb;
  --nano-form-element-border-color: #cfd5e2;
  --nano-form-element-color: #23262c;
  --nano-form-element-placeholder-color: var(--nano-muted-color);
  --nano-form-element-active-background-color: #fff;
  --nano-form-element-active-border-color: var(--nano-primary-border);
  --nano-form-element-focus-color: var(--nano-primary-border);
  --nano-form-element-disabled-background-color: var(--nano-form-element-background-color);
  --nano-form-element-disabled-border-color: var(--nano-form-element-border-color);
  --nano-form-element-disabled-opacity: 0.5;
  --nano-form-element-invalid-border-color: #b86a6b;
  --nano-form-element-invalid-active-border-color: #c84f48;
  --nano-form-element-invalid-focus-color: var(--nano-form-element-invalid-active-border-color);
  --nano-form-element-valid-border-color: #4c9b8a;
  --nano-form-element-valid-active-border-color: #279977;
  --nano-form-element-valid-focus-color: var(--nano-form-element-valid-active-border-color);
  --nano-switch-background-color: #bfc7d9;
  --nano-switch-color: var(--nano-primary-inverse);
  --nano-switch-checked-background-color: var(--nano-primary-background);
  --nano-range-border-color: #dfe3eb;
  --nano-range-active-border-color: #bfc7d9;
  --nano-range-thumb-border-color: var(--nano-background-color);
  --nano-range-thumb-color: var(--nano-secondary-background);
  --nano-range-thumb-active-color: var(--nano-primary-background);
  --nano-accordion-border-color: var(--nano-muted-border-color);
  --nano-accordion-active-summary-color: var(--nano-primary-hover);
  --nano-accordion-close-summary-color: var(--nano-color);
  --nano-accordion-open-summary-color: var(--nano-muted-color);
  --nano-card-background-color: var(--nano-background-color);
  --nano-card-border-color: var(--nano-muted-border-color);
  --nano-card-box-shadow: var(--nano-box-shadow);
  --nano-card-sectioning-background-color: #fbfcfc;
  --nano-dropdown-background-color: #fff;
  --nano-dropdown-border-color: #eff1f4;
  --nano-dropdown-box-shadow: var(--nano-box-shadow);
  --nano-dropdown-color: var(--nano-color);
  --nano-dropdown-hover-background-color: #eff1f4;
  --nano-loading-spinner-opacity: 0.5;
  --nano-modal-overlay-background-color: rgba(232, 234, 237, 0.75);
  --nano-progress-background-color: #dfe3eb;
  --nano-progress-color: var(--nano-primary-background);
  --nano-tooltip-background-color: var(--nano-contrast-background);
  --nano-tooltip-color: var(--nano-contrast-inverse);
  --nano-icon-valid: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(29, 106, 84)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Cpolyline points='20 6 9 17 4 12'%3E%3C/polyline%3E%3C/svg%3E");
  --nano-icon-invalid: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(136, 57, 53)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Ccircle cx='12' cy='12' r='10'%3E%3C/circle%3E%3Cline x1='12' y1='8' x2='12' y2='12'%3E%3C/line%3E%3Cline x1='12' y1='16' x2='12.01' y2='16'%3E%3C/line%3E%3C/svg%3E");
  color-scheme: light;
}
[data-theme=light] input:is([type=submit],
[type=button],
[type=reset],
[type=checkbox],
[type=radio],
[type=file]),
:root:not([data-theme=dark]) input:is([type=submit],
[type=button],
[type=reset],
[type=checkbox],
[type=radio],
[type=file]) {
  --nano-form-element-focus-color: var(--nano-primary-focus);
}

@media only screen and (prefers-color-scheme: dark) {
  :root:not([data-theme]) {
    --nano-background-color: #13171f;
    --nano-color: #c2c7d0;
    --nano-text-selection-color: rgba(1, 170, 255, 0.1875);
    --nano-muted-color: #7b8495;
    --nano-muted-border-color: #202632;
    --nano-primary: #01aaff;
    --nano-primary-background: #0172ad;
    --nano-primary-border: var(--nano-primary-background);
    --nano-primary-underline: rgba(1, 170, 255, 0.5);
    --nano-primary-hover: #79c0ff;
    --nano-primary-hover-background: #017fc0;
    --nano-primary-hover-border: var(--nano-primary-hover-background);
    --nano-primary-hover-underline: var(--nano-primary-hover);
    --nano-primary-focus: rgba(1, 170, 255, 0.375);
    --nano-primary-inverse: #fff;
    --nano-secondary: #969eaf;
    --nano-secondary-background: #525f7a;
    --nano-secondary-border: var(--nano-secondary-background);
    --nano-secondary-underline: rgba(150, 158, 175, 0.5);
    --nano-secondary-hover: #b3b9c5;
    --nano-secondary-hover-background: #5d6b89;
    --nano-secondary-hover-border: var(--nano-secondary-hover-background);
    --nano-secondary-hover-underline: var(--nano-secondary-hover);
    --nano-secondary-focus: rgba(144, 158, 190, 0.25);
    --nano-secondary-inverse: #fff;
    --nano-contrast: #dfe3eb;
    --nano-contrast-background: #eff1f4;
    --nano-contrast-border: var(--nano-contrast-background);
    --nano-contrast-underline: rgba(223, 227, 235, 0.5);
    --nano-contrast-hover: #fff;
    --nano-contrast-hover-background: #fff;
    --nano-contrast-hover-border: var(--nano-contrast-hover-background);
    --nano-contrast-hover-underline: var(--nano-contrast-hover);
    --nano-contrast-focus: rgba(207, 213, 226, 0.25);
    --nano-contrast-inverse: #000;
    --nano-box-shadow: 0.0145rem 0.029rem 0.174rem rgba(0, 0, 0, 0.01698), 0.0335rem 0.067rem 0.402rem rgba(0, 0, 0, 0.024), 0.0625rem 0.125rem 0.75rem rgba(0, 0, 0, 0.03), 0.1125rem 0.225rem 1.35rem rgba(0, 0, 0, 0.036), 0.2085rem 0.417rem 2.502rem rgba(0, 0, 0, 0.04302), 0.5rem 1rem 6rem rgba(0, 0, 0, 0.06), 0 0 0 0.0625rem rgba(0, 0, 0, 0.015);
    --nano-h1-color: #f0f1f3;
    --nano-h2-color: #e0e3e7;
    --nano-h3-color: #c2c7d0;
    --nano-h4-color: #b3b9c5;
    --nano-h5-color: #a4acba;
    --nano-h6-color: #8891a4;
    --nano-mark-background-color: #014063;
    --nano-mark-color: #fff;
    --nano-ins-color: #62af9a;
    --nano-del-color: #ce7e7b;
    --nano-blockquote-border-color: var(--nano-muted-border-color);
    --nano-blockquote-footer-color: var(--nano-muted-color);
    --nano-button-box-shadow: 0 0 0 rgba(0, 0, 0, 0);
    --nano-button-hover-box-shadow: 0 0 0 rgba(0, 0, 0, 0);
    --nano-table-border-color: var(--nano-muted-border-color);
    --nano-table-row-stripped-background-color: rgba(111, 120, 135, 0.0375);
    --nano-code-background-color: #1a1f28;
    --nano-code-color: #8891a4;
    --nano-code-kbd-background-color: var(--nano-color);
    --nano-code-kbd-color: var(--nano-background-color);
    --nano-form-element-background-color: #1c212c;
    --nano-form-element-selected-background-color: #2a3140;
    --nano-form-element-border-color: #2a3140;
    --nano-form-element-color: #e0e3e7;
    --nano-form-element-placeholder-color: #8891a4;
    --nano-form-element-active-background-color: #1a1f28;
    --nano-form-element-active-border-color: var(--nano-primary-border);
    --nano-form-element-focus-color: var(--nano-primary-border);
    --nano-form-element-disabled-background-color: var(--nano-form-element-background-color);
    --nano-form-element-disabled-border-color: var(--nano-form-element-border-color);
    --nano-form-element-disabled-opacity: 0.5;
    --nano-form-element-invalid-border-color: #964a50;
    --nano-form-element-invalid-active-border-color: #b7403b;
    --nano-form-element-invalid-focus-color: var(--nano-form-element-invalid-active-border-color);
    --nano-form-element-valid-border-color: #2a7b6f;
    --nano-form-element-valid-active-border-color: #16896a;
    --nano-form-element-valid-focus-color: var(--nano-form-element-valid-active-border-color);
    --nano-switch-background-color: #333c4e;
    --nano-switch-color: var(--nano-primary-inverse);
    --nano-switch-checked-background-color: var(--nano-primary-background);
    --nano-range-border-color: #202632;
    --nano-range-active-border-color: #2a3140;
    --nano-range-thumb-border-color: var(--nano-background-color);
    --nano-range-thumb-color: var(--nano-secondary-background);
    --nano-range-thumb-active-color: var(--nano-primary-background);
    --nano-accordion-border-color: var(--nano-muted-border-color);
    --nano-accordion-active-summary-color: var(--nano-primary-hover);
    --nano-accordion-close-summary-color: var(--nano-color);
    --nano-accordion-open-summary-color: var(--nano-muted-color);
    --nano-card-background-color: #181c25;
    --nano-card-border-color: var(--nano-card-background-color);
    --nano-card-box-shadow: var(--nano-box-shadow);
    --nano-card-sectioning-background-color: #1a1f28;
    --nano-dropdown-background-color: #181c25;
    --nano-dropdown-border-color: #202632;
    --nano-dropdown-box-shadow: var(--nano-box-shadow);
    --nano-dropdown-color: var(--nano-color);
    --nano-dropdown-hover-background-color: #202632;
    --nano-loading-spinner-opacity: 0.5;
    --nano-modal-overlay-background-color: rgba(8, 9, 10, 0.75);
    --nano-progress-background-color: #202632;
    --nano-progress-color: var(--nano-primary-background);
    --nano-tooltip-background-color: var(--nano-contrast-background);
    --nano-tooltip-color: var(--nano-contrast-inverse);
    --nano-icon-valid: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(98, 175, 154)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Cpolyline points='20 6 9 17 4 12'%3E%3C/polyline%3E%3C/svg%3E");
    --nano-icon-invalid: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(206, 126, 123)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Ccircle cx='12' cy='12' r='10'%3E%3C/circle%3E%3Cline x1='12' y1='8' x2='12' y2='12'%3E%3C/line%3E%3Cline x1='12' y1='16' x2='12.01' y2='16'%3E%3C/line%3E%3C/svg%3E");
    color-scheme: dark;
  }
  :root:not([data-theme]) input:is([type=submit],
  [type=button],
  [type=reset],
  [type=checkbox],
  [type=radio],
  [type=file]) {
    --nano-form-element-focus-color: var(--nano-primary-focus);
  }
  :root:not([data-theme]) details summary[role=button].contrast:not(.outline)::after {
    filter: brightness(0);
  }
  :root:not([data-theme]) [aria-busy=true]:not(input, select, textarea).contrast:is(button,
  [type=submit],
  [type=button],
  [type=reset],
  [role=button]):not(.outline)::before {
    filter: brightness(0);
  }
}
[data-theme=dark] {
  --nano-background-color: #13171f;
  --nano-color: #c2c7d0;
  --nano-text-selection-color: rgba(1, 170, 255, 0.1875);
  --nano-muted-color: #7b8495;
  --nano-muted-border-color: #202632;
  --nano-primary: #01aaff;
  --nano-primary-background: #0172ad;
  --nano-primary-border: var(--nano-primary-background);
  --nano-primary-underline: rgba(1, 170, 255, 0.5);
  --nano-primary-hover: #79c0ff;
  --nano-primary-hover-background: #017fc0;
  --nano-primary-hover-border: var(--nano-primary-hover-background);
  --nano-primary-hover-underline: var(--nano-primary-hover);
  --nano-primary-focus: rgba(1, 170, 255, 0.375);
  --nano-primary-inverse: #fff;
  --nano-secondary: #969eaf;
  --nano-secondary-background: #525f7a;
  --nano-secondary-border: var(--nano-secondary-background);
  --nano-secondary-underline: rgba(150, 158, 175, 0.5);
  --nano-secondary-hover: #b3b9c5;
  --nano-secondary-hover-background: #5d6b89;
  --nano-secondary-hover-border: var(--nano-secondary-hover-background);
  --nano-secondary-hover-underline: var(--nano-secondary-hover);
  --nano-secondary-focus: rgba(144, 158, 190, 0.25);
  --nano-secondary-inverse: #fff;
  --nano-contrast: #dfe3eb;
  --nano-contrast-background: #eff1f4;
  --nano-contrast-border: var(--nano-contrast-background);
  --nano-contrast-underline: rgba(223, 227, 235, 0.5);
  --nano-contrast-hover: #fff;
  --nano-contrast-hover-background: #fff;
  --nano-contrast-hover-border: var(--nano-contrast-hover-background);
  --nano-contrast-hover-underline: var(--nano-contrast-hover);
  --nano-contrast-focus: rgba(207, 213, 226, 0.25);
  --nano-contrast-inverse: #000;
  --nano-box-shadow: 0.0145rem 0.029rem 0.174rem rgba(0, 0, 0, 0.01698), 0.0335rem 0.067rem 0.402rem rgba(0, 0, 0, 0.024), 0.0625rem 0.125rem 0.75rem rgba(0, 0, 0, 0.03), 0.1125rem 0.225rem 1.35rem rgba(0, 0, 0, 0.036), 0.2085rem 0.417rem 2.502rem rgba(0, 0, 0, 0.04302), 0.5rem 1rem 6rem rgba(0, 0, 0, 0.06), 0 0 0 0.0625rem rgba(0, 0, 0, 0.015);
  --nano-h1-color: #f0f1f3;
  --nano-h2-color: #e0e3e7;
  --nano-h3-color: #c2c7d0;
  --nano-h4-color: #b3b9c5;
  --nano-h5-color: #a4acba;
  --nano-h6-color: #8891a4;
  --nano-mark-background-color: #014063;
  --nano-mark-color: #fff;
  --nano-ins-color: #62af9a;
  --nano-del-color: #ce7e7b;
  --nano-blockquote-border-color: var(--nano-muted-border-color);
  --nano-blockquote-footer-color: var(--nano-muted-color);
  --nano-button-box-shadow: 0 0 0 rgba(0, 0, 0, 0);
  --nano-button-hover-box-shadow: 0 0 0 rgba(0, 0, 0, 0);
  --nano-table-border-color: var(--nano-muted-border-color);
  --nano-table-row-stripped-background-color: rgba(111, 120, 135, 0.0375);
  --nano-code-background-color: #1a1f28;
  --nano-code-color: #8891a4;
  --nano-code-kbd-background-color: var(--nano-color);
  --nano-code-kbd-color: var(--nano-background-color);
  --nano-form-element-background-color: #1c212c;
  --nano-form-element-selected-background-color: #2a3140;
  --nano-form-element-border-color: #2a3140;
  --nano-form-element-color: #e0e3e7;
  --nano-form-element-placeholder-color: #8891a4;
  --nano-form-element-active-background-color: #1a1f28;
  --nano-form-element-active-border-color: var(--nano-primary-border);
  --nano-form-element-focus-color: var(--nano-primary-border);
  --nano-form-element-disabled-background-color: var(--nano-form-element-background-color);
  --nano-form-element-disabled-border-color: var(--nano-form-element-border-color);
  --nano-form-element-disabled-opacity: 0.5;
  --nano-form-element-invalid-border-color: #964a50;
  --nano-form-element-invalid-active-border-color: #b7403b;
  --nano-form-element-invalid-focus-color: var(--nano-form-element-invalid-active-border-color);
  --nano-form-element-valid-border-color: #2a7b6f;
  --nano-form-element-valid-active-border-color: #16896a;
  --nano-form-element-valid-focus-color: var(--nano-form-element-valid-active-border-color);
  --nano-switch-background-color: #333c4e;
  --nano-switch-color: var(--nano-primary-inverse);
  --nano-switch-checked-background-color: var(--nano-primary-background);
  --nano-range-border-color: #202632;
  --nano-range-active-border-color: #2a3140;
  --nano-range-thumb-border-color: var(--nano-background-color);
  --nano-range-thumb-color: var(--nano-secondary-background);
  --nano-range-thumb-active-color: var(--nano-primary-background);
  --nano-accordion-border-color: var(--nano-muted-border-color);
  --nano-accordion-active-summary-color: var(--nano-primary-hover);
  --nano-accordion-close-summary-color: var(--nano-color);
  --nano-accordion-open-summary-color: var(--nano-muted-color);
  --nano-card-background-color: #181c25;
  --nano-card-border-color: var(--nano-card-background-color);
  --nano-card-box-shadow: var(--nano-box-shadow);
  --nano-card-sectioning-background-color: #1a1f28;
  --nano-dropdown-background-color: #181c25;
  --nano-dropdown-border-color: #202632;
  --nano-dropdown-box-shadow: var(--nano-box-shadow);
  --nano-dropdown-color: var(--nano-color);
  --nano-dropdown-hover-background-color: #202632;
  --nano-loading-spinner-opacity: 0.5;
  --nano-modal-overlay-background-color: rgba(8, 9, 10, 0.75);
  --nano-progress-background-color: #202632;
  --nano-progress-color: var(--nano-primary-background);
  --nano-tooltip-background-color: var(--nano-contrast-background);
  --nano-tooltip-color: var(--nano-contrast-inverse);
  --nano-icon-valid: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(98, 175, 154)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Cpolyline points='20 6 9 17 4 12'%3E%3C/polyline%3E%3C/svg%3E");
  --nano-icon-invalid: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(206, 126, 123)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Ccircle cx='12' cy='12' r='10'%3E%3C/circle%3E%3Cline x1='12' y1='8' x2='12' y2='12'%3E%3C/line%3E%3Cline x1='12' y1='16' x2='12.01' y2='16'%3E%3C/line%3E%3C/svg%3E");
  color-scheme: dark;
}
[data-theme=dark] input:is([type=submit],
[type=button],
[type=reset],
[type=checkbox],
[type=radio],
[type=file]) {
  --nano-form-element-focus-color: var(--nano-primary-focus);
}
[data-theme=dark] details summary[role=button].contrast:not(.outline)::after {
  filter: brightness(0);
}
[data-theme=dark] [aria-busy=true]:not(input, select, textarea).contrast:is(button,
[type=submit],
[type=button],
[type=reset],
[role=button]):not(.outline)::before {
  filter: brightness(0);
}

progress,
[type=checkbox],
[type=radio],
[type=range] {
  accent-color: var(--nano-primary);
}`,-1),M1={__name:"CssVariablesView",setup(t){return(o,a)=>(C(),S(J,null,[Qg,n(),e1,n(),e("div",t1,[n1,n(),e("section",null,[o1,n(),e("article",a1,[l1,n(),s1,n(),r1,n(),e("footer",i1,[f(k,null,{default:v(()=>[c1]),_:1})])])]),n(),e("section",null,[d1,n(),u1,n(),e("div",h1,[f(k,null,{default:v(()=>[p1]),_:1})]),n(),f1,n(),m1,n(),b1,n(),e("div",g1,[f(k,null,{default:v(()=>[_1]),_:1})]),n(),v1]),n(),e("section",null,[y1,n(),w1,n(),x1,n(),k1,n(),e("details",null,[$1,n(),e("div",C1,[f(k,null,{default:v(()=>[S1]),_:1})])]),n(),e("details",null,[E1,n(),e("div",T1,[f(k,null,{default:v(()=>[A1]),_:1})])])])])],64))}},P1=e("hgroup",null,[e("p",{class:"chapter"},`
      Components
    `),n(),e("h1",null,"Dropdown"),n(),e("p",null,"Create dropdown menus and custom selects with minimal and semantic HTML, without JavaScript.")],-1),N1=G('<aside id="table-of-contents"><nav><details open=""><summary>Content</summary> <ul><li><a class="secondary" href="/docs/dropdown" aria-current="page">Syntax</a></li> <li><a class="secondary" href="/docs/dropdown#dropdowns-with-checkboxes-and-radios">Checkboxes and radios</a></li> <li><a class="secondary" href="/docs/dropdown#button-variants">Button variants</a></li> <li><a class="secondary" href="/docs/dropdown#validation-states">Validation states</a></li> <li><a class="secondary" href="/docs/dropdown#usage-with-nav">Usage with nav</a></li></ul></details></nav></aside>',1),O1={role:"document"},R1=e("code",null,'<details class="dropdown">',-1),I1=e("code",null,"<summary>",-1),L1=e("code",null,"<ul>",-1),B1=e("code",null,"width: 100%;",-1),D1=e("p",null,"For style consistency with the form elements, dropdowns are styled like a select by default.",-1),z1={"aria-label":"Dropdowns as selects",class:"component"},V1=G(`<div class="grid"><details class="dropdown"><summary>Dropdown</summary> <ul><li><a href="/docs/dropdown">Solid</a></li> <li><a href="/docs/dropdown">Liquid</a></li> <li><a href="/docs/dropdown">Gas</a></li> <li><a href="/docs/dropdown">Plasma</a></li></ul></details> <select required=""><option value="" disabled="" selected="">
              Select
            </option> <option>Solid</option> <option>Liquid</option> <option>Gas</option> <option>Plasma</option></select></div>`,1),j1={class:"code","data-theme":"dark"},F1=e("pre",null,`<!-- Dropdown -->
<details class="dropdown">
  <summary>Dropdown</summary>
  <ul>
    <li><a href="#">Solid</a></li>
    <li><a href="#">Liquid</a></li>
    <li><a href="#">Gas</a></li>
    <li><a href="#">Plasma</a></li>
  </ul>
</details>

<!-- Select -->
<select required>
  <option selected disabled value="">Select</option>
  <option>Solid</option>
  <option>Liquid</option>
  <option>Gas</option>
  <option>Plasma</option>false
</select>`,-1),H1=e("h2",null,[n(`
        Dropdowns with checkboxes and radios`),e("a",{id:"dropdowns-with-checkboxes-and-radios",href:"#dropdowns-with-checkboxes-and-radios",class:"secondary",tabindex:"-1"},"#")],-1),q1=e("p",null,[n(`
        Dropdowns can be used as custom selects with `),e("code",null,'<input type="radio">'),n(` or
        `),e("code",null,'<input type="checkbox">'),n(`.
      `)],-1),U1={"aria-label":"Dropdowns with radio buttons or checkboxes",class:"component"},G1=G('<details class="dropdown"><summary>Select a phase of matter...</summary> <ul><li><label><input type="radio" name="phase" value="Solid">Solid</label></li> <li><label><input type="radio" name="phase" value="Liquid">Liquid</label></li> <li><label><input type="radio" name="phase" value="Gas">Gas</label></li> <li><label><input type="radio" name="phase" value="Plasma">Plasma</label></li></ul></details>',1),W1=G('<details class="dropdown"><summary>Select phases of matter...</summary> <ul><li><label><input type="checkbox" name="solid" value="Solid">Solid</label></li> <li><label><input type="checkbox" name="liquid" value="Liquid">Liquid</label></li> <li><label><input type="checkbox" name="gas" value="Gas">Gas</label></li> <li><label><input type="checkbox" name="plasma" value="Plasma">Plasma</label></li></ul></details>',1),K1={class:"code","data-theme":"dark"},Y1=e("pre",null,`<!-- Dropdown -->
<details class="dropdown">
  <summary>Dropdown</summary>
  <ul>
    <li><a href="#">Solid</a></li>
    <li><a href="#">Liquid</a></li>
    <li><a href="#">Gas</a></li>
    <li><a href="#">Plasma</a></li>
  </ul>
</details>

<!-- Select -->
<select required>
  <option selected disabled value="">Select</option>
  <option>Solid</option>
  <option>Liquid</option>
  <option>Gas</option>
  <option>Plasma</option>false
</select>`,-1),J1=e("p",null,`
        Pico does not include JavaScript code. You will probably need some JavaScript to interact with these custom
        dropdowns.
      `,-1),Z1=e("h2",null,[n(`
        Button variants`),e("a",{id:"button-variants",href:"#button-variants",class:"secondary",tabindex:"-1"},"#")],-1),X1=e("p",null,[e("code",null,'<summary role="button">'),n(" transforms the dropdown into a button.")],-1),Q1={"aria-label":"Dropdowns as buttons",class:"component"},e_=e("details",{class:"dropdown"},[e("summary",{role:"button"},`
            Dropdown as a button
          `),n(),e("ul",null,[e("li",null,[e("a",{href:"/docs/dropdown"},"Solid")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Liquid")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Gas")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Plasma")])])],-1),t_={class:"code","data-theme":"dark"},n_=e("pre",null,`<!-- Dropdown -->
<details class="dropdown">
  <summary role="button">Dropdown as a button</summary>
  <ul>
    <li><a href="#">Solid</a></li>
    <li><a href="#">Liquid</a></li>
    <li><a href="#">Gas</a></li>
    <li><a href="#">Plasma</a></li>
  </ul>
</details>

<!-- Select -->
<select required>
  <option selected disabled value="">Select</option>
  <option>Solid</option>
  <option>Liquid</option>
  <option>Gas</option>
  <option>Plasma</option>false
</select>`,-1),o_=e("p",null,[n(`
        Like regular buttons, they come with `),e("code",null,".secondary"),n(", "),e("code",null,".contrast"),n(", and "),e("code",null,".outline"),n(`.
      `)],-1),a_={"aria-label":"Dropdowns as buttons",class:"component"},l_=e("details",{class:"dropdown"},[e("summary",{role:"button"},`
            Primary
          `),n(),e("ul",null,[e("li",null,[e("a",{href:"/docs/dropdown"},"Solid")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Liquid")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Gas")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Plasma")])])],-1),s_=e("details",{class:"dropdown"},[e("summary",{role:"button",class:"secondary"},`
            Secondary
          `),n(),e("ul",null,[e("li",null,[e("a",{href:"/docs/dropdown"},"Solid")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Liquid")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Gas")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Plasma")])])],-1),r_=e("details",{class:"dropdown"},[e("summary",{role:"button",class:"contrast"},`
            Contrast
          `),n(),e("ul",null,[e("li",null,[e("a",{href:"/docs/dropdown"},"Solid")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Liquid")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Gas")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Plasma")])])],-1),i_=e("details",{class:"dropdown"},[e("summary",{role:"button",class:"outline"},`
            Primary outline
          `),n(),e("ul",null,[e("li",null,[e("a",{href:"/docs/dropdown"},"Solid")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Liquid")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Gas")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Plasma")])])],-1),c_=e("details",{class:"dropdown"},[e("summary",{role:"button",class:"outline secondary"},`
            Secondary outline
          `),n(),e("ul",null,[e("li",null,[e("a",{href:"/docs/dropdown"},"Solid")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Liquid")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Gas")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Plasma")])])],-1),d_=e("details",{class:"dropdown"},[e("summary",{role:"button",class:"outline contrast"},`
            Contrast outline
          `),n(),e("ul",null,[e("li",null,[e("a",{href:"/docs/dropdown"},"Solid")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Liquid")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Gas")]),n(),e("li",null,[e("a",{href:"/docs/dropdown"},"Plasma")])])],-1),u_={class:"code","data-theme":"dark"},h_=e("pre",null,`<!-- Dropdown -->
<details class="dropdown">
  <summary>Dropdown</summary>
  <ul>
    <li><a href="#">Solid</a></li>
    <li><a href="#">Liquid</a></li>
    <li><a href="#">Gas</a></li>
    <li><a href="#">Plasma</a></li>
  </ul>
</details>

<!-- Select -->
<select required>
  <option selected disabled value="">Select</option>
  <option>Solid</option>
  <option>Liquid</option>
  <option>Gas</option>
  <option>Plasma</option>false
</select>`,-1),p_=e("h2",null,[n(`
        Validation states`),e("a",{id:"validation-states",href:"#validation-states",class:"secondary",tabindex:"-1"},"#")],-1),f_=e("p",null,[n("Just like any form elements, validation states are provided with "),e("code",null,"aria-invalid"),n(".")],-1),m_={"aria-label":"Dropdowns with validation states",class:"component"},b_=G(`<details class="dropdown"><summary aria-invalid="false">
            Valid phase of matter: Solid
          </summary> <ul><li><a href="/docs/dropdown">Solid</a></li> <li><a href="/docs/dropdown">Liquid</a></li> <li><a href="/docs/dropdown">Gas</a></li> <li><a href="/docs/dropdown">Plasma</a></li></ul></details>`,1),g_=G(`<details class="dropdown"><summary aria-invalid="true">
            Debated classification: Plasma
          </summary> <ul><li><a href="/docs/dropdown">Solid</a></li> <li><a href="/docs/dropdown">Liquid</a></li> <li><a href="/docs/dropdown">Gas</a></li> <li><a href="/docs/dropdown">Plasma</a></li></ul></details>`,1),__={class:"code","data-theme":"dark"},v_=e("pre",null,`<!-- Dropdown -->
<details class="dropdown">
  <summary>Dropdown</summary>
  <ul>
    <li><a href="#">Solid</a></li>
    <li><a href="#">Liquid</a></li>
    <li><a href="#">Gas</a></li>
    <li><a href="#">Plasma</a></li>
  </ul>
</details>

<!-- Select -->
<select required>
  <option selected disabled value="">Select</option>
  <option>Solid</option>
  <option>Liquid</option>
  <option>Gas</option>
  <option>Plasma</option>false
</select>`,-1),y_=e("h2",null,[n(`
        Usage with `),e("code",null,"<nav>"),e("a",{id:"usage-with-nav",href:"#usage-with-nav",class:"secondary",tabindex:"-1"},"#")],-1),w_=e("p",null,[n("You can use dropdowns inside "),e("a",{href:"/docs/nav"},"Nav"),n(".")],-1),x_=e("p",null,[n("To change the alignment of the submenu, simply use "),e("code",null,'<ul dir="rtl">'),n(".")],-1),k_={"aria-label":"Dropdowns inside nav",class:"component"},$_=G('<nav><ul><li><strong>Acme Corp</strong></li></ul> <ul><li><a class="secondary" href="/docs/dropdown">Services</a></li> <li><details class="dropdown"><summary>Account</summary> <ul dir="rtl"><li><a href="/docs/dropdown">Profile</a></li> <li><a href="/docs/dropdown">Settings</a></li> <li><a href="/docs/dropdown">Security</a></li> <li><a href="/docs/dropdown">Logout</a></li></ul></details></li></ul></nav>',1),C_={class:"code","data-theme":"dark"},S_=e("pre",null,`<!-- Dropdown -->
<details class="dropdown">
  <summary>Dropdown</summary>
  <ul>
    <li><a href="#">Solid</a></li>
    <li><a href="#">Liquid</a></li>
    <li><a href="#">Gas</a></li>
    <li><a href="#">Plasma</a></li>
  </ul>
</details>

<!-- Select -->
<select required>
  <option selected disabled value="">Select</option>
  <option>Solid</option>
  <option>Liquid</option>
  <option>Gas</option>
  <option>Plasma</option>false
</select>`,-1),E_={__name:"DropdownView",setup(t){return(o,a)=>(C(),S(J,null,[P1,n(),N1,n(),e("div",O1,[e("section",null,[e("p",null,[n(`
        Dropdowns are built with `),R1,n(` as a wrapper and
        `),I1,n(" and "),L1,n(` as direct childrens. Unless they are in a
        `),f($(_e),{to:"/docs/nav"},{default:v(()=>[n(`
          Nav
        `)]),_:1}),n(", dropdowns are "),B1,n(` by default.
      `)]),n(),D1,n(),e("article",z1,[V1,n(),e("footer",j1,[f(k,null,{default:v(()=>[F1]),_:1})])])]),n(),e("section",null,[H1,n(),q1,n(),e("article",U1,[G1,n(),W1,n(),e("footer",K1,[f(k,null,{default:v(()=>[Y1]),_:1})])]),n(),J1]),n(),e("section",null,[Z1,n(),X1,n(),e("article",Q1,[e_,n(),e("footer",t_,[f(k,null,{default:v(()=>[n_]),_:1})])]),n(),o_,n(),e("article",a_,[l_,n(),s_,n(),r_,n(),i_,n(),c_,n(),d_,n(),e("footer",u_,[f(k,null,{default:v(()=>[h_]),_:1})])])]),n(),e("section",null,[p_,n(),f_,n(),e("article",m_,[b_,n(),g_,n(),e("footer",__,[f(k,null,{default:v(()=>[v_]),_:1})])])]),n(),e("section",null,[y_,n(),w_,n(),x_,n(),e("article",k_,[$_,n(),e("footer",C_,[f(k,null,{default:v(()=>[S_]),_:1})])])])])],64))}},T_=e("hgroup",null,[e("p",{class:"chapter"},`
      Forms
    `),n(),e("h1",null,"Checkboxes"),n(),e("p",null,[n("The native "),e("code",null,'<input type="checkbox">'),n(" with a custom and responsive style.")])],-1),A_=G('<aside id="table-of-contents"><nav><details open=""><summary>Content</summary> <ul><li><a class="secondary" href="/docs/forms/checkboxes" aria-current="page">Syntax</a></li> <li><a class="secondary" href="/docs/forms/checkboxes#horizontal-stacking">Horizontal Stacking</a></li> <li><a class="secondary" href="/docs/forms/checkboxes#indeterminate">Indeterminate</a></li> <li><a class="secondary" href="/docs/forms/checkboxes#validation-states">Validation States</a></li></ul></details></nav></aside>',1),M_={role:"document"},P_={"aria-label":"Checkboxes example",class:"component"},N_=G('<fieldset><legend>Language preferences:</legend> <label><input type="checkbox" name="english" checked="">English</label><label><input type="checkbox" name="french" checked="">French</label><label><input type="checkbox" name="mandarin">Mandarin</label><label><input type="checkbox" name="thai">Thai</label><label aria-disabled="true"><input type="checkbox" name="dothraki" disabled="">Dothraki</label></fieldset>',1),O_={class:"code","data-theme":"dark"},R_=e("pre",null,`<fieldset>
  <legend>Language preferences:</legend>
  <label>
    <input type="checkbox" name="english" checked />
    English
  </label>
  <label>
    <input type="checkbox" name="french" checked />
    French
  </label>
  <label>
    <input type="checkbox" name="mandarin" />
    Mandarin
  </label>
  <label>
    <input type="checkbox" name="thai" />
    Thai
  </label>
  <label aria-disabled="true">
    <input type="checkbox" name="dothraki" disabled />
    Dothraki
  </label>
</fieldset>`,-1),I_=e("h2",null,[n(`
        Horizontal Stacking`),e("a",{id:"horizontal-stacking",href:"#horizontal-stacking",class:"secondary",tabindex:"-1"},"#")],-1),L_={"aria-label":"Horizontal stacking example",class:"component"},B_=G('<fieldset><legend>Language preferences:</legend> <input id="hindi" type="checkbox" name="hindi" checked=""><label for="hindi">Hindi</label><input id="swahili" type="checkbox" name="swahili"><label for="swahili">Swahili</label><input id="navi " type="checkbox" name="navi" disabled=""><label for="navi" aria-disabled="true">Na&#39;vi</label></fieldset>',1),D_={class:"code","data-theme":"dark"},z_=e("pre",null,`<fieldset>
  <legend>Language preferences:</legend>
  <input type="checkbox" id="hindi" name="hindi" checked />
  <label htmlFor="hindi">Hindi</label>
  <input type="checkbox" id="swahili" name="swahili" />
  <label htmlFor="swahili">Swahili</label>
  <input type="checkbox" id="navi " name="navi" disabled />
  <label htmlFor="navi" aria-disabled="true">
</fieldset>`,-1),V_=e("h2",null,[n(`
        Indeterminate`),e("a",{id:"indeterminate",href:"#indeterminate",class:"secondary",tabindex:"-1"},"#")],-1),j_=e("p",null,[n(`
        You can change a checkbox to an indeterminate state by setting the `),e("code",null,"indeterminate"),n(` property to
        `),e("code",null,"true"),n(`.
      `)],-1),F_={"aria-label":"Indeterminate checkbox example",class:"component"},H_={class:"code","data-theme":"dark"},q_=e("pre",null,`<label>
  <input type="checkbox" id="indeterminate" name="indeterminate" />
  Indeterminate
</label>

<script>
  const checkbox = document.querySelector('#indeterminate');
  checkbox.indeterminate = true;
<\/script>`,-1),U_=e("h2",null,[n(`
        Validation States`),e("a",{id:"validation-states",href:"#validation-states",class:"secondary",tabindex:"-1"},"#")],-1),G_=e("p",null,[n("Validation states are provided with "),e("code",null,[e("span",{class:""},"aria-invalid")]),n(".")],-1),W_={"aria-label":"Validation states example",class:"component"},K_=e("label",null,[e("input",{type:"checkbox",name:"valid","aria-invalid":"false"}),n("Valid")],-1),Y_=e("label",null,[e("input",{type:"checkbox",name:"invalid","aria-invalid":"true"}),n("Invalid")],-1),J_={class:"code","data-theme":"dark"},Z_=e("pre",null,`<label>
  <input type="checkbox" name="valid" aria-invalid="false" />
  Valid
</label>

<label>
  <input type="checkbox" name="invalid" aria-invalid="true" />
  Invalid
</label>`,-1),X_={__name:"FormCheckboxesView",setup(t){const o=Ce(null);return uo(()=>{o.value.indeterminate=!0}),(a,l)=>(C(),S(J,null,[T_,n(),A_,n(),e("div",M_,[e("section",null,[e("article",P_,[N_,n(),e("footer",O_,[f(k,null,{default:v(()=>[R_]),_:1})])])]),n(),e("section",null,[I_,n(),e("article",L_,[B_,n(),e("footer",D_,[f(k,null,{default:v(()=>[z_]),_:1})])])]),n(),e("section",null,[V_,n(),j_,n(),e("article",F_,[e("label",null,[e("input",{ref_key:"checkbox",ref:o,type:"checkbox",name:"indeterminate"},null,512),n("Indeterminate")]),n(),e("footer",H_,[f(k,null,{default:v(()=>[q_]),_:1})])])]),n(),e("section",null,[U_,n(),G_,n(),e("article",W_,[K_,Y_,n(),e("footer",J_,[f(k,null,{default:v(()=>[Z_]),_:1})])])])])],64))}},Q_=e("hgroup",null,[e("p",{class:"chapter"},`
      Forms
    `),n(),e("h1",null,"Input"),n(),e("p",null,"All input types are consistently styled and come with validation states.")],-1),e0=G('<aside id="table-of-contents"><nav><details open=""><summary>Content</summary> <ul><li><a class="secondary" href="/docs/forms/input" aria-current="page">Syntax</a></li> <li><a class="secondary" href="/docs/forms/input#datetime">Datetime</a></li> <li><a class="secondary" href="/docs/forms/input#search">Search</a></li> <li><a class="secondary" href="/docs/forms/input#color">Color</a></li> <li><a class="secondary" href="/docs/forms/input#file">File</a></li> <li><a class="secondary" href="/docs/forms/input#disabled">Disabled</a></li> <li><a class="secondary" href="/docs/forms/input#readonly">Readonly</a></li> <li><a class="secondary" href="/docs/forms/input#validation-states">Validation States</a></li></ul></details></nav></aside>',1),t0={role:"document"},n0={"aria-label":"Input examples",class:"component"},o0=G('<input type="text" placeholder="Text" aria-label="Text"><input type="email" placeholder="Email" aria-label="Email"><input type="number" placeholder="Number" aria-label="Number"><input type="password" placeholder="Password" aria-label="Password"><input type="tel" placeholder="Tel" aria-label="Tel"><input type="url" placeholder="Url" aria-label="Url">',6),a0={class:"code","data-theme":"dark"},l0=e("pre",null,`<input type="text" placeholder="Text" aria-label="Text" />
<input type="email" placeholder="Email" aria-label="Email" />
<input type="number" placeholder="Number" aria-label="Number" />
<input type="password" placeholder="Password" aria-label="Password" />
<input type="tel" placeholder="Tel" aria-label="Tel" />
<input type="url" placeholder="Url" aria-label="Url" />`,-1),s0=e("h2",null,[n(`
        Datetime Input`),e("a",{id:"datetime",href:"#datetime",class:"secondary",tabindex:"-1"},"#")],-1),r0=e("p",null,"Datetime inputs come with an icon.",-1),i0={"aria-label":"Datetime input examples",class:"component"},c0=e("input",{type:"date","aria-label":"Date"},null,-1),d0=e("input",{type:"datetime-local","aria-label":"Datetime local"},null,-1),u0=e("input",{type:"month","aria-label":"Month"},null,-1),h0=e("input",{type:"time","aria-label":"Time"},null,-1),p0={class:"code","data-theme":"dark"},f0=e("pre",null,`<input type="date" aria-label="Date" />
<input type="datetime-local" aria-label="Datetime local" />
<input type="month" aria-label="Month" />
<input type="time" aria-label="Time" />`,-1),m0=e("h2",null,[n(`
        Search Input`),e("a",{id:"search",href:"#search",class:"secondary",tabindex:"-1"},"#")],-1),b0=e("p",null,[e("code",null,[e("span",{class:""},'type="search"')]),n(" comes with a distinctive style.")],-1),g0={"aria-label":"Search input example",class:"component"},_0=e("input",{type:"search",placeholder:"Search","aria-label":"Search"},null,-1),v0={class:"code","data-theme":"dark"},y0=e("pre",null,`<input
  type="search"
  placeholder="Search"
  aria-label="Search"
/>`,-1),w0=e("h2",null,[n(`
        Color Input`),e("a",{id:"color",href:"#color",class:"secondary",tabindex:"-1"},"#")],-1),x0=e("p",null,[e("code",null,[e("span",{class:""},'type="color"')]),n(" is also consistent with the other input types.")],-1),k0={"aria-label":"Color input example",class:"component"},$0=e("input",{type:"color","aria-label":"Color picker",value:"#be03ae"},null,-1),C0={class:"code","data-theme":"dark"},S0=e("pre",null,`<input
  type="color"
  value="#be03ae"
  aria-label="Color picker"
/>`,-1),E0=e("h2",null,[n(`
        File Input`),e("a",{id:"file",href:"#file",class:"secondary",tabindex:"-1"},"#")],-1),T0=e("p",null,[n("Input type file button has a "),e("a",{href:"/docs/button#variants"},[e("i",null,"secondary"),n(" button style")]),n(".")],-1),A0={"aria-label":"File input example",class:"component"},M0=e("input",{type:"file"},null,-1),P0={class:"code small","data-theme":"dark"},N0=e("pre",null,'<input type="file" />',-1),O0=e("h2",null,[n(`
        Disabled Input`),e("a",{id:"disabled",href:"#disabled",class:"secondary",tabindex:"-1"},"#")],-1),R0={"aria-label":"Disabled example",class:"component"},I0=e("input",{type:"text",placeholder:"Disabled","aria-label":"Disabled input",disabled:""},null,-1),L0={class:"code","data-theme":"dark"},B0=e("pre",null,`<input
  type="text"
  placeholder="Disabled"
  aria-label="Disabled input"
  disabled
/>`,-1),D0=e("h2",null,[n(`
        Read-only Input`),e("a",{id:"readonly",href:"#readonly",class:"secondary",tabindex:"-1"},"#")],-1),z0={"aria-label":"Disabled example",class:"component"},V0=e("input",{type:"text","aria-label":"Read-only input",readonly:"",value:"Read-only"},null,-1),j0={class:"code","data-theme":"dark"},F0=e("pre",null,`<input
  type="text"
  value="Read-only"
  aria-label="Read-only input"
  readonly
/>`,-1),H0=e("h2",null,[n(`
        Validation States`),e("a",{id:"validation-states",href:"#validation-states",class:"secondary",tabindex:"-1"},"#")],-1),q0=e("p",null,[n("Validation states are provided with "),e("code",null,[e("span",{class:""},"aria-invalid")]),n(".")],-1),U0={"aria-label":"Validation states example",class:"component"},G0=e("input",{type:"text",placeholder:"Valid","aria-invalid":"false",value:"Valid"},null,-1),W0=e("input",{type:"text",placeholder:"Invalid","aria-invalid":"true",value:"Invalid"},null,-1),K0={class:"code","data-theme":"dark"},Y0=e("pre",null,`<input
  type="text"
  value="Valid"
  aria-invalid="false"
/>

<input
  type="text"
  value="Invalid"
  aria-invalid="true"
/>`,-1),J0=e("p",null,[n(`
        Helper texts, defined with `),e("code",null,"<small>"),n(` below the form element, inherit the validation state
        color.
      `)],-1),Z0={"aria-label":"Validation states example",class:"component"},X0=e("input",{type:"text",placeholder:"Valid","aria-invalid":"false","aria-describedby":"valid-helper",value:"Valid"},null,-1),Q0=e("small",{id:"valid-helper"},"Looks good!",-1),ev=e("input",{type:"text",placeholder:"Invalid","aria-invalid":"true","aria-describedby":"invalid-helper",value:"Invalid"},null,-1),tv=e("small",{id:"invalid-helper"},"Please provide a valid value!",-1),nv={class:"code","data-theme":"dark"},ov=e("pre",null,`<input
  type="text"
  value="Valid"
  aria-invalid="false"
  aria-describedby="valid-helper"
/>
<small id="valid-helper">Looks good!</small>

<input
  type="text"
  value="Invalid"
  aria-invalid="true"
  aria-describedby="invalid-helper"
/>
<small id="invalid-helper">
  Please provide a valid value!
</small>`,-1),av={__name:"FormInputView",setup(t){return(o,a)=>(C(),S(J,null,[Q_,n(),e0,n(),e("div",t0,[e("section",null,[e("article",n0,[o0,n(),e("footer",a0,[f(k,null,{default:v(()=>[l0]),_:1})])])]),n(),e("section",null,[s0,n(),r0,n(),e("article",i0,[c0,d0,u0,h0,n(),e("footer",p0,[f(k,null,{default:v(()=>[f0]),_:1})])])]),n(),e("section",null,[m0,n(),b0,n(),e("article",g0,[_0,n(),e("footer",v0,[f(k,null,{default:v(()=>[y0]),_:1})])])]),n(),e("section",null,[w0,n(),x0,n(),e("article",k0,[$0,n(),e("footer",C0,[f(k,null,{default:v(()=>[S0]),_:1})])])]),n(),e("section",null,[E0,n(),T0,n(),e("article",A0,[M0,n(),e("footer",P0,[f(k,null,{default:v(()=>[N0]),_:1})])])]),n(),e("section",null,[O0,n(),e("article",R0,[I0,n(),e("footer",L0,[f(k,null,{default:v(()=>[B0]),_:1})])])]),n(),e("section",null,[D0,n(),e("article",z0,[V0,n(),e("footer",j0,[f(k,null,{default:v(()=>[F0]),_:1})])])]),n(),e("section",null,[H0,n(),q0,n(),e("article",U0,[G0,W0,n(),e("footer",K0,[f(k,null,{default:v(()=>[Y0]),_:1})])]),n(),J0,n(),e("article",Z0,[X0,Q0,ev,tv,n(),e("footer",nv,[f(k,null,{default:v(()=>[ov]),_:1})])])])])],64))}},lv=e("hgroup",null,[e("p",{class:"chapter"},`
      Forms
    `),n(),e("h1",null,"Forms Overview"),n(),e("p",null,`
      All form elements are fully responsive with pure semantic HTML, enabling forms to scale gracefully across devices
      and viewports.
    `)],-1),sv=G('<aside id="table-of-contents"><nav><details open><summary>Content</summary> <ul><li><a class="secondary" href="/docs/forms">Introduction</a></li> <li><a class="secondary" href="/docs/forms#helper-text">Helper text</a></li> <li><a class="secondary" href="/docs/forms#usage-with-grid">Usage with grid</a></li> <li><a class="secondary" href="/docs/forms#usage-with-group">Usage with group</a></li></ul></details></nav></aside>',1),rv={role:"document"},iv={"aria-label":"Introduction"},cv=e("p",null,[n(`
        Inputs are `),e("code",null,"width: 100%;"),n(` by default and are the same size as the buttons to build consitent forms.
      `)],-1),dv={"aria-label":"Form example",class:"component"},uv=e("form",null,[e("fieldset",null,[e("label",null,[n("First name"),e("input",{name:"first_name",placeholder:"First name"})]),e("label",null,[n("Email"),e("input",{type:"email",name:"email",placeholder:"Email",autoComplete:"email"})])]),n(),e("input",{type:"submit",value:"Subscribe"})],-1),hv={class:"code","data-theme":"dark"},pv=e("pre",null,`<form>
  <fieldset>
    <label>
      First name
      <input
        name="first_name"
        placeholder="First name"
      />
    </label>
    <label>
      Email
      <input
        type="email"
        name="email"
        placeholder="Email"
        autocomplete="email"
      />
    </label>
  </fieldset>

  <input
    type="submit"
    value="Subscribe"
  />
</form>`,-1),fv=e("p",null,[e("code",null,"<input>"),n(" can be inside or outside "),e("code",null,"<label>"),n(".")],-1),mv={"aria-label":"Label and input syntax",class:"component"},bv=e("form",null,[e("label",null,[n("First name"),e("input",{name:"first_name",placeholder:"First name"})]),e("label",{for:"email"},"Email"),e("input",{type:"email",name:"email",placeholder:"Email",autocomplete:"email"})],-1),gv={class:"code","data-theme":"dark"},_v=e("pre",null,`<form>
  <!-- Input inside label -->
  <label>
    First name
    <input
      name="first_name"
      placeholder="First name"
    />
  </label>

  <!-- Input outside label -->
  <label for="email">Email</label>
  <input
    type="email"
    name="email"
    placeholder="Email"
    autocomplete="email"
  />

</form>`,-1),vv=e("h2",null,[n(`
        Helper text`),e("a",{id:"helper-text",href:"#helper-text",class:"secondary",tabindex:"-1"},"#")],-1),yv=e("p",null,[e("code",null,"<small>"),n(" below form elements are muted and act as helper texts.")],-1),wv={"aria-label":"Form helpers example",class:"component"},xv=e("input",{type:"email",name:"email",placeholder:"Email",autoComplete:"email","aria-label":"Email","aria-describedby":"email-helper"},null,-1),kv=e("small",{id:"email-helper"},"We'll never share your email with anyone else.",-1),$v={class:"code","data-theme":"dark"},Cv=e("pre",null,`<input
  type="email"
  name="email"
  placeholder="Email"
  autoComplete="email"
  aria-label="Email"
  aria-describedby="email-helper"
/>
<small id="email-helper">
  We'll never share your email with anyone else.
</small>`,-1),Sv=e("h2",null,[n(`
        Usage with grid`),e("a",{id:"usage-with-grid",href:"#usage-with-grid",class:"secondary",tabindex:"-1"},"#")],-1),Ev=e("p",null,[n("You can use "),e("code",null,[e("span",{class:""},".grid")]),n(" inside a form. See "),e("a",{href:"/docs/grid"},"Grid"),n(".")],-1),Tv={"aria-label":"Form and grid example",class:"component"},Av=e("form",null,[e("fieldset",{class:"grid"},[e("input",{name:"login",placeholder:"Login","aria-label":"Login",autoComplete:"nickname"}),e("input",{type:"password",name:"password",placeholder:"Password","aria-label":"Password",autoComplete:"current-password"}),e("input",{type:"submit",value:"Log in"})])],-1),Mv={class:"code","data-theme":"dark"},Pv=e("pre",null,`<form>
  <fieldset class="grid">
    <input
      name="login"
      placeholder="Login"
      aria-label="Login"
      autocomplete="nickname"
    />
    <input
      type="password"
      name="password"
      placeholder="Password"
      aria-label="Password"
      autocomplete="current-password"
    />
    <input
      type="submit"
      value="Log in"
    />
  </fieldset>
</form>`,-1),Nv=e("h2",null,[n(`
        Usage with group`),e("a",{id:"usage-with-group",href:"#usage-with-group",class:"secondary",tabindex:"-1"},"#")],-1),Ov=e("p",null,[n(`
        You can use `),e("code",null,'role="group"'),n(" with form elements. See "),e("a",{href:"/docs/group"},"Group"),n(`.
      `)],-1),Rv={class:"component","aria-label":"Form group example"},Iv=e("form",{role:"group"},[e("input",{type:"email",placeholder:"Enter your email"}),e("input",{type:"submit",value:"Subscribe"})],-1),Lv={class:"code","data-theme":"dark"},Bv=e("pre",null,`<form role="group">
  <input type="email" placeholder="Enter your email" />
  <input type="submit" value="Subscribe" />
</form>`,-1),Dv={__name:"FormOverviewView",setup(t){return(o,a)=>(C(),S(J,null,[lv,n(),sv,n(),e("div",rv,[e("section",iv,[cv,n(),e("article",dv,[uv,n(),e("footer",hv,[f(k,null,{default:v(()=>[pv]),_:1})])]),n(),fv,n(),e("article",mv,[bv,n(),e("footer",gv,[f(k,null,{default:v(()=>[_v]),_:1})])])]),n(),e("section",null,[vv,n(),yv,n(),e("article",wv,[xv,kv,n(),e("footer",$v,[f(k,null,{default:v(()=>[Cv]),_:1})])])]),n(),e("section",null,[Sv,n(),Ev,n(),e("article",Tv,[Av,n(),e("footer",Mv,[f(k,null,{default:v(()=>[Pv]),_:1})])])]),n(),e("section",null,[Nv,n(),Ov,n(),e("article",Rv,[Iv,n(),e("footer",Lv,[f(k,null,{default:v(()=>[Bv]),_:1})])])])])],64))}},zv=e("hgroup",null,[e("p",{class:"chapter"},`
      Forms
    `),n(),e("h1",null,"Radios"),n(),e("p",null,[n("The native "),e("code",null,'<input type="radio">'),n(" with a custom and responsive style.")])],-1),Vv=G('<aside id="table-of-contents"><nav><details open=""><summary>Content</summary> <ul><li><a class="secondary" href="/docs/forms/radios" aria-current="page">Syntax</a></li> <li><a class="secondary" href="/docs/forms/radios#horizontal-stacking">Horizontal Stacking</a></li> <li><a class="secondary" href="/docs/forms/radios#validation-states">Validation States</a></li></ul></details></nav></aside>',1),jv={role:"document"},Fv={"aria-label":"Radios example",class:"component"},Hv=G('<fieldset><legend>Language preference:</legend> <label><input type="radio" name="language" checked="">English</label><label><input type="radio" name="language">French</label><label><input type="radio" name="language">Mandarin</label><label><input type="radio" name="language">Thai</label><label aria-disabled="true"><input type="radio" name="language" disabled="">Dothraki</label></fieldset>',1),qv={class:"code","data-theme":"dark"},Uv=e("pre",null,`<fieldset>
  <legend>Language preference:</legend>
  <label>
    <input type="radio" name="language" checked />
    English
  </label>
  <label>
    <input type="radio" name="language" />
    French
  </label>
  <label>
    <input type="radio" name="language" />
    Mandarin
  </label>
  <label>
    <input type="radio" name="language" />
    Thai
  </label>
  <label aria-disabled="true">
    <input type="radio" name="language" disabled />
    Dothraki
  </label>
</fieldset>`,-1),Gv=e("h2",null,[n(`
        Horizontal Stacking`),e("a",{id:"horizontal-stacking",href:"#horizontal-stacking",class:"secondary",tabindex:"-1"},"#")],-1),Wv={"aria-label":"Horizontal stacking example",class:"component"},Kv=G('<fieldset><legend>Second language:</legend> <input id="hindi" type="radio" name="second-language" checked=""><label for="hindi">Hindi</label><input id="swahili" type="radio" name="second-language"><label for="swahili">Swahili</label><input id="navi " type="radio" name="second-language" disabled=""><label for="navi" aria-disabled="true">Na&#39;vi</label></fieldset>',1),Yv={class:"code","data-theme":"dark"},Jv=e("pre",null,`<fieldset>
  <legend>Second language:</legend>
  <input type="radio" id="hindi" name="second-language" checked />
  <label htmlFor="hindi">Hindi</label>
  <input type="radio" id="swahili" name="second-language" />
  <label htmlFor="swahili">Swahili</label>
  <input type="radio" id="navi " name="second-language" disabled />
  <label htmlFor="navi" aria-disabled="true">Na'vi</label>
</fieldset>`,-1),Zv=e("h2",null,[n(`
        Validation States`),e("a",{id:"validation-states",href:"#validation-states",class:"secondary",tabindex:"-1"},"#")],-1),Xv=e("p",null,[n("Validation states are provided with "),e("code",null,[e("span",{class:""},"aria-invalid")]),n(".")],-1),Qv={"aria-label":"Validation states example",class:"component"},e2=e("fieldset",null,[e("label",null,[e("input",{type:"radio",name:"validation-states","aria-invalid":"false"}),n("Valid")]),e("label",null,[e("input",{type:"radio",name:"validation-states","aria-invalid":"true"}),n("Invalid")])],-1),t2={class:"code","data-theme":"dark"},n2=e("pre",null,`<fieldset>
  <label>
    <input type="radio" name="validation-states" aria-invalid="false" />
    Valid
  </label>

  <label>
    <input type="radio" name="validation-states" aria-invalid="true" />
    Invalid
  </label>
</fieldset>`,-1),o2={__name:"FormRadiosView",setup(t){return(o,a)=>(C(),S(J,null,[zv,n(),Vv,n(),e("div",jv,[e("section",null,[e("article",Fv,[Hv,n(),e("footer",qv,[f(k,null,{default:v(()=>[Uv]),_:1})])])]),n(),e("section",null,[Gv,n(),e("article",Wv,[Kv,n(),e("footer",Yv,[f(k,null,{default:v(()=>[Jv]),_:1})])])]),n(),e("section",null,[Zv,n(),Xv,n(),e("article",Qv,[e2,n(),e("footer",t2,[f(k,null,{default:v(()=>[n2]),_:1})])])])])],64))}},a2=e("hgroup",null,[e("p",{class:"chapter"},`
      Forms
    `),n(),e("h1",null,"Range"),n(),e("p",null,[n("Create a slider control with the input type "),e("code",null,'<input type="range">'),n(".")])],-1),l2={role:"document"},s2={"aria-label":"Range examples",class:"component"},r2=e("label",null,[n("Brightness"),e("input",{type:"range"})],-1),i2=e("label",null,[n("Contrast"),e("input",{type:"range",value:"40"})],-1),c2={class:"code","data-theme":"dark"},d2=e("pre",null,`<label>
  Brightness
  <input type="range" />
</label>

<label>
  Contrast
  <input type="range" value="40" />
</label>`,-1),u2={__name:"FormRangeView",setup(t){return(o,a)=>(C(),S(J,null,[a2,n(),e("div",l2,[e("section",null,[e("article",s2,[r2,i2,n(),e("footer",c2,[f(k,null,{default:v(()=>[d2]),_:1})])])])])],64))}},h2=e("hgroup",null,[e("p",{class:"chapter"},`
      Forms
    `),n(),e("h1",null,"Select"),n(),e("p",null,[n("The native "),e("code",null,"<select>"),n(" is styled like the input for consistency.")])],-1),p2=G('<aside id="table-of-contents"><nav><details open=""><summary>Content</summary> <ul><li><a class="secondary" href="/docs/forms/select" aria-current="page">Syntax</a></li> <li><a class="secondary" href="/docs/forms/select#multiple">Multiple</a></li> <li><a class="secondary" href="/docs/forms/select#disabled">Disabled</a></li> <li><a class="secondary" href="/docs/forms/select#validation-states">Validation States</a></li> <li><a class="secondary" href="/docs/forms/select#dropdown">Dropdown</a></li></ul></details></nav></aside>',1),f2={role:"document"},m2={"aria-label":"Select example",class:"component"},b2=e("select",{id:"favorite-pizza-toppings",required:""},[e("option",{disabled:"",value:"",selected:""},`
            Select your favorite pizza toppings...
          `),n(),e("option",null,"Ham"),n(),e("option",null,"Cheese"),n(),e("option",null,"Pineapple"),n(),e("option",null,"Egg"),n(),e("option",null,"Prawns")],-1),g2={class:"code","data-theme":"dark"},_2=e("pre",null,`<select required>
  <option selected disabled value="">
    Select your favorite cuisine...
  </option>
  <option>Ham</option>
  <option>Cheese</option>
  <option>Pineapple</option>
  <option>Egg</option>
  <option>Prawns</option>
</select>`,-1),v2=e("h2",null,[n(`
        Select Multiple`),e("a",{id:"multiple",href:"#multiple",class:"secondary",tabindex:"-1"},"#")],-1),y2={"aria-label":"Disabled and read-only example",class:"component"},w2=e("select",{multiple:"",size:"6"},[e("option",{disabled:""},`
            Select your favorite snacks...
          `),n(),e("option",null,"Cheese"),n(),e("option",{selected:""},`
            Fruits
          `),n(),e("option",{selected:""},`
            Nuts
          `),n(),e("option",null,"Chocolate"),n(),e("option",null,"Crackers")],-1),x2={class:"code","data-theme":"dark"},k2=e("pre",null,`<select multiple size="6">
  <option disabled>
    Select your favorite snacks...
  </option>
  <option>Cheese</option>
  <option selected>Fruits</option>
  <option selected>Nuts</option>
  <option>Chocolate</option>
  <option>Crackers</option>
</select>`,-1),$2=e("h2",null,[n(`
        Disabled Select`),e("a",{id:"disabled",href:"#disabled",class:"secondary",tabindex:"-1"},"#")],-1),C2={"aria-label":"Disabled example",class:"component"},S2=e("select",{disabled:""},[e("option",null,"Select a meal type...")],-1),E2={class:"code","data-theme":"dark"},T2=e("pre",null,`<select disabled>
  <option>Select a meal type...</option>
  <option>...</option>
</select>`,-1),A2=e("h2",null,[n(`
        Validation States`),e("a",{id:"validation-states",href:"#validation-states",class:"secondary",tabindex:"-1"},"#")],-1),M2=e("p",null,[n("Validation states are provided with "),e("code",null,[e("span",{class:""},"aria-invalid")]),n(".")],-1),P2={"aria-label":"Validation states example",class:"component"},N2=e("select",{"aria-invalid":"false"},[e("option",{disabled:""},`
            Select your favorite pizza topping...
          `),n(),e("option",{selected:""},`
            Pepperoni
          `),n(),e("option",null,"Mushrooms"),n(),e("option",null,"Onions"),n(),e("option",null,"Green Peppers"),n(),e("option",null,"Olives")],-1),O2=e("small",null,"Great choice!",-1),R2=e("select",{required:"","aria-invalid":"true"},[e("option",{disabled:"",value:"",selected:""},`
            Select your favorite pizza topping...
          `),n(),e("option",null,"Pepperoni"),n(),e("option",null,"Mushrooms"),n(),e("option",null,"Onions"),n(),e("option",null,"Green Peppers"),n(),e("option",null,"Olives")],-1),I2=e("small",null,"Please select your favorite pizza topping!",-1),L2={class:"code","data-theme":"dark"},B2=e("pre",null,`<select aria-invalid="false">
  ...
</select>
<small>Great choice!</small>

<select required aria-invalid="true">
  ...
</select>
<small>
  Please select your favorite pizza topping!
</small>`,-1),D2=e("h2",null,[n(`
        Dropdown`),e("a",{id:"dropdown",href:"#dropdown",class:"secondary",tabindex:"-1"},"#")],-1),z2=G('<article aria-label="Dropdowns as selects" class="component"><div class="grid"><details class="dropdown"><summary>Select your favorite French dessert...</summary> <ul><li><label><input type="radio" name="french-dessert" value="Crème brûlée">Crème brûlée</label></li> <li><label><input type="radio" name="french-dessert" value="Macarons">Macarons</label></li> <li><label><input type="radio" name="french-dessert" value="Tarte tatin">Tarte tatin</label></li> <li><label><input type="radio" name="french-dessert" value="Éclair">Éclair</label></li></ul></details></div></article>',1),V2={__name:"FormSelectView",setup(t){return(o,a)=>(C(),S(J,null,[h2,n(),p2,n(),e("div",f2,[e("section",null,[e("article",m2,[b2,n(),e("footer",g2,[f(k,null,{default:v(()=>[_2]),_:1})])])]),n(),e("section",null,[v2,n(),e("article",y2,[w2,n(),e("footer",x2,[f(k,null,{default:v(()=>[k2]),_:1})])])]),n(),e("section",null,[$2,n(),e("article",C2,[S2,n(),e("footer",E2,[f(k,null,{default:v(()=>[T2]),_:1})])])]),n(),e("section",null,[A2,n(),M2,n(),e("article",P2,[N2,n(),O2,n(),R2,n(),I2,n(),e("footer",L2,[f(k,null,{default:v(()=>[B2]),_:1})])])]),n(),e("section",null,[D2,n(),e("p",null,[n(`
        The dropdown component allows you to build a custom select with the same style as the native select. See
        `),f($(_e),{to:"/docs/dropdown"},{default:v(()=>[n(`
          Dropdown
        `)]),_:1}),n(`.
      `)]),n(),z2])])],64))}},j2=e("hgroup",null,[e("p",{class:"chapter"},`
      Forms
    `),n(),e("h1",null,"Switch"),n(),e("p",null,"A switch component in pure CSS, using the checkbox syntax.")],-1),F2=G('<aside id="table-of-contents"><nav><details open=""><summary>Content</summary> <ul><li><a class="secondary" href="/docs/forms/switch" aria-current="page">Syntax</a></li> <li><a class="secondary" href="/docs/forms/switch#disabled">Disabled</a></li> <li><a class="secondary" href="/docs/forms/switch#validation-states">Validation States</a></li></ul></details></nav></aside>',1),H2={role:"document"},q2={"aria-label":"Switch example",class:"component"},U2=e("fieldset",null,[e("label",null,[e("input",{type:"checkbox",role:"switch"}),n("I agree to the Terms")]),e("label",null,[e("input",{type:"checkbox",role:"switch",checked:""}),n("I want to receive updates")])],-1),G2={class:"code","data-theme":"dark"},W2=e("pre",null,`<fieldset>
  <label>
    <input type="checkbox" role="switch" />
    I agree to the Terms
  </label>
  <label>
    <input type="checkbox" role="switch" checked />
    Receive news and offers
  </label>
</fieldset>`,-1),K2=e("h2",null,[n(`
        Disabled`),e("a",{id:"disabled",href:"#disabled",class:"secondary",tabindex:"-1"},"#")],-1),Y2={"aria-label":"Disabled switch example",class:"component"},J2=e("fieldset",null,[e("label",{"aria-disabled":"true"},[e("input",{type:"checkbox",role:"switch",disabled:""}),n("Publish on my profile")]),e("label",{"aria-disabled":"true"},[e("input",{type:"checkbox",role:"switch",disabled:"",checked:""}),n("Change my password at next login")])],-1),Z2={class:"code","data-theme":"dark"},X2=e("pre",null,`<fieldset>
  <label>
    <input type="checkbox" role="switch" disabled />
    Publish on my profile
  </label>
  <label>
    <input type="checkbox" role="switch" checked disabled />
    Change my password at next login
  </label>
</fieldset>`,-1),Q2=e("h2",null,[n(`
        Validation States`),e("a",{id:"validation-states",href:"#validation-states",class:"secondary",tabindex:"-1"},"#")],-1),ey={"aria-label":"Validation states example",class:"component"},ty=e("fieldset",null,[e("label",null,[e("input",{type:"checkbox",role:"switch","aria-invalid":"false"}),n("Enable two-factor authentication")]),e("label",null,[e("input",{type:"checkbox",role:"switch","aria-invalid":"true"}),n("Automatic subscription renewal")])],-1),ny={class:"code","data-theme":"dark"},oy=e("pre",null,`<fieldset>
  <label>
    <input type="checkbox" role="switch" aria-invalid="false" />
    Enable two-factor authentication
  </label>
  <label>
    <input type="checkbox" role="switch" aria-invalid="true" />
    Automatic subscription renewal
  </label>
</fieldset>`,-1),ay={__name:"FormSwitchView",setup(t){return(o,a)=>(C(),S(J,null,[j2,n(),F2,n(),e("div",H2,[e("section",null,[e("article",q2,[U2,n(),e("footer",G2,[f(k,null,{default:v(()=>[W2]),_:1})])])]),n(),e("section",null,[K2,n(),e("article",Y2,[J2,n(),e("footer",Z2,[f(k,null,{default:v(()=>[X2]),_:1})])])]),n(),e("section",null,[Q2,n(),e("article",ey,[ty,n(),e("footer",ny,[f(k,null,{default:v(()=>[oy]),_:1})])])])])],64))}},ly=e("hgroup",null,[e("p",{class:"chapter"},`
      Forms
    `),n(),e("h1",null,"Textarea"),n(),e("p",null,[n("The native "),e("code",null,"<textarea>"),n(" is styled like the input for consistency.")])],-1),sy=G('<aside id="table-of-contents"><nav><details open=""><summary>Content</summary> <ul><li><a class="secondary" href="/docs/forms/textarea">Syntax</a></li> <li><a class="secondary" href="/docs/forms/textarea#disabled">Disabled</a></li> <li><a class="secondary" href="/docs/forms/textarea#readonly">Readonly</a></li> <li><a class="secondary" href="/docs/forms/textarea#validation-states" aria-current="page">Validation States</a></li></ul></details></nav></aside>',1),ry={role:"document"},iy={"aria-label":"Textarea example",class:"component"},cy=e("textarea",{placeholder:"Write a professional short bio...","aria-label":"Professional short bio"},null,-1),dy={class:"code","data-theme":"dark"},uy=e("pre",null,`<textarea
  placeholder="Write a professional short bio..."
  aria-label="Professional short bio"
>
</textarea>`,-1),hy=e("h2",null,[n(`
        Disabled Textarea`),e("a",{id:"disabled",href:"#disabled",class:"secondary",tabindex:"-1"},"#")],-1),py={"aria-label":"Disabled example",class:"component"},fy=e("textarea",{disabled:""},"Disabled",-1),my={class:"code","data-theme":"dark"},by=e("pre",null,`<textarea disabled>
  Disabled
</textarea>`,-1),gy=e("h2",null,[n(`
        Read-only Textarea`),e("a",{id:"readonly",href:"#readonly",class:"secondary",tabindex:"-1"},"#")],-1),_y={"aria-label":"Disabled example",class:"component"},vy=e("textarea",{"aria-label":"Read-only textarea",readonly:""},"Read-only",-1),yy={class:"code","data-theme":"dark"},wy=e("pre",null,`<textarea readonly>
  Read-only
</textarea>`,-1),xy=e("h2",null,[n(`
        Validation States`),e("a",{id:"validation-states",href:"#validation-states",class:"secondary",tabindex:"-1"},"#")],-1),ky=e("p",null,[n("Validation states are provided with "),e("code",null,[e("span",{class:""},"aria-invalid")]),n(".")],-1),$y={"aria-label":"Validation states example",class:"component"},Cy=e("textarea",{placeholder:"Valid","aria-invalid":"false"},"Valid",-1),Sy=e("textarea",{placeholder:"Invalid","aria-invalid":"true"},"Invalid",-1),Ey={class:"code","data-theme":"dark"},Ty=e("pre",null,`<textarea aria-invalid="false">
  Valid
</textarea>

<textarea aria-invalid="true">
  Invalid
</textarea>`,-1),Ay=e("p",null,[n(`
        Helper texts, defined with `),e("code",null,"<small>"),n(` below the textarea, inherit the validation state color.
      `)],-1),My={"aria-label":"Validation states example",class:"component"},Py=e("textarea",{placeholder:"Valid","aria-invalid":"false","aria-describedby":"valid-helper"},"Valid",-1),Ny=e("small",{id:"valid-helper"},"Looks good!",-1),Oy=e("textarea",{placeholder:"Invalid","aria-invalid":"true","aria-describedby":"invalid-helper"},"Invalid",-1),Ry=e("small",{id:"invalid-helper"},"Please provide a valid value!",-1),Iy={class:"code","data-theme":"dark"},Ly=e("pre",null,`<textarea
  aria-invalid="false"
  aria-describedby="valid-helper"
>
  Valid
</textarea>
<small id="valid-helper">Looks good!</small>

<textarea
  aria-invalid="true"
  aria-describedby="invalid-helper"
>
  Invalid
</textarea>
<small id="invalid-helper">
  Please provide a valid value!
</small>`,-1),By={__name:"FormTextareaView",setup(t){return(o,a)=>(C(),S(J,null,[ly,n(),sy,n(),e("div",ry,[e("section",null,[e("article",iy,[cy,n(),e("footer",dy,[f(k,null,{default:v(()=>[uy]),_:1})])])]),n(),e("section",null,[hy,n(),e("article",py,[fy,n(),e("footer",my,[f(k,null,{default:v(()=>[by]),_:1})])])]),n(),e("section",null,[gy,n(),e("article",_y,[vy,n(),e("footer",yy,[f(k,null,{default:v(()=>[wy]),_:1})])])]),n(),e("section",null,[xy,n(),ky,n(),e("article",$y,[Cy,n(),Sy,n(),e("footer",Ey,[f(k,null,{default:v(()=>[Ty]),_:1})])]),n(),Ay,n(),e("article",My,[Py,n(),Ny,n(),Oy,n(),Ry,n(),e("footer",Iy,[f(k,null,{default:v(()=>[Ly]),_:1})])])])])],64))}},Dy=e("hgroup",null,[e("p",{class:"chapter"},`
      Layout
    `),n(),e("h1",null,"Grid"),n(),e("p",null,[n("Create minimal responsive layouts with "),e("code",null,".grid"),n(" to enable auto-layout columns.")])],-1),zy=e("aside",{id:"table-of-contents"},[e("nav",null,[e("details",{open:""},[e("summary",null,"Content"),n(),e("ul",null,[e("li",null,[e("a",{class:"secondary",href:"/docs/grid","aria-current":"page"},"Syntax")]),n(),e("li",null,[e("a",{class:"secondary",href:"/docs/grid#about-css-grids"},"About CSS Grids")])])])])],-1),Vy={role:"document"},jy=e("svg",{xmlns:"http://www.w3.org/2000/svg",width:"24",height:"24",viewBox:"0 0 24 24",fill:"none",stroke:"currentColor","stroke-width":"2","stroke-linecap":"round","stroke-linejoin":"round"},[e("circle",{cx:"12",cy:"12",r:"10"}),n(),e("line",{x1:"12",y1:"8",x2:"12",y2:"16"}),n(),e("line",{x1:"8",y1:"12",x2:"16",y2:"12"})],-1),Fy=e("svg",{xmlns:"http://www.w3.org/2000/svg",width:"24",height:"24",viewBox:"0 0 24 24",fill:"none",stroke:"currentColor","stroke-width":"2","stroke-linecap":"round","stroke-linejoin":"round"},[e("circle",{cx:"12",cy:"12",r:"10"}),n(),e("line",{x1:"8",y1:"12",x2:"16",y2:"12"})],-1),Hy={class:"component","aria-label":"Grid example"},qy={id:"grid-example",class:"grid"},Uy={class:"code","data-theme":"dark"},Gy=e("p",null,[n("Columns intentionally collapse on small devices ("),e("code",null,"<768px"),n(").")],-1),Wy={__name:"GridView",setup(t){const o=Ce([1,2,3,4]),a=ue(()=>`<div class="grid">
${o.value.reduce((i,c)=>`${i}  <div>${c}</div>
`,"")}</div>`);function l(){o.value.length<12&&o.value.push(o.value.length+1)}function s(){o.value.length>1&&o.value.pop()}return(r,i)=>(C(),S(J,null,[Dy,n(),zy,n(),e("div",Vy,[e("section",null,[e("p",{class:"btn-group"},[e("button",{class:"secondary",onClick:l},[jy,n(`
          Add column
        `)]),n(),e("button",{class:"secondary",onClick:s},[Fy,n(`
          Remove column
        `)])]),n(),e("article",Hy,[e("div",qy,[(C(!0),S(J,null,Oe(o.value,c=>(C(),S("div",{key:c},re(c),1))),128))]),n(),e("footer",Uy,[f(k,null,{default:v(()=>[e("pre",null,re(a.value),1)]),_:1})])]),n(),Gy])])],64))}},Ky=e("hgroup",null,[e("p",{class:"chapter"},`
      Components
    `),n(),e("h1",null,"Group"),n(),e("p",null,[n("Stack forms elements and buttons horizontally with "),e("code",null,'role="group"'),n(" and "),e("code",null,'role="search"'),n(".")])],-1),Yy=G('<aside id="table-of-contents"><nav><details open=""><summary>Content</summary> <ul><li><a class="secondary" href="/docs/group">Forms</a></li> <li><a class="secondary" href="/docs/group#search">Search</a></li> <li><a class="secondary" href="/docs/group#buttons" aria-current="page">Buttons</a></li></ul></details></nav></aside>',1),Jy={role:"document"},Zy=e("p",null,[e("code",null,'role="group"'),n(" is used to stack children horizontally.")],-1),Xy=e("p",null,[n(`
        When used with the `),e("code",null,"<form>"),n(" tag, the group is "),e("code",null,[e("span",{class:""},"width: 100%;")]),n(`.
      `)],-1),Qy=e("p",null,[n("Unlike "),e("code",null,".grid"),n(" (see "),e("a",{href:"/docs/grid"},"Grid"),n("), columns are not collapsed on mobile devices.")],-1),ew={class:"component","aria-label":"Form group example"},tw=e("form",{role:"group"},[e("input",{type:"email",placeholder:"Enter your email"}),e("input",{type:"submit",value:"Subscribe"})],-1),nw={class:"code","data-theme":"dark"},ow=e("pre",null,`<form role="group">
  <input type="email" placeholder="Enter your email" />
  <input type="submit" value="Subscribe" />
</form>`,-1),aw=e("p",null,[n(`
        This component is mainly designed for form elements and buttons. It brings a `),e("code",null,":focus"),n(` style to the
        group depending on whether the focused child is an `),e("code",null,"<input>"),n(" or a "),e("code",null,"<button>"),n(`.
      `)],-1),lw=e("p",null,[n(`
        The group `),e("code",null,":focus"),n(" style relies on the "),e("code",null,":has()"),n(` CSS selector and is therefore not (yet)
        supported by Firefox (see on `),e("a",{rel:"noopener noreferrer",href:"https://caniuse.com/css-has",target:"_blank"},"caniuse"),n("). When "),e("code",null,":has()"),n(" is not supported the children have their regular "),e("code",null,":focus"),n(`
        style.
      `)],-1),sw={class:"component","aria-label":"Form group example"},rw=e("form",{role:"group"},[e("input",{type:"email",placeholder:"Email"}),e("input",{type:"password",placeholder:"Password"}),e("input",{type:"submit",value:"Log in"})],-1),iw={class:"code","data-theme":"dark"},cw=e("pre",null,`<form role="group">
  <input type="email" placeholder="Email" />
  <input type="password" placeholder="Password" />
  <input type="submit" value="Log in" />
</form>`,-1),dw=e("h2",null,[n(`
        Search`),e("a",{id:"search",href:"#search",class:"secondary",tabindex:"-1"},"#")],-1),uw=e("p",null,[e("code",null,'role="search"'),n(` also stacks children horizontally and brings a special style, consistent with
        `),e("code",null,'<input type="search" />'),n(" (see "),e("a",{href:"/docs/forms/input#search"},"Search input"),n(`).
      `)],-1),hw={class:"component","aria-label":"Searcgh group example"},pw=e("form",{role:"search"},[e("input",{type:"search",placeholder:"Search"}),e("input",{type:"submit",value:"Search"})],-1),fw={class:"code","data-theme":"dark"},mw=e("pre",null,`<form role="search">
  <input type="search" placeholder="Search" />
  <input type="submit" value="Search" />
</form>`,-1),bw=e("h2",null,[n(`
        Buttons`),e("a",{id:"buttons",href:"#buttons",class:"secondary",tabindex:"-1"},"#")],-1),gw=e("p",null,[e("code",null,'role="group"'),n(" is also useful for grouping a series of buttons.")],-1),_w={class:"component","aria-label":"Group example"},vw=e("div",{role:"group"},[e("button",null,"Button"),e("button",null,"Button"),e("button",null,"Button")],-1),yw={class:"code","data-theme":"dark"},ww=e("pre",null,`<div role="group">
  <button>Button</button>
  <button>Button</button>
  <button>Button</button>
</div>`,-1),xw={class:"component","aria-label":"Group example"},kw=e("div",{role:"group"},[e("button",{"aria-current":"true"},`
            Active
          `),e("button",null,"Button"),e("button",null,"Button")],-1),$w={class:"code","data-theme":"dark"},Cw=e("pre",null,`<div role="group">
  <button aria-current="true">Active</button>
  <button>Button</button>
  <button>Button</button>
</div>`,-1),Sw={class:"component","aria-label":"Group example"},Ew=e("div",{role:"group"},[e("button",null,"Button"),e("button",{class:"secondary"},`
            Button
          `),e("button",{class:"contrast"},`
            Button
          `)],-1),Tw={class:"code","data-theme":"dark"},Aw=e("pre",null,`<div role="group">
  <button>Button</button>
  <button class="secondary">Button</button>
  <button class="contrast">Button</button>
</div>`,-1),Mw={__name:"GroupView",setup(t){return(o,a)=>(C(),S(J,null,[Ky,n(),Yy,n(),e("div",Jy,[e("section",null,[Zy,n(),Xy,n(),Qy,n(),e("article",ew,[tw,n(),e("footer",nw,[f(k,null,{default:v(()=>[ow]),_:1})])]),n(),aw,n(),lw,n(),e("article",sw,[rw,n(),e("footer",iw,[f(k,null,{default:v(()=>[cw]),_:1})])])]),n(),e("section",null,[dw,n(),uw,n(),e("article",hw,[pw,n(),e("footer",fw,[f(k,null,{default:v(()=>[mw]),_:1})])])]),n(),e("section",null,[bw,n(),gw,n(),e("article",_w,[vw,n(),e("footer",yw,[f(k,null,{default:v(()=>[ww]),_:1})])]),n(),e("article",xw,[kw,n(),e("footer",$w,[f(k,null,{default:v(()=>[Cw]),_:1})])]),n(),e("article",Sw,[Ew,n(),e("footer",Tw,[f(k,null,{default:v(()=>[Aw]),_:1})])])])])],64))}},Pw=e("hgroup",null,[e("p",{class:"chapter"},`
      Layout
    `),n(),e("h1",null,"Landmarks & Sections"),n(),e("p",null,"Structure your pages with semantic landmarks and sections for better accessibility and graceful spacings.")],-1),Nw=G('<aside id="table-of-contents"><nav><details open><summary>Content</summary> <ul><li><a class="secondary" href="/docs/landmarks-section">Landmarks</a></li> <li><a class="secondary" href="/docs/landmarks-section#root-container">Custom root container</a></li> <li><a class="secondary" href="/docs/landmarks-section#section">Section</a></li></ul></details></nav></aside>',1),Ow={role:"document"},Rw=e("p",null,[e("code",null,"<header>"),n(", "),e("code",null,"<main>"),n(" and "),e("code",null,"<footer>"),n(` as direct children of
        `),e("code",null,"<body>"),n(" provide a responsive vertical "),e("code",null,"padding"),n(`.
      `)],-1),Iw={class:"code","data-theme":"dark"},Lw=e("pre",null,`<body>
  <header>...</header>
  <main>...</main>
  <footer>...</footer>
</body>`,-1),Bw=e("h2",null,[n(`
        Root container`),e("a",{id:"root-container",href:"#root-container",class:"secondary",tabindex:"-1"},"#")],-1),Dw=e("p",null,[n(`
        If you need to customise the default root container for `),e("code",null,"<header>"),n(", "),e("code",null,"<main>"),n(`,
        and `),e("code",null,"<footer>"),n(`, you can recompile Nano with another CSS selector.
      `)],-1),zw=e("p",null,[n(`
        Useful for `),e("a",{href:"https://reactjs.org/"},"React"),n(", "),e("a",{href:"https://www.gatsbyjs.com/"},"Gatsby"),n(`, or
        `),e("a",{href:"https://nextjs.org/"},"Next.js"),n(`.
      `)],-1),Vw={class:"code","data-theme":"dark"},jw=e("pre",null,`// custom version for react
@use "nano" with (

  // define the root element used to target <header>, <main>, <footer>
  // with $enable-semantic-container and $enable-responsive-spacings
  $semantic-root-element: "#root";

  // enable <header>, <main>, <footer> inside $semantic-root-element as containers
  $enable-semantic-container: true;
)`,-1),Fw=e("p",null,"The code above will compile Nano with the containers defined like this:",-1),Hw={class:"code","data-theme":"dark"},qw=e("pre",null,`// containers
#root > header,
#root > main,
#root > footer {
  ...
}`,-1),Uw=e("section",null,[e("h2",null,[n(`
        Section`),e("a",{id:"section",href:"#section",class:"secondary",tabindex:"-1"},"#")]),n(),e("p",null,[e("code",null,"<section>"),n(" provides a responsive "),e("code",null,"margin-bottom"),n(" to separate your sections.")])],-1),Gw={__name:"LandmarkSectionView",setup(t){return(o,a)=>(C(),S(J,null,[Pw,n(),Nw,n(),e("div",Ow,[e("section",null,[Rw,n(),e("div",Iw,[f(k,null,{default:v(()=>[Lw]),_:1})])]),n(),e("section",null,[Bw,n(),Dw,n(),zw,n(),e("div",Vw,[f(k,null,{default:v(()=>[jw]),_:1})]),n(),Fw,n(),e("div",Hw,[f(k,null,{default:v(()=>[qw]),_:1})]),n(),e("p",null,[n(`
        Learn more about `),f($(_e),{to:"/docs/sass"},{default:v(()=>[n(`
          compiling a custom version of Nano with SASS
        `)]),_:1}),n(`.
      `)])]),n(),Uw])],64))}},Ww=e("hgroup",null,[e("p",{class:"chapter"},`
      Content
    `),n(),e("h1",null,"Link"),n(),e("p",null,[n(`
      Links come with `),e("code",{class:"language-css"},".secondary"),n(" and "),e("code",{class:"language-css"},".contrast"),n(`
      styles.
    `)])],-1),Kw={role:"document"},Yw={"aria-label":"Links example",class:"component"},Jw=e("a",{href:"/docs/link"},"Primary",-1),Zw=e("br",null,null,-1),Xw=e("a",{class:"secondary",href:"/docs/link"},"Secondary",-1),Qw=e("br",null,null,-1),ex=e("a",{class:"contrast",href:"/docs/link"},"Contrast",-1),tx=e("br",null,null,-1),nx={class:"code","data-theme":"dark"},ox=e("pre",null,`<a href="#">Primary</a>
<a href="#" class="secondary">Secondary</a>
<a href="#" class="contrast">Contrast</a>`,-1),ax=e("p",null,[e("code",{class:"language-css"},"aria-current"),n(` send the active state to assistive technologies and is displayed as
      the hover links.
    `)],-1),lx={"aria-label":"Active link example",class:"component"},sx=e("a",{href:"/docs/link"},"Regular link",-1),rx=e("br",null,null,-1),ix=e("a",{"aria-current":"page",href:"/docs/link"},"Active link",-1),cx=e("br",null,null,-1),dx=e("a",{href:"/docs/link"},"Regular link",-1),ux=e("br",null,null,-1),hx={class:"code","data-theme":"dark"},px=e("pre",null,`<a href="#">Regular link</a>
<a href="#" aria-current="page">Active link</a>
<a href="#">Regular link</a>`,-1),fx={__name:"LinkView",setup(t){return(o,a)=>(C(),S(J,null,[Ww,n(),e("div",Kw,[e("article",Yw,[Jw,Zw,Xw,Qw,ex,tx,n(),e("footer",nx,[f(k,null,{default:v(()=>[ox]),_:1})])]),n(),ax,n(),e("article",lx,[sx,rx,ix,cx,dx,ux,n(),e("footer",hx,[f(k,null,{default:v(()=>[px]),_:1})])])])],64))}},mx=e("hgroup",null,[e("p",{class:"chapter"},`
      Components
    `),n(),e("h1",null,"Loading"),n(),e("p",null,[n("Add a loading indicator with "),e("code",null,[e("span",{class:""},'aria-busy="true"')]),n(".")])],-1),bx={role:"document"},gx=e("p",null,"It can be applied to any block:",-1),_x=e("article",{"aria-label":"Loading example","aria-busy":"true"},null,-1),vx={class:"code","data-theme":"dark"},yx=e("pre",null,'<article aria-busy="true"></article>',-1),wx=e("p",null,"Any inline element:",-1),xx={"aria-label":"Loading example"},kx=e("a",{"aria-busy":"true",href:"/docs/loading"},"Your link is being generated...",-1),$x={class:"code","data-theme":"dark"},Cx=e("pre",null,'<a href="#" aria-busy="true">Your link is being generated...</a>',-1),Sx=e("p",null,"Any button:",-1),Ex={"aria-label":"Loading button examples"},Tx=e("div",{id:"buttons",class:"grid"},[e("button",{"aria-busy":"true","aria-label":"Please wait…"}),e("button",{"aria-busy":"true","aria-label":"Please wait…",class:"secondary"}),e("button",{"aria-busy":"true","aria-label":"Please wait…",class:"contrast"})],-1),Ax=e("div",{id:"outline-buttons",class:"grid"},[e("button",{"aria-busy":"true",class:"outline"},`
            Please wait…
          `),e("button",{"aria-busy":"true",class:"outline secondary"},`
            Please wait…
          `),e("button",{"aria-busy":"true",class:"outline contrast"},`
            Please wait…
          `)],-1),Mx={class:"code","data-theme":"dark"},Px=e("pre",null,`<button aria-busy="true" aria-label="Please wait…" />
<button aria-busy="true" aria-label="Please wait…" class="secondary" />
<button aria-busy="true" aria-label="Please wait…" class="contrast" />
<button aria-busy="true" class="outline">Please wait…</button>
<button aria-busy="true" class="outline secondary">Please wait…</button>
<button aria-busy="true" class="outline contrast">Please wait…</button>`,-1),Nx={__name:"LoadingView",setup(t){return(o,a)=>(C(),S(J,null,[mx,n(),e("div",bx,[e("section",null,[gx,n(),_x,n(),e("div",vx,[f(k,null,{default:v(()=>[yx]),_:1})]),n(),wx,n(),e("article",xx,[kx,n(),e("footer",$x,[f(k,null,{default:v(()=>[Cx]),_:1})])]),n(),Sx,n(),e("article",Ex,[Tx,n(),Ax,n(),e("footer",Mx,[f(k,null,{default:v(()=>[Px]),_:1})])])])])],64))}},Ox=e("hgroup",null,[e("p",{class:"chapter"},`
      About
    `),n(),e("h1",null,"Mission"),n(),e("p",null,`
      Nano is aimed at being a minimal starter kit for building web-based UIs. Providing enough styling that prioritises
      semantic HTML, making every HTML tag responsive and elegant by default. All alongside a collection of lightweight
      Vue components to help build interfaces quicker.
    `)],-1),Rx={role:"document"},Ix=e("p",null,[n(`
        With a focus on simplicity, Nano provides a clean starting point for building web-based UIs without the need for
        `),e("span",{"data-tooltip":"🤮"},"excessive CSS classes"),n(", "),e("span",{"data-tooltip":"🙄"},"wrappers"),n(` and
        `),e("span",{"data-tooltip":"💩"},"excessive JS dependencies"),n(`.
      `)],-1),Lx=e("p",null,[n(`
        After many frustrated attempts at bending Bootstrap to my will, I went in search for a better starting position.
        That's when I came across `),e("a",{href:"https://github.com/picocss/pico"},"Pico CSS"),n(`, a framework that provided just
        enough structure to build big things but was small enough to stay out of your way.
      `)],-1),Bx=e("p",null,`
        With an amazing CSS foundation to build upon, I set out to create a collection of Vue components that would help
        me build interfaces quicker. The result is Nano.
      `,-1),Dx={class:"code","data-theme":"dark"},zx=e("pre",null,`<main>
  <h1>Join the pursuit of HTML simplicity.</h1>
  <p>Let’s avoid meaningless div and classes and make it beautiful!</p>
  <form>
    <input type="email" placeholder="Your email" />
    <input type="submit">Let’s start!</input>
  </form>
</main>

<footer>This page should be fast enough to please the algorithms.</footer>`,-1),Vx={__name:"MissionView",setup(t){return(o,a)=>(C(),S(J,null,[Ox,n(),e("div",Rx,[e("section",null,[Ix,n(),Lx,n(),Bx,n(),e("div",Dx,[f(k,null,{default:v(()=>[zx]),_:1})])])])],64))}},jx=e("h2",null,"Confirm Your Membership",-1),Fx=e("p",null,"Thank you for signing up for a membership! Please review the membership details below:",-1),Hx=e("ul",null,[e("li",null,"Membership: Individual"),n(),e("li",null,"Price: $10")],-1),qx=e("hgroup",null,[e("p",{class:"chapter"},`
      Components
    `),n(),e("h1",null,"Modal"),n(),e("p",null,[n(`
      The classic modal component with graceful spacings across devices and viewports, using the semantic HTML tag
      `),e("code",null,"<dialog>"),n(`.
    `)])],-1),Ux=G('<aside id="table-of-contents"><nav><details open=""><summary>Content</summary> <ul><li><a class="secondary" href="/docs/modal">Syntax</a></li> <li><a class="secondary" href="/docs/modal#demo">Demo</a></li> <li><a class="secondary" href="/docs/modal#utilities">Utilities</a></li></ul></details></nav></aside>',1),Gx={role:"document"},Wx=e("p",null,[n(`
        Modals are built with `),e("code",null,"<dialog>"),n(" as a wrapper and "),e("code",null,"<article>"),n(` for the modal
        content.
      `)],-1),Kx=e("p",null,[n(`
        Inside `),e("code",null,"<header>"),n(),e("code",null,'<a class="close">'),n(" is defined to "),e("code",null,"float: right;"),n(`
        allowing a close icon to be top aligned with a title.
      `)],-1),Yx=e("dialog",{class:"example",open:""},[e("article",null,[e("header",null,[e("a",{"aria-label":"Close",class:"close",href:"/docs/modal"}),n(),e("p",null,[e("strong",null,"🗓️ Thank You for Registering!")])]),n(),e("p",null,`
            We're excited to have you join us for our upcoming event. Please arrive at the museum on time to check in
            and get started.
          `),n(),e("ul",null,[e("li",null,"Date: Saturday, April 15"),n(),e("li",null,"Time: 10:00am - 12:00pm")])])],-1),Jx={class:"code","data-theme":"dark"},Zx=e("pre",null,`<dialog open>
  <article>
    <header>
      <Link to="#" aria-label="Close" class="close" />
      <p>
        <strong>🗓️ Thank You for Registering!</strong>
      </p>
    </header>
    <p>
      We're excited to have you join us for our
      upcoming event. Please arrive at the museum
      on time to check in and get started.
    </p>
    <ul>
      <li>Date: Saturday, April 15</li>
      <li>Time: 10:00am - 12:00pm</li>
    </ul>
  </article>
</dialog>`,-1),Xx=e("p",null,[n("Inside "),e("code",null,"<footer>"),n(", the content is right aligned by default.")],-1),Qx=G(`<dialog open="" class="example"><article><h2>Confirm Your Membership</h2> <p>Thank you for signing up for a membership! Please review the membership details below:</p> <ul><li>Membership: Individual</li> <li>Price: $10</li></ul> <footer><button class="secondary">
              Cancel
            </button><button>Confirm</button></footer></article></dialog>`,1),e3={class:"code","data-theme":"dark"},t3=e("pre",null,`<dialog open>
  <article>
    <h2>Confirm Your Membership</h2>
    <p>
      Thank you for signing up for a membership!
      Please review the membership details below:
    </p>
    <ul>
      <li>Membership: Individual</li>
      <li>Price: $10</li>
    </ul>
    <footer>
      <button className="secondary">
        Cancel
      </button>
      <button>Confirm</button>
    </footer>
  </article>
</dialog>`,-1),n3=e("h2",null,[n(`
        Demo`),e("a",{id:"demo",href:"#demo",class:"secondary",tabindex:"-1"},"#")],-1),o3=e("p",null,"Toggle a modal by clicking the button below.",-1),a3={id:"modal-demo","aria-label":"Modal demo"},l3=e("p",null,"nano does not include JavaScript code. You need to implement your JS to interact with modals.",-1),s3=e("p",null,[n("To open a modal, add the "),e("code",null,"open"),n(" attribute to the"),e("code",null,"<dialog>"),n(" container.")],-1),r3=e("h2",null,[n(`
        Utilities`),e("a",{id:"utilities",href:"#utilities",class:"secondary",tabindex:"-1"},"#")],-1),i3=e("p",null,"Modals come with 3 utility classes.",-1),c3=e("p",null,[e("code",null,[e("span",{class:""},".modal-is-open")]),n(" prevents any scrolling and interactions below the modal.")],-1),d3={class:"code","data-theme":"dark"},u3=e("pre",null,`<!doctype html>
<html class="modal-is-open">
  ...
</html>`,-1),h3=e("p",null,[e("code",null,[e("span",{class:""},".modal-is-opening")]),n(" brings an opening animation.")],-1),p3={class:"code","data-theme":"dark"},f3=e("pre",null,`<!doctype html>
<html class="modal-is-open modal-is-opening">
  ...
</html>`,-1),m3=e("p",null,[e("code",null,[e("span",{class:""},".modal-is-closing")]),n(" brings a closing animation.")],-1),b3={class:"code","data-theme":"dark"},g3=e("pre",null,`<!doctype html>
<html class="modal-is-open modal-is-closing">
  ...
</html>`,-1),_3={__name:"ModalView",setup(t){const o=Ce(!1),a=()=>{o.value=!1},l=()=>{o.value=!0};return(s,r)=>(C(),S(J,null,[f(da,{open:o.value},{default:v(()=>[jx,n(),Fx,n(),Hx,n(),e("footer",null,[e("button",{class:"secondary",onClick:r[0]||(r[0]=i=>a())},`
        Cancel
      `),e("button",{onClick:r[1]||(r[1]=i=>a())},`
        Confirm
      `)])]),_:1},8,["open"]),n(),qx,n(),Ux,n(),e("div",Gx,[e("section",null,[Wx,n(),Kx,n(),Yx,n(),e("div",Jx,[f(k,null,{default:v(()=>[Zx]),_:1})]),n(),Xx,n(),Qx,n(),e("div",e3,[f(k,null,{default:v(()=>[t3]),_:1})])]),n(),e("section",null,[n3,n(),o3,n(),e("article",a3,[e("button",{class:"contrast",onClick:r[2]||(r[2]=i=>l())},`
          Open modal
        `)]),n(),l3,n(),s3]),n(),e("section",null,[r3,n(),i3,n(),c3,n(),e("div",d3,[f(k,null,{default:v(()=>[u3]),_:1})]),n(),h3,n(),e("div",p3,[f(k,null,{default:v(()=>[f3]),_:1})]),n(),m3,n(),e("div",b3,[f(k,null,{default:v(()=>[g3]),_:1})])])])],64))}},v3=e("hgroup",null,[e("p",{class:"chapter"},`
      Components
    `),n(),e("h1",null,"Nav"),n(),e("p",null,"The essential navbar component in pure semantic HTML.")],-1),y3=G('<aside id="table-of-contents"><nav><details open=""><summary>Content</summary> <ul><li><a class="secondary" href="/docs/nav" aria-current="page">Syntax</a></li> <li><a class="secondary" href="/docs/nav#link-variants">Link variants</a></li> <li><a class="secondary" href="/docs/nav#buttons">Buttons</a></li> <li><a class="secondary" href="/docs/nav#vertical-stacking">Vertical stacking</a></li> <li><a class="secondary" href="/docs/nav#breadcrumb">Breadcrumb</a></li></ul></details></nav></aside>',1),w3={role:"document"},x3={"aria-label":"Nav example"},k3=e("nav",null,[e("ul",null,[e("li",null,[e("strong",null,"Acme Corp")])]),n(),e("ul",null,[e("li",null,[e("a",{href:"/docs/nav"},"About")]),n(),e("li",null,[e("a",{href:"/docs/nav"},"Services")]),n(),e("li",null,[e("a",{href:"/docs/nav"},"Products")])])],-1),$3={class:"code","data-theme":"dark"},C3=e("pre",null,`<nav>
  <ul>
    <li><strong>Acme Corp</strong></li>
  </ul>
  <ul>
    <li><a href="#">About</a></li>
    <li><a href="#">Services</a></li>
    <li><a href="#">Products</a></li>
  </ul>
</nav>`,-1),S3=e("p",null,[e("code",null,"<ul>"),n(" are automatically distributed horizontally.")],-1),E3=e("p",null,[e("code",null,"<li>"),n(" are unstyled and inlined.")],-1),T3=e("p",null,[e("code",null,"<a>"),n(" are underlined only on "),e("code",null,":hover"),n(".")],-1),A3=e("h2",null,[n(`
        Link variants`),e("a",{id:"link-variants",href:"#link-variants",class:"secondary",tabindex:"-1"},"#")],-1),M3=e("p",null,[n("You can use "),e("code",null,".secondary"),n(", "),e("code",null,".contrast"),n(", and "),e("code",null,".outline"),n(" classes.")],-1),P3={"aria-label":"Nav example"},N3=e("nav",null,[e("ul",null,[e("li",null,[e("strong",null,"Acme Corp")])]),n(),e("ul",null,[e("li",null,[e("a",{class:"contrast",href:"/docs/nav"},"About")]),n(),e("li",null,[e("a",{class:"contrast",href:"/docs/nav"},"Services")]),n(),e("li",null,[e("a",{class:"contrast",href:"/docs/nav"},"Products")])])],-1),O3={class:"code","data-theme":"dark"},R3=e("pre",null,`<nav>
  <ul>
    <li><strong>Acme Corp</strong></li>
  </ul>
  <ul>
    <li><a href="#" class="contrast">About</a></li>
    <li><a href="#" class="contrast">Services</a></li>
    <li><a href="#" class="contrast">Products</a></li>
  </ul>
</nav>`,-1),I3={"aria-label":"Nav example"},L3=G('<nav><ul><li><a class="secondary" aria-label="Menu" href="/docs/nav"><svg xmlns="http://www.w3.org/2000/svg" width="100%" height="1rem" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path> <path d="M4 6l16 0"></path> <path d="M4 12l16 0"></path> <path d="M4 18l16 0"></path></svg></a></li></ul> <ul><li><strong>Acme Corp</strong></li></ul> <ul><li><a class="secondary" aria-label="Twitter" href="/docs/nav"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="1rem" width="100%"><path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path></svg></a></li></ul></nav>',1),B3={class:"code","data-theme":"dark"},D3=e("pre",null,`<nav>
  <ul>
    <li><a href="#" class="secondary">...</a></li>
  </ul>
  <ul>
    <li><strong>Acme Corp</strong></li>
  </ul>
  <ul>
    <li><a href="#" class="secondary">...</a>
    </li>
  </ul>
</nav>`,-1),z3=e("h2",null,[n(`
        Buttons`),e("a",{id:"buttons",href:"#buttons",class:"secondary",tabindex:"-1"},"#")],-1),V3=e("p",null,[n("You can use "),e("code",null,"<button>"),n(" inside "),e("code",null,"<li>"),n(".")],-1),j3=e("p",null,"Button sizes automatically match link size and margin.",-1),F3={"aria-label":"Nav example"},H3=e("nav",null,[e("ul",null,[e("li",null,[e("strong",null,"Acme Corp")])]),n(),e("ul",null,[e("li",null,[e("a",{href:"/docs/nav"},"About")]),n(),e("li",null,[e("a",{href:"/docs/nav"},"Services")]),n(),e("li",null,[e("button",{class:"secondary"},`
                Products
              `)])])],-1),q3={class:"code","data-theme":"dark"},U3=e("pre",null,`<nav>
  <ul>
    <li><strong>Acme Corp</strong></li>
  </ul>
  <ul>
    <li><a href="#">About</a></li>
    <li><a href="#">Services</a></li>
    <li><button class="secondary">Products</button></li>
  </ul>
</nav>`,-1),G3=e("h2",null,[n(`
        Vertical stacking`),e("a",{id:"vertical-stacking",href:"#vertical-stacking",class:"secondary",tabindex:"-1"},"#")],-1),W3=e("p",null,[n("Inside "),e("code",null,"<aside>"),n(", navs items are stacked vertically.")],-1),K3={"aria-label":"Vertical nav example"},Y3=e("aside",null,[e("nav",null,[e("ul",null,[e("li",null,[e("a",{href:"/docs/nav"},"About")]),n(),e("li",null,[e("a",{href:"/docs/nav"},"Services")]),n(),e("li",null,[e("a",{href:"/docs/nav"},"Products")])])])],-1),J3={class:"code","data-theme":"dark"},Z3=e("pre",null,`<aside>
  <nav>
    <ul>
      <li><a href="#">About</a></li>
      <li><a href="#">Services</a></li>
      <li><a href="#">Products</a></li>
    </ul>
  </nav>
</aside>`,-1),X3=e("p",null,[n("You can also use "),e("a",{href:"/docs/dropdown"},"Dropdowns"),n(" inside navs.")],-1),Q3=e("h2",null,[n(`
        Breadcrumb`),e("a",{id:"breadcrumb",href:"#breadcrumb",class:"secondary",tabindex:"-1"},"#")],-1),e4=e("p",null,[n("With "),e("code",null,'<nav aria-label="breadcrumb">'),n(", you can turn a nav into a breadcrumb.")],-1),t4={"aria-label":"Breadcrumb example"},n4=e("nav",{"aria-label":"breadcrumb"},[e("ul",null,[e("li",null,[e("a",{href:"/docs/nav"},"Home")]),n(),e("li",null,[e("a",{href:"/docs/nav"},"Services")]),n(),e("li",null,"Design")])],-1),o4={class:"code","data-theme":"dark"},a4=e("pre",null,`<nav aria-label="breadcrumb">
  <ul>
    <li><a href="#">Home</a></li>
    <li><a href="#">Services</a></li>
    <li>Design</li>
  </ul>
</nav>`,-1),l4=e("p",null,[n("You can change the divider with a local CSS custom property "),e("code",null,"--nano-nav-breadcrumb-divider"),n(".")],-1),s4={"aria-label":"Breadcrumb example"},r4=e("nav",{"aria-label":"breadcrumb",style:{"--nano-nav-breadcrumb-divider":"'/'"}},[e("ul",null,[e("li",null,[e("a",{href:"/docs/nav"},"Home")]),n(),e("li",null,[e("a",{href:"/docs/nav"},"Services")]),n(),e("li",null,"Design")])],-1),i4={class:"code","data-theme":"dark"},c4=e("pre",null,`<nav
  aria-label="breadcrumb"
  style="--nano-nav-breadcrumb-divider: '/';"
>
  <ul>
    <li><a href="#">Home</a></li>
    <li><a href="#">Services</a></li>
    <li>Design</li>
  </ul>
</nav>`,-1),d4={__name:"NavView",setup(t){return(o,a)=>(C(),S(J,null,[v3,n(),y3,n(),e("div",w3,[e("section",null,[e("article",x3,[k3,n(),e("footer",$3,[f(k,null,{default:v(()=>[C3]),_:1})])]),n(),S3,n(),E3,n(),T3]),n(),e("section",null,[A3,n(),M3,n(),e("article",P3,[N3,n(),e("footer",O3,[f(k,null,{default:v(()=>[R3]),_:1})])]),n(),e("article",I3,[L3,n(),e("footer",B3,[f(k,null,{default:v(()=>[D3]),_:1})])])]),n(),e("section",null,[z3,n(),V3,n(),j3,n(),e("article",F3,[H3,n(),e("footer",q3,[f(k,null,{default:v(()=>[U3]),_:1})])])]),n(),e("section",null,[G3,n(),W3,n(),e("article",K3,[Y3,n(),e("footer",J3,[f(k,null,{default:v(()=>[Z3]),_:1})])])]),n(),X3,n(),e("section",null,[Q3,n(),e4,n(),e("article",t4,[n4,n(),e("footer",o4,[f(k,null,{default:v(()=>[a4]),_:1})])]),n(),l4,n(),e("article",s4,[r4,n(),e("footer",i4,[f(k,null,{default:v(()=>[c4]),_:1})])])])])],64))}},u4=e("hgroup",null,[e("p",{class:"chapter"},`
      Components
    `),n(),e("h1",null,"Note"),n(),e("p",null,"Enable users to quickly understand the context of the information they are reading with the handful of available and flexible note messages.")],-1),h4=e("aside",{id:"table-of-contents"},[e("nav",null,[e("details",{open:""},[e("summary",null,"Content"),n(),e("ul",null,[e("li",null,[e("a",{class:"secondary",href:"/docs/card","aria-current":"page"},"Syntax")]),n(),e("li",null,[e("a",{class:"secondary",href:"/docs/card#sectioning"},"Sectioning")])])])])],-1),p4={role:"document"},f4=G("<p>Notes can be created using the <code>role=&quot;note&quot;</code> attribute and can be further styled with the <code>primary</code>, <code>secondary</code>, <code>contrast</code>, <code>danger</code>, <code>info</code>, <code>success</code> and <code>warning</code> classes.</p>",1),m4=e("div",{class:"primary",role:"note"},[n("A simple "),e("strong",null,"primary"),n(" note.")],-1),b4=e("div",{class:"secondary",role:"note"},[n("A simple "),e("strong",null,"secondary"),n(" note.")],-1),g4=e("div",{class:"contrast",role:"note"},[n("A simple "),e("strong",null,"contrast"),n(" note.")],-1),_4=e("div",{class:"danger",role:"note"},[e("p",null,[n("A simple "),e("strong",null,"danger"),n(" note is expected to be multiline supported. Still got some more text to get this bad buy wrapping.")]),n(),e("p",null,"Paragraphs are also supposed to be styled accordingly.")],-1),v4=e("div",{class:"info",role:"note"},[e("p",null,[n("A simple "),e("strong",null,"info"),n(" note.")]),n(),e("p",null,[n("This line has a "),e("a",{href:"#"},"link"),n(" in it.")])],-1),y4=e("div",{class:"success",role:"note"},[n("A simple "),e("strong",null,"success"),n(" note.")],-1),w4=e("div",{class:"warning",role:"note"},[n("A simple "),e("strong",null,"warning"),n(" note.")],-1),x4={class:"code","data-theme":"dark"},k4=e("pre",null,`<div class="danger" role="note">
  A simple <strong>danger</strong> note.
</div>
<div class="info" role="note">
  A simple <strong>info</strong> note.
</div>
<div class="success" role="note">
  A simple <strong>success</strong> note.
</div>
<div class="warning" role="note">
  A simple <strong>warning</strong> note.
</div>
`,-1),$4={__name:"NoteView",setup(t){return(o,a)=>(C(),S(J,null,[u4,n(),h4,n(),e("div",p4,[e("section",null,[f4,n(),m4,n(),b4,n(),g4,n(),_4,n(),v4,n(),y4,n(),w4,n(),e("div",x4,[f(k,null,{default:v(()=>[k4]),_:1})])])])],64))}},C4=e("p",{class:"chapter"},`
      Layout
    `,-1),S4=e("h1",null,"Overflow Auto",-1),E4={role:"document"},T4=G('<div class="overflow-auto"><table><thead><tr><th>Heading</th> <th>Heading</th> <th>Heading</th> <th>Heading</th> <th>Heading</th> <th>Heading</th> <th>Heading</th> <th>Heading</th> <th>Heading</th> <th>Heading</th></tr></thead> <tbody><tr><td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td></tr> <tr><td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td></tr> <tr><td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td> <td>Cell</td></tr></tbody></table></div>',1),A4={class:"code","data-theme":"dark"},M4=e("pre",null,`<div class="overflow-auto">
  <table>
    ...
  </table>
</div>`,-1),P4={__name:"OverflowAutoView",setup(t){return(o,a)=>{const l=Sn("highlight");return C(),S(J,null,[e("hgroup",null,[C4,n(),S4,n(),e("p",null,[$e((C(),S("code",null,[n(".overflow-auto")])),[[l]]),n(" enables automatic scrollbars to an element if its content extends beyond its limits.")])]),n(),e("div",E4,[e("section",null,[e("p",null,[n("Useful to have responsive "),$e((C(),S("code",null,[n("<table>")])),[[l]]),n(".")]),n(),T4,n(),e("div",A4,[f(k,{language:"xml"},{default:v(()=>[M4]),_:1})])])])],64)}}},N4=e("hgroup",null,[e("p",{class:"chapter"},`
      Components
    `),n(),e("h1",null,"Progress"),n(),e("p",null,"The progress bar element in pure HTML, without JavaScript.")],-1),O4=e("aside",{id:"table-of-contents"},[e("nav",null,[e("details",{open:""},[e("summary",null,"Content"),n(),e("ul",null,[e("li",null,[e("a",{class:"secondary",href:"/docs/progress","aria-current":"page"},"Syntax")]),n(),e("li",null,[e("a",{class:"secondary",href:"/docs/progress#indeterminate"},"Indeterminate")])])])])],-1),R4={role:"document"},I4={"aria-label":"Progress example",class:"component"},L4=e("progress",{value:"94",max:"100"},null,-1),B4={class:"code","data-theme":"dark"},D4=e("pre",null,'<progress value="51" max="100" />',-1),z4=e("h2",null,[n(`
        Indeterminate`),e("a",{id:"indeterminate",href:"#indeterminate",class:"secondary",tabindex:"-1"},"#")],-1),V4={"aria-label":"Indeterminate progress example",class:"component"},j4=e("progress",null,null,-1),F4={class:"code","data-theme":"dark"},H4=e("pre",null,"<progress />",-1),q4={__name:"ProgressView",setup(t){return(o,a)=>(C(),S(J,null,[N4,n(),O4,n(),e("div",R4,[e("section",null,[e("article",I4,[L4,n(),e("footer",B4,[f(k,null,{default:v(()=>[D4]),_:1})])])]),n(),e("section",null,[z4,n(),e("article",V4,[j4,n(),e("footer",F4,[f(k,null,{default:v(()=>[H4]),_:1})])])])])],64))}},U4=e("hgroup",null,[e("p",{class:"chapter"},`
      Getting started
    `),n(),e("h1",null,"Quick Start"),n(),e("p",null,[n(`
      Link `),e("code",null,"nano.css"),n(` manually for a dependency-free setup. If you want the Vue extensions then also link
      `),e("code",null,"nano-vue.js"),n(`.
    `)])],-1),G4=e("aside",{id:"table-of-contents"},[e("nav",null,[e("details",{open:""},[e("summary",null,"Content"),n(),e("ul",null,[e("li",null,[e("a",{class:"secondary",href:"/docs#install-manually"},"Install manually")]),n(),e("li",null,[e("a",{class:"secondary",href:"/docs#starter-html-template"},"Starter HTML template")])])])])],-1),W4={role:"document"},K4=e("section",{"aria-label":"Introduction"},[e("p",null,"There is only 1 way to get started with nano.css:")],-1),Y4=e("h2",null,[n(`
        Install manually`),e("a",{id:"install-manually",href:"#install-manually",class:"secondary",tabindex:"-1"},"#")],-1),J4=e("a",{rel:"noopener noreferrer",href:"https://gitlab.com/firnsy/nano/-/archive/develop/nano-develop.zip",target:"_blank"},"Download Nano",-1),Z4=e("code",null,"/dist/css/nano.min.css",-1),X4={class:"language-xml"},Q4={class:"code small","data-theme":"dark"},e5=e("pre",null,'<link rel="stylesheet" href="css/nano.min.css" />',-1),t5=e("h2",null,[n(`
        Starter HTML template`),e("a",{id:"starter-html-template",href:"#starter-html-template",class:"secondary",tabindex:"-1"},"#")],-1),n5={class:"code","data-theme":"dark"},o5=e("pre",null,`<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/nano.min.css">
    <script src="js/nano-vue.min.js" />
    <title>Hello, world!</title>
  </head>
  <body>
    <main class="container">
      <h1>Hello, world!</h1>
    </main>
  </body>
</html>`,-1),a5={__name:"QuickStartView",setup(t){return(o,a)=>{const l=Sn("highlight");return C(),S(J,null,[U4,n(),G4,n(),e("div",W4,[K4,n(),e("section",null,[Y4,n(),e("p",null,[J4,n(" and link "),Z4,n(" in the "),$e((C(),S("code",X4,[n("<head>")])),[[l]]),n(` of your website.
      `)]),n(),e("div",Q4,[f(k,null,{default:v(()=>[e5]),_:1})])]),n(),e("section",null,[t5,n(),e("div",n5,[f(k,null,{default:v(()=>[o5]),_:1})])])])],64)}}},l5=e("hgroup",null,[e("p",{class:"chapter"},`
      Customisation
    `),n(),e("h1",null,"Sass"),n(),e("p",null,[n(`
      Build your own minimal design system by compiling a custom version of Nano's CSS framework with `),e("a",{rel:"noopener noreferrer",href:"https://sass-lang.com/",target:"_blank"},"SASS"),n(`.
    `)])],-1),s5=G('<aside id="table-of-contents"><nav><details open><summary>Content</summary> <ul><li><a class="secondary" href="/docs/sass">Introduction</a></li> <li><a class="secondary" href="/docs/sass#settings">Settings</a></li> <li><a class="secondary" href="/docs/sass#custom-theme">Custom theme</a></li></ul></details></nav></aside>',1),r5={role:"document"},i5=e("p",null,`
        To get the most out of Nano, we recommend compiling your own version with SASS. This way, you can include only
        the required modules and personalize the settings without overriding CSS styles.
      `,-1),c5=e("p",null,`
        Avoid modifying Nano’s core files whenever possible. This approach allows you to keep Nano up to date without
        conflicts since the Nano code and your custom code are separated.
      `,-1),d5=e("p",null,[n(`
        You can import Nano into your SCSS file with `),e("a",{rel:"noopener noreferrer",href:"https://sass-lang.com/documentation/at-rules/use",target:"_blank"},"@use"),n(`:
      `)],-1),u5={class:"code","data-theme":"dark"},h5=e("pre",null,'@use "nano";',-1),p5=e("p",null,[n(`
        If you are using `),e("a",{rel:"noopener noreferrer",href:"https://sass-lang.com/documentation/cli/dart-sass",target:"_blank"},"Sass Command-Line Interface"),n(" to compile your "),e("code",null,".scss"),n(` files, you can define the load path using
        `),e("code",null,"sass --load-path=node_modules/@nans/nano/scss/"),n(` to avoid using relative URLs like:
      `)],-1),f5={class:"code","data-theme":"dark"},m5=e("pre",null,'@use "../../../node_modules/@nano/nano/scss/nano";',-1),b5=e("h2",null,[n(`
        Settings`),e("a",{id:"settings",href:"#settings",class:"secondary",tabindex:"-1"},"#")],-1),g5=e("p",null,[n(`
        You can set custom settings with `),e("code",null,'@use "nano" with ( ... );'),n(`. These custom values will
        override the default variables.
      `)],-1),_5=e("p",null,"Here is an example to generate the classless version:",-1),v5={class:"code","data-theme":"dark"},y5=e("pre",null,`// nano classless version
@use "nano" with (
  $enable-semantic-container: true,
  $enable-classes: false
); `,-1),w5=e("p",null,[n(`
        Example to generate a lightweight version without `),e("code",null,".classes"),n(`, uncommon form elements, and components.
      `)],-1),x5=e("p",null,"This version reduces the weight of Nano by ~50%.",-1),k5={class:"code","data-theme":"dark"},$5=e("pre",null,`// nano lightweight version
@use "nano" with (
  $enable-semantic-container: true,
  $enable-classes: false,
  $modules: (
    "content/code": false,
    "forms/input-color": false,
    "forms/input-date": false,
    "forms/input-file": false,
    "forms/input-range": false,
    "forms/input-search": false,
    "components/accordion": false,
    "components/card": false,
    "components/dropdown": false,
    "components/loading": false,
    "components/modal": false,
    "components/nav": false,
    "components/progress": false,
    "components/tooltip": false,
    "utilities/accessibility": false,
    "utilities/reduce-motion": false
  )
); `,-1),C5=e("summary",{role:"button",class:"secondary"},`
          All default settings
        `,-1),S5={class:"code","data-theme":"dark"},E5=e("pre",null,`// prefix for css variables
$css-var-prefix: "nano-" !default;

// define the root element used to target <header>, <main>, <footer>
// with $enable-semantic-container and $enable-responsive-spacings
$semantic-root-element: "body" !default;

// enable <header>, <main>, <footer> inside $semantic-root-element as containers
$enable-semantic-container: false !default;

// enable a centered viewport for <header>, <main>, <footer> inside $semantic-root-element
// fluid layout if disabled
$enable-viewport: true !default;

// enable responsive spacings for <header>, <main>, <footer>, <section>, <article>
// fixed spacings if disabled
$enable-responsive-spacings: true !default;

// enable responsive typography
// fixed root element size (rem) if disabled
$enable-responsive-typography: true !default;

// enable .classes
// .classless version if disabled
$enable-classes: true !default;

// enable transitions
$enable-transitions: true !default;

// enable overriding with !important
$enable-important: true !default;

$breakpoints: map.deep-merge(
  (
    // extra small (less than landscape phones)
    // font size: 16px
    xs: (
      breakpoint: 0,
      viewport: 100%,
      root-font-size: 100%,
    ),
    // small (landscape phones)
    // font size: 17px
    sm:
      (
        breakpoint: 576px,
        viewport: 540px,
        root-font-size: 106.25%,
      ),

    // medium (tablets)
    // font size: 18px
    md:
      (
        breakpoint: 768px,
        viewport: 720px,
        root-font-size: 112.5%,
      ),

    // large
    // font size: 19px
    lg:
      (
        breakpoint: 992px,
        viewport: 960px,
        root-font-size: 118.75%,
      ),

    // extra large
    // font size: 20px
    xl:
      (
        breakpoint: 1280px,
        viewport: 1200px,
        root-font-size: 125%,
      ),

    // extra extra large
    // font size: 21px
    xxl:
      (
        breakpoint: 1536px,
        viewport: 1440px,
        root-font-size: 131.25%,
      )
  ),
  $breakpoints
);

// modules to export
$modules: () !default;
$modules: map.merge(
  (
    // theme
    "themes/default": true,

    // layout
    "layout/document": true,
    "layout/landmarks": true,
    "layout/container": true,
    "layout/section": true,
    "layout/grid": true,
    "layout/scroller": true,

    // content
    "content/link": true,
    "content/typography": true,
    "content/embedded": true,
    "content/button": true,
    "content/table": true,
    "content/code": true,
    "content/miscs": true,

    // forms
    "forms/basics": true,
    "forms/checkbox-radio-switch": true,
    "forms/input-color": true,
    "forms/input-date": true,
    "forms/input-file": true,
    "forms/input-range": true,
    "forms/input-search": true,

    // components
    "components/accordion": true,
    "components/card": true,
    "components/dropdown": true,
    "components/group": true,
    "components/loading": true,
    "components/modal": true,
    "components/nav": true,
    "components/progress": true,
    "components/tooltip": true,

    // utilities
    "utilities/accessibility": true,
    "utilities/reduce-motion": true
  ),
  $modules
);

// Shortcut for CSS vars prefix
$p: --#{$css-var-prefix};`,-1),T5=e("h2",null,[n(`
        Custom theme`),e("a",{id:"custom-theme",href:"#custom-theme",class:"secondary",tabindex:"-1"},"#")],-1),A5=e("p",null,"To create a custom version of Nano with a fully custom theme that reflects your brand identity, you can:",-1),M5=e("ol",null,[e("li",null,"Exclude the default theme from compilation,"),n(),e("li",null,[n(`
          Import your custom theme (you can duplicate `),e("a",{rel:"noopener noreferrer",href:"https://github.com/nanocss/nano/tree/v2/scss/themes/",target:"_blank"},"Nano's default theme"),n(` as a starting point and customise it to match your brand's style).
        `)])],-1),P5={class:"code","data-theme":"dark"},N5=e("pre",null,`// your custom theme
@use "path/custom-theme";

// nano without default theme
@use "nano" with (
  $modules: (
    "themes/default": false
  )
);`,-1),O5={__name:"SassView",setup(t){return(o,a)=>(C(),S(J,null,[l5,n(),s5,n(),e("div",r5,[e("section",null,[i5,n(),c5,n(),d5,n(),e("div",u5,[f(k,null,{default:v(()=>[h5]),_:1})]),n(),p5,n(),e("div",f5,[f(k,null,{default:v(()=>[m5]),_:1})])]),n(),e("section",null,[b5,n(),g5,n(),_5,n(),e("div",v5,[f(k,null,{default:v(()=>[y5]),_:1})]),n(),w5,n(),x5,n(),e("div",k5,[f(k,null,{default:v(()=>[$5]),_:1})]),n(),e("details",null,[C5,n(),e("div",S5,[f(k,null,{default:v(()=>[E5]),_:1})])])]),n(),e("section",null,[T5,n(),A5,n(),M5,n(),e("div",P5,[f(k,null,{default:v(()=>[N5]),_:1})])])])],64))}},R5=e("hgroup",null,[e("p",{class:"chapter"},`
      Content
    `),n(),e("h1",null,"Table"),n(),e("p",null,[n(`
      Clean and minimal styles for `),e("code",null,"<table>"),n(`, providing consistent spacings and a minimal unbordered
      look.
    `)])],-1),I5=G('<aside id="table-of-contents"><nav><details open><summary>Content</summary> <ul><li><a class="secondary" href="/docs/table">Syntax</a></li> <li><a class="secondary" href="/docs/table#color-schemes">Color schemes</a></li> <li><a class="secondary" href="/docs/table#striped">Striped</a></li></ul></details></nav></aside>',1),L5={role:"document"},B5=G(`<div class="overflow-auto"><table><thead><tr><th scope="col">
                Planet
              </th> <th scope="col">
                Diam. (km)
              </th> <th scope="col">
                Dist. to Sun (AU)
              </th> <th scope="col">
                Grav. (m/s²)
              </th></tr></thead> <tbody><tr><th scope="row">
                Mercury
              </th> <td>4,880</td> <td>0.39</td> <td>3.7</td></tr> <tr><th scope="row">
                Venus
              </th> <td>12,104</td> <td>0.72</td> <td>8.9</td></tr> <tr><th scope="row">
                Earth
              </th> <td>12,742</td> <td>1.00</td> <td>9.8</td></tr> <tr><th scope="row">
                Mars
              </th> <td>6,779</td> <td>1.52</td> <td>3.7</td></tr></tbody> <tfoot><tr><th scope="row">
                Average
              </th> <td>9,126</td> <td>0.91</td> <td>6.5</td></tr></tfoot></table></div>`,1),D5={class:"code","data-theme":"dark"},z5=e("pre",null,`<table>
  <thead>
    <tr>
      <th scope="col">Planet</th>
      <th scope="col">Diameter (km)</th>
      <th scope="col">Distance to Sun (AU)</th>
      <th scope="col">Orbit (days)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Mercury</th>
      <td>4,880</td>
      <td>0.39</td>
      <td>88</td>
    </tr>
    <tr>
      <th scope="row">Venus</th>
      <td>12,104</td>
      <td>0.72</td>
      <td>225</td>
    </tr>
    <tr>
      <th scope="row">Earth</th>
      <td>12,742</td>
      <td>1.00</td>
      <td>365</td>
    </tr>
    <tr>
      <th scope="row">Mars</th>
      <td>6,779</td>
      <td>1.52</td>
      <td>687</td>
    </tr>
  </tbody>
  <tfoot>
    <tr>
      <th scope="row">Average</th>
      <td>9,126</td>
      <td>0.91</td>
      <td>341</td>
    </tr>
  </tfoot>
</table>`,-1),V5=e("h2",null,[n(`
        Color schemes`),e("a",{id:"color-schemes",href:"#color-schemes",class:"secondary",tabindex:"-1"},"#")],-1),j5=G(`<p><code>data-theme=&quot;light&quot;</code> or <code>data-theme=&quot;dark&quot;</code> can be used at any level:
        <code>&lt;table&gt;</code>, <code>&lt;thead&gt;</code>, <code>&lt;tbody&gt;</code>, <code>&lt;tfoot&gt;</code>,
        <code>&lt;tr&gt;</code>, <code>&lt;th&gt;</code>, <code>&lt;td&gt;</code>.
      </p>`,1),F5=G(`<div class="overflow-auto"><table><thead data-theme="dark"><tr><th scope="col">
                Planet
              </th> <th scope="col">
                Diam. (km)
              </th> <th scope="col">
                Dist. to Sun (AU)
              </th> <th scope="col">
                Grav. (m/s²)
              </th></tr></thead> <tbody><tr><th scope="row">
                Mercury
              </th> <td>4,880</td> <td>0.39</td> <td>3.7</td></tr> <tr><th scope="row">
                Venus
              </th> <td>12,104</td> <td>0.72</td> <td>8.9</td></tr> <tr><th scope="row">
                Earth
              </th> <td>12,742</td> <td>1.00</td> <td>9.8</td></tr> <tr><th scope="row">
                Mars
              </th> <td>6,779</td> <td>1.52</td> <td>3.7</td></tr></tbody> <tfoot><tr><th scope="row">
                Average
              </th> <td>9,126</td> <td>0.91</td> <td>6.5</td></tr></tfoot></table></div>`,1),H5=e("pre",null,`<table>
  <thead data-theme="dark">
    ...
  </thead>
  <tbody>...</tbody>
  <tfoot>...</tfoot>
</table>`,-1),q5=e("h2",null,[n(`
        Striped table`),e("a",{id:"striped",href:"#striped",class:"secondary",tabindex:"-1"},"#")],-1),U5=e("p",null,[e("code",null,".striped"),n(" enable striped rows.")],-1),G5=G(`<div class="overflow-auto"><table class="striped"><thead><tr><th scope="col">
                Planet
              </th> <th scope="col">
                Diam. (km)
              </th> <th scope="col">
                Dist. to Sun (AU)
              </th> <th scope="col">
                Grav. (m/s²)
              </th></tr></thead> <tbody><tr><th scope="row">
                Mercury
              </th> <td>4,880</td> <td>0.39</td> <td>3.7</td></tr> <tr><th scope="row">
                Venus
              </th> <td>12,104</td> <td>0.72</td> <td>8.9</td></tr> <tr><th scope="row">
                Earth
              </th> <td>12,742</td> <td>1.00</td> <td>9.8</td></tr> <tr><th scope="row">
                Mars
              </th> <td>6,779</td> <td>1.52</td> <td>3.7</td></tr></tbody> <tfoot><tr><th scope="row">
                Average
              </th> <td>9,126</td> <td>0.91</td> <td>6.5</td></tr></tfoot></table></div>`,1),W5={class:"code","data-theme":"dark"},K5=G('<a class="copy-to-clipboard" tabindex="-1" aria-label="Copy code" data-tooltip="Copy to clipboard" data-placement="left" href="/docs/table"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round" class="clipboard"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path> <path d="M8 8m0 2a2 2 0 0 1 2 -2h8a2 2 0 0 1 2 2v8a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2z"></path> <path d="M16 8v-2a2 2 0 0 0 -2 -2h-8a2 2 0 0 0 -2 2v8a2 2 0 0 0 2 2h2"></path></svg></a>',1),Y5=e("h2",null,[n(`
        Hover table`),e("a",{id:"hover",href:"#hover",class:"secondary",tabindex:"-1"},"#")],-1),J5=e("p",null,[e("code",null,".hover"),n(" enable striped rows.")],-1),Z5=G(`<div class="overflow-auto"><table class="hover"><thead><tr><th scope="col">
                Planet
              </th> <th scope="col">
                Diam. (km)
              </th> <th scope="col">
                Dist. to Sun (AU)
              </th> <th scope="col">
                Grav. (m/s²)
              </th></tr></thead> <tbody><tr><th scope="row">
                Mercury
              </th> <td>4,880</td> <td>0.39</td> <td>3.7</td></tr> <tr><th scope="row">
                Venus
              </th> <td>12,104</td> <td>0.72</td> <td>8.9</td></tr> <tr><th scope="row">
                Earth
              </th> <td>12,742</td> <td>1.00</td> <td>9.8</td></tr> <tr><th scope="row">
                Mars
              </th> <td>6,779</td> <td>1.52</td> <td>3.7</td></tr></tbody> <tfoot><tr><th scope="row">
                Average
              </th> <td>9,126</td> <td>0.91</td> <td>6.5</td></tr></tfoot></table></div>`,1),X5={class:"code","data-theme":"dark"},Q5=e("pre",null,`<table class="hover">
  ...
</table>`,-1),ek={__name:"TableView",setup(t){return(o,a)=>{const l=Sn("highlight");return C(),S(J,null,[R5,n(),I5,n(),e("div",L5,[e("section",null,[B5,n(),e("div",D5,[f(k,null,{default:v(()=>[z5]),_:1})])]),n(),e("section",null,[V5,n(),j5,n(),F5,n(),f(k,null,{default:v(()=>[H5]),_:1})]),n(),e("section",null,[q5,n(),U5,n(),G5,n(),e("div",W5,[K5,n(),e("pre",null,[$e((C(),S("code",null,[n(`
<table class="striped">
  ...
</table>
        `)])),[[l]])])])]),n(),e("section",null,[Y5,n(),J5,n(),Z5,n(),e("div",X5,[f(k,null,{default:v(()=>[Q5]),_:1})])])])],64)}}},tk=e("hgroup",null,[e("p",{class:"chapter"},`
      Components
    `),n(),e("h1",null,"Tabs"),n(),e("p",null,"ARIA compabitble tabs with semantic HTML.")],-1),nk=e("aside",{id:"table-of-contents"},[e("nav",null,[e("details",{open:""},[e("summary",null,"Content"),n(),e("ul",null,[e("li",null,[e("a",{class:"secondary",href:"/docs/tabs","aria-current":"page"},"Syntax")])])])])],-1),ok={role:"document"},ak={"aria-label":"Nav example"},lk=e("nav",{role:"tablist","aria-label":"tabs"},[e("ul",null,[e("li",null,[e("a",{id:"tab-1",role:"tab","aria-controls":"panel-1",tabindex:"0"},"First Tab")]),n(),e("li",null,[e("a",{id:"tab-2",role:"tab","aria-controls":"panel-2",tabindex:"-1"},"Second Tab")]),n(),e("li",null,[e("a",{id:"tab-3",role:"tab","aria-controls":"panel-3",tabindex:"-1"},"Third Tab")])])],-1),sk=e("section",null,[e("div",{id:"panel-1",role:"tabpanel",tabindex:"0","aria-labelledby":"tab-1"},[e("p",null,"Content for the first panel")]),n(),e("div",{id:"panel-2",role:"tabpanel",tabindex:"0","aria-labelledby":"tab-2",hidden:""},[e("p",null,"Content for the second panel")]),n(),e("div",{id:"panel-3",role:"tabpanel",tabindex:"0","aria-labelledby":"tab-3",hidden:""},[e("p",null,"Content for the third panel")])],-1),rk={class:"code","data-theme":"dark"},ik=e("pre",null,`<nav role="tablist" aria-label="tabs">
 <ul>
    <li><a role="tab" id="tab-1">First Tab</a></li>
    <li><a role="tab" id="tab-2">Second Tab</a></li>
    <li><a role="tab" id="tab-3">Third Tab</a></li>
  </ul>
</nav>
<section>
  <div id="panel-1" role="tabpanel" aria-labelledby="tab-1">
    <p>Content for the first panel</p>
  </div>
  <div id="panel-2" role="tabpanel" aria-labelledby="tab-2" hidden>
    <p>Content for the second panel</p>
  </div>
  <div id="panel-3" role="tabpanel" aria-labelledby="tab-3" hidden>
    <p>Content for the third panel</p>
  </div>
</section>`,-1),ck=e("p",null,[e("code",null,"<ul>"),n(" are automatically distributed horizontally.")],-1),dk=e("p",null,[e("code",null,"<li>"),n(" are unstyled and inlined.")],-1),uk=e("p",null,[e("code",null,"<a>"),n(" are underlined only on "),e("code",null,":hover"),n(".")],-1),hk={__name:"TabsView",setup(t){return(o,a)=>(C(),S(J,null,[tk,n(),nk,n(),e("div",ok,[e("section",null,[e("article",ak,[lk,n(),sk,n(),e("footer",rk,[f(k,null,{default:v(()=>[ik]),_:1})])]),n(),ck,n(),dk,n(),uk])])],64))}},pk=e("hgroup",null,[e("p",{class:"chapter"},`
      Components
    `),n(),e("h1",null,"Tooltip"),n(),e("p",null,"Enable tooltips everywhere, without JavaScript.")],-1),fk=e("aside",{id:"table-of-contents"},[e("nav",null,[e("details",{open:""},[e("summary",null,"Content"),n(),e("ul",null,[e("li",null,[e("a",{class:"secondary",href:"/docs/tooltip","aria-current":"page"},"Syntax")]),n(),e("li",null,[e("a",{class:"secondary",href:"/docs/tooltip#placement"},"Placement")])])])])],-1),mk={role:"document"},bk={"aria-label":"Tooltip example"},gk=e("p",null,[n(`
          Tooltip on a `),e("a",{"data-tooltip":"Tooltip",href:"/docs/tooltip"},"link")],-1),_k=e("p",null,[n("Tooltip on "),e("em",{"data-tooltip":"Tooltip"},"inline element")],-1),vk=e("p",null,[e("button",{"data-tooltip":"Tooltip"},`
            Tooltip on a button
          `)],-1),yk={class:"code","data-theme":"dark"},wk=e("pre",null,`<p>Tooltip on a <a href="#" data-tooltip="Tooltip">link</a></p>
<p>Tooltip on <em data-tooltip="Tooltip">inline element</em></p>
<p><button data-tooltip="Tooltip">Tooltip on a button</button></p>`,-1),xk=e("h2",null,[n(`
        Placement`),e("a",{id:"placement",href:"#placement",class:"secondary",tabindex:"-1"},"#")],-1),kk=e("p",null,[n(`
        The tooltip is displayed on top by default but you can change it with the `),e("code",null,"data-placement"),n(` attribute.
      `)],-1),$k={id:"tooltip-placement","aria-label":"Tooltip placement example"},Ck=G(`<main class="grid"><button data-tooltip="Top">
            Top
          </button><button data-tooltip="Right" data-placement="right">
            Right
          </button><button data-tooltip="Bottom" data-placement="bottom">
            Bottom
          </button><button data-tooltip="Left" data-placement="left">
            Left
          </button></main>`,1),Sk={class:"code","data-theme":"dark"},Ek=e("pre",null,`<button data-tooltip="Top">Top</button>
<button data-tooltip="Right" data-placement="right">Right</button>
<button data-tooltip="Bottom" data-placement="bottom">Bottom</button>
<button data-tooltip="Left" data-placement="left">Left</button>`,-1),Tk={__name:"TooltipView",setup(t){return(o,a)=>(C(),S(J,null,[pk,n(),fk,n(),e("div",mk,[e("section",null,[e("article",bk,[gk,n(),_k,n(),vk,n(),e("footer",yk,[f(k,null,{default:v(()=>[wk]),_:1})])])]),n(),e("section",null,[xk,n(),kk,n(),e("article",$k,[Ck,n(),e("footer",Sk,[f(k,null,{default:v(()=>[Ek]),_:1})])])])])],64))}},Ak=e("hgroup",null,[e("p",{class:"chapter"},`
      Content
    `),n(),e("h1",null,"Typography"),n(),e("p",null,"All typographic elements are responsive and scale gracefully across devices and viewports.")],-1),Mk=G('<aside id="table-of-contents"><nav><details open><summary>Content</summary> <ul><li><a class="secondary" href="/docs/typography">Font sizes</a></li> <li><a class="secondary" href="/docs/typography#headings">Headings</a></li> <li><a class="secondary" href="/docs/typography#heading-group">Heading group</a></li> <li><a class="secondary" href="/docs/typography#inline-text-elements">Inline text elements</a></li> <li><a class="secondary" href="/docs/typography#blockquote">Blockquote</a></li> <li><a class="secondary" href="/docs/typography#horizontal-rule">Horizontal rule</a></li></ul></details></nav></aside>',1),Pk={role:"document"},Nk={id:"responsive-font-sizes",class:"striped"},Ok=e("caption",null,"Responsive Font Sizes",-1),Rk=e("th",null,"Breakpoint",-1),Ik=["data-tooltip"],Lk=e("td",null,"Base",-1),Bk={key:0,"data-placement":"bottom"},Dk=["data-tooltip"],zk={key:0,"data-placement":"bottom"},Vk=["data-tooltip"],jk=e("p",null,[n(`
        To ensure that the user's default font size is followed, the base font size is defined as a percentage that
        grows with the user's screen size, while HTML elements are defined in `),e("code",null,"rem"),n(`.
      `)],-1),Fk=e("p",null,[n(`
        Since `),e("code",null,"rem"),n(` is a multiplier of the HTML document font size, all HTML element's font sizes grow
        proportionally with the size of the user's screen.
      `)],-1),Hk=e("h2",null,[n(`
        Headings`),e("a",{id:"headings",href:"#headings",class:"secondary",tabindex:"-1"},"#")],-1),qk={"aria-label":"Headings example",class:"component"},Uk=e("h1",null,"Heading 1",-1),Gk=e("h2",null,"Heading 2",-1),Wk=e("h3",null,"Heading 3",-1),Kk=e("h4",null,"Heading 4",-1),Yk=e("h5",null,"Heading 5",-1),Jk=e("h6",null,"Heading 6",-1),Zk={class:"code","data-theme":"dark"},Xk=e("pre",null,`<h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<h5>Heading 5</h5>
<h6>Heading 6</h6>`,-1),Qk=e("h2",null,[n(`
        Heading group`),e("a",{id:"heading-group",href:"#heading-group",class:"secondary",tabindex:"-1"},"#")],-1),e$=e("p",null,[n("Inside a "),e("code",null,"<hgroup>"),n(", margins are collapsed, and the "),e("code",null,":last-child"),n(" is muted.")],-1),t$={"aria-label":"Headings example",class:"component"},n$=e("hgroup",null,[e("h2",null,"Get inspired with CSS"),n(),e("p",null,"How to use CSS to add glam to your Website?")],-1),o$={class:"code","data-theme":"dark"},a$=e("pre",null,`<hgroup>
  <h2>Get inspired with CSS</h2>
  <p>How to use CSS to add glam to your Website?</p>
</hgroup>`,-1),l$=G(`<section><h2>
        Inline text elements<a id="inline-text-elements" href="#inline-text-elements" class="secondary" tabindex="-1">#</a></h2> <div class="container"><div class="row"><div class="col"><p><abbr title="Abbreviation">Abbr.</abbr> <code class="language-xml">&lt;abbr&gt;</code></p> <p><strong>Bold</strong> <code class="language-xml">&lt;strong&gt;</code> <code class="language-xml">&lt;b&gt;</code></p> <p><em>Italic</em> <code class="language-xml">&lt;i&gt;</code> <code class="language-xml">&lt;em&gt;</code> <code class="language-xml">&lt;cite&gt;</code></p> <p><del>Deleted</del> <code class="language-xml">&lt;del&gt;</code></p> <p><ins>Inserted</ins> <code class="language-xml">&lt;ins&gt;</code></p> <p><kbd>Ctrl + S</kbd> <code class="language-xml">&lt;kbd&gt;</code></p></div> <div class="col"><p><mark>Highlighted</mark> <code class="language-xml">&lt;mark&gt;</code></p> <p><s>Strikethrough</s> <code class="language-xml">&lt;s&gt;</code></p> <p><small>Small </small><code class="language-xml">&lt;small&gt;</code></p> <p>Text <sub>Sub</sub> <code class="language-xml">&lt;sub&gt;</code></p> <p>Text <sup>Sup</sup> <code class="language-xml">&lt;sup&gt;</code></p> <p><u>Underline</u> <code class="language-xml">&lt;u&gt;</code></p></div></div></div></section>`,1),s$=e("h2",null,[n(`
        Blockquote`),e("a",{id:"blockquote",href:"#blockquote",class:"secondary",tabindex:"-1"},"#")],-1),r$=e("blockquote",null,[n(`
        “Design is a funny word. Some people think design means how it looks. But of course, if you dig deeper, it's
        really how it works.”
        `),e("footer",null,[e("cite",null,"— Steve Jobs")])],-1),i$={class:"code","data-theme":"dark"},c$=e("pre",null,`<blockquote>
  “Design is a funny word. Some people think
  design means how it looks. But of course, if
  you dig deeper, it's really how it works.”
  <footer>
    <cite>— Steve Jobs</cite>
  </footer>
</blockquote>`,-1),d$=e("h2",null,[n(`
        Horizontal rule`),e("a",{id:"horizontal-rule",href:"#horizontal-rule",class:"secondary",tabindex:"-1"},"#")],-1),u$=e("p",null,[n("The "),e("code",null,"<hr>"),n(" tag renders a horizontal line.")],-1),h$={"aria-label":"Horizontal rule example",class:"component"},p$=e("p",null,"Paragraph before the horizontal line.",-1),f$=e("hr",null,null,-1),m$=e("p",null,"Paragraph after the horizontal line.",-1),b$={class:"code","data-theme":"dark"},g$=e("pre",null,`<p>Paragraph before the horizontal line.</p>
<hr />
<p>Paragraph after the horizontal line.</p>`,-1),_$="16px",v$={__name:"TypographyView",setup(t){const o=[{name:"xs",label:"Extra Small < 576px",fontSize:16,fontSizeRemPx:"16px",fontSizePercent:"100%"},{name:"sm",label:"Small ≥ 576px",fontSize:17,fontSizeRemPx:"17px",fontSizePercent:"106.25%"},{name:"md",label:"Medium ≥ 768px",fontSize:18,fontSizeRemPx:"18px",fontSizePercent:"112.5%"},{name:"lg",label:"Large ≥ 1024px",fontSize:19,fontSizeRemPx:"19px",fontSizePercent:"118.75%"},{name:"xl",label:"Extra Large ≥ 1280px",fontSize:20,fontSizeRemPx:"20px",fontSizePercent:"125%"},{name:"xxl",label:"Extra Extra Large ≥ 1536px",fontSize:21,fontSizeRemPx:"21px",fontSizePercent:"131.25%"}],a=[{label:"<h1>",value:2,unit:"rem"},{label:"<h2>",value:1.75,unit:"rem"},{label:"<h3>",value:1.5,unit:"rem"},{label:"<h4>",value:1.25,unit:"rem"},{label:"<h5>",value:1.125,unit:"rem"},{label:"<h6>",value:1,unit:"rem"},{label:"<small>",value:.875,unit:"em"}],l=Ce(!1),s=ue(()=>l.value?"Display font sizes in pixels.":"Display font sizes in percent and rem.");function r(){l.value=!l.value}function i(u){return l.value?`${u.fontSizePercent}`:`${u.fontSize}px`}function c(u,p){return l.value?`${p.value}${p.unit}`:`${u.fontSize*p.value}px`}function d(u){return`${u.fontSizePercent} * ${_$}`}function h(u,p){return`${u.fontSizePercent} * ${p.value}${p.unit} * ${u.fontSizeRemPx}`}return(u,p)=>(C(),S(J,null,[Ak,n(),Mk,n(),e("div",Pk,[e("section",null,[e("table",Nk,[Ok,n(),e("thead",null,[e("tr",null,[Rk,n(),(C(),S(J,null,Oe(o,m=>e("th",{key:m.name},[e("span",{"data-tooltip":m.label,"data-placement":"bottom"},re(m.name),9,Ik)])),64))])]),n(),e("tbody",null,[e("tr",null,[Lk,n(),(C(),S(J,null,Oe(o,m=>e("td",{key:m.name},[l.value?(C(),S("span",Bk,re(i(m)),1)):(C(),S("span",{key:1,"data-tooltip":d(m),"data-placement":"bottom"},re(i(m)),9,Dk))])),64))]),n(),(C(),S(J,null,Oe(a,m=>e("tr",{key:m.label},[e("td",null,[e("code",null,re(m.label),1)]),n(),(C(),S(J,null,Oe(o,g=>e("td",{key:g.name},[l.value?(C(),S("span",zk,re(c(g,m)),1)):(C(),S("span",{key:1,"data-tooltip":h(g,m),"data-placement":"bottom"},re(c(g,m)),9,Vk))])),64))])),64))])]),n(),e("p",null,[e("a",{href:"#",onClick:p[0]||(p[0]=m=>r())},re(s.value),1)]),n(),jk,n(),Fk]),n(),e("section",null,[Hk,n(),e("article",qk,[Uk,n(),Gk,n(),Wk,n(),Kk,n(),Yk,n(),Jk,n(),e("footer",Zk,[f(k,null,{default:v(()=>[Xk]),_:1})])])]),n(),e("section",null,[Qk,n(),e$,n(),e("article",t$,[n$,n(),e("footer",o$,[f(k,{language:"xml"},{default:v(()=>[a$]),_:1})])])]),n(),l$,n(),e("section",null,[s$,n(),r$,n(),e("div",i$,[f(k,null,{default:v(()=>[c$]),_:1})])]),n(),e("section",null,[d$,n(),u$,n(),e("article",h$,[p$,n(),f$,n(),m$,n(),e("footer",b$,[f(k,null,{default:v(()=>[g$]),_:1})])])])])],64))}},y$=e("h2",null,"Confirm Your Membership",-1),w$=e("p",null,"Thank you for signing up for a membership! Please review the membership details below:",-1),x$=e("ul",null,[e("li",null,"Membership: Individual"),n(),e("li",null,"Price: $10")],-1),k$=e("hgroup",null,[e("p",{class:"chapter"},`
      Vue Components
    `),n(),e("h1",null,"Modal"),n(),e("p",null,[n(`
      The classic modal component with graceful spacings across devices and viewports, using the semantic HTML tag
      `),e("code",null,"<dialog>"),n(".")])],-1),$$=e("aside",{id:"table-of-contents"},[e("nav",null,[e("details",{open:""},[e("summary",null,"Content"),n(),e("ul",null,[e("li",null,[e("a",{class:"secondary",href:"/docs/vue/modal"},"Syntax")])])])])],-1),C$={role:"document"},S$=e("p",null,"Nano's Vue integration allow for clean templating and managment of modal dialogs.",-1),E$=e("p",null,[e("code",null,[e("span",{class:""},".modal-is-open")]),n(" prevents any scrolling and interactions below the modal.")],-1),T$={id:"modal-demo","aria-label":"Modal demo"},A$={class:"code","data-theme":"dark"},M$=e("pre",null,`<script setup>
  import { ref } from 'vue';
  import { NModal } from 'nano-vue';

  const modalOpen = ref(false);

  function closeModal() {
    modalOpen.value = false;
  }

  function openModal() {
    modalOpen.value = true;
  }
<\/script>

<template>
  <n-modal :open="modalOpen">
    <h2>Confirm Your Membership</h2>
    <p>Thank you for signing up for a membership! Please review the membership details below:</p>
    <ul>
      <li>Membership: Individual</li>
      <li>Price: $10</li>
    </ul>
    <footer>
      <button class="secondary" @click="closeModal">Cancel</button>
      <button @click="closeModal">Confirm</button>
    </footer>
  </n-modal>

  <button class="contrast" @click="openModal">Open modal</button>
</template>`,-1),P$={__name:"NanoModalView",setup(t){const o=Ce(!1);function a(){o.value=!1}function l(){o.value=!0}return(s,r)=>(C(),S(J,null,[f(da,{open:o.value},{default:v(()=>[y$,n(),w$,n(),x$,n(),e("footer",null,[e("button",{class:"secondary",onClick:a},`
        Cancel
      `),n(),e("button",{onClick:a},`
        Confirm
      `)])]),_:1},8,["open"]),n(),k$,n(),$$,n(),e("div",C$,[e("section",null,[S$,n(),E$,n(),e("article",T$,[e("button",{class:"contrast",onClick:l},`
          Open modal
        `),n(),e("footer",A$,[f(k,null,{default:v(()=>[M$]),_:1})])])])])],64))}};function Xn(...t){function o(s,r){return Object.keys(r).forEach(i=>{const c=s[i],d=r[i];At(c)&&At(d)?s[i]=c.concat(d):wt(c)&&wt(d)?s[i]=o({...c},d):s[i]=d}),s}if(t.length<2)throw new Error("deepMerge: this function expects at least 2 objects to be provided");if(t.some(s=>!wt(s)))throw new Error('deepMerge: all values should be of type "object"');const a=t.shift();let l;for(;l=t.shift();)o(a,l);return a}const wl=0,di=1,xl=2,Mt=1,Bn=2,Ko={GROUP_TYPE_AND:{label:"AND",shortCode:"$and"},GROUP_TYPE_NOT_AND:{label:"NOT AND",shortCode:"$nand"},GROUP_TYPE_OR:{label:"OR",shortCode:"$or"},GROUP_TYPE_NOT_OR:{label:"NOT OR",shortCode:"$nor"}},ui=[{label:"Equals",shortCode:"$eq",types:["string","number","date","datetime"]},{label:"Not Equals",shortCode:"$neq",types:["string","number","date","datetime"]},{label:"Greater Than",shortCode:"$gt",types:["number","date","datetime"]},{label:"Greater Than or Equal To",shortCode:"$gte",types:["number","date","datetime"]},{label:"Less Than",shortCode:"$lt",types:["number","date","datetime"]},{label:"Less Than or Equal To",shortCode:"$lte",types:["number","date","datetime"]},{label:"Contains",shortCode:"$c",types:["string"]},{label:"Does Not Contain",shortCode:"$nc",types:["string"]},{label:"Starts With",shortCode:"$sw",types:["string"]},{label:"Ends With",shortCode:"$ew",types:["string"]}],Os={type:Mt,groupType:Ko.GROUP_TYPE_AND,filters:[]},Co=t=>t,N$={string:Co,number:Co,date:Co,datetime:Co},Yo={};ui.forEach(t=>{Yo[t.shortCode]={filterType:Bn,entry:t}});Object.values(Ko).forEach(t=>{Yo[t.shortCode]={filterType:Mt,entry:t}});function O$(t={}){const o=t.fetch??Go,a=t.initialPageSize??10,l=t.initialPage??1,s=Ce(!1),r=Ce([]),i=Ce(L$(t.columns??[])),c=Ce({column:t.selectionColumn??"id",global:!1,indexes:{}}),d=Ce({pageSize:a,page:l,pageCount:0,itemCount:0,firstItem:0,lastItem:0}),h=Ce(structuredClone(Os)),u=Ce(0),p=Ce({});function m(){return structuredClone(Os)}function g(V){const R=Qn(V),q=Qn(h);Wo(R,q)||x({filter:V.value})}async function x(V,R=!1){if(s.value)return;s.value=!0;let q={};R||(q=Xn({sorter:Se(p.value),pager:{page:d.value.page??l,pageSize:d.value.pageSize??a}},V??{}),q.filter=Qn(q.filter??h.value),q.sorter=I$(q.sorter));try{const ee=await o(q);r.value=ee.items;const Q=hi(ee.filter??[]);h.value=Q,u.value=B$(Q),p.value=R$(ee.sorter??[]),d.value.page=ee.pager.page,d.value.pageSize=ee.pager.pageSize,d.value.pageCount=ee.pager.pageCount,d.value.itemCount=ee.pager.itemCount,d.value.firstItem=ee.pager.itemCount>0?(ee.pager.page-1)*ee.pager.pageSize+1:0,d.value.lastItem=ee.pager.itemCount>0?d.value.firstItem+ee.items.length-1:0}finally{s.value=!1}}function B(V){const R=Xn({page:d.value.page,pageSize:d.value.pageSize},V);(R.page!==d.value.page||R.pageSize!==d.value.pageSize)&&x({pager:R})}function W(V,R,q){q^c.value.global?(Object.keys(c.value.indexes).forEach(ee=>{delete c.value.indexes[ee]}),c.value.global=q):Pe(V)&&(q^R?c.value.indexes[V]=!0:delete c.value.indexes[V])}function F(V){let R={};if(wt(V)){const q=Se(ln(p));Object.values(q).forEach(ee=>{ee.priority+=1}),R=Xn({},q,V)}Wo(R,p.value)||x({sorter:R})}return x(),{isLoading:s,refresh:x,columns:i,rows:r,filter:h,filterCount:u,filterNew:m,filterUpdate:g,pager:d,pagerUpdate:B,selection:c,selectionUpdate:W,sorter:p,sorterUpdate:F}}function hi(t){let o={};if(At(t)){if(t.length===0)return o;const a=D$(t[0]);if(a.filterType===Mt)o={type:Mt,groupType:a.entry,filters:[]},t.slice(1).forEach(l=>{o.filters.push(hi(l))});else if(a.filterType===Bn){if(t.length!==3)throw Error("Invalid filter length");o={type:Bn,method:a.entry,columnName:t[1],argument:t[2]}}else throw Error("Unsupported filter type")}return o}function R$(t){const o={};return t.forEach((a,l)=>{const s=a[0]==="-"?xl:di,r=a.substr(1);o[r]={direction:s,priority:l}}),o}function Qn(t,o=0){let a=[];const l=Se(ln(t));if(l.type===Mt){const s=l.filters.map(r=>Qn(r,o+1)).filter(r=>r.length);s.length&&(a=[l.groupType.shortCode].concat(s))}else ih(l)&&l.method&&l.columnName&&l.argument&&(a=[l.method.shortCode,l.columnName,l.argument]);return a}function I$(t){return Object.entries(t).map(([o,a])=>Xn({column:o,priority:0},a)).filter(o=>o.direction!==wl).sort((o,a)=>o.priority-a.priority).map(o=>(o.direction===xl?"-":"+")+o.column)}function L$(t){if(!At(t))throw Error("Must specify an array");const o={visible:!0,sortable:!1,filterable:!1,type:"string",sort:{direction:wl,priority:0}};return t.map(l=>{let s={};if(Ka(l))s={...o,name:l,label:l};else if(wt(l))s={...o,...l};else throw Error("Unsupported column definition");return Bt(s.renderer)||(s.renderer=N$[s.type??"string"]),s})}function B$(t){let o=structuredClone((t==null?void 0:t.filters)??[]),a=0;for(;o.length>0;){const l=o.pop();Pe(l)&&(l.type===Mt?o=o.concat(structuredClone(l.filters)):a+=1)}return a}function D$(t){if(!Yo.hasOwnProperty(t))throw Error("Unknown filter short code");return Yo[t]}const z$={class:"n-data-table"},V$={class:"overflow-auto"},j$={key:0},F$=["checked",".indeterminate"],H$=["data-index","onClick"],q$={key:0},U$={key:0,xmlns:"http://www.w3.org/2000/svg",height:"16",width:"10",viewBox:"0 0 320 512"},G$=e("path",{d:"M41 288h238c21.4 0 32.1 25.9 17 41L177 448c-9.4 9.4-24.6 9.4-33.9 0L24 329c-15.1-15.1-4.4-41 17-41z"},null,-1),W$=[G$],K$={key:1,xmlns:"http://www.w3.org/2000/svg",height:"16",width:"10",viewBox:"0 0 320 512"},Y$=e("path",{d:"M279 224H41c-21.4 0-32.1-25.9-17-41L143 64c9.4-9.4 24.6-9.4 33.9 0l119 119c15.2 15.1 4.5 41-16.9 41z"},null,-1),J$=[Y$],Z$={key:2,xmlns:"http://www.w3.org/2000/svg",height:"16",width:"10",viewBox:"0 0 320 512"},X$=e("path",{d:"M272 288H48.1c-42.6 0-64.2 51.7-33.9 81.9l111.9 112c18.7 18.7 49.1 18.7 67.9 0l112-112c30-30.1 8.7-81.9-34-81.9zM160 448L48 336h224L160 448zM48 224h223.9c42.6 0 64.2-51.7 33.9-81.9l-111.9-112c-18.7-18.7-49.1-18.7-67.9 0l-112 112C-16 172.2 5.3 224 48 224zM160 64l112 112H48L160 64z"},null,-1),Q$=[X$],e6={key:0},t6=["colspan"],n6=["data-index"],o6={key:0,scope:"row"},a6=["checked","onClick"],l6="n-string",Ea=!0,Rs={__name:"NDataTable",props:{selection:{type:Object,default:null},rows:{type:Array,default(){return[]}},columns:{type:Array,default(){return[]}},sorter:{type:Object,default:null}},emits:["update","selection-update","row-update","column-update"],setup(t,{emit:o}){const a={number:"n-number",date:"n-date",datetime:"n-datetime",time:"n-time",string:"n-string"},l=t,s=o,{selection:r,sorter:i}=dr(l),c=ue(()=>l.rows.length>0),d=ue(()=>Object.keys(r.value.indexes).length>0),h=ue(()=>l.selection!==null),u=ue(()=>l.columns.filter(A=>A.visible).map(A=>Xn({filterable:!0,sortable:!0,sort:{direction:wl},type:"string"},{...A,classes:m(A),sort:i.value[A.name]??{}}))),p=ue(()=>u.value.length+(h.value?1:0));function m(A){const D=[];return A.sortable&&D.push("sortable"),D.push(a[A.type]??l6),D}function g(A){return!!(r.value.global^r.value.indexes[A[l.selection.column]])}function x(){s("selection-update",{global:!r.value.global}),s("update",{selection:{global:!r.value.global}})}function B(A){s("selection-update",{index:A[l.selection.column],state:!g(A),global:r.value.global}),s("update",{selection:{index:A[l.selection.column],state:!g(A),global:r.value.global}})}function W(A){var D;if(A.sortable){const j={...Se(i.value)};j[A.name]={direction:((((D=A==null?void 0:A.sort)==null?void 0:D.direction)??0)+1)%3,priority:0},s("update",{sort:j})}}function F(A){A.dataTransfer.setData("text/plain",A.target.dataset.index)}function V(A){A.currentTarget.classList.add("hover")}function R(A){A.currentTarget.classList.remove("hover")}function q(A){A.preventDefault()}function ee(A){A.preventDefault();const D=A.dataTransfer.getData("text/plain"),j=A.target.parentNode.dataset.index;s("column-update",{shift:{from:D,to:j}})}function Q(A){A.dataTransfer.setData("text/plain",A.target.dataset.index)}function P(A){A.currentTarget.classList.add("hover")}function I(A){A.currentTarget.classList.remove("hover")}function te(A){A.preventDefault()}function oe(A){A.preventDefault();const D=A.dataTransfer.getData("text/plain"),j=A.target.parentNode.dataset.index;s("row-update",{shift:{from:D,to:j}})}return(A,D)=>(C(),S("div",z$,[e("div",V$,[e("table",{class:Fe({selectable:h.value,draggable:Ea,empty:!c.value})},[e("thead",null,[e("tr",null,[h.value?(C(),S("th",j$,[e("input",{type:"checkbox",checked:$(r).global,".indeterminate":d.value,onClick:x},null,40,F$)])):He("",!0),n(),(C(!0),S(J,null,Oe(u.value,(j,be)=>(C(),S("th",{key:j.name,class:Fe(j.classes),"data-index":be,draggable:Ea,onClick:xe=>W(j),onDragstart:F,onDragenter:V,onDragleave:R,onDragover:q,onDrop:ee},[e("span",null,re(j.label),1),n(),j.sortable?(C(),S("span",q$,[j.sort.direction===$(xl)?(C(),S("svg",U$,W$)):j.sort.direction===$(di)?(C(),S("svg",K$,J$)):(C(),S("svg",Z$,Q$))])):He("",!0)],42,H$))),128))])]),n(),e("tbody",null,[c.value?He("",!0):(C(),S("tr",e6,[e("td",{class:"no-data",colspan:p.value},`
              No data to show.
            `,8,t6)])),n(),(C(!0),S(J,null,Oe(l.rows,(j,be)=>(C(),S("tr",{key:j.id,"data-index":be,draggable:Ea,onDragstart:Q,onDragenter:P,onDragleave:I,onDragover:te,onDrop:oe},[h.value?(C(),S("td",o6,[e("input",{type:"checkbox",checked:g(j),onClick:xe=>B(j)},null,8,a6)])):He("",!0),n(),(C(!0),S(J,null,Oe(u.value,xe=>(C(),S("td",{key:xe.name,class:Fe(xe.classes)},re(xe.renderer(j[xe.name])),3))),128))],40,n6))),128))])],2)])]))}},s6={class:"n-filter-group"},r6={class:"n-filter-group-add"},i6=["value"],c6=e("svg",{xmlns:"http://www.w3.org/2000/svg",height:"16",width:"12",viewBox:"0 0 384 512"},[e("path",{d:"M368 224H224V80c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v144H16c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h144v144c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V288h144c8.8 0 16-7.2 16-16v-32c0-8.8-7.2-16-16-16z"})],-1),d6=[c6],u6=e("svg",{xmlns:"http://www.w3.org/2000/svg",height:"16",width:"16",viewBox:"0 0 512 512"},[e("path",{d:"M492.9 354.6L413.2 320l79.7-34.6c12.2-5.3 17.7-19.4 12.5-31.6-5.3-12.2-19.4-17.7-31.6-12.5l-217.2 94.7L71.9 256l170.5-74c12.2-5.3 17.7-19.4 12.5-31.6-5.3-12.2-19.4-17.7-31.6-12.5L19.2 226.6C7.5 231.6 0 243.2 0 256s7.5 24.4 19.1 29.4L98.8 320l-79.7 34.6C7.5 359.6 0 371.2 0 384c0 12.8 7.5 24.4 19.1 29.4l218.3 94.7a46.5 46.5 0 0 0 18.6 3.9c6.3 0 12.7-1.3 18.6-3.9l218.3-94.7c11.6-5 19.2-16.6 19.2-29.4 0-12.9-7.5-24.4-19.1-29.4zM256.5 464.1L71.9 384l87.2-37.8 78.3 34c5.9 2.6 12.3 3.9 18.6 3.9s12.7-1.3 18.6-3.8l78.3-34 87.3 37.9-183.7 80.1zM496 88h-72V16c0-8.8-7.2-16-16-16h-16c-8.8 0-16 7.2-16 16v72h-72c-8.8 0-16 7.2-16 16v16c0 8.8 7.2 16 16 16h72v72c0 8.8 7.2 16 16 16h16c8.8 0 16-7.2 16-16v-72h72c8.8 0 16-7.2 16-16v-16c0-8.8-7.2-16-16-16z"})],-1),h6=[u6],p6=e("svg",{xmlns:"http://www.w3.org/2000/svg",height:"16",width:"10",viewBox:"0 0 320 512"},[e("path",{d:"M207.6 256l107.7-107.7c6.2-6.2 6.2-16.3 0-22.6l-25-25c-6.2-6.2-16.3-6.2-22.6 0L160 208.4 52.3 100.7c-6.2-6.2-16.3-6.2-22.6 0L4.7 125.7c-6.2 6.2-6.2 16.3 0 22.6L112.4 256 4.7 363.7c-6.2 6.2-6.2 16.3 0 22.6l25 25c6.2 6.2 16.3 6.2 22.6 0L160 303.6l107.7 107.7c6.2 6.2 16.3 6.2 22.6 0l25-25c6.2-6.2 6.2-16.3 0-22.6L207.6 256z"})],-1),f6=[p6],m6={key:0,class:"n-filter-group-children"},b6={__name:"NFilterGroup",props:{group:{type:Object,required:!0},groupTypes:{type:Array,required:!0},removable:{type:Boolean,required:!0}},emits:["addFilter","deleteGroup"],setup(t,{emit:o}){const a=t,l=o;function s(r){l("addFilter",a.group.filters,r),document.activeElement.blur()}return(r,i)=>(C(),S("div",s6,[e("div",r6,[rt(r.$slots,"groupTypes",{group:t.group,groupTypes:t.groupTypes},()=>[$e(e("select",{"onUpdate:modelValue":i[0]||(i[0]=c=>t.group.groupType=c)},[(C(!0),S(J,null,Oe(t.groupTypes,c=>(C(),S("option",{key:c,value:c},re(c.label),9,i6))),128))],512),[[Uo,t.group.groupType]])]),n(),rt(r.$slots,"filterAdd",Ma(Ho({addFilter:s})),()=>[e("button",{"data-tooltip":"Add Criterion",onClick:i[1]||(i[1]=c=>s($(Bn)))},d6),n(),e("button",{"data-tooltip":"Add Group",onClick:i[2]||(i[2]=c=>s($(Mt)))},h6)]),n(),t.removable?rt(r.$slots,"groupDeletion",{key:0,deleteGroup:()=>l("deleteGroup",t.group)},()=>[e("button",{onClick:i[3]||(i[3]=c=>l("deleteGroup",t.group))},f6)]):He("",!0)]),n(),t.group.filters.length?(C(),S("div",m6,[rt(r.$slots,"groupChildren")])):He("",!0)]))}},g6={class:"n-filter-criterion"},_6=["value"],v6=["value"],y6=["type"],w6=e("svg",{xmlns:"http://www.w3.org/2000/svg",height:"16",width:"10",viewBox:"0 0 320 512"},[e("path",{d:"M207.6 256l107.7-107.7c6.2-6.2 6.2-16.3 0-22.6l-25-25c-6.2-6.2-16.3-6.2-22.6 0L160 208.4 52.3 100.7c-6.2-6.2-16.3-6.2-22.6 0L4.7 125.7c-6.2 6.2-6.2 16.3 0 22.6L112.4 256 4.7 363.7c-6.2 6.2-6.2 16.3 0 22.6l25 25c6.2 6.2 16.3 6.2 22.6 0L160 303.6l107.7 107.7c6.2 6.2 16.3 6.2 22.6 0l25-25c6.2-6.2 6.2-16.3 0-22.6L207.6 256z"})],-1),x6=[w6],k6={__name:"NFilterCriterion",props:{criterion:{type:Object,required:!0},columns:{type:Object,required:!0},comparators:{type:Array,required:!0}},emits:["updateCriterion","deleteCriterion"],setup(t,{emit:o}){const a={string:"text",number:"number",date:"date",datetime:"datetime-local"},l=t,s=o,r=ue(()=>l.columns.filter(h=>h.filterable)),i=ue(()=>l.comparators.filter(h=>h.types.includes(l.criterion.dataType))),c=ue(()=>a[l.criterion.dataType]??"text");function d(h){s("updateCriterion",l.criterion,h)}return(h,u)=>(C(),S("div",g6,[rt(h.$slots,"columnUpdate",Ma(Ho({columns:r.value,criterion:t.criterion,updateCriterion:d})),()=>[$e(e("select",{"onUpdate:modelValue":u[0]||(u[0]=p=>t.criterion.columnName=p),class:"n-filter-criterian-column",onChange:u[1]||(u[1]=p=>d(p.target.value))},[(C(!0),S(J,null,Oe(r.value,p=>(C(),S("option",{key:p,value:p.name},re(p.label),9,_6))),128))],544),[[Uo,t.criterion.columnName]])]),n(),rt(h.$slots,"methodUpdate",Ma(Ho({methods:i.value,criterion:t.criterion})),()=>[$e(e("select",{"onUpdate:modelValue":u[2]||(u[2]=p=>t.criterion.method=p),class:"n-filter-criterian-method"},[(C(!0),S(J,null,Oe(i.value,p=>(C(),S("option",{key:p,value:p},re(p.label),9,v6))),128))],512),[[Uo,t.criterion.method]])]),n(),rt(h.$slots,"argumentUpdate",{criterion:t.criterion},()=>[$e(e("input",{"onUpdate:modelValue":u[3]||(u[3]=p=>t.criterion.argument=p),type:c.value,class:"n-filter-criterian-argument"},null,8,y6),[[qd,t.criterion.argument]])]),n(),rt(h.$slots,"criterionDelete",{deleteCriterion:()=>h.$emit("deleteCriterion",t.criterion)},()=>[e("button",{onClick:u[4]||(u[4]=p=>h.$emit("deleteCriterion",t.criterion))},x6)])]))}},Is={__name:"NFilter",props:{filter:{type:Object,required:!0},filterOptions:{type:Object,required:!0},columns:{type:Object,required:!0}},setup(t){const o=pl(),a=t,{filter:l}=dr(a),s=ue(()=>a.columns.filter(m=>m.filterable)),r=u();function i(m,g){if(g===Mt)m.push({type:Mt,groupType:Ko.GROUP_TYPE_AND,filters:[]});else{const{name:x,type:B}=s.value[0],W=h(B)[0];m.push({type:Bn,columnName:x,dataType:B,method:W,argument:""})}}function c(m){return m.type===Mt?ft(b6,{group:m,groupTypes:Object.values(Ko),removable:m!==l.value,onAddFilter:i,onDeleteGroup:d},{groupTypes:o.groupTypes,filterAdd:o.filterAdd,groupDeletion:o.groupDeletion,groupChildren:()=>m.filters.map(c)}):m.type===Bn?ft(k6,{criterion:m,columns:a.columns,comparators:h(m.dataType),onUpdateCriterion:p,onDeleteCriterion:d},{fieldUpdate:o.fieldUpdate,methodUpdate:o.methodUpdate,argumentUpdate:o.argumentUpdate,critereDelete:o.critereDelete}):ft("div","Unknown filter type")}function d(m){function g(x,B,W){x===m?W.splice(B,1):x.type===Mt&&x.filters.map(g)}m!==l.value&&g(l.value)}function h(m){const{criterionTypes:g=[]}=a.filterOptions;return g.filter(x=>x.types.includes(m))}function u(){return c(l.value)}function p(m,g){const{type:x}=s.value.find(B=>B.name===g);if(m.dataType!==x){const B=h(x)[0];m.method=B,m.argument="",m.dataType=x}}return(m,g)=>(C(),En($(r)))}},$6={class:"n-pager"},C6={"aria-label":"pagination"},S6=["disabled"],E6=e("svg",{xmlns:"http://www.w3.org/2000/svg",height:"16",width:"10",viewBox:"0 0 320 512"},[e("path",{d:"M34.5 239L228.9 44.7c9.4-9.4 24.6-9.4 33.9 0l22.7 22.7c9.4 9.4 9.4 24.5 0 33.9L131.5 256l154 154.8c9.3 9.4 9.3 24.5 0 33.9l-22.7 22.7c-9.4 9.4-24.6 9.4-33.9 0L34.5 273c-9.4-9.4-9.4-24.6 0-33.9z"})],-1),T6=[E6],A6=["aria-label","aria-selected","aria-current","disabled","onClick"],M6={key:1,class:"separator"},P6=["disabled"],N6=e("svg",{xmlns:"http://www.w3.org/2000/svg",height:"16",width:"10",viewBox:"0 0 320 512"},[e("path",{d:"M285.5 273L91.1 467.3c-9.4 9.4-24.6 9.4-33.9 0l-22.7-22.7c-9.4-9.4-9.4-24.5 0-33.9L188.5 256 34.5 101.3c-9.3-9.4-9.3-24.5 0-33.9l22.7-22.7c9.4-9.4 24.6-9.4 33.9 0L285.5 239c9.4 9.4 9.4 24.6 0 33.9z"})],-1),O6=[N6],So=7,Ls={__name:"NPager",props:{pager:{type:Object,required:!0}},emits:["update"],setup(t,{emit:o}){const a=t,l=o,s=ur(a,"pager"),r=ue(()=>s.value.pageCount>0&&s.value.page<s.value.pageCount),i=ue(()=>s.value.pageCount>0&&s.value.page>1),c=ue(()=>{const g=s.value.pageCount,x=[];if(s.value.pageCount>0){const B=Math.floor((So-2)/2);let W=s.value.page-B,F=s.value.page+B;x.push({label:1,page:1,link:!0,current:d(1)});const V=So-2;if(s.value.pageCount<So){if(s.value.pageCount>2){F=s.value.pageCount-1;for(let R=2;R<=F;R++)x.push({label:R,page:R,link:!0,current:d(R)})}}else{W<1+1&&(F+=Math.abs(1-W+1),W=1+1),F>g-1&&(W-=Math.abs(g-F-1),F=g-1);for(let R=W,q=0;R<=F&&q<V;R++,q++)x.push({label:R,page:R,link:!0,current:d(R)})}s.value.pageCount>1&&x.push({label:g,page:g,link:!0,current:d(g)}),g>So&&(x.at(1).page>x.at(0).page+1&&x.splice(1,1,{label:"&hellip;",link:!1}),x.at(-1).page>x.at(-2).page+1&&x.splice(x.length-2,1,{label:"&hellip;",link:!1}))}return x});function d(m){return m===s.value.page}function h(m){d(m)||l("update",{page:m})}function u(){h(s.value.page+1)}function p(){h(s.value.page-1)}return(m,g)=>(C(),S("div",$6,[e("nav",C6,[e("ul",null,[e("li",null,[e("a",{"aria-label":"Goto previous page",disabled:i.value?null:"",onClick:g[0]||(g[0]=x=>p())},T6,8,S6)]),n(),(C(!0),S(J,null,Oe(c.value,x=>(C(),S("li",{key:x},[x.link?(C(),S("a",{key:0,"aria-label":`Goto page ${x.label}`,"aria-selected":x.current?!0:null,"aria-current":x.current?"page":null,disabled:x.current?"":null,onClick:B=>h(x.page)},re(x.label),9,A6)):(C(),S("span",M6,"…"))]))),128)),n(),e("li",null,[e("a",{"aria-label":"Goto next page",disabled:r.value?null:"",onClick:g[1]||(g[1]=x=>u())},O6,8,P6)])])])]))}},R6=e("hgroup",null,[e("p",{class:"chapter"},`
      Vue Components
    `),n(),e("h1",null,"Data Tables"),n(),e("p",null,[n(`
      Nano's `),e("code",null,"<n-data-table>"),n(", "),e("code",null,"<n-pager>"),n(`, and
      `),e("code",null,"<n-filter>"),n(` provides a lean paginated, sortable and filterable
      connection to backend APIs.
    `)])],-1),I6=G('<aside id="table-of-contents"><nav><details open><summary>Content</summary> <ul><li><a class="secondary" href="/docs/vue/data-table">Syntax</a></li> <li><a class="secondary" href="/docs/vue/data-table/#paging">Paging</a></li> <li><a class="secondary" href="/docs/vue/data-table/#filtering">Filtering</a></li></ul></details></nav></aside>',1),L6={role:"document"},B6={class:"code","data-theme":"dark"},D6=e("pre",null,`<n-data-table
  :selection
  :columns
  :rows
  :sorter
  @update="tableUpdate"
/>`,-1),z6=e("p",null,[n("The available attributes for "),e("code",null,"<n-data-table>"),n(" are:")],-1),V6=G("<ul><li><code>selection</code> - The current selection state of the table.</li> <li><code>columns</code> - The columns to display in the table.</li> <li><code>rows</code> - The rows to display in the table.</li> <li><code>sorter</code> - The current sort state of the table.</li></ul>",1),j6=e("p",null,[n("The available events for "),e("code",null,"<n-data-table>"),n(" are:")],-1),F6=e("ul",null,[e("li",null,[e("code",null,"selection-update"),n(" - Emitted when the selection state has changed.")]),n(),e("li",null,[e("code",null,"sorter-update"),n(" - Emitted when the sort state has changed.")]),n(),e("li",null,[e("code",null,"update"),n(" - Emitted when the table state has changed.")])],-1),H6=e("h3",null,[n(`
        Selection`),e("a",{id:"selection",href:"#selection",class:"secondary",tabindex:"-1"},"#")],-1),q6=e("p",null,[n(`
        The `),e("code",null,"selection"),n(` attribute is an object that contains the following properties:
      `)],-1),U6=e("ul",null,[e("li",null,[e("code",null,"column"),n(" - The name of the column that contains the unique identifier.")]),n(),e("li",null,[e("code",null,"global"),n(" - The current global selection state of the table.")]),n(),e("li",null,[e("code",null,"indexes"),n(` - An object whose keys represent the selected indicies relative to
          the global state. All values should be `),e("code",null,"true"),n(`.
        `)])],-1),G6=e("p",null,[n(`
        If the global state is `),e("code",null,"false"),n(`, then no rows are selected, any indexes specified
        have been selected. If the global state is `),e("code",null,"true"),n(`, then all rows are considered
        selected and therefore any indexes specified are considered as unselected.
      `)],-1),W6={class:"code","data-theme":"dark"},K6=e("h3",null,[n(`
        Selection API`),e("a",{id:"selection-api",href:"#selection-api",class:"secondary",tabindex:"-1"},"#")],-1),Y6=e("p",null,[n(`
        When communicating with a backend API, the `),e("code",null,"selection"),n(` attribute should be used
        to determine which rows are to be processed by the server. The format should either be the
        form described above, or the slightly condensed form described below.
      `)],-1),J6={class:"code","data-theme":"dark"},Z6=e("h3",null,[n(`
        Columns`),e("a",{id:"columns",href:"#columns",class:"secondary",tabindex:"-1"},"#")],-1),X6=e("p",null,[n(`
        The `),e("code",null,"columns"),n(` attribute is an array of objects that represent the columns to
        display in the table. Each object should contain the following properties:
      `)],-1),Q6=G(`<ul><li><code>name</code> - The name of the column.</li> <li><code>label</code> - The label to display in the table header.</li> <li><code>visible</code> - Whether or not the column is visible.</li> <li><code>sortable</code> - Whether or not the column is sortable.</li> <li><code>filterable</code> - Whether or not the column is filterable.</li> <li><code>type</code> - The type of data in the column. This is used for sorting, filtering
          and rendering.
        </li></ul>`,1),e8=e("p",null,[n("The "),e("code",null,"type"),n(" attribute can be one of the following:")],-1),t8=G("<ul><li><code>string</code> - A string value.</li> <li><code>number</code> - A numeric value.</li> <li><code>date</code> - A date value.</li> <li><code>datetime</code> - A date and time value.</li></ul>",1),n8=e("p",null,"The order of the columns in the array is the order they will be displayed in the table.",-1),o8={class:"overflow-auto"},a8=e("thead",null,[e("tr",null,[e("th",null,"Column"),n(),e("th",null,"Label"),n(),e("th",{class:"text-center"},`
                  Visible
                `),n(),e("th",{class:"text-center"},`
                  Sortable
                `),n(),e("th",{class:"text-center"},`
                  Filterable
                `),n(),e("th",null,"Type")])],-1),l8={class:"text-center"},s8=["onUpdate:modelValue"],r8={class:"text-center"},i8=["onUpdate:modelValue"],c8={class:"text-center"},d8=["onUpdate:modelValue"],u8={key:0},h8=e("td",{colspan:"6"},`
                  No columns defined.
                `,-1),p8=[h8],f8=G(`<section><h3>
        Rows<a id="rows" href="#rows" class="secondary" tabindex="-1">#</a></h3> <p>
        The <code>rows</code> attribute is an array of objects that represent the rows to display in
        the table. Each object should contain the following properties:
      </p> <ul><li><code>id</code> - The unique identifier for the row.</li> <li><code>...</code> - Any other properties that match the column names.</li></ul></section>`,1),m8=e("h2",null,[n(`
        Paging`),e("a",{id:"paging",href:"#paging",class:"secondary",tabindex:"-1"},"#")],-1),b8={class:"code","data-theme":"dark"},g8=e("pre",null,`<n-pager
  :pager
  @update="pagerUpdate"
/>`,-1),_8=e("p",null,"The pager attribute is an object that contains the following properties:",-1),v8=G("<ul><li><code>page</code> - The current page number.</li> <li><code>pageSize</code> - The number of rows to display per page.</li> <li><code>itemCount</code> - The total number of rows.</li> <li><code>pageCount</code> - The total number of pages.</li> <li><code>firstItem</code> - The 1-based index of the first row on the current page.</li> <li><code>lastItem</code> - The 1-based index of the last row on the current page.</li></ul>",1),y8=e("p",null,[n("The available events for "),e("code",null,"<n-pager>"),n(" are:")],-1),w8=e("ul",null,[e("li",null,[e("code",null,"update"),n(" - Emitted when the pager state has changed.")])],-1),x8={class:"code","data-theme":"dark"},k8=e("h3",null,[n(`
        Paging API`),e("a",{id:"pager-api",href:"#pager-api",class:"secondary",tabindex:"-1"},"#")],-1),$8=e("p",null,[n(`
        When communicating with a backend API, the `),e("code",null,"pager"),n(` attribute should be used to
        determine which rows are to be processed by the server. The format is described above with
        only the `),e("code",null,"page"),n(" and "),e("code",null,"pageSize"),n(` being required, all other attributes
        should be ignored by the server.
      `)],-1),C8={class:"code","data-theme":"dark"},S8=e("h2",null,[n(`
        Filtering`),e("a",{id:"filtering",href:"#filtering",class:"secondary",tabindex:"-1"},"#")],-1),E8={class:"code","data-theme":"dark"},T8=e("pre",null,`<n-filter
  :filter
  :columns
  :filter-options
  @filter-update="onFilterUpdate"
/>`,-1),A8=e("h3",null,[n(`
        Filter API`),e("a",{id:"filter-api",href:"#filter-api",class:"secondary",tabindex:"-1"},"#")],-1),M8=e("p",null,[n(`
        When communicating with a backend API, the `),e("code",null,"filter"),n(` attribute should be used to
        determine which rows are to be processed by the server. The format is described below.
      `)],-1),P8={class:"code","data-theme":"dark"},N8=e("h2",null,[n(`
        Styling`),e("a",{id:"styling",href:"#styling",class:"secondary",tabindex:"-1"},"#")],-1),O8=e("p",null,[n(`
        Nano's `),e("code",null,"<n-data-table>"),n(", "),e("code",null,"<n-pager>"),n(`, and
        `),e("code",null,"<n-filter>"),n(` utilise the base content and components to provide the
        necessary DOM structures that aligning to a11y recommendations best practices with minimal
        opinion on style.
      `)],-1),R8=e("p",null,`
        Styling is left to the developer to ensure that the components align with the overall design
        of the application.
      `,-1),I8={id:"style-example","data-theme":"light"},L8={class:"header-bar"},B8=e("svg",{xmlns:"http://www.w3.org/2000/svg",height:"16",width:"16",viewBox:"0 0 512 512"},[e("path",{d:"M483.5 28.5L431.4 80.7C386.5 35.8 324.5 8 256 8 123.2 8 14.8 112.3 8.3 243.5 8 250.3 13.5 256 20.3 256h28c6.4 0 11.6-5 12-11.3C66.2 141.6 151.5 60 256 60c54.2 0 103.2 21.9 138.6 57.4l-54.1 54.1c-7.6 7.6-2.2 20.5 8.5 20.5H492c6.6 0 12-5.4 12-12V37c0-10.7-12.9-16-20.5-8.5zM491.7 256h-28c-6.4 0-11.6 5-12 11.3C445.8 370.4 360.5 452 256 452c-54.2 0-103.2-21.9-138.6-57.4l54.1-54.1c7.6-7.6 2.2-20.5-8.5-20.5H20c-6.6 0-12 5.4-12 12v143c0 10.7 12.9 16 20.5 8.5L80.7 431.4C125.5 476.2 187.5 504 256 504c132.8 0 241.2-104.3 247.7-235.5 .3-6.8-5.2-12.5-12-12.5z"})],-1),D8=[B8],z8=e("span",null,[e("svg",{xmlns:"http://www.w3.org/2000/svg",height:"16",width:"16",viewBox:"0 0 512 512"},[e("path",{d:"M464 0H48.1C5.4 0-16.1 51.7 14.1 81.9L176 243.9V416c0 15.1 7.1 29.3 19.2 40l64 47.1c31.3 21.9 76.8 1.5 76.8-38.4V243.9L497.9 81.9C528 51.8 506.7 0 464 0zM288 224v240l-64-48V224L48 48h416L288 224z"})])],-1),V8={class:"filter-count"},j8=e("button",{class:"columns","data-tooltip":"Columns"},[e("span",null,[e("svg",{xmlns:"http://www.w3.org/2000/svg",height:"16",width:"16",viewBox:"0 0 512 512"},[e("path",{d:"M464 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zM232 432H54a6 6 0 0 1 -6-6V112h184v320zm226 0H280V112h184v314a6 6 0 0 1 -6 6z"})])])],-1),F8=e("h2",null,"Filter",-1),H8={class:"footer-bar"},q8=e("option",{value:"10",selected:""},`
              10
            `,-1),U8=e("option",{value:"25"},`
              25
            `,-1),G8=e("option",{value:"50"},`
              50
            `,-1),W8=e("option",{value:"100"},`
              100
            `,-1),K8={__name:"NanoDataTableView",setup(t){const o=[{name:"id",label:"ID",sortable:!1,filterable:!0,type:"number"},{name:"first_name",label:"First Name",sortable:!0,filterable:!0,type:"string"},{name:"last_name",label:"Last Name",visible:!1,sortable:!0,filterable:!0,type:"string"},{name:"phone_1",label:"Phone 1",sortable:!0,filterable:!0,type:"string"},{name:"phone_2",label:"Phone 2",visible:!1,sortable:!0,filterable:!0,type:"string"},{name:"subscription_date",label:"Subscription Date",sortable:!0,filterable:!0,type:"date"},{name:"dob",label:"D.O.B.",visible:!1,sortable:!0,filterable:!0,type:"datetime"}],a=window.location.origin,{refresh:l,columns:s,rows:r,filterNew:i,filterUpdate:c,filterCount:d,pager:h,pagerUpdate:u,selection:p,selectionUpdate:m,sorter:g,sorterUpdate:x}=O$({fetch:async(...A)=>fetch(a,{method:"POST",headers:{"Content-Type":"application/json"},body:JSON.stringify(A[0])}).then(D=>D.json()),columns:o,selectionColumn:"id"}),B=Ce(i()),W={criterionTypes:ui},F=Ce(!1),V=Ce([]),R=ue(()=>{const{page:A,pageSize:D}=h.value;return{page:A,pageSize:D}}),q=ue(()=>{const{global:A,indexes:D}=p.value;return{global:A,indexes:Object.keys(D).map(j=>parseInt(j,10))}});function ee(){F.value=!1}function Q(){c(B),V.value=Qn(B.value),ee()}function P(){B.value.filters.splice(0,B.value.filters.length),c(B),ee()}function I(){F.value=!0}function te(A){const{selection:D,sort:j}=A;Pe(D)&&m(D.index,D.state,D.global),Pe(j)&&x(j)}function oe(A){u({pageSize:parseInt(A.target.value,10)})}return(A,D)=>(C(),S(J,null,[R6,n(),I6,n(),e("div",L6,[e("section",null,[e("article",null,[f(Rs,{selection:$(p),columns:$(s),rows:$(r),sorter:$(g),onUpdate:te},null,8,["selection","columns","rows","sorter"]),n(),e("footer",B6,[f(k,{language:"xml"},{default:v(()=>[D6]),_:1})])]),n(),z6,n(),V6,n(),j6,n(),F6]),n(),e("section",null,[H6,n(),q6,n(),U6,n(),G6,n(),e("div",W6,[e("pre",null,[e("code",null,re($(p)),1)])])]),n(),e("section",null,[K6,n(),Y6,n(),e("div",J6,[e("pre",null,[e("code",null,re(q.value),1)])])]),n(),e("section",null,[Z6,n(),X6,n(),Q6,n(),e8,n(),t8,n(),n8,n(),e("article",null,[e("div",o8,[e("table",null,[a8,n(),e("tbody",null,[(C(!0),S(J,null,Oe($(s),j=>(C(),S("tr",{key:j.name},[e("td",null,re(j.name),1),n(),e("td",null,re(j.label),1),n(),e("td",l8,[$e(e("input",{"onUpdate:modelValue":be=>j.visible=be,type:"checkbox"},null,8,s8),[[Ro,j.visible]])]),n(),e("td",r8,[$e(e("input",{"onUpdate:modelValue":be=>j.sortable=be,type:"checkbox"},null,8,i8),[[Ro,j.sortable]])]),n(),e("td",c8,[$e(e("input",{"onUpdate:modelValue":be=>j.filterable=be,type:"checkbox"},null,8,d8),[[Ro,j.filterable]])]),n(),e("td",null,re(j.type),1)]))),128)),n(),$(s).length?He("",!0):(C(),S("tr",u8,p8))])])])])]),n(),f8,n(),e("section",null,[m8,n(),e("article",null,[f(Ls,{pager:$(h),onUpdate:$(u)},null,8,["pager","onUpdate"]),n(),e("footer",b8,[f(k,{language:"xml"},{default:v(()=>[g8]),_:1})])]),n(),_8,n(),v8,n(),y8,n(),w8,n(),e("div",x8,[e("pre",null,[e("code",null,re($(h)),1)])])]),n(),e("section",null,[k8,n(),$8,n(),e("div",C8,[e("pre",null,[e("code",null,re(R.value),1)])])]),n(),e("section",null,[S8,n(),e("article",null,[f(Is,{filter:B.value,columns:$(s),"filter-options":W},null,8,["filter","columns"]),n(),e("button",{onClick:Q},`
          Apply Filter
        `),n(),e("button",{onClick:P},`
          Clear Filter
        `),n(),e("footer",E8,[f(k,{language:"xml"},{default:v(()=>[T8]),_:1})])])]),n(),e("section",null,[A8,n(),M8,n(),e("div",P8,[e("pre",null,[e("code",null,re(V.value),1)])])]),n(),e("section",null,[N8,n(),O8,n(),R8,n(),e("article",I8,[e("div",L8,[e("button",{"data-tooltip":"Refresh",onClick:D[0]||(D[0]=(...j)=>$(l)&&$(l)(...j))},D8),n(),e("button",{class:"filter","data-tooltip":"Filters",onClick:I},[z8,n(),e("span",V8,re($(d)),1)]),n(),j8]),n(),f(da,{open:F.value},{default:v(()=>[F8,n(),f(Is,{filter:B.value,columns:$(s),"filter-options":W},null,8,["filter","columns"]),n(),e("footer",null,[e("button",{class:"secondary",onClick:ee},`
              Cancel
            `),n(),e("button",{onClick:P},`
              Clear Filter
            `),n(),e("button",{class:"primary",onClick:Q},`
              Apply Filter
            `)])]),_:1},8,["open"]),n(),f(Rs,{selection:$(p),columns:$(s),rows:$(r),sorter:$(g),onUpdate:te},null,8,["selection","columns","rows","sorter"]),n(),e("div",H8,[e("select",{id:"page-size",name:"page-size",onChange:D[1]||(D[1]=j=>oe(j))},[q8,n(),U8,n(),G8,n(),W8],32),n(),f(Ls,{pager:$(h),onUpdate:$(u)},null,8,["pager","onUpdate"])])])])])],64))}},Y8=["disabled"],J8=["id","aria-selected","aria-controls"],Eo=Object.assign({__NANO_TAB__:!0},{__name:"NTab",props:{name:{type:String,required:!0},disabled:{type:Boolean,default:!1}},setup(t){const o=t,a=Je("NODE_TAB_ACTIVE_IK",""),l=ue(()=>a.value===o.name),s=ue(()=>`n-tab-${o.name}`);function r(){o.disabled||l.value||(a.value=o.name)}return(i,c)=>(C(),S("li",{disabled:o.disabled?"true":null},[e("a",{role:"tab",id:s.value,"aria-selected":l.value,"aria-controls":s.value,onClick:r},[rt(i.$slots,"default")],8,J8)],8,Y8))}}),Z8={role:"tablist","aria-label":"tabs",class:"n-tabs"},X8={__name:"NTabs",props:{modelValue:{type:String,required:!0}},emits:["update:modelValue"],setup(t,{emit:o}){const a=o,l=t,s=ue({get:()=>l.modelValue,set:r=>a("update:modelValue",r)});return Ft("NODE_TAB_ACTIVE_IK",s),(r,i)=>(C(),S("nav",Z8,[e("ul",null,[rt(r.$slots,"default")])]))}};function Q8(t,o){let a=null;return(...s)=>{a!==null&&(clearTimeout(a),a=null),a=setTimeout(()=>t(...s),o)}}const Ta={__name:"NSparkline",props:{width:{type:Number,default:200},height:{type:Number,default:50},options:{type:Object,default:()=>({})},smooth:{type:Boolean,default:!0},preserveAspectRatio:{type:String,default:"none"},lineStyles:{type:Object,default:()=>({stroke:"slategray",strokeWidth:1,spotRadius:3,fill:"none"})},indicatorStyles:{type:[Object,Boolean],default:()=>({stroke:"#dd2c00"})},tooltipProps:{type:Object},tooltipStyles:{type:Object,default:()=>({display:"none",background:"rgba(0, 0, 0, 0.6)",borderRadius:"3px",minWidth:"30px",padding:"3px",color:"#fff",fontSize:"12px"})},tooltipDelay:{type:Number,default:0},data:{type:Array},limit:{type:Number,default:3},margin:{type:Number,default:3},styles:{type:Object,default:()=>({})},max:Number,min:Number},setup(t){const o={type:"line",fill:"var(--nano-primary)",stroke:"var(--nano-secondary)",strokeWidth:2,marker:{size:3,shape:"diamond",fill:"green",stroke:"green",strokeWidth:2},padding:{top:5,bottom:5},highlightStyle:{size:7,fill:"rgb(124, 255, 178)",strokeWidth:0}},a=t,l=Ce(null),s=Ce(null),r=Ce(!1),i=Ce({}),c=Ce(!0),d=ue(()=>a.indicatorStyles&&l.value&&c.value),h=ue(()=>{const R=a.styles;return R.width=a.width,R.height=a.height,R});function u(R,q,ee,Q,P){return parseFloat((ee-(P-R)*ee/(q-R)+Q).toFixed(2))}function p(){const{fill:R,marker:q,stroke:ee,strokeWidth:Q}={...o,...a.options},P=q.size,I=q.size*2,te=a.data.length-1;(a.width-I*2)/te;const oe=a.height-I-Q*2,A=Math.min(...a.data),D=Math.max(...a.data);u(A,D,oe,Q+P,0);let j=[];return a.options.type==="column"?j=g():a.options.type==="line"?j=x():j=m(),j}function m(){const R=[],{fill:q,marker:ee,stroke:Q,strokeWidth:P}={...o,...a.options},I=ee.size,te=ee.size*2,oe=a.data.length-1,A=(a.width-te*2)/oe,D=a.height-te-P*2,j=Math.min(...a.data),be=Math.max(...a.data);let xe=u(j,be,D,P+I,0);u(j,be,D,P+I,a.data[0]);let fe=0,he=[`M${fe},${xe}`];if(a.smooth)for(let de=0;de<a.data.length;de++){const Be=u(j,be,D,P+I,a.data[de]);if(de===0)he.push(`L${fe},${Be}`);else{const Ye=a.data[de-1],qe=u(j,be,D,P+I,Ye),je=fe-A/2,it=qe,ot=fe-A/2,kt=Be;he.push(`C${je},${it},${ot},${kt},${fe},${Be}`)}fe+=A}else for(let de=0;de<a.data.length;de++){const Be=u(j,be,D,P+I,a.data[de]);he.push(`L${fe},${Be}`),fe+=A}return he.push(`L${fe-A},${xe} Z`),typeof a.indicatorStyles!="boolean"&&(a.indicatorStyles["shape-rendering"]="crispEdges",R.push(ft("line",{display:"none",style:a.indicatorStyles,x1:0,y1:0,x2:0,y2:a.height,ref:l}))),R.push(ft("path",{d:he.join(" "),stroke:Q,"stroke-width":P,"stroke-linejoin":"round","stroke-linecap":"round",fill:q})),R}function g(){const R=[],{fill:q,marker:ee,stroke:Q,strokeWidth:P}={...o,...a.options},I=0,te=0,oe=10;a.data.length-1;const A=a.height-P*2,D=Math.min(...a.data),j=Math.max(...a.data),be=u(D,j,A,P,0);let xe=te,fe=u(D,j,A,P,a.data[0]);const he=(a.width-P+2-te*2)/a.data.length;let de=he-oe,Be=[];for(let Ye=0;Ye<a.data.length;Ye++)fe=u(D,j,A,P+I,a.data[Ye]),Be.push(`M${xe},${be} L${xe+de},${be} L${xe+de},${fe} L${xe},${fe} Z`),xe+=he;return typeof a.indicatorStyles!="boolean"&&(a.indicatorStyles["shape-rendering"]="crispEdges",R.push(ft("line",{display:"none",style:a.indicatorStyles,x1:0,y1:0,x2:0,y2:a.height,ref:l}))),R.push(ft("path",{d:Be.join(" "),stroke:Q,"stroke-width":P,"stroke-linejoin":"round","stroke-linecap":"round",fill:q})),R}function x(){const R=[],{fill:q,marker:ee,stroke:Q,strokeWidth:P}={...o,...a.options},I=ee.size,te=ee.size*2,oe=a.data.length-1,A=(a.width-te*2)/oe,D=a.height-te-P*2,j=Math.min(...a.data),be=Math.max(...a.data);let xe=0,fe=[];if(a.smooth)for(let he=0;he<a.data.length;he++){const de=u(j,be,D,P+I,a.data[he]);if(he===0)fe.push(`M${xe},${de}`);else{const Be=a.data[he-1],Ye=u(j,be,D,P+I,Be),qe=xe-A/2,je=Ye,it=xe-A/2,ot=de;fe.push(`C${qe},${je},${it},${ot},${xe},${de}`)}xe+=A}else for(let he=0;he<a.data.length;he++){const de=u(j,be,D,P+I,a.data[he]);he===0?fe.push(`M${xe},${de}`):fe.push(`L${xe},${de}`),xe+=A}return typeof a.indicatorStyles!="boolean"&&(a.indicatorStyles["shape-rendering"]="crispEdges",R.push(ft("line",{display:"none",style:a.indicatorStyles,x1:0,y1:0,x2:0,y2:a.height,ref:l}))),R.push(ft("path",{d:fe.join(" "),stroke:Q,"stroke-width":P,"stroke-linejoin":"round","stroke-linecap":"round",fill:"none"})),R}function B(R=!0){const q=R?"":"none";s.value&&(s.value.style.display=q),l.value&&(l.value.style.display=q)}function W(){if(!r.value)return!1;$(i.value)}const F=Q8(W,a.tooltipDelay);function V(){return ft("div",{class:"ui-sparkline"},[ft("svg",{style:h,viewBox:`0 0 ${a.width} ${a.height}`,preserveAspectRatio:a.preserveAspectRatio,onMouseenter:()=>{},onMousemove:R=>{if(!d)return;const q=R.offsetX,ee=R.offsetY,Q=R.clientX,P=R.clientY;R.target,r.value=!0,F()},onMouseleave:()=>{r.value=!1,B(!1)}},p()),ft("div",{class:"sparkline__tooltip",display:"none",style:{...a.tooltipStyles,position:"fixed"},ref:s})])}return(R,q)=>(C(),En(V))}},e7=e("hgroup",null,[e("p",{class:"chapter"},`
      Vue Components
    `),n(),e("h1",null,"Sparkline"),n(),e("p",null,"SVG inline charts.")],-1),t7=e("aside",{id:"table-of-contents"},[e("nav",null,[e("details",{open:""},[e("summary",null,"Content"),n(),e("ul",null,[e("li",null,[e("a",{class:"secondary",href:"/docs/tabs","aria-current":"page"},"Syntax")])])])])],-1),n7={role:"document"},o7=e("p",null,"Nano's sparklines are optimised SVG charts that are ideal for displaying in table cells and can be used to provide insight into data trends at a glance.",-1),a7={"aria-label":"Nav example"},l7={class:"code","data-theme":"dark"},s7=e("pre",null,`<n-sparkline
  :width=400
  :height=50
  :data
  :options="{ type: 'area' }"
/>

<n-sparkline
  :width=400
  :height=50
  :data
  :options="{ type: 'column' }"
/>

<n-sparkline
  :width=400
  :height=50
  :data
  :options="{ type: 'line' }"
/>`,-1),r7={__name:"NanoSparklineView",setup(t){Ce("panel-1");const o=Ce([1,3,5,3,9,8,10,3,2,3]),a={type:"area"},l={type:"column"},s={type:"line"};return setInterval(()=>{for(o.value.push(Math.floor(Math.random()*20)-10);o.value.length>10;)o.value.shift()},2e3),(r,i)=>(C(),S(J,null,[e7,n(),t7,n(),e("div",n7,[e("section",null,[o7,n(),e("article",a7,[f(Ta,{width:400,height:50,data:o.value,options:a},null,8,["data"]),n(),f(Ta,{width:400,height:50,data:o.value,options:l},null,8,["data"]),n(),f(Ta,{width:400,height:50,data:o.value,options:s},null,8,["data"]),n(),e("footer",l7,[f(k,null,{default:v(()=>[s7]),_:1})])])])])],64))}},i7=["id","aria-labelledby","hidden"],To=Object.assign({__NANO_TAB__:!0},{__name:"NTabPanel",props:{name:{type:String,required:!0},disabled:{type:Boolean}},setup(t){const o=t,a=Je("NODE_TAB_ACTIVE_IK",""),l=ue(()=>a.value===o.name),s=ue(()=>`n-tabpanel-${o.name}`),r=ue(()=>`n-tab-${o.name}`);return(i,c)=>(C(),S("div",{role:"tabpanel",id:s.value,"aria-labelledby":r.value,hidden:!l.value},[rt(i.$slots,"default")],8,i7))}}),c7={class:"n-tab-panels"},d7={__name:"NTabPanels",props:{modelValue:{type:String,required:!0}},emits:["update:modelValue"],setup(t,{emit:o}){const a=o,l=t,s=ue({get:()=>l.modelValue,set:r=>a("update:modelValue",r)});return Ft("NODE_TAB_ACTIVE_IK",s),(r,i)=>(C(),S("div",c7,[rt(r.$slots,"default")]))}},u7=e("hgroup",null,[e("p",{class:"chapter"},`
      Vue Components
    `),n(),e("h1",null,"Tabs"),n(),e("p",null,"ARIA compabitble tabs with semantic HTML.")],-1),h7=e("aside",{id:"table-of-contents"},[e("nav",null,[e("details",{open:""},[e("summary",null,"Content"),n(),e("ul",null,[e("li",null,[e("a",{class:"secondary",href:"/docs/tabs","aria-current":"page"},"Syntax")])])])])],-1),p7={role:"document"},f7={"aria-label":"Nav example"},m7=e("p",null,"Content for the first panel",-1),b7=e("p",null,"Content for the second panel",-1),g7=e("p",null,"Content for the third panel",-1),_7=e("p",null,"Content for the fourth panel",-1),v7={class:"code","data-theme":"dark"},y7=e("pre",null,`<n-tabs v-model="active">
  <n-tab name="tab1">First Tab</n-tab>
  <n-tab name="tab2" hidden>Second Tab</n-tab>
  <n-tab name="tab3">Third Tab</n-tab>
  <n-tab name="tab4" disabled>Fourth Tab</n-tab>
</n-tabs>
<n-tab-panels v-model="active">
  <n-tab-panel name="tab1">
    Content for the first panel
  </n-tab-panel>
  <n-tab-panel name="tab2">
    Content for the second panel
  </n-tab-panel>
  <n-tab-panel name="tab3">
    Content for the third panel
  </n-tab-panel>
  <n-tab-panel name="tab4">
    Content for the fourth panel
  </n-tab-panel>
</n-tab-panels>`,-1),w7={__name:"NanoTabsView",setup(t){const o=Ce("tab1");return(a,l)=>(C(),S(J,null,[u7,n(),h7,n(),e("div",p7,[e("section",null,[e("article",f7,[f(X8,{modelValue:o.value,"onUpdate:modelValue":l[0]||(l[0]=s=>o.value=s)},{default:v(()=>[f(Eo,{name:"tab1"},{default:v(()=>[n("First Tab")]),_:1}),n(),f(Eo,{name:"tab2",hidden:""},{default:v(()=>[n("Second Tab")]),_:1}),n(),f(Eo,{name:"tab3"},{default:v(()=>[n("Third Tab")]),_:1}),n(),f(Eo,{name:"tab4",disabled:""},{default:v(()=>[n("Fourth Tab")]),_:1})]),_:1},8,["modelValue"]),n(),f(d7,{modelValue:o.value,"onUpdate:modelValue":l[1]||(l[1]=s=>o.value=s)},{default:v(()=>[f(To,{name:"tab1"},{default:v(()=>[m7]),_:1}),n(),f(To,{name:"tab2"},{default:v(()=>[b7]),_:1}),n(),f(To,{name:"tab3"},{default:v(()=>[g7]),_:1}),n(),f(To,{name:"tab4"},{default:v(()=>[_7]),_:1})]),_:1},8,["modelValue"]),n(),e("footer",v7,[f(k,null,{default:v(()=>[y7]),_:1})])])])])],64))}},x7=["aria-invalid"],k7=["id","type","placeholder","aria-label","aria-invalid"],$7={key:0,xmlns:"http://www.w3.org/2000/svg",width:"18",height:"16",viewBox:"0 0 576 512",fill:"currentColor"},C7=e("path",{d:"M129.1 361.4C93.6 327.2 67.7 286.9 52.5 256c15.1-30.9 41-71.2 76.6-105.4C171.8 109.5 224.9 80 288 80s116.2 29.5 158.9 70.6c35.6 34.3 61.5 74.5 76.6 105.4c-15.1 30.9-41 71.2-76.6 105.4C404.2 402.5 351.1 432 288 432s-116.2-29.5-158.9-70.6zM288 480c158.4 0 258-149.3 288-224C546 181.3 446.4 32 288 32S30 181.3 0 256c30 74.7 129.6 224 288 224zm0-144c-44.2 0-80-35.8-80-80c0-5.4 .5-10.6 1.5-15.7L288 256l-15.7-78.5c5.1-1 10.3-1.5 15.7-1.5c44.2 0 80 35.8 80 80s-35.8 80-80 80zM160 256c0 70.7 57.3 128 128 128s128-57.3 128-128s-57.3-128-128-128c-8.6 0-17 .8-25.1 2.5c-50.3 10-90 49.5-100.3 99.7l-.1 .7c-1.6 8.1-2.5 16.5-2.5 25.1z"},null,-1),S7=[C7],E7={key:1,xmlns:"http://www.w3.org/2000/svg",width:"20",height:"16",viewBox:"0 0 640 512",fill:"currentColor"},T7=e("path",{d:"M48.4 14.8L29.4 .1 0 38 19 52.7 591.5 497.2l19 14.7L639.9 474l-19-14.7L524 384.1c41.9-44 70.2-93.9 84-128.1C578 181.3 478.4 32 320 32c-66.9 0-123.2 26.6-168.3 63L48.4 14.8zM190.8 125.4C227.6 98 270.8 80 320 80c63 0 116.2 29.5 158.9 70.6c35.6 34.3 61.5 74.5 76.6 105.4c-14.1 28.9-37.6 65.8-69.6 98.5L434 314.2c8.9-17.5 14-37.2 14-58.2c0-70.7-57.3-128-128-128c-8.6 0-17 .8-25.1 2.5c-22.5 4.5-42.9 14.9-59.5 29.5l-44.6-34.6zM395 283.9l-82.2-63.8-8.5-42.6c5.1-1 10.3-1.5 15.7-1.5c44.2 0 80 35.8 80 80c0 9.8-1.8 19.2-5 27.9zm49.9 162.7l-41.6-32.7C377.9 425.3 350.1 432 320 432c-63.1 0-116.2-29.5-158.9-70.6C125.6 327.2 99.7 286.9 84.5 256c9.1-18.7 22.2-40.7 38.9-62.8L85.7 163.5C60.2 197.1 42.1 230.8 32 256c30 74.7 129.6 224 288 224c46.9 0 88.6-13.1 124.9-33.4zm-86.7-68.3L302 334c-23.5-5.4-43.1-21.2-53.7-42.3l-56.1-44.2c-.2 2.8-.3 5.6-.3 8.5c0 70.7 57.3 128 128 128c13.3 0 26.1-2 38.2-5.8z"},null,-1),A7=[T7],_n=Object.assign({__NANO_INPUT__:!0},{__name:"NPassword",props:{name:{type:String,required:!0},modelValue:{type:String,default:null},placeholder:{type:String,default:"Password"},source:{type:Object,default:null},validity:{type:Object,default:null},blur:{},disabled:{type:Boolean},ariaInvalid:{type:Boolean,default:null},momentary:{type:Boolean,default:!1},toggle:{type:Boolean,default:!0}},emits:["update:modelValue"],setup(t,{emit:o}){const a=o,l=t,s=Je("NODE_INPUT_SOURCE",null),r=Je("NODE_INPUT_VALIDITY",null),i=Je("NODE_INPUT_BLUR",null),c=ue({get:()=>{if(Pe(s))return s[l.name];if(Pe(l.modelValue))return l.modelValue},set:W=>{Pe(s)?s[l.name]=W:Pe(l.modelValue)&&a("update:modelValue",W)}}),d=ue(()=>{var F,V;let W=null;return Pe(r)?W=(F=r.value[l.name])==null?void 0:F.ariaInv:Pe(l.validity)?W=((V=l.validity[l.name])==null?void 0:V.ariaInv)??"":Pe(l.ariaInvalid)&&(W=l.ariaInvalid??""),Wa(W)?null:W}),h=l.name+"-error",u=Ce(!1),p=ue(()=>u.value?"text":"password"),m=()=>{Bt(i)?i(l.name):Bt(l.blur)&&l.blur(l.name)},g=()=>{l.momentary&&(u.value=!0)},x=()=>{l.momentary&&(u.value=!1)},B=()=>{l.momentary||(u.value=!u.value)};return(W,F)=>(C(),S("div",{class:"n-password","aria-invalid":d.value},[$e(e("input",{"onUpdate:modelValue":F[0]||(F[0]=V=>c.value=V),id:t.name,type:p.value,placeholder:t.placeholder,"aria-label":t.placeholder,"aria-invalid":d.value,"aria-describedby":h,onBlur:m},null,40,k7),[[ct,c.value]]),n(),t.toggle?(C(),S("div",{key:0,class:"toggle",onClick:B,onMousedown:Ha(g,["left"]),onMouseup:Ha(x,["left"]),onMouseleave:x,"aria-label":"Toggle password visibility"},[u.value?(C(),S("svg",$7,S7)):(C(),S("svg",E7,A7))],32)):He("",!0)],8,x7))}}),M7=e("hgroup",null,[e("p",{class:"chapter"},`
      Vue Components
    `),n(),e("h1",null,"Password"),n(),e("p",null,"A password input field with visibility toggle.")],-1),P7=e("aside",{id:"table-of-contents"},[e("nav",null,[e("details",{open:""},[e("summary",null,"Content"),n(),e("ul",null,[e("li",null,[e("a",{class:"secondary",href:"/docs/tabs","aria-current":"page"},"Syntax")])])])])],-1),N7={role:"document"},O7=e("p",null,"The password component is a simple input field that allows the user to enter a password. The password field can be toggled to show the password in plain text.",-1),R7={"aria-label":"Nav example"},I7={class:"code","data-theme":"dark"},L7=e("pre",null,'<n-password name="foo" v-model="password" />',-1),B7=e("h2",null,[n(`
        Validity`),e("a",{id:"validity",href:"#validity",class:"secondary",tabindex:"-1"},"#")],-1),D7=e("p",null,[n("The password component can be marked as invalid by setting the "),e("code",null,"aria-invalid"),n(" attribute to "),e("code",null,"true"),n(".")],-1),z7={"aria-label":"Nav example"},V7={class:"code","data-theme":"dark"},j7=e("pre",null,`<n-password name="foo" v-model="password" aria-invalid="true" />
<n-password name="foo" v-model="password" aria-invalid="false" />`,-1),F7=e("h2",null,[n(`
        Momentary`),e("a",{id:"momentary",href:"#momentary",class:"secondary",tabindex:"-1"},"#")],-1),H7=e("p",null,[n("The password component can be set to momentary mode by setting the "),e("code",null,"momentary"),n(" attribute to "),e("code",null,"true"),n(". This will show the password in plain text while the mouse button is held down.")],-1),q7={"aria-label":"Nav example"},U7={class:"code","data-theme":"dark"},G7=e("pre",null,'<n-password name="foo" v-model="password" momentary="true" />',-1),W7=e("h2",null,[n(`
        Toggle`),e("a",{id:"toggle",href:"#toggle",class:"secondary",tabindex:"-1"},"#")],-1),K7=e("p",null,[n("The password component visibility toggle can be disabled by setting the "),e("code",null,"toggle"),n(" attribute to "),e("code",null,"false"),n(". This effectivly falls back to the vanilla "),e("code",null,'<input type="password" ... />'),n(" component.")],-1),Y7={"aria-label":"Nav example"},J7={class:"code","data-theme":"dark"},Z7=e("pre",null,`<n-password nmae="foo" v-model="password" toggle="false" />
<n-password name="foo" v-model="password" toggle="false" aria-invalid="false" />`,-1),X7={__name:"NanoPasswordView",setup(t){const o=Ce("");return(a,l)=>(C(),S(J,null,[M7,n(),P7,n(),e("div",N7,[e("section",null,[O7,n(),e("article",R7,[f(_n,{name:"foo",modelValue:o.value,"onUpdate:modelValue":l[0]||(l[0]=s=>o.value=s)},null,8,["modelValue"]),n(),e("footer",I7,[f(k,null,{default:v(()=>[L7]),_:1})])])]),n(),e("section",null,[B7,n(),D7,n(),e("article",z7,[f(_n,{name:"foo",modelValue:o.value,"onUpdate:modelValue":l[1]||(l[1]=s=>o.value=s),"aria-invalid":!0},null,8,["modelValue"]),n(),f(_n,{name:"foo",modelValue:o.value,"onUpdate:modelValue":l[2]||(l[2]=s=>o.value=s),"aria-invalid":!1},null,8,["modelValue"]),n(),e("footer",V7,[f(k,null,{default:v(()=>[j7]),_:1})])])]),n(),e("section",null,[F7,n(),H7,n(),e("article",q7,[f(_n,{name:"foo",modelValue:o.value,"onUpdate:modelValue":l[3]||(l[3]=s=>o.value=s),momentary:!0},null,8,["modelValue"]),n(),e("footer",U7,[f(k,null,{default:v(()=>[G7]),_:1})])])]),n(),e("section",null,[W7,n(),K7,n(),e("article",Y7,[f(_n,{name:"foo",modelValue:o.value,"onUpdate:modelValue":l[4]||(l[4]=s=>o.value=s),toggle:!1},null,8,["modelValue"]),n(),f(_n,{name:"foo",modelValue:o.value,"onUpdate:modelValue":l[5]||(l[5]=s=>o.value=s),"aria-invalid":!1,toggle:!1},null,8,["modelValue"]),n(),e("footer",J7,[f(k,null,{default:v(()=>[Z7]),_:1})])])])])],64))}};function Ya(t,o=!0,a=[]){return t.forEach(l=>{if(l!==null){if(typeof l!="object"){(typeof l=="string"||typeof l=="number")&&a.push(n(String(l)));return}if(Array.isArray(l)){Ya(l,o,a);return}if(l.type===J){if(l.children===null)return;Array.isArray(l.children)&&Ya(l.children,o,a)}else l.type!==gt&&a.push(l)}}),a}const Q7={validate(t){return/^[a-zA-Z]*$/.test(t)},message:"Must be alphabetic, only letters allowed"},eC={validate(t){return/^[a-zA-Z0-9]*$/.test(t)},message:"Must be alphanumeric, only letters and numbers allowed"},tC={validate(t){return/^\d*$/.test(t)},message:"Must be numeric, only digits allowed"},nC={validate(t){return/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(t)},message:"Must be a valid email address"},oC={validate(t,o){return Ln(t)?t>=o.value:Pe(t)?t.length>=o.value:!1},params:["value"],message(t,o,a){return Ln(o)?"Must be greater than {value}":"Must have at least {value} characters"}},aC={validate(t,o){return Ln(t)?t<=o.value:Pe(t)?t.length<=o.value:!1},params:["value"],message(t,o,a){return Ln(o)?"Must be be less than {value}":"Must have {value} characters at most"}},lC={validate(t,o){if(Ln(t))return t>=o.min&&t<=o.max;if(!Pe(t))return!1;const a=t.length;return a>=o.min&&a<=o.max},params:["min","max"],message(t,o,a){return Ln(o)?"Must be between {min} and {max}":"Must have at least {min} characters and {max} characters at most"}},sC={validate(t){return{valid:t!=null&&t!=="",required:!0}},message:"Field is required",computesRequired:!0},rC={validate(t,o){const a=Wa(t),l=Wa(o.target);return{valid:a,required:!l}},params:["target"],message:"Field is required if target field is not empty",computesRequired:!0},Bs={alpha:Q7,alphanum:eC,digits:tC,email:nC,max:aC,min:oC,minmax:lC,required:sC,required_if:rC};function Io(t,o,a={}){const{bail:l=!0,manual:s=!1,immediate:r=!0,debounce:i=0,maxWait:c=void 0,validator:d=pi}=a,h=ur(t),u=zo(ee(h.value)),p=zo({}),m=Ce(!0),g=Ce(!r||s),x=ue(()=>d.schema(ln(o))),B=ue(()=>Q(u.value,p.value)),W=(P=[])=>{const I=structuredClone(u.value);(At(P)?P.length>0?P:Object.keys(I):[P]).forEach(oe=>{I[oe].touched=!0,I[oe].untouched=!1}),u.value=I},F=P=>{const I=structuredClone(u.value);(At(P)?P.length>0?P:Object.keys(I):[P]).forEach(oe=>{I[oe].pristine=!1,I[oe].dirty=!0}),u.value=I},V=P=>{const I=structuredClone(u.value);(At(P)?P.length>0?P:Object.keys(I):[P]).forEach(oe=>{I[oe].pristine=!0,I[oe].dirty=!1,I[oe].changed=!1}),u.value=I},R=P=>{const I=structuredClone(u.value);(At(P)?P.length>0?P:Object.keys(I):[P]).forEach(oe=>{I[oe].dirty=!0,I[oe].pristine=!1,I[oe].changed=h.value[oe]!==I[oe].initialValue}),u.value=I},q=async(P,I)=>{m.value=!1,g.value=!1;try{const A=await x.value.validate(h.value,{bail:l});g.value=!0,p.value={}}catch(A){A instanceof Error?console.error("[n-validator] ",A):p.value=A}finally{m.value=!0}const te=structuredClone(u.value);Pe(I)&&Pe(P)&&Object.keys(P).forEach(A=>{Pe(P[A])&&P[A]!==I[A]&&(te[A].dirty=!0,te[A].pristine=!1),te[A].changed=P[A]!==te[A].initialValue});const oe=Pe(x.value)?x.value.fieldsRequired():[];return Object.keys(te).forEach(A=>{te[A].validated=!0,te[A].passed=!0,te[A].failed=te[A].invalid,te[A].required=oe.includes(A)}),u.value=te,{pass:g.value,validity:B.value}};return s||lh(()=>({...h.value}),(P,I)=>q(P,I),{debounce:i,deep:!0,immediate:r,maxWait:c}),{isFinished:m,pass:g,validity:B,blur:W,dirty:F,pristine:V,update:R,validate:q};function ee(P){return Object.fromEntries(Object.keys(P).map(I=>[I,{initialValue:P[I],changed:!1,dirty:!1,pristine:!0,required:!1,touched:!1,untouched:!0,validated:!1,passed:!1,failed:!1,ariaInv:""}]))}function Q(P={},I={}){return Object.fromEntries(Object.keys(h.value).map(te=>{const oe=I[te]||[],A=P[te]||{},D=A.dirty||A.touched&&A.required?oe.length>0?"true":"false":"",j={errors:oe,valid:oe.length===0,invalid:oe.length>0,changed:A.changed,dirty:A.dirty,pristine:A.pristine,required:A.required,touched:A.touched,untouched:A.untouched,validated:A.validated,passed:A.validated&&oe.length===0,failed:A.validated&&oe.length>0,ariaInv:D};return[te,j]}))}}class iC{constructor(){this._schema={},this._rules={}}fields(){return Object.keys(this._schema)}fieldsRequired(){return Object.keys(this._schema).filter(o=>this._schema[o].required)}rule(o,a){let l={message:"{_field_} is not valid",params:[],computesRequired:!1,required:!1,transform:s=>s,...this._rules[o]??{}};if(Bt(a))l.validate=a;else if(wt(a))l={...l,...a};else throw new Error("The validator must be a function or an object");if(!Bt(l.validate))throw new Error("A validate function must be defined");return this._rules[o]=l,this}schema(o){if(!wt(o))throw new Error("The schema must be an object");return Object.keys(o).forEach(a=>{let l=o[a],s=l.rules??{};if(!wt(s))throw new Error("The rules must be an object");this._schema[a]=l,Object.keys(s).forEach(r=>{if(!this._rules[r])throw new Error(`Cannot find the validator for rule: ${r}`)})}),this}async validate(o,a={}){if(!this._schema)throw new Error("Cannot validate without a schema");if(!wt(o))throw new Error("The data must be an object");const l={},s=Object.keys(this._schema);for(let r=0;r<s.length;r++){const i=s[r],c=this._schema[i],d=c.rules,h=o[i],u=Object.keys(d);for(let p=0;p<u.length;p++){const m=u[p],g=this._rules[m],x=cC(g,d[m]);if(!g)throw new Error(`Cannot find the validator for rule: ${g.validator}`);let B=await Promise.resolve(g.validate(h,x));if(g.computesRequired){if(!wt(B)||!Pe(B.required)||!Pe(B.valid))throw new Error("The validator must return an object, with required and valid keys, when computing required");c.required=B.required,B=B.valid}else g.required===!0&&(c.required=!0);if(Ka(B)||B===!1){const W=dC(Ka(B)?B:g.message,d[m],i,h);if(l[i]=l[i]||[],l[i].push(W),a.bail)break}}}return l.length===0?Promise.resolve():Promise.reject(l)}}function cC(t,o){let a={};if(At(o))o.forEach((l,s)=>{a[t.params[s]]=l});else if(wt(o))a=o;else return a;return Object.keys(a).forEach(l=>{t.params.includes(l)||delete a[l]}),a}function dC(t,o,a,l){return Bt(t)&&(t=t(a,l,o)),t.replace(/{([^}]+)}/g,(s,r)=>r==="_field_"?a:r==="_value_"?l:o[r]??"")}const pi=new iC;Object.keys(Bs).forEach(t=>{pi.rule(t,Bs[t])});const uC={class:"n-form"},hC={__name:"NForm",props:{source:{type:Object,required:!0},schema:{type:Object,required:!0}},emits:["update:modelValue"],setup(t,{emit:o}){const a=t,s=pl().default;ue(()=>s?Ya(s()).filter(c=>{var d;return((d=c.type)==null?void 0:d.__NANO_INPUT__)??!1}):[]);const{validity:r,blur:i}=Io(a.source,a.schema);return Ft("NODE_INPUT_SOURCE",a.source),Ft("NODE_INPUT_VALIDITY",r),Ft("NODE_INPUT_BLUR",i),(c,d)=>(C(),S("form",uC,[rt(c.$slots,"default")]))}},pC=["id","name","type","disabled","aria-invalid"],Ds=Object.assign({__NANO_INPUT__:!0},{__name:"NInput",props:{name:{type:String,required:!0},type:{type:String,required:!0},modelValue:{type:String,default:null},source:{type:Object,default:null},validity:{type:Object,default:null},blur:{},disabled:{type:Boolean}},emits:["update:modelValue"],setup(t,{emit:o}){const a=o,l=t,s=Je("NODE_INPUT_SOURCE",null),r=Je("NODE_INPUT_VALIDITY",null),i=Je("NODE_INPUT_BLUR",null),c=ue({get:()=>{if(Pe(s))return s[l.name];if(Pe(l.modelValue))return l.modelValue},set:p=>{Pe(s)?s[l.name]=p:Pe(l.modelValue)&&a("update:modelValue",p)}}),d=ue(()=>{var p,m;return Pe(r)?((p=r.value[l.name])==null?void 0:p.ariaInv)??"":Pe(l.validity)?((m=l.validity[l.name])==null?void 0:m.ariaInv)??"":""}),h=l.name+"-error",u=()=>{Bt(i)?i(l.name):Bt(l.blur)&&l.blur(l.name)};return(p,m)=>$e((C(),S("input",{"onUpdate:modelValue":m[0]||(m[0]=g=>c.value=g),id:t.name,name:t.name,type:t.type,disabled:t.disabled,"aria-invalid":d.value,"aria-describedby":h,onBlur:u},null,40,pC)),[[ct,c.value]])}}),Aa=Object.assign({__NANO_INPUT__:!0},{__name:"NInputError",props:{name:{type:String,required:!0},validity:{type:Object,default:null}},emits:["update:modelValue"],setup(t,{emit:o}){const a=t,l=Je("NODE_INPUT_VALIDITY",null),s=ue(()=>Pe(l)?l.value[a.name]:Pe(a.validity)?a.validity[a.name]:{}),r=a.name+"-error";return(i,c)=>s.value.ariaInv?(C(!0),S(J,{key:0},Oe(s.value.errors,d=>(C(),S("small",{class:"n-input-error",label:r,role:"alert"},re(d),1))),256)):He("",!0)}}),fC=e("hgroup",null,[e("p",{class:"chapter"},`
      Vue Components
    `),n(),e("h1",null,"Validation"),n(),e("p",null,"Validation composition for forms and other data.")],-1),mC=G('<aside id="table-of-contents"><nav><details open=""><summary>Content</summary> <ul><li><a class="secondary" href="/docs/vue/validation">Syntax</a></li> <li><a class="secondary" href="/docs/vue/validation#flags">Flags</a></li> <li><a class="secondary" href="/docs/vue/validation#add-rules">Adding Rules</a></li> <li><a class="secondary" href="/docs/vue/validation#available-rules">Available Rules</a></li></ul></details></nav></aside>',1),bC={role:"document"},gC=e("p",null,"Validation is a key part of any form, and nano provides a simple and flexible way to validate form data using a set of rules and messages. The validation system is powered by a rule-based system, where each field in the form can have one or more rules that are applied to it.",-1),_C=e("p",null,[n("Nano provides a number of high-level components and composable functions that make it easy to validate form data and display error messages to the user. The "),e("code",null,"useValidator"),n(" composable function provides the core validation logic, while the "),e("code",null,"NForm"),n(", "),e("code",null,"NInput"),n(", and "),e("code",null,"NInputError"),n(" components provide the UI components to display the form and error messages.")],-1),vC={"aria-label":"rules-alphanum"},yC={class:"code","data-theme":"dark"},wC=e("pre",null,`<script setup> 
import { reactive } from "vue";
import NForm from "@/components/NForm.vue";
import NInput from "@/components/NInput.vue";
import NInputError from "@/components/NInputError.vue";
import NPassword from '@/components/NPassword.vue';

const schema = {
  foo: {
    rules: {
      alpha: {},
      minmax: { min: 3, max: 8 },
    },
  },
  bar: {
    rules: {
      required: {},
      minmax: { min: 2, max: 6 },
    },
  },
  bar: {
    rules: {
      required: {},
      email: {},
    },
  }
}
const source = reactive({foo: "", bar: null, baz: ""});
<\/script>

<template>
  <n-form :source :schema>
    <n-input name="foo" type="text" />
    <n-input-error name="foo" />
    <n-input name="bar" type="number" />
    <n-input-error name="bar" />
    <n-ipassword name="baz" />
    <n-input-error name="baz" />
  </n-form>`,-1),xC=e("p",null,[n("The "),e("code",null,"NForm"),n(" component is a high-level component that wraps the form and provides the core validation logic. The "),e("code",null,"NInput"),n(" component is a low-level component that represents a form field, and the "),e("code",null,"NInputError"),n(" component is a low-level component that displays the error messages for a form field.")],-1),kC=e("p",null,[n("Not all forms are created equally, Nano deliberately seperates the error message from the input field, this allows for more flexibility in the layout and design of the form. As long as your "),e("code",null,"NInput"),n(" and "),e("code",null,"NInputError"),n(" components sit within the same "),e("code",null,"NForm"),n(" component, they will be linked together.")],-1),$C=e("h3",null,[n("Composable"),e("a",{id:"usage",href:"#usage",class:"secondary",tabindex:"-1"},"#")],-1),CC=e("p",null,[n("The "),e("code",null,"useValidator"),n(" composable provides a simple way to validate form data using a set of rules and messages and provides the core validation to "),e("code",null,"NForm"),n(", "),e("code",null,"NInput"),n(" and "),e("code",null,"NInputError"),n(" components.")],-1),SC={"aria-label":"Composable Example"},EC=["aria-invalid"],TC={id:"foo-error",role:"alert"},AC=["aria-invalid"],MC={role:"alert"},PC=e("h3",null,[n("Usage"),e("a",{id:"usage",href:"#usage",class:"secondary",tabindex:"-1"},"#")],-1),NC=e("p",null,[n("The "),e("code",null,"useValidator"),n(" composable function takes two required arguments, and an optional third. The first is the data object that you want to validate, the second is the schema object that defines the rules for each field in the data object, and the third is the options object that allows you to customise the behavior of the validator.")],-1),OC={class:"code","data-theme":"dark"},RC=e("pre",null,`const {
  isFinished,
  pass,
  validity,
  blur,
  dirty,
  pristine,
  update,
  validate
} = useValidator(data, schema, options);`,-1),IC=e("h4",null,"Data",-1),LC=e("p",null,"The data object is a reactive object that contains the form data that you want to validate. Each key in the data object represents a field in the form, and the value is the current value of that field.",-1),BC={class:"code","data-theme":"dark"},DC=e("pre",null,'const data = reactive({foo: "", bar: null});',-1),zC=e("h4",null,"Schema",-1),VC=e("p",null,"The schema object is a simple object that has the same keys as the data object, but the values are objects that define the rules for that field.",-1),jC=e("p",null,"Here is an example of a schema object:",-1),FC={class:"code","data-theme":"dark"},HC=e("pre",null,`const schema = {
  foo: {
    rules: {
      alpha: {},
      minmax: { min: 3, max: 8 },
    },
  },
  bar: {
    rules: {
      required: {},
      minmax: { min: 2, max: 6 },
    },
  }
    
}`,-1),qC=G("<p>The schema object defines two fields, <code>foo</code> and <code>bar</code>, each with their own set of rules. The <code>foo</code> field has two rules, <code>alpha</code> and <code>minmax</code>, while the <code>bar</code> field has two rules, <code>required</code> and <code>minmax</code>.</p>",1),UC=e("p",null,[n("The "),e("code",null,"alpha"),n(" rule checks if the field value contains only alphabetic characters, while the "),e("code",null,"minmax"),n(" rule checks if the field value is between the specified min and max values. The "),e("code",null,"required"),n(" rule checks if the field value is not empty.")],-1),GC=e("h4",null,"Options",-1),WC=e("p",null,"The options object is an object that allows you to customise the behavior of the validator. It has the following properties:",-1),KC=G('<table><thead><tr><th scope="col">Name</th> <th scope="col">Type</th> <th scope="col">Default</th> <th scope="col">Description</th></tr></thead> <tbody><tr><th scope="row">bail</th> <td>boolean</td> <td>false</td> <td>Stop validation on the first failed rule.</td></tr> <tr><th scope="row">debounce</th> <td>number</td> <td>0</td> <td>The debounce time in milliseconds before the validation is triggered.</td></tr> <tr><th scope="row">immediate</th> <td>boolean</td> <td>true</td> <td>The validation is triggered immediately when the function is called.</td></tr> <tr><th scope="row">manual</th> <td>boolean</td> <td>false</td> <td>Disable automatic validation and trigger validation manually.</td></tr> <tr><th scope="row">maxWait</th> <td>number</td> <td>0</td> <td>The maximum time in milliseconds to wait before the validation is triggered.</td></tr> <tr><th scope="row">validator</th> <td>object</td> <td>{}</td> <td>A custom validator object that contains custom rules.</td></tr></tbody></table>',1),YC=e("h4",null,"Example",-1),JC=e("p",null,[n("Here is an example of how to use the "),e("code",null,"useValidator"),n(" composable function:")],-1),ZC={class:"code","data-theme":"dark"},XC=e("pre",null,`<script setup>
import { reactive } from "vue";
import { useValidator } from "@/composables/useValidator";

const schema = {
  foo: {
    rules: {
      alpha: {},
      minmax: { min: 3, max: 8 },
    },
  },
  bar: {
    rules: {
      required: {},
      minmax: { min: 2, max: 6 },
    },
  }
}
const data = reactive({foo: "", bar: null});
const { validity, blur } = useValidator(data, schema);
<\/script>

<template>
  <input
    v-model="data.foo"
    type="text"
    aria-label="Text"
    aria-describedby="foo-error"
    :aria-invalid="validity.foo.ariaInv"
    @blur="blur('foo')"
  />
  <small
    id="foo-error"
    v-for="e in validity.foo.errors"
    role="alert"
    v-if="validity.foo.ariaInv"
  >{{e}}</small>
  <input
    v-model="data.bar"
    type="number"
    aria-label="Number"
    aria-describedby="bar-error"
    :aria-invalid="validity.bar.ariaInv"
    @blur="blur('bar')"
  />
  <small
    id="bar-error"
    v-for="e in validity.bar.errors"
    role="alert"
    v-if="validity.bar.ariaInv"
  >{{e}}</small>
</template>`,-1),QC=e("p",null,[n("The "),e("code",null,"useValidator"),n(" composable function returns and object with a number of properties that you can use to manipulate and determine the validity of the form fields.")],-1),e9=e("h4",null,"isFinished",-1),t9=e("p",null,[n("The "),e("code",null,"isFinished"),n(" property is a reactive boolean value that indicates if the validation has been completed.")],-1),n9=e("h4",null,"pass",-1),o9=e("p",null,[n("The "),e("code",null,"pass"),n(" property is a reactive boolean value that indicates if the validation has passed.")],-1),a9=e("h4",null,"validity",-1),l9=e("p",null,[n("The "),e("code",null,"validity"),n(" property is a reactive object that contains the validity state of each field in the form. Each key in the object is the name of a field in the form, and the value is an object that contains the validity state of that field.")],-1),s9=e("p",null,"The validation flags are a set of boolean values that gives you information about the field you are validating, for example you may want to know if the field is currently valid, or if it has been blurred by the user.",-1),r9=e("p",null,"This is a table of all the flags available that you can access per field:",-1),i9=G('<table id="flags"><thead><tr><th scope="col">Name</th> <th scope="col">Type</th> <th scope="col">Default</th> <th scope="col">Description</th></tr></thead> <tbody><tr><th scope="row">valid</th> <td>boolean</td> <td>false</td> <td>The field is valid.</td></tr> <tr><th scope="row">invalid</th> <td>boolean</td> <td>false</td> <td>The field is invalid.</td></tr> <tr><th scope="row">changed</th> <td>boolean</td> <td>false</td> <td>The field value has been changed.</td></tr> <tr><th scope="row">dirty</th> <td>boolean</td> <td>false</td> <td>The field value has been manipulated.</td></tr> <tr><th scope="row">pristine</th> <td>boolean</td> <td>true</td> <td>The field value was not manipulated.</td></tr> <tr><th scope="row">required</th> <td>boolean</td> <td>false</td> <td>The field is required.</td></tr> <tr><th scope="row">touched</th> <td>boolean</td> <td>false</td> <td>The field has been blurred.</td></tr> <tr><th scope="row">untouched</th> <td>boolean</td> <td>true</td> <td>The field has not been blurred.</td></tr> <tr><th scope="row">validated</th> <td>boolean</td> <td>false</td> <td>The field has been validated at least once.</td></tr> <tr><th scope="row">passed</th> <td>boolean</td> <td>false</td> <td>The field has been validated and is valid.</td></tr> <tr><th scope="row">failed</th> <td>boolean</td> <td>false</td> <td>The field has been validated and is invalid.</td></tr> <tr><th scope="row">ariaInv</th> <td>string</td> <td>&quot;&quot;</td> <td>The field invalidity should be announced to the user.</td></tr></tbody></table>',1),c9={"aria-label":"Nav example"},d9=["aria-invalid"],u9={id:"v-foo-error",role:"alert"},h9={class:"grid states"},p9={class:"grid states"},f9={class:"grid states"},m9={class:"grid states"},b9=e("h4",null,"blur()",-1),g9=e("p",null,[n("The "),e("code",null,"blur"),n(" function is a function that you can call to manually blur a field. This is useful when you want to trigger the validation of a field manually or indicate that the field has been visited by the user.")],-1),_9=e("h4",null,"dirty()",-1),v9=e("p",null,[n("The "),e("code",null,"dirty"),n(" function is a function that you can call to manually mark a field as dirty. This is useful when you want to indicate that the field value has been manipulated.")],-1),y9=e("h4",null,"pristine()",-1),w9=e("p",null,[n("The "),e("code",null,"pristine"),n(" function is a function that you can call to manually mark a field as pristine. This is useful when you want to indicate that the field value has not been manipulated.")],-1),x9=e("h4",null,"update()",-1),k9=e("p",null,[n("The "),e("code",null,"update"),n(" function is a function that you can call to manually update the value of a field. This is useful when you want to update the value of a field without triggering the validation.")],-1),$9=e("h4",null,"validate()",-1),C9=e("p",null,[n("The "),e("code",null,"validate"),n(" function is a function that you can call to manually validate the form. This is useful when you want to trigger the validation of the entire form manually.")],-1),S9=e("h3",null,[n("Adding Rules"),e("a",{id:"add-rules",href:"#add-rules",class:"secondary",tabindex:"-1"},"#")],-1),E9=e("p",null,"nano comes with a number of validation rules by default which work for most use cases. Additional rules can be added, or pre-existing rules modified via the extend function.",-1),T9=e("p",null,"Adding new rules with extend is straight forward, in its simplest form it looks like this:",-1),A9={class:"code","data-theme":"dark"},M9=e("pre",null,`import { Validator } from 'nano';

validator = new Validator();
validator.rule('positive', value => {
return value >= 0;
});
        `,-1),P9=e("p",null,"That last snippet can be placed any where in your app, typically you should define your rules before you use them in your template, so your entry file or a dedicated validation.js file is a great place to start.",-1),N9=e("p",null,"The extend function accepts the name of the rule and the validator function to use for that rule.",-1),O9=e("p",null,"You can use the newly defined positive rule in a schema definition like this:",-1),R9={class:"code","data-theme":"dark"},I9=e("pre",null,`const schema = {
  foo: {
    rules: {
      positive: {},
    },
  },
};`,-1),L9=e("p",null,"That's it! You have successfully added a new rule to your validation schema.",-1),B9=e("h3",null,[n("Avaliable Rules"),e("a",{id:"selection",href:"#available-rules",class:"secondary",tabindex:"-1"},"#")],-1),D9=e("p",null,"Nano's validation system is powered by a rule-based system, each field can have one or more rules that are applied to it. The rules are simple functions that return a boolean value, true if the field is valid, and false if it's not.",-1),z9=e("p",null,"Here is a list of the built-in rules that come with nano:",-1),V9=G('<table><thead><tr><th scope="col">Name</th> <th scope="col">Description</th></tr></thead> <tbody><tr><th scope="row">alpha</th> <td>The field value must contain only alphabetic characters.</td></tr> <tr><th scope="row">alphanum</th> <td>The field value must contain only alpha-numeric characters.</td></tr> <tr><th scope="row">email</th> <td>The field value must be a valid email address.</td></tr> <tr><th scope="row">max</th> <td>The field value must be less than or equal to the specified max value.</td></tr> <tr><th scope="row">min</th> <td>The field value must be greater than or equal to the specified min value.</td></tr> <tr><th scope="row">minmax</th> <td>The field value must be between the specified min and max values.</td></tr> <tr><th scope="row">required</th> <td>The field value must not be empty.</td></tr> <tr><th scope="row">required_if</th> <td>The field is required if the target field is not empty.</td></tr></tbody></table>',1),j9=e("h4",null,"alpha",-1),F9=e("p",null,[n("The "),e("code",null,"alpha"),n(" rule checks if the field value contains only alphabetic characters.")],-1),H9={"aria-label":"rules-alpha"},q9=["aria-invalid"],U9={id:"rules-alpha-error",role:"alert"},G9={class:"code","data-theme":"dark"},W9=e("pre",null,`import { useValidator } from 'nano';
const data = reactive({ val: "" });
const { validity, blur } = useValidator(data, {
  val: {
    rules: {
      alpha: {}
    }
  }
});`,-1),K9=e("p",null,"There are no parameters for the alpha rule. When no parameters are required, you can simply pass an empty object to the rules property.",-1),Y9=e("h4",null,"alphanum",-1),J9=e("p",null,[n("The "),e("code",null,"alphanum"),n(" rule checks if the field value contains only alpha-numeric characters.")],-1),Z9={"aria-label":"rules-alphanum"},X9=["aria-invalid"],Q9={id:"rules-alphanum-error",role:"alert"},eS={class:"code","data-theme":"dark"},tS=e("pre",null,`import { useValidator } from 'nano';
  const data = reactive({ val: "" });
  const { validity, blur } = useValidator(data, {
    val: {
      rules: {
        alphanum: {}
      }
    }
  });`,-1),nS=e("h4",null,"digits",-1),oS=e("p",null,[n("The "),e("code",null,"digits"),n(" rule checks if the field value contains only digits.")],-1),aS={"aria-label":"rules-digits"},lS=["aria-invalid"],sS={id:"rules-digits-error",role:"alert"},rS={class:"code","data-theme":"dark"},iS=e("pre",null,`import { useValidator } from 'nano';
  const data = reactive({ val: "" });
  const { validity, blur } = useValidator(data, {
    val: {
      rules: {
        digits: {}
      }
    }
  });`,-1),cS=e("h4",null,"email",-1),dS=e("p",null,[n("The "),e("code",null,"email"),n(" rule checks if the field value is a valid email address.")],-1),uS={"aria-label":"rules-email"},hS=["aria-invalid"],pS={id:"rules-email-error",role:"alert"},fS={class:"code","data-theme":"dark"},mS=e("pre",null,`import { useValidator } from 'nano';
  const data = reactive({ val: "" });
  const { validity, blur } = useValidator(data, {
    val: {
      rules: {
        email: {}
      }
    }
  });`,-1),bS=e("h4",null,"max",-1),gS=e("p",null,[n("The "),e("code",null,"max"),n(" rule checks if the field value is less than or equal to the specified max value. This rule is type aware, for numbers it checks if the value is less than or equal to the specified max value, for strings it checks if the length of the string is less than or equal to the specified max value.")],-1),_S={"aria-label":"rules-max"},vS=["aria-invalid"],yS={id:"rules-max-error",role:"alert"},wS={class:"code","data-theme":"dark"},xS=e("pre",null,`import { useValidator } from 'nano';
  const data = reactive({ val: "" });
  const { validity, blur } = useValidator(data, {
    val: {
      rules: {
        max: { value: 10 }
      }
    }
  });`,-1),kS=G('<table><thead><tr><th scope="col">Param</th> <th scope="col">Required</th> <th scope="col">Description</th></tr></thead> <tbody><tr><td>value</td> <td>Y</td> <td>The maximum value the field value must be less than or equal to.</td></tr></tbody></table>',1),$S=e("h4",null,"min",-1),CS=e("p",null,[n("The "),e("code",null,"min"),n(" rule checks if the field value is greater than or equal to the specified min value. This rule is type aware, for numbers it checks if the value is greater than or equal to the specified min value, for strings it checks if the length of the string is greater than or equal to the specified min value.")],-1),SS={"aria-label":"rules-min"},ES=["aria-invalid"],TS={id:"rules-min-error",role:"alert"},AS={class:"code","data-theme":"dark"},MS=e("pre",null,`import { useValidator } from 'nano';
  const data = reactive({ val: "" });
  const { validity, blur } = useValidator(data, {
    val: {
      rules: {
        min: { value: 10 }
      }
    }
  });`,-1),PS=G('<table><thead><tr><th scope="col">Param</th> <th scope="col">Required</th> <th scope="col">Description</th></tr></thead> <tbody><tr><td>value</td> <td>Y</td> <td>The minimum value the field value must be greater than or equal to.</td></tr></tbody></table>',1),NS=e("h4",null,"minmax",-1),OS=e("p",null,[n("The "),e("code",null,"minmax"),n(" rule checks if the field value is between the specified min and max values. This rule is type aware, for numbers it checks if the value is between the specified min and max values, for strings it checks if the length of the string is between the specified min and max values.")],-1),RS={"aria-label":"rules-minmax"},IS=["aria-invalid"],LS={id:"rules-minmax-error",role:"alert"},BS={class:"code","data-theme":"dark"},DS=e("pre",null,`import { useValidator } from 'nano';
  const data = reactive({ val: "" });
  const { validity, blur } = useValidator(data, {
    val: {
      rules: {
        minmax: { min: 10, max: 20 }
      }
    }
  });`,-1),zS=G('<table><thead><tr><th scope="col">Param</th> <th scope="col">Required</th> <th scope="col">Description</th></tr></thead> <tbody><tr><td>min</td> <td>Y</td> <td>The minimum value the field value must be greater than or equal to.</td></tr> <tr><td>max</td> <td>Y</td> <td>The maximum value the field value must be less than or equal to.</td></tr></tbody></table>',1),VS=e("h4",null,"required",-1),jS=e("p",null,[n("The "),e("code",null,"required"),n(" rule checks if the field value is not empty.")],-1),FS={"aria-label":"rules-required"},HS=["aria-invalid"],qS={id:"rules-required-error",role:"alert"},US={class:"code","data-theme":"dark"},GS=e("pre",null,`import { useValidator } from 'nano';
  const data = reactive({ val: "" });
  const { validity, blur } = useValidator(data, {
    val: {
      rules: {
        required: {}
      }
    }
  });`,-1),WS=e("h4",null,"required_if",-1),KS=e("p",null,[n("The "),e("code",null,"required_if"),n(" rule checks if the field is required if the target field is not empty.")],-1),YS={"aria-label":"rules-required_if"},JS=["aria-invalid"],ZS={id:"rules-required_if-error",role:"alert"},XS={class:"code","data-theme":"dark"},QS=e("pre",null,`import { useValidator } from 'nano';
  const data = reactive({ val: "" });
  const { validity, blur } = useValidator(data, {
    val: {
      rules: {
        required_if: { target: 'foo' }
      }
    }
  });`,-1),eE=G('<table><thead><tr><th scope="col">Param</th> <th scope="col">Required</th> <th scope="col">Description</th></tr></thead> <tbody><tr><td>target</td> <td>Y</td> <td>The name of the target field that must not be empty for this field to be required.</td></tr></tbody></table>',1),tE=e("h3",null,"Rules Object Expression",-1),nE=e("p",null,"Rules can be expressed as objects instead of functions, this is useful when you want to add more metadata to your rules. Here is an example of the positive rule expressed as an object:",-1),oE={class:"code","data-theme":"dark"},aE=e("pre",null,`import { Validator } from 'nano';
const validator = new Validator();
validator.rule('positive', {
  validate: value => {
    return value >= 0;
  }
});`,-1),lE=e("p",null,[n("Rules expressed as objects can have additional properties, one of them is the "),e("code",null,"message"),n(" property which allows you to define a custom error message for the rule. Here is an example of the positive rule with a custom error message:")],-1),sE={class:"code","data-theme":"dark"},rE=e("pre",null,`import { Validator } from 'nano';
const validator = new Validator();
validator.rule('positive', {
  validate: value => {
    return value >= 0;
  },
  message: 'The field must be a positive number.'
});`,-1),iE=e("p",null,[n("Rules expressed as objects can also have the "),e("code",null,"params"),n(" property which allows you to define the parameters that the rule requires. Here is an example of the min rule expressed as an object:")],-1),cE={class:"code","data-theme":"dark"},dE=e("pre",null,`import { Validator } from 'nano';
const validator = new Validator();
validator.rule('min', {
  validate: (value, args) => {
    return value.length >= args.length;
  },
  params: ['length']
});`,-1),uE=e("h4",null,"Transform",-1),hE=e("p",null,"Sometimes it is necessary to transform a value before validation, possibly to coerce the value or to sanitize it in some way. To do this add a transform function to the validation rule. The property is transformed prior to validation and returned as promise result or callback result when pass validation. Here is an example of a rule that transforms the value before validation:",-1),pE={class:"code","data-theme":"dark"},fE=e("pre",null,`import { Validator } from 'nano';
const validator = new Validator();
validator.rule('positive', {
  validate: value => {
    return value >= 0;
  },
  transform: value => {
    return parseInt(value);
  }
});`,-1),mE=e("h4",null,"Message",-1),bE=e("p",null,"Nano generates error messages for your fields, the last examples had This field is invalid message which is the default message configured for all rules. You can change that by returning strings in the validation function itself:",-1),gE={class:"code","data-theme":"dark"},_E=e("pre",null,`import { Validator } from 'nano';
const validator = new Validator();
validator.rule('positive', {
  validate: value => {
    if (value >= 0) {
      return true;
    }

    return 'The field must be a positive number.';
  }
});`,-1),vE=e("p",null,"You can also leave out messages from the validator function and instead use the extended format to pass a dedicated message property:",-1),yE={class:"code","data-theme":"dark"},wE=e("pre",null,`import { Validator } from 'nano';
const validator = new Validator();
validator.rule('positive', {
  validate: value => {
    reutrn value >= 0;
  },
  message: 'The field must be a positive number.'
});`,-1),xE=e("p",null,"This allows your validator functions to be much clearer.",-1),kE=e("h5",null,"Field Name Placeholders",-1),$E=e("p",null,"Sometimes you want your messages to have the following format:",-1),CE={class:"code","data-theme":"dark"},SE=e("pre",null,"The {_field_} must be positive.",-1),EE=e("p",null,[n("The validator function doesn't accept the field name anywhere, but Nano offers simple interpolation mechanism for returned messages, you can use the "),e("code",null,"{_field_}"),n(" placeholder in your string and it will be replaced with your field name automatically")],-1),TE=e("h5",null,"Argument Placeholders",-1),AE=e("p",null,`You can't really have the min rule message to be "this field is invalid", this is not only confusing to the user, they will have no knowledge on how to fix them.`,-1),ME=e("p",null,[n("Client-side validation is all about UX, so Nano interpolation can parse placeholders that match the rule parameters, so to define such a message for the min rule you can use a "),e("code",null,"{length}"),n(" placeholder in the error message like this:")],-1),PE={class:"code","data-theme":"dark"},NE=e("pre",null,`import { Validator } from 'nano';
const validator = new Validator();
validator.rule('min', {
  validate: (value, args) => {
    return value.length >= args.length;
  },
  params: ['length'],
  message: 'The field {_field_} must be at least {length} characters long.'
});`,-1),OE=e("p",null,[n("One thing to note is that the parameter placeholder doesn't have underscores "),e("code",null,"_"),n(" around it unlike the "),e("code",null,"{_field_}"),n(" placeholder. This is a convention of Nano as there are a special set of placeholders that have underscores around them. This is to prevent collisions and to make them distinct from rule parameters.")],-1),RE=e("h5",null,"Messages as Functions",-1),IE=e("p",null,"If using interpolated strings is not flexible enough for you, using functions is also allowed. When using a function as your message, it has to return a string. Function messages receive the field name and an object containing the placeholders mentioned earlier.",-1),LE=e("p",null,"This is the previous example but with a function as our message:",-1),BE={class:"code","data-theme":"dark"},DE=e("pre",null,`import { Validator } from 'nano';
const validator = new Validator();
validator.rule('minmax', {
  validate: (value, { min, max }) => {
    return value.length >= min && value.length <= max;
  },
  params: ['min', 'max'],
  message: (fieldName, placeholders) => {
    return \`The \${fieldName} field must have at least \${placeholders.min} characters and \${placeholders.max} characters at most\`;
  }
});`,-1),zE=e("p",null,"This allows you to manually interpolate or generate messages depending on your needs, this will come in handy when implementing localization using popular plugins like vue-i18n.",-1),VE=e("p",null,"For reference these are the contents of the placeholders object:",-1),jE=G('<table><thead><tr><th scope="col">Prop</th> <th scope="col">Description</th></tr></thead> <tbody><tr><td><code>_field_</code></td> <td>The field name.</td></tr> <tr><td><code>_value_</code></td> <td>The field value that was validated.</td></tr> <tr><td><code>_rule_</code></td> <td>The rule name that triggered this message.</td></tr></tbody></table>',1),FE=e("p",null,[n("Along side any parameters configured in the "),e("code",null,"params"),n(" array.")],-1),HE={__name:"NanoValidationView",setup(t){const o={foo:{rules:{required:{},email:{}}},bar:{rules:{required:{}}},baz:{rules:{min:{value:3}}}},a=tn({foo:"",bar:null,baz:""}),l=tn({foo:"",bar:null}),{validity:s,blur:r}=Io(l,o),i=tn({foo:""}),{validity:c,blur:d}=Io(i,o),h=tn({alpha:"",alphanum:"",digits:"",email:"",max:null,min:null,minmax:null,required:"",required_if:""}),{validity:u,blur:p}=Io(h,{alpha:{rules:{alpha:{}}},alphanum:{rules:{alphanum:{}}},digits:{rules:{digits:{}}},email:{rules:{email:{}}},max:{rules:{max:{value:10}}},min:{rules:{min:{value:10}}},minmax:{rules:{minmax:{min:10,max:20}}},required:{rules:{required:{}}},required_if:{rules:{required_if:{target:"foo"}}}});return(m,g)=>(C(),S(J,null,[fC,n(),mC,n(),e("div",bC,[e("section",null,[gC,n(),_C,n(),e("article",vC,[f(hC,{source:a,schema:o},{default:v(()=>[f(Ds,{name:"foo",type:"text"}),n(),f(Aa,{name:"foo"}),n(),f(Ds,{name:"bar",type:"number"}),n(),f(Aa,{name:"bar"}),n(),f(_n,{name:"baz"}),n(),f(Aa,{name:"baz"})]),_:1},8,["source"]),n(),e("footer",yC,[f(k,null,{default:v(()=>[wC]),_:1})])]),n(),xC,n(),kC]),n(),e("section",null,[$C,n(),CC,n(),e("article",SC,[$e(e("input",{"onUpdate:modelValue":g[0]||(g[0]=x=>l.foo=x),type:"text","aria-label":"Text","aria-invalid":$(s).foo.ariaInv,"aria-describedby":"foo-error",onBlur:g[1]||(g[1]=x=>$(r)("foo"))},null,40,EC),[[ct,l.foo]]),n(),$(s).foo.ariaInv?(C(!0),S(J,{key:0},Oe($(s).foo.errors,x=>(C(),S("small",TC,re(x),1))),256)):He("",!0),n(),$e(e("input",{"onUpdate:modelValue":g[2]||(g[2]=x=>l.bar=x),type:"number","aria-label":"Number","aria-invalid":$(s).bar.ariaInv,onBlur:g[3]||(g[3]=x=>$(r)("bar"))},null,40,AC),[[ct,l.bar]]),n(),$(s).bar.ariaInv?(C(!0),S(J,{key:1},Oe($(s).bar.errors,x=>(C(),S("small",MC,re(x),1))),256)):He("",!0)])]),n(),e("section",null,[PC,n(),NC,n(),e("div",OC,[f(k,null,{default:v(()=>[RC]),_:1})]),n(),IC,n(),LC,n(),e("div",BC,[f(k,null,{default:v(()=>[DC]),_:1})]),n(),zC,n(),VC,n(),jC,n(),e("div",FC,[f(k,null,{default:v(()=>[HC]),_:1})]),n(),qC,n(),UC,n(),GC,n(),WC,n(),KC,n(),YC,n(),JC,n(),e("div",ZC,[f(k,null,{default:v(()=>[XC]),_:1})]),n(),QC,n(),e9,n(),t9,n(),n9,n(),o9,n(),a9,n(),l9,n(),s9,n(),r9,n(),i9,n(),e("article",c9,[$e(e("input",{"onUpdate:modelValue":g[4]||(g[4]=x=>i.foo=x),type:"text","aria-label":"Text","aria-describedby":"v-foo-error","aria-invalid":$(c).foo.ariaInv,onBlur:g[5]||(g[5]=x=>$(d)("foo"))},null,40,d9),[[ct,i.foo]]),n(),$(c).foo.ariaInv?(C(!0),S(J,{key:0},Oe($(c).foo.errors,x=>(C(),S("small",u9,re(x),1))),256)):He("",!0),n(),e("div",h9,[e("div",{class:Fe([$(c).foo.valid?"on":"off"])},"valid: "+re($(c).foo.valid),3),n(),e("div",{class:Fe([$(c).foo.invalid?"on":"off"])},"invalid: "+re($(c).foo.invalid),3),n(),e("div",{class:Fe([$(c).foo.changed?"on":"off"])},"changed: "+re($(c).foo.changed),3)]),n(),e("div",p9,[e("div",{class:Fe([$(c).foo.dirty?"on":"off"])},"dirty: "+re($(c).foo.dirty),3),n(),e("div",{class:Fe([$(c).foo.pristine?"on":"off"])},"pristine: "+re($(c).foo.pristine),3),n(),e("div",{class:Fe([$(c).foo.required?"on":"off"])},"required: "+re($(c).foo.required),3)]),n(),e("div",f9,[e("div",{class:Fe([$(c).foo.touched?"on":"off"])},"touched: "+re($(c).foo.touched),3),n(),e("div",{class:Fe([$(c).foo.untouched?"on":"off"])},"untouched: "+re($(c).foo.untouched),3),n(),e("div",{class:Fe([$(c).foo.ariaInv?"on":"off"])},"ariaInv: "+re($(c).foo.ariaInv),3)]),n(),e("div",m9,[e("div",{class:Fe([$(c).foo.validated?"on":"off"])},"validated: "+re($(c).foo.validated),3),n(),e("div",{class:Fe([$(c).foo.passed?"on":"off"])},"passed: "+re($(c).foo.passed),3),n(),e("div",{class:Fe([$(c).foo.failed?"on":"off"])},"failed: "+re($(c).foo.failed),3)])]),n(),b9,n(),g9,n(),_9,n(),v9,n(),y9,n(),w9,n(),x9,n(),k9,n(),$9,n(),C9]),n(),e("section",null,[S9,n(),E9,n(),T9,n(),e("div",A9,[f(k,null,{default:v(()=>[M9]),_:1})]),n(),P9,n(),N9,n(),O9,n(),e("div",R9,[f(k,null,{default:v(()=>[I9]),_:1})]),n(),L9]),n(),e("section",null,[B9,n(),D9,n(),z9,n(),V9,n(),j9,n(),F9,n(),e("article",H9,[$e(e("input",{"onUpdate:modelValue":g[6]||(g[6]=x=>h.alpha=x),type:"text","aria-label":"Text","aria-describedby":"rules-alpha-error","aria-invalid":$(u).alpha.ariaInv,onBlur:g[7]||(g[7]=x=>$(p)("alpha"))},null,40,q9),[[ct,h.alpha]]),n(),$(u).alpha.ariaInv?(C(!0),S(J,{key:0},Oe($(u).alpha.errors,x=>(C(),S("small",U9,re(x),1))),256)):He("",!0),n(),e("footer",G9,[f(k,null,{default:v(()=>[W9]),_:1})])]),n(),K9,n(),Y9,n(),J9,n(),e("article",Z9,[$e(e("input",{"onUpdate:modelValue":g[8]||(g[8]=x=>h.alphanum=x),type:"text","aria-label":"Text","aria-describedby":"rules-alphanum-error","aria-invalid":$(u).alphanum.ariaInv,onBlur:g[9]||(g[9]=x=>$(p)("alphanum"))},null,40,X9),[[ct,h.alphanum]]),n(),$(u).alphanum.ariaInv?(C(!0),S(J,{key:0},Oe($(u).alphanum.errors,x=>(C(),S("small",Q9,re(x),1))),256)):He("",!0),n(),e("footer",eS,[f(k,null,{default:v(()=>[tS]),_:1})])]),n(),nS,n(),oS,n(),e("article",aS,[$e(e("input",{"onUpdate:modelValue":g[10]||(g[10]=x=>h.digits=x),type:"text","aria-label":"Text","aria-describedby":"rules-digits-error","aria-invalid":$(u).digits.ariaInv,onBlur:g[11]||(g[11]=x=>$(p)("digits"))},null,40,lS),[[ct,h.digits]]),n(),$(u).digits.ariaInv?(C(!0),S(J,{key:0},Oe($(u).digits.errors,x=>(C(),S("small",sS,re(x),1))),256)):He("",!0),n(),e("footer",rS,[f(k,null,{default:v(()=>[iS]),_:1})])]),n(),cS,n(),dS,n(),e("article",uS,[$e(e("input",{"onUpdate:modelValue":g[12]||(g[12]=x=>h.email=x),type:"text","aria-label":"Text","aria-describedby":"rules-email-error","aria-invalid":$(u).email.ariaInv,onBlur:g[13]||(g[13]=x=>$(p)("email"))},null,40,hS),[[ct,h.email]]),n(),$(u).email.ariaInv?(C(!0),S(J,{key:0},Oe($(u).email.errors,x=>(C(),S("small",pS,re(x),1))),256)):He("",!0),n(),e("footer",fS,[f(k,null,{default:v(()=>[mS]),_:1})])]),n(),bS,n(),gS,n(),e("article",_S,[$e(e("input",{"onUpdate:modelValue":g[14]||(g[14]=x=>h.max=x),type:"number","aria-label":"Number","aria-describedby":"rules-max-error","aria-invalid":$(u).max.ariaInv,onBlur:g[15]||(g[15]=x=>$(p)("max"))},null,40,vS),[[ct,h.max]]),n(),$(u).max.ariaInv?(C(!0),S(J,{key:0},Oe($(u).max.errors,x=>(C(),S("small",yS,re(x),1))),256)):He("",!0),n(),e("footer",wS,[f(k,null,{default:v(()=>[xS]),_:1})])]),n(),kS,n(),$S,n(),CS,n(),e("article",SS,[$e(e("input",{"onUpdate:modelValue":g[16]||(g[16]=x=>h.min=x),type:"number","aria-label":"Number","aria-describedby":"rules-min-error","aria-invalid":$(u).min.ariaInv,onBlur:g[17]||(g[17]=x=>$(p)("min"))},null,40,ES),[[ct,h.min]]),n(),$(u).min.ariaInv?(C(!0),S(J,{key:0},Oe($(u).min.errors,x=>(C(),S("small",TS,re(x),1))),256)):He("",!0),n(),e("footer",AS,[f(k,null,{default:v(()=>[MS]),_:1})])]),n(),PS,n(),NS,n(),OS,n(),e("article",RS,[$e(e("input",{"onUpdate:modelValue":g[18]||(g[18]=x=>h.minmax=x),type:"number","aria-label":"Number","aria-describedby":"rules-minmax-error","aria-invalid":$(u).minmax.ariaInv,onBlur:g[19]||(g[19]=x=>$(p)("minmax"))},null,40,IS),[[ct,h.minmax]]),n(),$(u).minmax.ariaInv?(C(!0),S(J,{key:0},Oe($(u).minmax.errors,x=>(C(),S("small",LS,re(x),1))),256)):He("",!0),n(),e("footer",BS,[f(k,null,{default:v(()=>[DS]),_:1})])]),n(),zS,n(),VS,n(),jS,n(),e("article",FS,[$e(e("input",{"onUpdate:modelValue":g[20]||(g[20]=x=>h.required=x),type:"text","aria-label":"Text","aria-describedby":"rules-required-error","aria-invalid":$(u).required.ariaInv,onBlur:g[21]||(g[21]=x=>$(p)("required"))},null,40,HS),[[ct,h.required]]),n(),$(u).required.ariaInv?(C(!0),S(J,{key:0},Oe($(u).required.errors,x=>(C(),S("small",qS,re(x),1))),256)):He("",!0),n(),e("footer",US,[f(k,null,{default:v(()=>[GS]),_:1})])]),n(),WS,n(),KS,n(),e("article",YS,[$e(e("input",{"onUpdate:modelValue":g[22]||(g[22]=x=>h.required_if=x),type:"text","aria-label":"Text","aria-describedby":"rules-required_if-error","aria-invalid":$(u).required_if.ariaInv,onBlur:g[23]||(g[23]=x=>$(p)("required_if"))},null,40,JS),[[ct,h.required_if]]),n(),$(u).required_if.ariaInv?(C(!0),S(J,{key:0},Oe($(u).required_if.errors,x=>(C(),S("small",ZS,re(x),1))),256)):He("",!0),n(),e("footer",XS,[f(k,null,{default:v(()=>[QS]),_:1})])]),n(),eE,n(),tE,n(),nE,n(),e("div",oE,[f(k,null,{default:v(()=>[aE]),_:1})]),n(),lE,n(),e("div",sE,[f(k,null,{default:v(()=>[rE]),_:1})]),n(),iE,n(),e("div",cE,[f(k,null,{default:v(()=>[dE]),_:1})]),n(),uE,n(),hE,n(),e("div",pE,[f(k,null,{default:v(()=>[fE]),_:1})]),n(),mE,n(),bE,n(`>
      `),e("div",gE,[f(k,null,{default:v(()=>[_E]),_:1})]),n(),vE,n(),e("div",yE,[f(k,null,{default:v(()=>[wE]),_:1})]),n(),xE,n(),kE,n(),$E,n(),e("div",CE,[f(k,null,{default:v(()=>[SE]),_:1})]),n(),EE,n(),TE,n(),AE,n(),ME,n(),e("div",PE,[f(k,null,{default:v(()=>[NE]),_:1})]),n(),OE,n(),RE,n(),IE,n(),LE,n(),e("div",BE,[f(k,null,{default:v(()=>[DE]),_:1})]),n(),zE,n(),VE,n(),jE,n(),FE])])],64))}},qE=Xu({history:Au("/"),routes:[{path:"/",name:"home",component:Fh},{path:"/docs",name:"docs",component:a5,meta:{section:"getting-started"}},{path:"/docs/color-schemes",name:"color-schemes",component:sm,meta:{section:"getting-started"}},{path:"/docs/css-variables",name:"css-variables",component:M1,meta:{section:"customisation"}},{path:"/docs/sass",name:"sass",component:O5,meta:{section:"customisation"}},{path:"/docs/colors",name:"colors",component:xb,meta:{section:"customisation"}},{path:"/docs/container",name:"container",component:Xg,meta:{section:"layout"}},{path:"/docs/landmark-section",name:"landmark-section",component:Gw,meta:{section:"layout"}},{path:"/docs/grid",name:"grid",component:Wy,meta:{section:"layout"}},{path:"/docs/overflow-auto",name:"overflow-auto",component:P4,meta:{section:"layout"}},{path:"/docs/typography",name:"typography",component:v$,meta:{section:"content"}},{path:"/docs/link",name:"link",component:fx,meta:{section:"content"}},{path:"/docs/button",name:"button",component:Mf,meta:{section:"content"}},{path:"/docs/table",name:"table",component:ek,meta:{section:"content"}},{path:"/docs/forms/overview",name:"forms-overview",component:Dv,meta:{section:"forms"}},{path:"/docs/forms/input",name:"forms-input",component:av,meta:{section:"forms"}},{path:"/docs/forms/textarea",name:"forms-textarea",component:By,meta:{section:"forms"}},{path:"/docs/forms/select",name:"select",component:V2,meta:{section:"forms"}},{path:"/docs/forms/checkboxes",name:"forms-checkboxes",component:X_,meta:{section:"forms"}},{path:"/docs/forms/radios",name:"forms-radios",component:o2,meta:{section:"forms"}},{path:"/docs/forms/switch",name:"forms-switch",component:ay,meta:{section:"forms"}},{path:"/docs/forms/range",name:"forms-range",component:u2,meta:{section:"forms"}},{path:"/docs/accordion",name:"accordion",component:yp,meta:{section:"components"}},{path:"/docs/alert",name:"alert",component:Rp,meta:{section:"components"}},{path:"/docs/card",name:"card",component:Uf,meta:{section:"components"}},{path:"/docs/dropdown",name:"dropdown",component:E_,meta:{section:"components"}},{path:"/docs/group",name:"group",component:Mw,meta:{section:"components"}},{path:"/docs/loading",name:"loading",component:Nx,meta:{section:"components"}},{path:"/docs/modal",name:"modal",component:_3,meta:{section:"components"}},{path:"/docs/nav",name:"nav",component:d4,meta:{section:"components"}},{path:"/docs/note",name:"note",component:$4,meta:{section:"components"}},{path:"/docs/progress",name:"progress",component:q4,meta:{section:"components"}},{path:"/docs/tooltip",name:"tooltip",component:Tk,meta:{section:"components"}},{path:"/docs/tabs",name:"tabs",component:hk,meta:{section:"components"}},{path:"/docs/vue/modal",name:"n-modal",component:P$,meta:{section:"vue-components"}},{path:"/docs/vue/data-table",name:"n-data-table",component:K8,meta:{section:"vue-components"}},{path:"/docs/vue/password",name:"n-passwordj",component:X7,meta:{section:"vue-components"}},{path:"/docs/vue/sparkline",name:"n-sparkline",component:r7,meta:{section:"vue-components"}},{path:"/docs/vue/tabs",name:"n-tabs",component:w7,meta:{section:"vue-components"}},{path:"/docs/vue/validation",name:"n-validation",component:HE,meta:{section:"vue-components"}},{path:"/docs/mission",name:"mission",component:Vx,meta:{section:"about"}}]}),UE={install:(t,o)=>{const{hljs:a}=o;t.directive("highlight",l=>{l.hasAttribute("data-highlighted")||a.highlightElement(l)})}};/*!
  Highlight.js v11.9.0 (git: f47103d4f1)
  (c) 2006-2023 undefined and other contributors
  License: BSD-3-Clause
 */var un=function(){function t(_){return _ instanceof Map?_.clear=_.delete=_.set=()=>{throw Error("map is read-only")}:_ instanceof Set&&(_.add=_.clear=_.delete=()=>{throw Error("set is read-only")}),Object.freeze(_),Object.getOwnPropertyNames(_).forEach(y=>{const T=_[y],z=typeof T;z!=="object"&&z!=="function"||Object.isFrozen(T)||t(T)}),_}class o{constructor(y){y.data===void 0&&(y.data={}),this.data=y.data,this.isMatchIgnored=!1}ignoreMatch(){this.isMatchIgnored=!0}}function a(_){return _.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/"/g,"&quot;").replace(/'/g,"&#x27;")}function l(_,...y){const T=Object.create(null);for(const z in _)T[z]=_[z];return y.forEach(z=>{for(const Z in z)T[Z]=z[Z]}),T}const s=_=>!!_.scope;class r{constructor(y,T){this.buffer="",this.classPrefix=T.classPrefix,y.walk(this)}addText(y){this.buffer+=a(y)}openNode(y){if(!s(y))return;const T=((z,{prefix:Z})=>{if(z.startsWith("language:"))return z.replace("language:","language-");if(z.includes(".")){const N=z.split(".");return[`${Z}${N.shift()}`,...N.map((Y,H)=>`${Y}${"_".repeat(H+1)}`)].join(" ")}return`${Z}${z}`})(y.scope,{prefix:this.classPrefix});this.span(T)}closeNode(y){s(y)&&(this.buffer+="</span>")}value(){return this.buffer}span(y){this.buffer+=`<span class="${y}">`}}const i=(_={})=>{const y={children:[]};return Object.assign(y,_),y};class c{constructor(){this.rootNode=i(),this.stack=[this.rootNode]}get top(){return this.stack[this.stack.length-1]}get root(){return this.rootNode}add(y){this.top.children.push(y)}openNode(y){const T=i({scope:y});this.add(T),this.stack.push(T)}closeNode(){if(this.stack.length>1)return this.stack.pop()}closeAllNodes(){for(;this.closeNode(););}toJSON(){return JSON.stringify(this.rootNode,null,4)}walk(y){return this.constructor._walk(y,this.rootNode)}static _walk(y,T){return typeof T=="string"?y.addText(T):T.children&&(y.openNode(T),T.children.forEach(z=>this._walk(y,z)),y.closeNode(T)),y}static _collapse(y){typeof y!="string"&&y.children&&(y.children.every(T=>typeof T=="string")?y.children=[y.children.join("")]:y.children.forEach(T=>{c._collapse(T)}))}}class d extends c{constructor(y){super(),this.options=y}addText(y){y!==""&&this.add(y)}startScope(y){this.openNode(y)}endScope(){this.closeNode()}__addSublanguage(y,T){const z=y.root;T&&(z.scope="language:"+T),this.add(z)}toHTML(){return new r(this,this.options).value()}finalize(){return this.closeAllNodes(),!0}}function h(_){return _?typeof _=="string"?_:_.source:null}function u(_){return g("(?=",_,")")}function p(_){return g("(?:",_,")*")}function m(_){return g("(?:",_,")?")}function g(..._){return _.map(y=>h(y)).join("")}function x(..._){return"("+((T=>{const z=T[T.length-1];return typeof z=="object"&&z.constructor===Object?(T.splice(T.length-1,1),z):{}})(_).capture?"":"?:")+_.map(T=>h(T)).join("|")+")"}function B(_){return RegExp(_.toString()+"|").exec("").length-1}const W=/\[(?:[^\\\]]|\\.)*\]|\(\??|\\([1-9][0-9]*)|\\./;function F(_,{joinWith:y}){let T=0;return _.map(z=>{T+=1;const Z=T;let N=h(z),Y="";for(;N.length>0;){const H=W.exec(N);if(!H){Y+=N;break}Y+=N.substring(0,H.index),N=N.substring(H.index+H[0].length),H[0][0]==="\\"&&H[1]?Y+="\\"+(Number(H[1])+Z):(Y+=H[0],H[0]==="("&&T++)}return Y}).map(z=>`(${z})`).join(y)}const V="[a-zA-Z]\\w*",R="[a-zA-Z_]\\w*",q="\\b\\d+(\\.\\d+)?",ee="(-?)(\\b0[xX][a-fA-F0-9]+|(\\b\\d+(\\.\\d*)?|\\.\\d+)([eE][-+]?\\d+)?)",Q="\\b(0b[01]+)",P={begin:"\\\\[\\s\\S]",relevance:0},I={scope:"string",begin:"'",end:"'",illegal:"\\n",contains:[P]},te={scope:"string",begin:'"',end:'"',illegal:"\\n",contains:[P]},oe=(_,y,T={})=>{const z=l({scope:"comment",begin:_,end:y,contains:[]},T);z.contains.push({scope:"doctag",begin:"[ ]*(?=(TODO|FIXME|NOTE|BUG|OPTIMIZE|HACK|XXX):)",end:/(TODO|FIXME|NOTE|BUG|OPTIMIZE|HACK|XXX):/,excludeBegin:!0,relevance:0});const Z=x("I","a","is","so","us","to","at","if","in","it","on",/[A-Za-z]+['](d|ve|re|ll|t|s|n)/,/[A-Za-z]+[-][a-z]+/,/[A-Za-z][a-z]{2,}/);return z.contains.push({begin:g(/[ ]+/,"(",Z,/[.]?[:]?([.][ ]|[ ])/,"){3}")}),z},A=oe("//","$"),D=oe("/\\*","\\*/"),j=oe("#","$");var be=Object.freeze({__proto__:null,APOS_STRING_MODE:I,BACKSLASH_ESCAPE:P,BINARY_NUMBER_MODE:{scope:"number",begin:Q,relevance:0},BINARY_NUMBER_RE:Q,COMMENT:oe,C_BLOCK_COMMENT_MODE:D,C_LINE_COMMENT_MODE:A,C_NUMBER_MODE:{scope:"number",begin:ee,relevance:0},C_NUMBER_RE:ee,END_SAME_AS_BEGIN:_=>Object.assign(_,{"on:begin":(y,T)=>{T.data._beginMatch=y[1]},"on:end":(y,T)=>{T.data._beginMatch!==y[1]&&T.ignoreMatch()}}),HASH_COMMENT_MODE:j,IDENT_RE:V,MATCH_NOTHING_RE:/\b\B/,METHOD_GUARD:{begin:"\\.\\s*"+R,relevance:0},NUMBER_MODE:{scope:"number",begin:q,relevance:0},NUMBER_RE:q,PHRASAL_WORDS_MODE:{begin:/\b(a|an|the|are|I'm|isn't|don't|doesn't|won't|but|just|should|pretty|simply|enough|gonna|going|wtf|so|such|will|you|your|they|like|more)\b/},QUOTE_STRING_MODE:te,REGEXP_MODE:{scope:"regexp",begin:/\/(?=[^/\n]*\/)/,end:/\/[gimuy]*/,contains:[P,{begin:/\[/,end:/\]/,relevance:0,contains:[P]}]},RE_STARTERS_RE:"!|!=|!==|%|%=|&|&&|&=|\\*|\\*=|\\+|\\+=|,|-|-=|/=|/|:|;|<<|<<=|<=|<|===|==|=|>>>=|>>=|>=|>>>|>>|>|\\?|\\[|\\{|\\(|\\^|\\^=|\\||\\|=|\\|\\||~",SHEBANG:(_={})=>{const y=/^#![ ]*\//;return _.binary&&(_.begin=g(y,/.*\b/,_.binary,/\b.*/)),l({scope:"meta",begin:y,end:/$/,relevance:0,"on:begin":(T,z)=>{T.index!==0&&z.ignoreMatch()}},_)},TITLE_MODE:{scope:"title",begin:V,relevance:0},UNDERSCORE_IDENT_RE:R,UNDERSCORE_TITLE_MODE:{scope:"title",begin:R,relevance:0}});function xe(_,y){_.input[_.index-1]==="."&&y.ignoreMatch()}function fe(_,y){_.className!==void 0&&(_.scope=_.className,delete _.className)}function he(_,y){y&&_.beginKeywords&&(_.begin="\\b("+_.beginKeywords.split(" ").join("|")+")(?!\\.)(?=\\b|\\s)",_.__beforeBegin=xe,_.keywords=_.keywords||_.beginKeywords,delete _.beginKeywords,_.relevance===void 0&&(_.relevance=0))}function de(_,y){Array.isArray(_.illegal)&&(_.illegal=x(..._.illegal))}function Be(_,y){if(_.match){if(_.begin||_.end)throw Error("begin & end are not supported with match");_.begin=_.match,delete _.match}}function Ye(_,y){_.relevance===void 0&&(_.relevance=1)}const qe=(_,y)=>{if(!_.beforeMatch)return;if(_.starts)throw Error("beforeMatch cannot be used with starts");const T=Object.assign({},_);Object.keys(_).forEach(z=>{delete _[z]}),_.keywords=T.keywords,_.begin=g(T.beforeMatch,u(T.begin)),_.starts={relevance:0,contains:[Object.assign(T,{endsParent:!0})]},_.relevance=0,delete T.beforeMatch},je=["of","and","for","in","not","or","if","then","parent","list","value"],it="keyword";function ot(_,y,T=it){const z=Object.create(null);return typeof _=="string"?Z(T,_.split(" ")):Array.isArray(_)?Z(T,_):Object.keys(_).forEach(N=>{Object.assign(z,ot(_[N],y,N))}),z;function Z(N,Y){y&&(Y=Y.map(H=>H.toLowerCase())),Y.forEach(H=>{const ve=H.split("|");z[ve[0]]=[N,kt(ve[0],ve[1])]})}}function kt(_,y){return y?Number(y):(T=>je.includes(T.toLowerCase()))(_)?0:1}const Ue={},M=_=>{console.error(_)},ae=(_,...y)=>{console.log("WARN: "+_,...y)},X=(_,y)=>{Ue[`${_}/${y}`]||(console.log(`Deprecated as of ${_}. ${y}`),Ue[`${_}/${y}`]=!0)},ie=Error();function Te(_,y,{key:T}){let z=0;const Z=_[T],N={},Y={};for(let H=1;H<=y.length;H++)Y[H+z]=Z[H],N[H+z]=!0,z+=B(y[H-1]);_[T]=Y,_[T]._emit=N,_[T]._multi=!0}function Le(_){(y=>{y.scope&&typeof y.scope=="object"&&y.scope!==null&&(y.beginScope=y.scope,delete y.scope)})(_),typeof _.beginScope=="string"&&(_.beginScope={_wrap:_.beginScope}),typeof _.endScope=="string"&&(_.endScope={_wrap:_.endScope}),(y=>{if(Array.isArray(y.begin)){if(y.skip||y.excludeBegin||y.returnBegin)throw M("skip, excludeBegin, returnBegin not compatible with beginScope: {}"),ie;if(typeof y.beginScope!="object"||y.beginScope===null)throw M("beginScope must be object"),ie;Te(y,y.begin,{key:"beginScope"}),y.begin=F(y.begin,{joinWith:""})}})(_),(y=>{if(Array.isArray(y.end)){if(y.skip||y.excludeEnd||y.returnEnd)throw M("skip, excludeEnd, returnEnd not compatible with endScope: {}"),ie;if(typeof y.endScope!="object"||y.endScope===null)throw M("endScope must be object"),ie;Te(y,y.end,{key:"endScope"}),y.end=F(y.end,{joinWith:""})}})(_)}function b(_){function y(Z,N){return RegExp(h(Z),"m"+(_.case_insensitive?"i":"")+(_.unicodeRegex?"u":"")+(N?"g":""))}class T{constructor(){this.matchIndexes={},this.regexes=[],this.matchAt=1,this.position=0}addRule(N,Y){Y.position=this.position++,this.matchIndexes[this.matchAt]=Y,this.regexes.push([Y,N]),this.matchAt+=B(N)+1}compile(){this.regexes.length===0&&(this.exec=()=>null);const N=this.regexes.map(Y=>Y[1]);this.matcherRe=y(F(N,{joinWith:"|"}),!0),this.lastIndex=0}exec(N){this.matcherRe.lastIndex=this.lastIndex;const Y=this.matcherRe.exec(N);if(!Y)return null;const H=Y.findIndex((we,ke)=>ke>0&&we!==void 0),ve=this.matchIndexes[H];return Y.splice(0,H),Object.assign(Y,ve)}}class z{constructor(){this.rules=[],this.multiRegexes=[],this.count=0,this.lastIndex=0,this.regexIndex=0}getMatcher(N){if(this.multiRegexes[N])return this.multiRegexes[N];const Y=new T;return this.rules.slice(N).forEach(([H,ve])=>Y.addRule(H,ve)),Y.compile(),this.multiRegexes[N]=Y,Y}resumingScanAtSamePosition(){return this.regexIndex!==0}considerAll(){this.regexIndex=0}addRule(N,Y){this.rules.push([N,Y]),Y.type==="begin"&&this.count++}exec(N){const Y=this.getMatcher(this.regexIndex);Y.lastIndex=this.lastIndex;let H=Y.exec(N);if(this.resumingScanAtSamePosition()&&!(H&&H.index===this.lastIndex)){const ve=this.getMatcher(0);ve.lastIndex=this.lastIndex+1,H=ve.exec(N)}return H&&(this.regexIndex+=H.position+1,this.regexIndex===this.count&&this.considerAll()),H}}if(_.compilerExtensions||(_.compilerExtensions=[]),_.contains&&_.contains.includes("self"))throw Error("ERR: contains `self` is not supported at the top-level of a language.  See documentation.");return _.classNameAliases=l(_.classNameAliases||{}),function Z(N,Y){const H=N;if(N.isCompiled)return H;[fe,Be,Le,qe].forEach(we=>we(N,Y)),_.compilerExtensions.forEach(we=>we(N,Y)),N.__beforeBegin=null,[he,de,Ye].forEach(we=>we(N,Y)),N.isCompiled=!0;let ve=null;return typeof N.keywords=="object"&&N.keywords.$pattern&&(N.keywords=Object.assign({},N.keywords),ve=N.keywords.$pattern,delete N.keywords.$pattern),ve=ve||/\w+/,N.keywords&&(N.keywords=ot(N.keywords,_.case_insensitive)),H.keywordPatternRe=y(ve,!0),Y&&(N.begin||(N.begin=/\B|\b/),H.beginRe=y(H.begin),N.end||N.endsWithParent||(N.end=/\B|\b/),N.end&&(H.endRe=y(H.end)),H.terminatorEnd=h(H.end)||"",N.endsWithParent&&Y.terminatorEnd&&(H.terminatorEnd+=(N.end?"|":"")+Y.terminatorEnd)),N.illegal&&(H.illegalRe=y(N.illegal)),N.contains||(N.contains=[]),N.contains=[].concat(...N.contains.map(we=>(ke=>(ke.variants&&!ke.cachedVariants&&(ke.cachedVariants=ke.variants.map(ht=>l(ke,{variants:null},ht))),ke.cachedVariants?ke.cachedVariants:w(ke)?l(ke,{starts:ke.starts?l(ke.starts):null}):Object.isFrozen(ke)?l(ke):ke))(we==="self"?N:we))),N.contains.forEach(we=>{Z(we,H)}),N.starts&&Z(N.starts,Y),H.matcher=(we=>{const ke=new z;return we.contains.forEach(ht=>ke.addRule(ht.begin,{rule:ht,type:"begin"})),we.terminatorEnd&&ke.addRule(we.terminatorEnd,{type:"end"}),we.illegal&&ke.addRule(we.illegal,{type:"illegal"}),ke})(H),H}(_)}function w(_){return!!_&&(_.endsWithParent||w(_.starts))}class E extends Error{constructor(y,T){super(y),this.name="HTMLInjectionError",this.html=T}}const L=a,O=l,K=Symbol("nomatch"),ne=_=>{const y=Object.create(null),T=Object.create(null),z=[];let Z=!0;const N="Could not find the language '{}', did you forget to load/include a language module?",Y={disableAutodetect:!0,name:"Plain text",contains:[]};let H={ignoreUnescapedHTML:!1,throwUnescapedHTML:!1,noHighlightRe:/^(no-?highlight)$/i,languageDetectRe:/\blang(?:uage)?-([\w-]+)\b/i,classPrefix:"hljs-",cssSelector:"pre code",languages:null,__emitter:d};function ve(se){return H.noHighlightRe.test(se)}function we(se,me,Ee){let tt="",Xe="";typeof me=="object"?(tt=se,Ee=me.ignoreIllegals,Xe=me.language):(X("10.7.0","highlight(lang, code, ...args) has been deprecated."),X("10.7.0",`Please use highlight(code, options) instead.
https://github.com/highlightjs/highlight.js/issues/2277`),Xe=se,tt=me),Ee===void 0&&(Ee=!0);const Qe={code:tt,language:Xe};po("before:highlight",Qe);const nt=Qe.result?Qe.result:ke(Qe.language,Qe.code,Ee);return nt.code=Qe.code,po("after:highlight",nt),nt}function ke(se,me,Ee,tt){const Xe=Object.create(null);function Qe(){if(!ye.keywords)return void at.addText(De);let le=0;ye.keywordPatternRe.lastIndex=0;let pe=ye.keywordPatternRe.exec(De),Ie="";for(;pe;){Ie+=De.substring(le,pe.index);const ze=Nt.case_insensitive?pe[0].toLowerCase():pe[0],lt=(Ne=ze,ye.keywords[Ne]);if(lt){const[Dt,fa]=lt;if(at.addText(Ie),Ie="",Xe[ze]=(Xe[ze]||0)+1,Xe[ze]<=7&&(mo+=fa),Dt.startsWith("_"))Ie+=pe[0];else{const bo=Nt.classNameAliases[Dt]||Dt;mt(pe[0],bo)}}else Ie+=pe[0];le=ye.keywordPatternRe.lastIndex,pe=ye.keywordPatternRe.exec(De)}var Ne;Ie+=De.substring(le),at.addText(Ie)}function nt(){ye.subLanguage!=null?(()=>{if(De==="")return;let le=null;if(typeof ye.subLanguage=="string"){if(!y[ye.subLanguage])return void at.addText(De);le=ke(ye.subLanguage,De,!0,Sl[ye.subLanguage]),Sl[ye.subLanguage]=le._top}else le=ht(De,ye.subLanguage.length?ye.subLanguage:null);ye.relevance>0&&(mo+=le.relevance),at.__addSublanguage(le._emitter,le.language)})():Qe(),De=""}function mt(le,pe){le!==""&&(at.startScope(pe),at.addText(le),at.endScope())}function We(le,pe){let Ie=1;const Ne=pe.length-1;for(;Ie<=Ne;){if(!le._emit[Ie]){Ie++;continue}const ze=Nt.classNameAliases[le[Ie]]||le[Ie],lt=pe[Ie];ze?mt(lt,ze):(De=lt,Qe(),De=""),Ie++}}function Et(le,pe){return le.scope&&typeof le.scope=="string"&&at.openNode(Nt.classNameAliases[le.scope]||le.scope),le.beginScope&&(le.beginScope._wrap?(mt(De,Nt.classNameAliases[le.beginScope._wrap]||le.beginScope._wrap),De=""):le.beginScope._multi&&(We(le.beginScope,pe),De="")),ye=Object.create(le,{parent:{value:ye}}),ye}function $l(le,pe,Ie){let Ne=((ze,lt)=>{const Dt=ze&&ze.exec(lt);return Dt&&Dt.index===0})(le.endRe,Ie);if(Ne){if(le["on:end"]){const ze=new o(le);le["on:end"](pe,ze),ze.isMatchIgnored&&(Ne=!1)}if(Ne){for(;le.endsParent&&le.parent;)le=le.parent;return le}}if(le.endsWithParent)return $l(le.parent,pe,Ie)}function mi(le){return ye.matcher.regexIndex===0?(De+=le[0],1):(pa=!0,0)}function bi(le){const pe=le[0],Ie=me.substring(le.index),Ne=$l(ye,le,Ie);if(!Ne)return K;const ze=ye;ye.endScope&&ye.endScope._wrap?(nt(),mt(pe,ye.endScope._wrap)):ye.endScope&&ye.endScope._multi?(nt(),We(ye.endScope,le)):ze.skip?De+=pe:(ze.returnEnd||ze.excludeEnd||(De+=pe),nt(),ze.excludeEnd&&(De=pe));do ye.scope&&at.closeNode(),ye.skip||ye.subLanguage||(mo+=ye.relevance),ye=ye.parent;while(ye!==Ne.parent);return Ne.starts&&Et(Ne.starts,le),ze.returnEnd?0:pe.length}let fo={};function Cl(le,pe){const Ie=pe&&pe[0];if(De+=le,Ie==null)return nt(),0;if(fo.type==="begin"&&pe.type==="end"&&fo.index===pe.index&&Ie===""){if(De+=me.slice(pe.index,pe.index+1),!Z){const Ne=Error(`0 width match regex (${se})`);throw Ne.languageName=se,Ne.badRule=fo.rule,Ne}return 1}if(fo=pe,pe.type==="begin")return(Ne=>{const ze=Ne[0],lt=Ne.rule,Dt=new o(lt),fa=[lt.__beforeBegin,lt["on:begin"]];for(const bo of fa)if(bo&&(bo(Ne,Dt),Dt.isMatchIgnored))return mi(ze);return lt.skip?De+=ze:(lt.excludeBegin&&(De+=ze),nt(),lt.returnBegin||lt.excludeBegin||(De=ze)),Et(lt,Ne),lt.returnBegin?0:ze.length})(pe);if(pe.type==="illegal"&&!Ee){const Ne=Error('Illegal lexeme "'+Ie+'" for mode "'+(ye.scope||"<unnamed>")+'"');throw Ne.mode=ye,Ne}if(pe.type==="end"){const Ne=bi(pe);if(Ne!==K)return Ne}if(pe.type==="illegal"&&Ie==="")return 1;if(ha>1e5&&ha>3*pe.index)throw Error("potential infinite loop, way more iterations than matches");return De+=Ie,Ie.length}const Nt=Ge(se);if(!Nt)throw M(N.replace("{}",se)),Error('Unknown language: "'+se+'"');const gi=b(Nt);let ua="",ye=tt||gi;const Sl={},at=new H.__emitter(H);(()=>{const le=[];for(let pe=ye;pe!==Nt;pe=pe.parent)pe.scope&&le.unshift(pe.scope);le.forEach(pe=>at.openNode(pe))})();let De="",mo=0,pn=0,ha=0,pa=!1;try{if(Nt.__emitTokens)Nt.__emitTokens(me,at);else{for(ye.matcher.considerAll();;){ha++,pa?pa=!1:ye.matcher.considerAll(),ye.matcher.lastIndex=pn;const le=ye.matcher.exec(me);if(!le)break;const pe=Cl(me.substring(pn,le.index),le);pn=le.index+pe}Cl(me.substring(pn))}return at.finalize(),ua=at.toHTML(),{language:se,value:ua,relevance:mo,illegal:!1,_emitter:at,_top:ye}}catch(le){if(le.message&&le.message.includes("Illegal"))return{language:se,value:L(me),illegal:!0,relevance:0,_illegalBy:{message:le.message,index:pn,context:me.slice(pn-100,pn+100),mode:le.mode,resultSoFar:ua},_emitter:at};if(Z)return{language:se,value:L(me),illegal:!1,relevance:0,errorRaised:le,_emitter:at,_top:ye};throw le}}function ht(se,me){me=me||H.languages||Object.keys(y);const Ee=(We=>{const Et={value:L(We),illegal:!1,relevance:0,_top:Y,_emitter:new H.__emitter(H)};return Et._emitter.addText(We),Et})(se),tt=me.filter(Ge).filter(Vn).map(We=>ke(We,se,!1));tt.unshift(Ee);const Xe=tt.sort((We,Et)=>{if(We.relevance!==Et.relevance)return Et.relevance-We.relevance;if(We.language&&Et.language){if(Ge(We.language).supersetOf===Et.language)return 1;if(Ge(Et.language).supersetOf===We.language)return-1}return 0}),[Qe,nt]=Xe,mt=Qe;return mt.secondBest=nt,mt}function Tn(se){let me=null;const Ee=(Qe=>{let nt=Qe.className+" ";nt+=Qe.parentNode?Qe.parentNode.className:"";const mt=H.languageDetectRe.exec(nt);if(mt){const We=Ge(mt[1]);return We||(ae(N.replace("{}",mt[1])),ae("Falling back to no-highlight mode for this block.",Qe)),We?mt[1]:"no-highlight"}return nt.split(/\s+/).find(We=>ve(We)||Ge(We))})(se);if(ve(Ee))return;if(po("before:highlightElement",{el:se,language:Ee}),se.dataset.highlighted)return void console.log("Element previously highlighted. To highlight again, first unset `dataset.highlighted`.",se);if(se.children.length>0&&(H.ignoreUnescapedHTML||(console.warn("One of your code blocks includes unescaped HTML. This is a potentially serious security risk."),console.warn("https://github.com/highlightjs/highlight.js/wiki/security"),console.warn("The element with unescaped HTML:"),console.warn(se)),H.throwUnescapedHTML))throw new E("One of your code blocks includes unescaped HTML.",se.innerHTML);me=se;const tt=me.textContent,Xe=Ee?we(tt,{language:Ee,ignoreIllegals:!0}):ht(tt);se.innerHTML=Xe.value,se.dataset.highlighted="yes",((Qe,nt,mt)=>{const We=nt&&T[nt]||mt;Qe.classList.add("hljs"),Qe.classList.add("language-"+We)})(se,Ee,Xe.language),se.result={language:Xe.language,re:Xe.relevance,relevance:Xe.relevance},Xe.secondBest&&(se.secondBest={language:Xe.secondBest.language,relevance:Xe.secondBest.relevance}),po("after:highlightElement",{el:se,result:Xe,text:tt})}let Ut=!1;function hn(){document.readyState!=="loading"?document.querySelectorAll(H.cssSelector).forEach(Tn):Ut=!0}function Ge(se){return se=(se||"").toLowerCase(),y[se]||y[T[se]]}function vt(se,{languageName:me}){typeof se=="string"&&(se=[se]),se.forEach(Ee=>{T[Ee.toLowerCase()]=me})}function Vn(se){const me=Ge(se);return me&&!me.disableAutodetect}function po(se,me){const Ee=se;z.forEach(tt=>{tt[Ee]&&tt[Ee](me)})}typeof window<"u"&&window.addEventListener&&window.addEventListener("DOMContentLoaded",()=>{Ut&&hn()},!1),Object.assign(_,{highlight:we,highlightAuto:ht,highlightAll:hn,highlightElement:Tn,highlightBlock:se=>(X("10.7.0","highlightBlock will be removed entirely in v12.0"),X("10.7.0","Please use highlightElement now."),Tn(se)),configure:se=>{H=O(H,se)},initHighlighting:()=>{hn(),X("10.6.0","initHighlighting() deprecated.  Use highlightAll() now.")},initHighlightingOnLoad:()=>{hn(),X("10.6.0","initHighlightingOnLoad() deprecated.  Use highlightAll() now.")},registerLanguage:(se,me)=>{let Ee=null;try{Ee=me(_)}catch(tt){if(M("Language definition for '{}' could not be registered.".replace("{}",se)),!Z)throw tt;M(tt),Ee=Y}Ee.name||(Ee.name=se),y[se]=Ee,Ee.rawDefinition=me.bind(null,_),Ee.aliases&&vt(Ee.aliases,{languageName:se})},unregisterLanguage:se=>{delete y[se];for(const me of Object.keys(T))T[me]===se&&delete T[me]},listLanguages:()=>Object.keys(y),getLanguage:Ge,registerAliases:vt,autoDetection:Vn,inherit:O,addPlugin:se=>{(me=>{me["before:highlightBlock"]&&!me["before:highlightElement"]&&(me["before:highlightElement"]=Ee=>{me["before:highlightBlock"](Object.assign({block:Ee.el},Ee))}),me["after:highlightBlock"]&&!me["after:highlightElement"]&&(me["after:highlightElement"]=Ee=>{me["after:highlightBlock"](Object.assign({block:Ee.el},Ee))})})(se),z.push(se)},removePlugin:se=>{const me=z.indexOf(se);me!==-1&&z.splice(me,1)}}),_.debugMode=()=>{Z=!1},_.safeMode=()=>{Z=!0},_.versionString="11.9.0",_.regex={concat:g,lookahead:u,either:x,optional:m,anyNumberOfTimes:p};for(const se in be)typeof be[se]=="object"&&t(be[se]);return Object.assign(_,be),_},U=ne({});return U.newInstance=()=>ne({}),U}();typeof fi=="object"&&typeof Ja<"u"&&(Ja.exports=un);/*! `css` grammar compiled for Highlight.js 11.9.0 */(()=>{var t=(()=>{const o=["a","abbr","address","article","aside","audio","b","blockquote","body","button","canvas","caption","cite","code","dd","del","details","dfn","div","dl","dt","em","fieldset","figcaption","figure","footer","form","h1","h2","h3","h4","h5","h6","header","hgroup","html","i","iframe","img","input","ins","kbd","label","legend","li","main","mark","menu","nav","object","ol","p","q","quote","samp","section","span","strong","summary","sup","table","tbody","td","textarea","tfoot","th","thead","time","tr","ul","var","video"],a=["any-hover","any-pointer","aspect-ratio","color","color-gamut","color-index","device-aspect-ratio","device-height","device-width","display-mode","forced-colors","grid","height","hover","inverted-colors","monochrome","orientation","overflow-block","overflow-inline","pointer","prefers-color-scheme","prefers-contrast","prefers-reduced-motion","prefers-reduced-transparency","resolution","scan","scripting","update","width","min-width","max-width","min-height","max-height"],l=["active","any-link","blank","checked","current","default","defined","dir","disabled","drop","empty","enabled","first","first-child","first-of-type","fullscreen","future","focus","focus-visible","focus-within","has","host","host-context","hover","indeterminate","in-range","invalid","is","lang","last-child","last-of-type","left","link","local-link","not","nth-child","nth-col","nth-last-child","nth-last-col","nth-last-of-type","nth-of-type","only-child","only-of-type","optional","out-of-range","past","placeholder-shown","read-only","read-write","required","right","root","scope","target","target-within","user-invalid","valid","visited","where"],s=["after","backdrop","before","cue","cue-region","first-letter","first-line","grammar-error","marker","part","placeholder","selection","slotted","spelling-error"],r=["align-content","align-items","align-self","all","animation","animation-delay","animation-direction","animation-duration","animation-fill-mode","animation-iteration-count","animation-name","animation-play-state","animation-timing-function","backface-visibility","background","background-attachment","background-blend-mode","background-clip","background-color","background-image","background-origin","background-position","background-repeat","background-size","block-size","border","border-block","border-block-color","border-block-end","border-block-end-color","border-block-end-style","border-block-end-width","border-block-start","border-block-start-color","border-block-start-style","border-block-start-width","border-block-style","border-block-width","border-bottom","border-bottom-color","border-bottom-left-radius","border-bottom-right-radius","border-bottom-style","border-bottom-width","border-collapse","border-color","border-image","border-image-outset","border-image-repeat","border-image-slice","border-image-source","border-image-width","border-inline","border-inline-color","border-inline-end","border-inline-end-color","border-inline-end-style","border-inline-end-width","border-inline-start","border-inline-start-color","border-inline-start-style","border-inline-start-width","border-inline-style","border-inline-width","border-left","border-left-color","border-left-style","border-left-width","border-radius","border-right","border-right-color","border-right-style","border-right-width","border-spacing","border-style","border-top","border-top-color","border-top-left-radius","border-top-right-radius","border-top-style","border-top-width","border-width","bottom","box-decoration-break","box-shadow","box-sizing","break-after","break-before","break-inside","caption-side","caret-color","clear","clip","clip-path","clip-rule","color","column-count","column-fill","column-gap","column-rule","column-rule-color","column-rule-style","column-rule-width","column-span","column-width","columns","contain","content","content-visibility","counter-increment","counter-reset","cue","cue-after","cue-before","cursor","direction","display","empty-cells","filter","flex","flex-basis","flex-direction","flex-flow","flex-grow","flex-shrink","flex-wrap","float","flow","font","font-display","font-family","font-feature-settings","font-kerning","font-language-override","font-size","font-size-adjust","font-smoothing","font-stretch","font-style","font-synthesis","font-variant","font-variant-caps","font-variant-east-asian","font-variant-ligatures","font-variant-numeric","font-variant-position","font-variation-settings","font-weight","gap","glyph-orientation-vertical","grid","grid-area","grid-auto-columns","grid-auto-flow","grid-auto-rows","grid-column","grid-column-end","grid-column-start","grid-gap","grid-row","grid-row-end","grid-row-start","grid-template","grid-template-areas","grid-template-columns","grid-template-rows","hanging-punctuation","height","hyphens","icon","image-orientation","image-rendering","image-resolution","ime-mode","inline-size","isolation","justify-content","left","letter-spacing","line-break","line-height","list-style","list-style-image","list-style-position","list-style-type","margin","margin-block","margin-block-end","margin-block-start","margin-bottom","margin-inline","margin-inline-end","margin-inline-start","margin-left","margin-right","margin-top","marks","mask","mask-border","mask-border-mode","mask-border-outset","mask-border-repeat","mask-border-slice","mask-border-source","mask-border-width","mask-clip","mask-composite","mask-image","mask-mode","mask-origin","mask-position","mask-repeat","mask-size","mask-type","max-block-size","max-height","max-inline-size","max-width","min-block-size","min-height","min-inline-size","min-width","mix-blend-mode","nav-down","nav-index","nav-left","nav-right","nav-up","none","normal","object-fit","object-position","opacity","order","orphans","outline","outline-color","outline-offset","outline-style","outline-width","overflow","overflow-wrap","overflow-x","overflow-y","padding","padding-block","padding-block-end","padding-block-start","padding-bottom","padding-inline","padding-inline-end","padding-inline-start","padding-left","padding-right","padding-top","page-break-after","page-break-before","page-break-inside","pause","pause-after","pause-before","perspective","perspective-origin","pointer-events","position","quotes","resize","rest","rest-after","rest-before","right","row-gap","scroll-margin","scroll-margin-block","scroll-margin-block-end","scroll-margin-block-start","scroll-margin-bottom","scroll-margin-inline","scroll-margin-inline-end","scroll-margin-inline-start","scroll-margin-left","scroll-margin-right","scroll-margin-top","scroll-padding","scroll-padding-block","scroll-padding-block-end","scroll-padding-block-start","scroll-padding-bottom","scroll-padding-inline","scroll-padding-inline-end","scroll-padding-inline-start","scroll-padding-left","scroll-padding-right","scroll-padding-top","scroll-snap-align","scroll-snap-stop","scroll-snap-type","scrollbar-color","scrollbar-gutter","scrollbar-width","shape-image-threshold","shape-margin","shape-outside","speak","speak-as","src","tab-size","table-layout","text-align","text-align-all","text-align-last","text-combine-upright","text-decoration","text-decoration-color","text-decoration-line","text-decoration-style","text-emphasis","text-emphasis-color","text-emphasis-position","text-emphasis-style","text-indent","text-justify","text-orientation","text-overflow","text-rendering","text-shadow","text-transform","text-underline-position","top","transform","transform-box","transform-origin","transform-style","transition","transition-delay","transition-duration","transition-property","transition-timing-function","unicode-bidi","vertical-align","visibility","voice-balance","voice-duration","voice-family","voice-pitch","voice-range","voice-rate","voice-stress","voice-volume","white-space","widows","width","will-change","word-break","word-spacing","word-wrap","writing-mode","z-index"].reverse();return i=>{const c=i.regex,d=(u=>({IMPORTANT:{scope:"meta",begin:"!important"},BLOCK_COMMENT:u.C_BLOCK_COMMENT_MODE,HEXCOLOR:{scope:"number",begin:/#(([0-9a-fA-F]{3,4})|(([0-9a-fA-F]{2}){3,4}))\b/},FUNCTION_DISPATCH:{className:"built_in",begin:/[\w-]+(?=\()/},ATTRIBUTE_SELECTOR_MODE:{scope:"selector-attr",begin:/\[/,end:/\]/,illegal:"$",contains:[u.APOS_STRING_MODE,u.QUOTE_STRING_MODE]},CSS_NUMBER_MODE:{scope:"number",begin:u.NUMBER_RE+"(%|em|ex|ch|rem|vw|vh|vmin|vmax|cm|mm|in|pt|pc|px|deg|grad|rad|turn|s|ms|Hz|kHz|dpi|dpcm|dppx)?",relevance:0},CSS_VARIABLE:{className:"attr",begin:/--[A-Za-z_][A-Za-z0-9_-]*/}}))(i),h=[i.APOS_STRING_MODE,i.QUOTE_STRING_MODE];return{name:"CSS",case_insensitive:!0,illegal:/[=|'\$]/,keywords:{keyframePosition:"from to"},classNameAliases:{keyframePosition:"selector-tag"},contains:[d.BLOCK_COMMENT,{begin:/-(webkit|moz|ms|o)-(?=[a-z])/},d.CSS_NUMBER_MODE,{className:"selector-id",begin:/#[A-Za-z0-9_-]+/,relevance:0},{className:"selector-class",begin:"\\.[a-zA-Z-][a-zA-Z0-9_-]*",relevance:0},d.ATTRIBUTE_SELECTOR_MODE,{className:"selector-pseudo",variants:[{begin:":("+l.join("|")+")"},{begin:":(:)?("+s.join("|")+")"}]},d.CSS_VARIABLE,{className:"attribute",begin:"\\b("+r.join("|")+")\\b"},{begin:/:/,end:/[;}{]/,contains:[d.BLOCK_COMMENT,d.HEXCOLOR,d.IMPORTANT,d.CSS_NUMBER_MODE,...h,{begin:/(url|data-uri)\(/,end:/\)/,relevance:0,keywords:{built_in:"url data-uri"},contains:[...h,{className:"string",begin:/[^)]/,endsWithParent:!0,excludeEnd:!0}]},d.FUNCTION_DISPATCH]},{begin:c.lookahead(/@/),end:"[{;]",relevance:0,illegal:/:/,contains:[{className:"keyword",begin:/@-?\w[\w]*(-\w+)*/},{begin:/\s/,endsWithParent:!0,excludeEnd:!0,relevance:0,keywords:{$pattern:/[a-z-]+/,keyword:"and or not only",attribute:a.join(" ")},contains:[{begin:/[a-z-]+(?=:)/,className:"attribute"},...h,d.CSS_NUMBER_MODE]}]},{className:"selector-tag",begin:"\\b("+o.join("|")+")\\b"}]}}})();un.registerLanguage("css",t)})();/*! `javascript` grammar compiled for Highlight.js 11.9.0 */(()=>{var t=(()=>{const o="[A-Za-z$_][0-9A-Za-z$_]*",a=["as","in","of","if","for","while","finally","var","new","function","do","return","void","else","break","catch","instanceof","with","throw","case","default","try","switch","continue","typeof","delete","let","yield","const","class","debugger","async","await","static","import","from","export","extends"],l=["true","false","null","undefined","NaN","Infinity"],s=["Object","Function","Boolean","Symbol","Math","Date","Number","BigInt","String","RegExp","Array","Float32Array","Float64Array","Int8Array","Uint8Array","Uint8ClampedArray","Int16Array","Int32Array","Uint16Array","Uint32Array","BigInt64Array","BigUint64Array","Set","Map","WeakSet","WeakMap","ArrayBuffer","SharedArrayBuffer","Atomics","DataView","JSON","Promise","Generator","GeneratorFunction","AsyncFunction","Reflect","Proxy","Intl","WebAssembly"],r=["Error","EvalError","InternalError","RangeError","ReferenceError","SyntaxError","TypeError","URIError"],i=["setInterval","setTimeout","clearInterval","clearTimeout","require","exports","eval","isFinite","isNaN","parseFloat","parseInt","decodeURI","decodeURIComponent","encodeURI","encodeURIComponent","escape","unescape"],c=["arguments","this","super","console","window","document","localStorage","sessionStorage","module","global"],d=[].concat(i,s,r);return h=>{const u=h.regex,p=o,m={begin:/<[A-Za-z0-9\\._:-]+/,end:/\/[A-Za-z0-9\\._:-]+>|\/>/,isTrulyOpeningTag:(qe,je)=>{const it=qe[0].length+qe.index,ot=qe.input[it];if(ot==="<"||ot===",")return void je.ignoreMatch();let kt;ot===">"&&(((M,{after:ae})=>{const X="</"+M[0].slice(1);return M.input.indexOf(X,ae)!==-1})(qe,{after:it})||je.ignoreMatch());const Ue=qe.input.substring(it);((kt=Ue.match(/^\s*=/))||(kt=Ue.match(/^\s+extends\s+/))&&kt.index===0)&&je.ignoreMatch()}},g={$pattern:o,keyword:a,literal:l,built_in:d,"variable.language":c},x="[0-9](_?[0-9])*",B=`\\.(${x})`,W="0|[1-9](_?[0-9])*|0[0-7]*[89][0-9]*",F={className:"number",variants:[{begin:`(\\b(${W})((${B})|\\.)?|(${B}))[eE][+-]?(${x})\\b`},{begin:`\\b(${W})\\b((${B})\\b|\\.)?|(${B})\\b`},{begin:"\\b(0|[1-9](_?[0-9])*)n\\b"},{begin:"\\b0[xX][0-9a-fA-F](_?[0-9a-fA-F])*n?\\b"},{begin:"\\b0[bB][0-1](_?[0-1])*n?\\b"},{begin:"\\b0[oO][0-7](_?[0-7])*n?\\b"},{begin:"\\b0[0-7]+n?\\b"}],relevance:0},V={className:"subst",begin:"\\$\\{",end:"\\}",keywords:g,contains:[]},R={begin:"html`",end:"",starts:{end:"`",returnEnd:!1,contains:[h.BACKSLASH_ESCAPE,V],subLanguage:"xml"}},q={begin:"css`",end:"",starts:{end:"`",returnEnd:!1,contains:[h.BACKSLASH_ESCAPE,V],subLanguage:"css"}},ee={begin:"gql`",end:"",starts:{end:"`",returnEnd:!1,contains:[h.BACKSLASH_ESCAPE,V],subLanguage:"graphql"}},Q={className:"string",begin:"`",end:"`",contains:[h.BACKSLASH_ESCAPE,V]},P={className:"comment",variants:[h.COMMENT(/\/\*\*(?!\/)/,"\\*/",{relevance:0,contains:[{begin:"(?=@[A-Za-z]+)",relevance:0,contains:[{className:"doctag",begin:"@[A-Za-z]+"},{className:"type",begin:"\\{",end:"\\}",excludeEnd:!0,excludeBegin:!0,relevance:0},{className:"variable",begin:p+"(?=\\s*(-)|$)",endsParent:!0,relevance:0},{begin:/(?=[^\n])\s/,relevance:0}]}]}),h.C_BLOCK_COMMENT_MODE,h.C_LINE_COMMENT_MODE]},I=[h.APOS_STRING_MODE,h.QUOTE_STRING_MODE,R,q,ee,Q,{match:/\$\d+/},F];V.contains=I.concat({begin:/\{/,end:/\}/,keywords:g,contains:["self"].concat(I)});const te=[].concat(P,V.contains),oe=te.concat([{begin:/\(/,end:/\)/,keywords:g,contains:["self"].concat(te)}]),A={className:"params",begin:/\(/,end:/\)/,excludeBegin:!0,excludeEnd:!0,keywords:g,contains:oe},D={variants:[{match:[/class/,/\s+/,p,/\s+/,/extends/,/\s+/,u.concat(p,"(",u.concat(/\./,p),")*")],scope:{1:"keyword",3:"title.class",5:"keyword",7:"title.class.inherited"}},{match:[/class/,/\s+/,p],scope:{1:"keyword",3:"title.class"}}]},j={relevance:0,match:u.either(/\bJSON/,/\b[A-Z][a-z]+([A-Z][a-z]*|\d)*/,/\b[A-Z]{2,}([A-Z][a-z]+|\d)+([A-Z][a-z]*)*/,/\b[A-Z]{2,}[a-z]+([A-Z][a-z]+|\d)*([A-Z][a-z]*)*/),className:"title.class",keywords:{_:[...s,...r]}},be={variants:[{match:[/function/,/\s+/,p,/(?=\s*\()/]},{match:[/function/,/\s*(?=\()/]}],className:{1:"keyword",3:"title.function"},label:"func.def",contains:[A],illegal:/%/},xe={match:u.concat(/\b/,(fe=[...i,"super","import"],u.concat("(?!",fe.join("|"),")")),p,u.lookahead(/\(/)),className:"title.function",relevance:0};var fe;const he={begin:u.concat(/\./,u.lookahead(u.concat(p,/(?![0-9A-Za-z$_(])/))),end:p,excludeBegin:!0,keywords:"prototype",className:"property",relevance:0},de={match:[/get|set/,/\s+/,p,/(?=\()/],className:{1:"keyword",3:"title.function"},contains:[{begin:/\(\)/},A]},Be="(\\([^()]*(\\([^()]*(\\([^()]*\\)[^()]*)*\\)[^()]*)*\\)|"+h.UNDERSCORE_IDENT_RE+")\\s*=>",Ye={match:[/const|var|let/,/\s+/,p,/\s*/,/=\s*/,/(async\s*)?/,u.lookahead(Be)],keywords:"async",className:{1:"keyword",3:"title.function"},contains:[A]};return{name:"JavaScript",aliases:["js","jsx","mjs","cjs"],keywords:g,exports:{PARAMS_CONTAINS:oe,CLASS_REFERENCE:j},illegal:/#(?![$_A-z])/,contains:[h.SHEBANG({label:"shebang",binary:"node",relevance:5}),{label:"use_strict",className:"meta",relevance:10,begin:/^\s*['"]use (strict|asm)['"]/},h.APOS_STRING_MODE,h.QUOTE_STRING_MODE,R,q,ee,Q,P,{match:/\$\d+/},F,j,{className:"attr",begin:p+u.lookahead(":"),relevance:0},Ye,{begin:"("+h.RE_STARTERS_RE+"|\\b(case|return|throw)\\b)\\s*",keywords:"return throw case",relevance:0,contains:[P,h.REGEXP_MODE,{className:"function",begin:Be,returnBegin:!0,end:"\\s*=>",contains:[{className:"params",variants:[{begin:h.UNDERSCORE_IDENT_RE,relevance:0},{className:null,begin:/\(\s*\)/,skip:!0},{begin:/\(/,end:/\)/,excludeBegin:!0,excludeEnd:!0,keywords:g,contains:oe}]}]},{begin:/,/,relevance:0},{match:/\s+/,relevance:0},{variants:[{begin:"<>",end:"</>"},{match:/<[A-Za-z0-9\\._:-]+\s*\/>/},{begin:m.begin,"on:begin":m.isTrulyOpeningTag,end:m.end}],subLanguage:"xml",contains:[{begin:m.begin,end:m.end,skip:!0,contains:["self"]}]}]},be,{beginKeywords:"while if switch catch for"},{begin:"\\b(?!function)"+h.UNDERSCORE_IDENT_RE+"\\([^()]*(\\([^()]*(\\([^()]*\\)[^()]*)*\\)[^()]*)*\\)\\s*\\{",returnBegin:!0,label:"func.def",contains:[A,h.inherit(h.TITLE_MODE,{begin:p,className:"title.function"})]},{match:/\.\.\./,relevance:0},he,{match:"\\$"+p,relevance:0},{match:[/\bconstructor(?=\s*\()/],className:{1:"title.function"},contains:[A]},xe,{relevance:0,match:/\b[A-Z][A-Z_0-9]+\b/,className:"variable.constant"},D,de,{match:/\$[(.]/}]}}})();un.registerLanguage("javascript",t)})();/*! `json` grammar compiled for Highlight.js 11.9.0 */(()=>{var t=(()=>o=>{const a=["true","false","null"],l={scope:"literal",beginKeywords:a.join(" ")};return{name:"JSON",keywords:{literal:a},contains:[{className:"attr",begin:/"(\\.|[^\\"\r\n])*"(?=\s*:)/,relevance:1.01},{match:/[{}[\],:]/,className:"punctuation",relevance:0},o.QUOTE_STRING_MODE,l,o.C_NUMBER_MODE,o.C_LINE_COMMENT_MODE,o.C_BLOCK_COMMENT_MODE],illegal:"\\S"}})();un.registerLanguage("json",t)})();/*! `scss` grammar compiled for Highlight.js 11.9.0 */(()=>{var t=(()=>{const o=["a","abbr","address","article","aside","audio","b","blockquote","body","button","canvas","caption","cite","code","dd","del","details","dfn","div","dl","dt","em","fieldset","figcaption","figure","footer","form","h1","h2","h3","h4","h5","h6","header","hgroup","html","i","iframe","img","input","ins","kbd","label","legend","li","main","mark","menu","nav","object","ol","p","q","quote","samp","section","span","strong","summary","sup","table","tbody","td","textarea","tfoot","th","thead","time","tr","ul","var","video"],a=["any-hover","any-pointer","aspect-ratio","color","color-gamut","color-index","device-aspect-ratio","device-height","device-width","display-mode","forced-colors","grid","height","hover","inverted-colors","monochrome","orientation","overflow-block","overflow-inline","pointer","prefers-color-scheme","prefers-contrast","prefers-reduced-motion","prefers-reduced-transparency","resolution","scan","scripting","update","width","min-width","max-width","min-height","max-height"],l=["active","any-link","blank","checked","current","default","defined","dir","disabled","drop","empty","enabled","first","first-child","first-of-type","fullscreen","future","focus","focus-visible","focus-within","has","host","host-context","hover","indeterminate","in-range","invalid","is","lang","last-child","last-of-type","left","link","local-link","not","nth-child","nth-col","nth-last-child","nth-last-col","nth-last-of-type","nth-of-type","only-child","only-of-type","optional","out-of-range","past","placeholder-shown","read-only","read-write","required","right","root","scope","target","target-within","user-invalid","valid","visited","where"],s=["after","backdrop","before","cue","cue-region","first-letter","first-line","grammar-error","marker","part","placeholder","selection","slotted","spelling-error"],r=["align-content","align-items","align-self","all","animation","animation-delay","animation-direction","animation-duration","animation-fill-mode","animation-iteration-count","animation-name","animation-play-state","animation-timing-function","backface-visibility","background","background-attachment","background-blend-mode","background-clip","background-color","background-image","background-origin","background-position","background-repeat","background-size","block-size","border","border-block","border-block-color","border-block-end","border-block-end-color","border-block-end-style","border-block-end-width","border-block-start","border-block-start-color","border-block-start-style","border-block-start-width","border-block-style","border-block-width","border-bottom","border-bottom-color","border-bottom-left-radius","border-bottom-right-radius","border-bottom-style","border-bottom-width","border-collapse","border-color","border-image","border-image-outset","border-image-repeat","border-image-slice","border-image-source","border-image-width","border-inline","border-inline-color","border-inline-end","border-inline-end-color","border-inline-end-style","border-inline-end-width","border-inline-start","border-inline-start-color","border-inline-start-style","border-inline-start-width","border-inline-style","border-inline-width","border-left","border-left-color","border-left-style","border-left-width","border-radius","border-right","border-right-color","border-right-style","border-right-width","border-spacing","border-style","border-top","border-top-color","border-top-left-radius","border-top-right-radius","border-top-style","border-top-width","border-width","bottom","box-decoration-break","box-shadow","box-sizing","break-after","break-before","break-inside","caption-side","caret-color","clear","clip","clip-path","clip-rule","color","column-count","column-fill","column-gap","column-rule","column-rule-color","column-rule-style","column-rule-width","column-span","column-width","columns","contain","content","content-visibility","counter-increment","counter-reset","cue","cue-after","cue-before","cursor","direction","display","empty-cells","filter","flex","flex-basis","flex-direction","flex-flow","flex-grow","flex-shrink","flex-wrap","float","flow","font","font-display","font-family","font-feature-settings","font-kerning","font-language-override","font-size","font-size-adjust","font-smoothing","font-stretch","font-style","font-synthesis","font-variant","font-variant-caps","font-variant-east-asian","font-variant-ligatures","font-variant-numeric","font-variant-position","font-variation-settings","font-weight","gap","glyph-orientation-vertical","grid","grid-area","grid-auto-columns","grid-auto-flow","grid-auto-rows","grid-column","grid-column-end","grid-column-start","grid-gap","grid-row","grid-row-end","grid-row-start","grid-template","grid-template-areas","grid-template-columns","grid-template-rows","hanging-punctuation","height","hyphens","icon","image-orientation","image-rendering","image-resolution","ime-mode","inline-size","isolation","justify-content","left","letter-spacing","line-break","line-height","list-style","list-style-image","list-style-position","list-style-type","margin","margin-block","margin-block-end","margin-block-start","margin-bottom","margin-inline","margin-inline-end","margin-inline-start","margin-left","margin-right","margin-top","marks","mask","mask-border","mask-border-mode","mask-border-outset","mask-border-repeat","mask-border-slice","mask-border-source","mask-border-width","mask-clip","mask-composite","mask-image","mask-mode","mask-origin","mask-position","mask-repeat","mask-size","mask-type","max-block-size","max-height","max-inline-size","max-width","min-block-size","min-height","min-inline-size","min-width","mix-blend-mode","nav-down","nav-index","nav-left","nav-right","nav-up","none","normal","object-fit","object-position","opacity","order","orphans","outline","outline-color","outline-offset","outline-style","outline-width","overflow","overflow-wrap","overflow-x","overflow-y","padding","padding-block","padding-block-end","padding-block-start","padding-bottom","padding-inline","padding-inline-end","padding-inline-start","padding-left","padding-right","padding-top","page-break-after","page-break-before","page-break-inside","pause","pause-after","pause-before","perspective","perspective-origin","pointer-events","position","quotes","resize","rest","rest-after","rest-before","right","row-gap","scroll-margin","scroll-margin-block","scroll-margin-block-end","scroll-margin-block-start","scroll-margin-bottom","scroll-margin-inline","scroll-margin-inline-end","scroll-margin-inline-start","scroll-margin-left","scroll-margin-right","scroll-margin-top","scroll-padding","scroll-padding-block","scroll-padding-block-end","scroll-padding-block-start","scroll-padding-bottom","scroll-padding-inline","scroll-padding-inline-end","scroll-padding-inline-start","scroll-padding-left","scroll-padding-right","scroll-padding-top","scroll-snap-align","scroll-snap-stop","scroll-snap-type","scrollbar-color","scrollbar-gutter","scrollbar-width","shape-image-threshold","shape-margin","shape-outside","speak","speak-as","src","tab-size","table-layout","text-align","text-align-all","text-align-last","text-combine-upright","text-decoration","text-decoration-color","text-decoration-line","text-decoration-style","text-emphasis","text-emphasis-color","text-emphasis-position","text-emphasis-style","text-indent","text-justify","text-orientation","text-overflow","text-rendering","text-shadow","text-transform","text-underline-position","top","transform","transform-box","transform-origin","transform-style","transition","transition-delay","transition-duration","transition-property","transition-timing-function","unicode-bidi","vertical-align","visibility","voice-balance","voice-duration","voice-family","voice-pitch","voice-range","voice-rate","voice-stress","voice-volume","white-space","widows","width","will-change","word-break","word-spacing","word-wrap","writing-mode","z-index"].reverse();return i=>{const c=(m=>({IMPORTANT:{scope:"meta",begin:"!important"},BLOCK_COMMENT:m.C_BLOCK_COMMENT_MODE,HEXCOLOR:{scope:"number",begin:/#(([0-9a-fA-F]{3,4})|(([0-9a-fA-F]{2}){3,4}))\b/},FUNCTION_DISPATCH:{className:"built_in",begin:/[\w-]+(?=\()/},ATTRIBUTE_SELECTOR_MODE:{scope:"selector-attr",begin:/\[/,end:/\]/,illegal:"$",contains:[m.APOS_STRING_MODE,m.QUOTE_STRING_MODE]},CSS_NUMBER_MODE:{scope:"number",begin:m.NUMBER_RE+"(%|em|ex|ch|rem|vw|vh|vmin|vmax|cm|mm|in|pt|pc|px|deg|grad|rad|turn|s|ms|Hz|kHz|dpi|dpcm|dppx)?",relevance:0},CSS_VARIABLE:{className:"attr",begin:/--[A-Za-z_][A-Za-z0-9_-]*/}}))(i),d=s,h=l,u="@[a-z-]+",p={className:"variable",begin:"(\\$[a-zA-Z-][a-zA-Z0-9_-]*)\\b",relevance:0};return{name:"SCSS",case_insensitive:!0,illegal:"[=/|']",contains:[i.C_LINE_COMMENT_MODE,i.C_BLOCK_COMMENT_MODE,c.CSS_NUMBER_MODE,{className:"selector-id",begin:"#[A-Za-z0-9_-]+",relevance:0},{className:"selector-class",begin:"\\.[A-Za-z0-9_-]+",relevance:0},c.ATTRIBUTE_SELECTOR_MODE,{className:"selector-tag",begin:"\\b("+o.join("|")+")\\b",relevance:0},{className:"selector-pseudo",begin:":("+h.join("|")+")"},{className:"selector-pseudo",begin:":(:)?("+d.join("|")+")"},p,{begin:/\(/,end:/\)/,contains:[c.CSS_NUMBER_MODE]},c.CSS_VARIABLE,{className:"attribute",begin:"\\b("+r.join("|")+")\\b"},{begin:"\\b(whitespace|wait|w-resize|visible|vertical-text|vertical-ideographic|uppercase|upper-roman|upper-alpha|underline|transparent|top|thin|thick|text|text-top|text-bottom|tb-rl|table-header-group|table-footer-group|sw-resize|super|strict|static|square|solid|small-caps|separate|se-resize|scroll|s-resize|rtl|row-resize|ridge|right|repeat|repeat-y|repeat-x|relative|progress|pointer|overline|outside|outset|oblique|nowrap|not-allowed|normal|none|nw-resize|no-repeat|no-drop|newspaper|ne-resize|n-resize|move|middle|medium|ltr|lr-tb|lowercase|lower-roman|lower-alpha|loose|list-item|line|line-through|line-edge|lighter|left|keep-all|justify|italic|inter-word|inter-ideograph|inside|inset|inline|inline-block|inherit|inactive|ideograph-space|ideograph-parenthesis|ideograph-numeric|ideograph-alpha|horizontal|hidden|help|hand|groove|fixed|ellipsis|e-resize|double|dotted|distribute|distribute-space|distribute-letter|distribute-all-lines|disc|disabled|default|decimal|dashed|crosshair|collapse|col-resize|circle|char|center|capitalize|break-word|break-all|bottom|both|bolder|bold|block|bidi-override|below|baseline|auto|always|all-scroll|absolute|table|table-cell)\\b"},{begin:/:/,end:/[;}{]/,relevance:0,contains:[c.BLOCK_COMMENT,p,c.HEXCOLOR,c.CSS_NUMBER_MODE,i.QUOTE_STRING_MODE,i.APOS_STRING_MODE,c.IMPORTANT,c.FUNCTION_DISPATCH]},{begin:"@(page|font-face)",keywords:{$pattern:u,keyword:"@page @font-face"}},{begin:"@",end:"[{;]",returnBegin:!0,keywords:{$pattern:/[a-z-]+/,keyword:"and or not only",attribute:a.join(" ")},contains:[{begin:u,className:"keyword"},{begin:/[a-z-]+(?=:)/,className:"attribute"},p,i.QUOTE_STRING_MODE,i.APOS_STRING_MODE,c.HEXCOLOR,c.CSS_NUMBER_MODE]},c.FUNCTION_DISPATCH]}}})();un.registerLanguage("scss",t)})();/*! `xml` grammar compiled for Highlight.js 11.9.0 */(()=>{var t=(()=>o=>{const a=o.regex,l=a.concat(/[\p{L}_]/u,a.optional(/[\p{L}0-9_.-]*:/u),/[\p{L}0-9_.-]*/u),s={className:"symbol",begin:/&[a-z]+;|&#[0-9]+;|&#x[a-f0-9]+;/},r={begin:/\s/,contains:[{className:"keyword",begin:/#?[a-z_][a-z1-9_-]+/,illegal:/\n/}]},i=o.inherit(r,{begin:/\(/,end:/\)/}),c=o.inherit(o.APOS_STRING_MODE,{className:"string"}),d=o.inherit(o.QUOTE_STRING_MODE,{className:"string"}),h={endsWithParent:!0,illegal:/</,relevance:0,contains:[{className:"attr",begin:/[\p{L}0-9._:-]+/u,relevance:0},{begin:/=\s*/,relevance:0,contains:[{className:"string",endsParent:!0,variants:[{begin:/"/,end:/"/,contains:[s]},{begin:/'/,end:/'/,contains:[s]},{begin:/[^\s"'=<>`]+/}]}]}]};return{name:"HTML, XML",aliases:["html","xhtml","rss","atom","xjb","xsd","xsl","plist","wsf","svg"],case_insensitive:!0,unicodeRegex:!0,contains:[{className:"meta",begin:/<![a-z]/,end:/>/,relevance:10,contains:[r,d,c,i,{begin:/\[/,end:/\]/,contains:[{className:"meta",begin:/<![a-z]/,end:/>/,contains:[r,i,d,c]}]}]},o.COMMENT(/<!--/,/-->/,{relevance:10}),{begin:/<!\[CDATA\[/,end:/\]\]>/,relevance:10},s,{className:"meta",end:/\?>/,variants:[{begin:/<\?xml/,relevance:10,contains:[d]},{begin:/<\?[a-z][a-z0-9]+/}]},{className:"tag",begin:/<style(?=\s|>)/,end:/>/,keywords:{name:"style"},contains:[h],starts:{end:/<\/style>/,returnEnd:!0,subLanguage:["css","xml"]}},{className:"tag",begin:/<script(?=\s|>)/,end:/>/,keywords:{name:"script"},contains:[h],starts:{end:/<\/script>/,returnEnd:!0,subLanguage:["javascript","handlebars","xml"]}},{className:"tag",begin:/<>|<\/>/},{className:"tag",begin:a.concat(/</,a.lookahead(a.concat(l,a.either(/\/>/,/>/,/\s/)))),end:/\/?>/,contains:[{className:"name",begin:l,relevance:0,starts:h}]},{className:"tag",begin:a.concat(/<\//,a.lookahead(a.concat(l,/>/))),contains:[{className:"name",begin:l,relevance:0},{begin:/>/,relevance:0,endsParent:!0}]}]}})();un.registerLanguage("xml",t)})();un.configure({cssSelector:"code",ignoreUnescapedHTML:!0});const kl=Jd(jh);kl.use(UE,{hljs:un});kl.use(qE);kl.mount("#app")});export default GE();
