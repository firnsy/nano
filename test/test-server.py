#!/usr/bin/env python3

from http.server import HTTPServer, BaseHTTPRequestHandler
from nano.fsp import NanoFSP

import csv
import json
import os
import sqlite3

# create sqlite connection
conn = sqlite3.connect('test.db')
conn.row_factory = sqlite3.Row

# describe columns
columns = {
    "id": {
        "type": "number",
        "label": "ID",
    },
    "customer_id": {
        "type": "string",
        "label": "Customer ID",
    },
    "first_name": {
        "type": "string",
        "label": "First Name",
    },
    "last_name": {
        "type": "string",
        "label": "Last Name",
    },
    "company": {
        "type": "string",
        "label": "Company",
    },
    "city": {
        "type": "string",
        "label": "City",
    },
    "country": {
        "type": "string",
        "label": "Country",
    },
    "phone_1": {
        "type": "string",
        "label": "Phone 1",
    },
    "phone_2": {
        "type": "string",
        "label": "Phone 2",
    },
    "email": {
        "type": "string",
        "label": "Email",
    },
    "subscription_date": {
        "type": "date",
        "label": "Subscription Date",
    },
    "website": {
        "type": "string",
        "label": "Website",
    },
    "dob": {
        "type": "datetime",
        "label": "D.O.B.",
    },
}

def init_table(conn, create_table_sql):
    try:
        c = conn.cursor()
        c.execute("""
            CREATE TABLE IF NOT EXISTS users (
                id integer PRIMARY KEY,
                customer_id text NOT NULL,
                first_name text NOT NULL,
                last_name text NOT NULL,
                company text NOT NULL,
                city text NOT NULL,
                country text NOT NULL,
                phone_1 text NOT NULL,
                phone_2 text NOT NULL,
                email text NOT NULL,
                subscription_date text NOT NULL,
                website text NOT NULL
            );
        """)

         # load csv file into database
        with open('users.csv', 'r') as f:
            f.readline()

            csv_reader = csv.reader(f)
            c = conn.cursor()

            for row in csv_reader:
                c.execute("INSERT INTO users VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", row)

            conn.commit()

    except Error as e:
        print(e)


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/':
            self.path = 'index.html'

        elif self.path.startswith('/'):
            self.path = self.path[1:]

        if ".." in self.path:
            self.send_error(404,'File Not Found: %s' % self.path)
            return

        real_path = os.path.join('../dist/docs', self.path)

        # assume we're looking for an asset and fall back to index.html
        if not os.path.isfile(real_path):
            real_path = os.path.join('../dist/docs', 'index.html')

        try:
            f = open(real_path, 'rb')
            self.send_response(200)

            if self.path.endswith(".js"):
                self.send_header('Content-type', 'text/javascript')
            elif self.path.endswith(".css"):
                self.send_header('Content-type', 'text/css')
            else:
                self.send_header('Content-type', 'text/html')

            self.end_headers()
            self.wfile.write(f.read())
            f.close()
            return

        except IOError:
            self.send_error(404,'File Not Found: %s' % self.path)

    def do_OPTIONS(self):
        self.send_response(200)
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Headers', 'Content-Type')
        self.end_headers()

    def do_POST(self):
        # get the length of the request body
        content_length = int(self.headers['Content-Length'])

        # read the request body
        body = self.rfile.read(content_length)

        # parse the body as json
        data = json.loads(body)

        print("DATA: ", data)

        fsp = NanoFSP(
            columns,
            """
                SELECT
                    COUNT(id) OVER() AS item_count,
                    id,
                    customer_id,
                    first_name,
                    last_name,
                    company,
                    city,
                    country,
                    phone_1,
                    phone_2,
                    email,
                    subscription_date,
                    website,
                    dob
                FROM users
                 @filter @sorter @pager
            """
        )

        fsp_req = fsp.parse(data)

        # query the database
        c = conn.cursor()
        sql, sql_args = fsp_req.sql_params()
        print("GENERATED SQL: ", sql, sql_args)
        r = c.execute(sql, sql_args)

        rows = r.fetchall()
        item_count = 0
        items = []
        for row in rows:
            row = dict(row)
            item_count = row.pop("item_count")
            items.append(row)

        data = fsp_req.response(items, item_count)

        self.send_response(200)
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Content-type', 'application/json')
        self.end_headers()

        self.wfile.write(json.dumps(data).encode('utf-8'))



httpd = HTTPServer(('localhost', 3333), SimpleHTTPRequestHandler)
print("Server running on port 3333")

httpd.serve_forever()
