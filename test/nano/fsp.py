import math

class NanoFSP():
    """
    A class for managing filter, sort, and pager params.
    """

    def __init__(self, columns, sql, db_type="sqlite"):
        """
        Args:
            columns (dict): The columns schema to process and validate future requests against.
            sql (str): The SQL to execute.
            db_type (str): The database type. Default is "sqlite".
        """
        self._columns = columns
        self._sql = sql
        self._db_type = db_type

    def parse(self, data):
        """
        Parse the filter, pager, and sort params from the request body.

        Args:
            data (dict): The request body.
        """
        request = NanoFSPRequest(
            data,
            self._columns,
            self._sql,
            self._db_type
        )

        return request

class NanoFSPRequest():
    """
    A class for managing a nano client-side datatable request.
    """
    def __init__(self, request, columns, sql, db_type="sqlite"):
        self._request = request

        self._sql = sql
        self._columns = columns

        self._db_type = db_type

        if self._db_type in ["mysql", "sqlite"]:
            self._named_param = lambda p: f":{p}"

        elif self._db_type == "postgres":
            self._named_param = lambda p: f"%({p})s"

        else:
            raise Exception(f"Unsupported db type: {self._db_type}")

        self._filter = []
        self._pager = {}
        self._sorter = []

        if request is not None:
            self._filter = request.get("filter", [])
            self._pager = request.get("pager", {})
            self._sorter = request.get("sorter", [])

            self._filter_sql, self._filter_args = self._parse_filter(self._filter)
            self._sorter_sql, self._sorter_args = self._parse_sorter(self._sorter)
            self._pager_sql, self._pager_args = self._parse_pager(self._pager)

    def _parse_filter(self, data, index=0, depth=0):
        """
        Parse the filter params.

        Args:
            data (list): The filter params.
 
        Returns:
            str: The filter sql.
            list: The filter args.

        Available operators:
            $and: AND operator (groups only)
            $or: OR operator (groups only)
            $nand: NAND operator (groups only)
            $nor: NOR operator (groups only)
            $eq: Equal to (all column types)
            $neq: Not equal to (all column types)
            $gt: Greater than (numeric, date, datetime columns)
            $gte: Greater than or equal to (numeric, date, datetime columns)
            $lt: Less than (numeric, date, datetime columns)
            $lte: Less than or equal to (numeric, date, datetime columns)
            $c: Contains (string columns)
            $nc: Does not contain (string columns)
            $sw: Starts with (string columns)
            $ew: Ends with (string columns)
        """
        args = {}
        ret = ""

        if not isinstance(data, list):
            print("DATA: ", data)
            raise Exception("Filter must be a list")

        if not data:
            return "", {}

        if len(data) < 2:
            raise Exception("Filter must have at least 2 elements")

        opcode = data[0]
        param_key = f"filter_{depth}_{index}"
        param = self._named_param(param_key)

        if opcode == "$and":
            sub_args = {}
            sub_ret = []

            for i, f in enumerate(data[1:]):
                r, a = self._parse_filter(f, i, depth + 1)

                sub_args.update(a)
                sub_ret.append(r)

            ret += "(" + " AND ".join(sub_ret) + ")"
            args.update(sub_args)

        elif opcode == "$nand":
            sub_args = {}
            sub_ret = []

            for i, f in enumerate(data[1:]):
                r, a = self._parse_filter(f, i, depth + 1)

                sub_args.update(a)
                sub_ret.append(r)

            ret += "NOT (" + " AND ".join(sub_ret) + ")"
            args.update(sub_args)

        elif opcode == "$or":
            sub_args = {}
            sub_ret = []

            for i, f in enumerate(data[1:]):
                r, a = self._parse_filter(f, i, depth + 1)

                sub_args.update(a)
                sub_ret.append(r)

            ret += "(" + " OR ".join(sub_ret) + ")"
            args.update(sub_args)

        elif opcode == "$nor":
            sub_args = {}
            sub_ret = []

            for i, f in enumerate(data[1:]):
                r, a = self._parse_filter(f, i, depth + 1)

                sub_args.update(a)
                sub_ret.append(r)

            ret += "NOT (" + " OR ".join(sub_ret) + ")"
            args.update(sub_args)

        elif opcode in ["$eq", "$neq", "$gt", "$gte", "$lt", "$lte"]:
            if len(data) != 3:
                raise Exception(f"Invalid number of arguments for operator: {opcode}")

            field = data[1]
            field_type = self._columns[field]["type"]

            value = data[2]

            if field_type == "date":
                place_holder = f"date({param})"
            elif field_type == "datetime":
                place_holder = f"datetime({param})"
            else:
                place_holder = f"{param}"

            if opcode == "$eq":
                ret += f"{field} = {place_holder}"
            elif opcode == "$neq":
                ret += f"{field} != {place_holder}"
            elif opcode == "$gt":
                ret += f"{field} > {place_holder}"
            elif opcode == "$gte":
                ret += f"{field} >= {place_holder}"
            elif opcode == "$lt":
                ret += f"{field} < {place_holder}"
            elif opcode == "$lte":
                ret += f"{field} <= {place_holder}"

            args[param_key] = value

        elif opcode == "$c":
            if len(data) != 3:
                raise Exception(f"Invalid number of arguments for operator: {opcode}")

            field = data[1]
            value = data[2]

            ret += field + f" LIKE {param}"
            args[param_key] = "%" + value + "%"

        elif opcode == "$nc":
            if len(data) != 3:
                raise Exception(f"Invalid number of arguments for operator: {opcode}")

            field = data[1]
            value = data[2]

            ret += field + f" NOT LIKE {param}"
            args[param_key] = "%" + value + "%"

        elif opcode == "$sw":
            if len(data) != 3:
                raise Exception(f"Invalid number of arguments for operator: {opcode}")

            field = data[1]
            value = data[2]

            ret += field + f" LIKE {param}"
            args[param_key] = value + "%"

        elif opcode == "$ew":
            if len(data) != 3:
                raise Exception(f"Invalid number of arguments for operator: {opcode}")

            field = data[1]
            value = data[2]

            ret += field + f" LIKE {param}"
            args[param_key] = "%" + value

        else:
            raise Exception(f"Invalid opcode: {opcode}")

        return ret, args

    def _parse_sorter(self, data):
        if not isinstance(data, list):
            raise Exception("Invalid sort format")

        sorts = []

        for s in data:
            if s[0] == "-":
                sorts.append(s[1:] + " DESC")
            elif s[0] == "+":
                sorts.append(s[1:] + " ASC")

        return ", ".join(sorts), {}

    def _parse_pager(self, data):
        page_size = data.get("pageSize", 0)
        page = data.get("page", 1)

        if page_size <= 0:
            raise Exception(f"Invalid page size: {page_size}")

        elif page < 1:
            raise Exception(f"Invalid page: {page}")

        elif page_size == 0:
            return "", []

        offset = (page - 1) * page_size

        return f"LIMIT :pager_limit OFFSET :pager_offset", { "pager_limit": page_size, "pager_offset": offset }

    def get_filter_sql(self):
        return self._filter_sql

    def get_filter_args(self):
        return self._filter_args

    def get_pager_sql(self):
        return self._pager_sql

    def get_pager_args(self):
        return self._pager_args

    def get_sorter_sql(self):
        return self._sorter_sql

    def get_sorter_args(self):
        return self._sorter_args

    def get_sql(self):
        return self._filter_sql, self._sorter_sql, self._pager_sql

    def get_sql_args(self):
        return self._filter_args + self._sorter_args + self._pager_args

    def has_filter(self):
        """
        Check if the request has a filter.
        """
        return bool(self._filter)

    def has_pager(self):
        """
        Check if the request has a pager.
        """
        return bool(self._pager)

    def has_sorter(self):
        """
        Check if the request has a sorter.
        """
        return bool(self._sorter)

    def response(self, items=[], item_count=None):
        """
        Make a response object.

        Args:
            items (list): The items to return.

        Returns:
            dict: The response object.
        """

        response = {
            "items": items,
        }

        if self.has_filter():
            response["filter"] = self._filter

        if self.has_pager():
            if item_count is not None:
                self._pager["itemCount"] = item_count
                self._pager["pageCount"] = math.ceil(item_count / self._pager.get("pageSize", 10))

            response["pager"] = self._pager

        if self.has_sorter():
            response["sorter"] = self._sorter

        return response

    def sql_params(self):
        """
        Get the SQL and associated args for the request.

        Returns:
            str: The SQL.
            dict: The SQL args.

        Available operators:
            @filter: Inserts "WHERE" and the filter SQL. If no filter, it is removed.
            @filter_no_where: Inserts the filter SQL. If no filter, it is removed.
            @sorter: Inserts "ORDER BY" and the sorter SQL. If no sorter, it is removed.
            @sorter_no_order_by: Inserts the sorter SQL. If no sorter, it is removed.
            @pager: Inserts the pager SQL (includes "LIMIT" and "OFFSET"). If no pager, it is removed.
        """

        if not self._sql:
            raise Exception("No SQL provided")

        sql = self._sql
        sql_args = {}

        if "@filter" in sql:
            if self.has_filter():
                sql = sql.replace("@filter", "WHERE " + self._filter_sql)
                sql_args.update(self._filter_args)
            else:
                sql = sql.replace("@filter", "")

        if "@filter_no_where" in sql:
            if self.has_filter():
                sql = sql.replace("@filter_no_where", self._filter_sql)
                sql_args.update(self._filter_args)
            else:
                sql = sql.replace("@filter_no_where", "")

        if "@sorter" in sql:
            if self.has_sorter():
                sql = sql.replace("@sorter", "ORDER BY " + self._sorter_sql)
            else:
                sql = sql.replace("@sorter", "")

        if "@sorter_no_order_by" in sql:
            if self.has_sorter():
                sql = sql.replace("@sorter_no_order_by", self._sorter_sql)
            else:
                sql = sql.replace("@sorter_no_order_by", "")

        if "@pager" in sql:
            if self.has_pager():
                sql = sql.replace("@pager", self._pager_sql)    
                sql_args.update(self._pager_args)
            else:
                sql = sql.replace("@pager", "")

        return sql, sql_args
