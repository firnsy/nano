/**
 * This file is part of nano.
 *
 * Author(s):
 *  - Ian Firns <firnsy@gmail.com>
 *
 * License: MIT
 */

/**
 * @public
 * @desc debounce function to prevent multiple calls to a function
 *
 * @param {function} fn - the function to debounce
 * @param {number} delay - the delay in milliseconds
 *
 * @return {function} - the debounced function
 *
 * @example
 * const debounced = debounce(() => console.log('debounced'), 1000); 
 * debounced(); // will log 'debounced' after 1 second
 * 
 */
export function debounce(fn, delay) {
  let timeout = null;

  const debounce = (...args) => {
    if (timeout !== null) {
      clearTimeout(timeout)
      timeout = null
    }

    timeout = setTimeout(() => fn(...args), delay)
  }

  return debounce;
};

