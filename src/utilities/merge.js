import { isArray, isObject } from './is';
/**
 * @desc Deep merge objects.
 *
 * @param {Object[]} objects - The objects to merge.
 * @returns {Object} The merged object.
 *
 * @example
 *   const a = { foo: { bar: 3 } };
 *   const b = { foo: { baz: 4 }, qux: 5 };
 *   const c = merge(a, b);
 *   // c now equals { foo: { bar: 3, baz: 4 }, qux: 5 }
 */
export function merge(...objects) {
  function deepMergeInner(target, source) {
    Object.keys(source).forEach((key) => {
      const targetValue = target[key];
      const sourceValue = source[key];

      /* eslint-disable no-param-reassign */
      if (isArray(targetValue) && isArray(sourceValue)) {
        target[key] = targetValue.concat(sourceValue);
      } else if (isObject(targetValue) && isObject(sourceValue)) {
        target[key] = deepMergeInner({ ...targetValue }, sourceValue);
      } else {
        target[key] = sourceValue;
      }
    });

    return target;
  }

  if (objects.length < 2) {
    throw new Error('deepMerge: this function expects at least 2 objects to be provided');
  }

  if (objects.some((object) => !isObject(object))) {
    throw new Error('deepMerge: all values should be of type "object"');
  }

  const target = objects.shift();
  let source;

  // eslint-disable-next-line no-cond-assign
  while (source = objects.shift()) {
    deepMergeInner(target, source);
  }

  return target;
}
