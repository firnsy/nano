/**
 * @desc Check if value is an array
 *
 * @param {any} value
 * @return {boolean}
 *
 * @example
 *   isArray([])        // true
 *   isArray({})        // false
 *   isArray("")        // false
 *   isArray(1)         // false
 *   isArray(null)      // false
 *   isArray(undefined) // false
 */
export function isArray(value) {
  return Array.isArray(value);
}

/**
 * @desc Check if value is a boolean
 *
 * @param {any} value
 * @return {boolean}
 *
 * @example
 *   isBoolean(true)      // true
 *   isBoolean(false)     // true
 *   isBoolean("")        // false
 *   isBoolean(1)         // false
 *   isBoolean(null)      // false
 *   isBoolean(undefined) // false
 *   isBoolean({})        // false
 *   isBoolean([])        // false
 */
export function isBoolean(value) {
  return typeof (value) === "boolean";
}

export function isDefined(value) {
  return !isUndefinedOrNull(value);
}

export function isEmpty(value) {
  return isUndefinedOrNull(value) || value === "";
}

/**
 * @desc Determines if two objects or two values are equivalent. Supports value types, regular
 * expressions, arrays and objects.
 *
 * Two objects or values are considered equivalent if at least one of the following is true:
 *
 * * Both objects or values pass `===` comparison.
 * * Both objects or values are of the same type and all of their properties are equal by
 *   comparing them with `angular.equals`.
 * * Both values are NaN. (In JavaScript, NaN == NaN => false. But we consider two NaN as equal)
 * * Both values represent the same regular expression (In JavaScript,
 *   /abc/ == /abc/ => false. But we consider two regular expressions as equal when their textual
 *   representation matches).
 *
 * During a property comparison, properties of `function` type are ignored.
 *
 * @param {*} o1 Object or value to compare.
 * @param {*} o2 Object or value to compare.
 * @returns {boolean} True if arguments are equal.
 */
export function isEqual(o1, o2) {
  if (o1 === o2) {
    return true;
  }

  if (o1 === null || o2 === null) {
    return false;
  }

  // eslint-disable-next-line no-self-compare
  if (o1 !== o1 && o2 !== o2) {
    return true; // NaN === NaN
  }

  const t1 = typeof o1;
  const t2 = typeof o2;

  if (t1 === t2 && isObject(o1)) {
    if (isArray(o1)) {
      if (!isArray(o2)) {
        return false;
      }

      const { length } = o1;
      if (length === o2.length) {
        for (let key = 0; key < length; key++) {
          if (!isEqual(o1[key], o2[key])) {
            return false;
          }
        }

        return true;
      }
    } else {
      const keySet = {};

      // eslint-disable-next-line no-restricted-syntax
      for (const key in o1) {
        if (!isFunction(o1[key])) {
          if (!isEqual(o1[key], o2[key])) {
            return false;
          }

          keySet[key] = true;
        }
      }

      // eslint-disable-next-line no-restricted-syntax
      for (const key in o2) {
        if (
          !(key in keySet)
          && isDefined(o2[key])
          && !isFunction(o2[key])
        ) {
          return false;
        }
      }

      return true;
    }
  }

  return false;
}

/**
 * @desc Check if value is a function
 *
 * @param {any} value
 * @return {boolean}
 *
 * @example
 *   isFunction(function() {}) // true
 *   isFunction(() => {})      // true
 *   isFunction("")            // false
 *   isFunction(1)             // false
 *   isFunction(null)          // false
 *   isFunction(undefined)     // false
 *   isFunction({})            // false
 *   isFunction([])            // false
 */
export function isFunction(value) {
  return typeof (value) === "function";
}

export function isNull(value) {
  return value === null;
}

export function isNumber(value) {
  return typeof (value) === "number";
}

/**
 * @desc: quick object check, this is primarily used to tell objects from primitive values
 * when we know the value is a json-compliant type
 * note: object could be a complex type like array, date, etc.
 *
 * @param {any} value
 * @return {boolean}
 *
 * @example
 *   isObject({})           // true
 *   isObject([])           // true
 *   isObject(new Date())   // true
 *   isObject(new String()) // true
 *   isObject(null)         // false
 *   isObject(undefined)    // false
 */

export function isObject(value) {
  return (
    value !== null
    && typeof (value) === "object"
  );
}

/**
 * @desc: strict object type check, only returns true for plain javascript objects
 *
 * @param {any} value
 * @return {boolean}
 *
 * @example
 *   isObjectPlain({})           // true
 *   isObjectPlain([])           // false
 *   isObjectPlain(new Date())   // false
 *   isObjectPlain(new String()) // false
 *   isObjectPlain(null)         // false
 *   isObjectPlain(undefined)    // false
 */

export function isObjectPlain(value) {
  return Object.prototype.toString.call(value) === "[object Object]";
}

/**
 * @desc Check if value is a promise
 *
 * @param {any} value
 * @return {boolean}
 *
 * @example
 *   isPromise(Promise.resolve()) // true
 *   isPromise({})                // false
 *   isPromise("")                // false
 *   isPromise(1)                 // false
 *   isPromise(null)              // false
 *   isPromise(undefined)         // false
 */
export function isPromise(value) {
  return (
    !isUndefinedOrNull(value)
    && isFunction(value.then)
    && isFunction(value.catch)
  );
}

/**
 * @desc Check if value is a string
 * @param {any} value
 * @return {boolean}
 *
 * @example
 *   isString("")        // true
 *   isString("hello")   // true
 *   isString(1)         // false
 *   isString(true)      // false
 *   isString(false)     // false
 *   isString(null)      // false
 *   isString(undefined) // false
 */
export function isString(value) {
  return typeof (value) === "string";
}

/**
 * @desc Check if value is undefined
 *
 * @param {any} value
 * @return {boolean}
 *
 * @example
 *   isUndefined(undefined) // true
 *   isUndefined(null)      // false
 *   isUndefined("")        // false
 *   isUndefined(1)         // false
 *   isUndefined(true)      // false
 *   isUndefined(false)     // false
 *   isUndefined({})        // false
 *   isUndefined([])        // false
 */

export function isUndefined(value) {
  return value === undefined;
}

export function isUndefinedOrNull(value) {
  return (
    isUndefined(value)
    || isNull(value)
  );
}
