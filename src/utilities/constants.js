/**
 * This file is part of nano.
 *
 * Author(s):
 *  - Ian Firns <firnsy@gmail.com>
 *
 * License: MIT
 */

export const FN_NOOP = () => {};
