/**
 * This file is part of nano.
 *
 * Author(s):
 *  - Ian Firns <firnsy@gmail.com>
 *
 * License: MIT
 */

import { isDefined, isEmpty, isNumber } from '@/utilities/is';

/**
 * Validation rules
 *
 * Each rule is a function that returns an object with a `valid` property that
 * is either `true` or `false` depending on whether the value passed in is valid
 * or not. The object can also contain a `message` property that is a string
 * message to display if the value is invalid.
 *
 * The function can also have a `params` property that is an array of strings
 * that are the names of the parameters that the rule requires.
 *
 * @type {Object}
 */

const alpha = {
  validate(value) {
    return /^[a-zA-Z]*$/.test(value);
  },
  message: 'Must be alphabetic, only letters allowed'
};

const alphanum = {
  validate(value) {
    return /^[a-zA-Z0-9]*$/.test(value);
  },
  message: 'Must be alphanumeric, only letters and numbers allowed'
};

const digits = {
  validate(value) {
    return /^\d*$/.test(value);
  },
  message: 'Must be numeric, only digits allowed'
};

const email = {
  validate(value) {
      // eslint-disable-next-line
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return re.test(value);
  },
  message: 'Must be a valid email address'
};

const min = {
  validate(value, args) {
    if (isNumber(value)) {
      return value >= args.value;
    }

    return isDefined(value) ? value.length >= args.value : false;
  },
  params: ['value'],
  message(field, value, params) {
    if (isNumber(value)) {
      return 'Must be greater than {value}';
    }

    return 'Must have at least {value} characters'
  }
};

const max = {
  validate(value, args) {
    if (isNumber(value)) {
      return value <= args.value;
    }

    return isDefined(value) ? value.length <= args.value : false;
  },
  params: ['value'],
  message(field, value, params) {
    if (isNumber(value)) {
      return 'Must be be less than {value}';
    }

    return 'Must have {value} characters at most'
  }
};

const minmax = {
  validate(value, args) {
    if (isNumber(value)) {
      return value >= args.min && value <= args.max;
    }

    if (!isDefined(value)) {
      return false;
    }

    const length = value.length;
    return length >= args.min && length <= args.max;
  },
  params: ['min', 'max'],
  message(field, value, params) {
    if (isNumber(value)) {
      return 'Must be between {min} and {max}';
    }

    return 'Must have at least {min} characters and {max} characters at most'
  }
};

const required = {
  validate(value) {
    return {
      valid: value !== undefined && value !== null && value !== '',
      required: true,
    };
  },
  message: 'Field is required',
  computesRequired: true,
};

const required_if = {
  validate(value, args) {
    const isValueEmpty = isEmpty(value);
    const isTargetEmpty = isEmpty(args.target);

    return {
      valid: isValueEmpty,
      required: !isTargetEmpty,
    };
  },
  params: ['target'],
  message: 'Field is required if target field is not empty',
  computesRequired: true,
};

export const rules = {
  alpha,
  alphanum,
  digits,
  email,
  max,
  min,
  minmax,
  required,
  required_if
};
