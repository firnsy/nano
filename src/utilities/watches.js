/**
 * This file is part of nano.
 *
 * Author(s):
 *  - Ian Firns <firnsy@gmail.com>
 *
 * License: MIT
 */
import { readonly, ref, toValue, watch } from 'vue';

import { FN_NOOP} from '@/utilities/constants';

function createFilterWrapper(filter, fn) {
  function wrapper(...args) {
    return new Promise((resolve, reject) => {
      // make sure it's a promise
      Promise.resolve(
        filter(
          () => fn.apply(null, args),
          { fn, args }
        )
      )
        .then(resolve)
        .catch(reject);
    });
  }

  return wrapper;
}

const bypassFilter = (invoke) => invoke();

/**
 * @desc create a debounce filter
 *
 * @param {number} ms - the debounce duration in milliseconds
 * @param {object} options - the debounce options
 *
 * @returns {EventFilter}
 *
 */
function debounceFilter(ms, options = {}) {
  let timer;
  let maxTimer;
  let lastRejector = FN_NOOP;

  const _clearTimeout = (timer) => {
    clearTimeout(timer);
    lastRejector();
    lastRejector = FN_NOOP;
  }

  const filter = (invoke) => {
    const duration = toValue(ms);
    const maxDuration = toValue(options.maxWait);

    if (timer) {
      _clearTimeout(timer);
    }

    if (duration <= 0 || (maxDuration !== undefined && maxDuration <= 0)) {
      if (maxTimer) {
        _clearTimeout(maxTimer);
        maxTimer = null;
      }

      return Promise.resolve(invoke())
    }

    return new Promise((resolve, reject) => {
      lastRejector = options.rejectOnCancel ? reject : resolve;

      // create the maxTimer. clears the regular timer on invoke
      if (maxDuration && !maxTimer) {
        maxTimer = setTimeout(() => {
          if (timer) {
            _clearTimeout(timer);
          }

          maxTimer = null;
          resolve(invoke());
        }, maxDuration);
      }

      // Create the regular timer. Clears the max timer on invoke
      timer = setTimeout(() => {
        if (maxTimer) {
          _clearTimeout(maxTimer);
        }

        maxTimer = null;
        resolve(invoke());
      }, duration);
    })
  };

  return filter;
}

/**
 * @desc create a filter that can be paused and resumed
 *
 * @param {EventFilter} extendFilter - the filter to be extended
 *
 */
function pausableFilter(extendFilter = bypassFilter) {
  const isActive = ref(true);

  function pause() {
    isActive.value = false;
  }
  function resume() {
    isActive.value = true;
  }

  const eventFilter = (...args) => {
    if (isActive.value) {
      extendFilter(...args);
    }
  };

  return {
    isActive: readonly(isActive), pause, resume, eventFilter
  };
}

/**
 * @desc create a watch that is debounced
 *
 * @param {any} source
 * @param {any} cb
 * @param {WatchWithFilterOptions<Immediate>} options
 *
 * @returns {WatchStopHandle}
 */
function watchDebounced( source, cb, options = {}) {
  const {
    debounce = 0,
    maxWait = undefined,
    ...watchOptions
  } = options

  return watchWithFilter(
    source,
    cb,
    {
      ...watchOptions,
      eventFilter: debounceFilter(debounce, { maxWait }),
    },
  )
}

/**
 * @desc create a watch that can be paused and resumed
 *
 * @param {any} source
 * @param {any} cb
 * @param {WatchWithFilterOptions<Immediate>} options
 *
 * @returns {WatchPausableReturn}
 */
function watchPausable(source, cb, options = {}) {
  const {
    eventFilter: filter,
    ...watchOptions
  } = options;

  const {
    eventFilter, pause, resume, isActive
  } = pausableFilter(filter);

  const stop = watchWithFilter(
    source,
    cb,
    {
      ...watchOptions,
      eventFilter
    }
  );

  return {
    stop, pause, resume, isActive
  };
}

/**
 * watchWithFilter
 *
 * @param {any} source
 * @param {any} cb
 * @param {WatchWithFilterOptions<Immediate>} options
 *
 * @returns {WatchStopHandle}
 */
function watchWithFilter(source, cb, options = {}) {
  const {
    eventFilter = bypassFilter,
    ...watchOptions
  } = options;

  return watch(
    source,
    createFilterWrapper(
      eventFilter,
      cb
    ),
    watchOptions
  );
}

// ALIASES
export {
  watchDebounced as debouncedWatch,
  watchPausable as pausableWatch
};
