import { Comment, createTextVNode, Fragment } from 'vue';

/**
 * @desc Flatten an array of nested VNodes in O(n).
 *
 * @param {import('vue').VNode[]} vNodes
 * @param {boolean} filterCommentNode
 * @param {import('vue').VNode[]} result
 * @returns {import('vue').VNode[]}
 */
export function flatten(vNodes, filterCommentNode = true, result = []) {
  vNodes.forEach((vNode) => {
    if (vNode === null) {
      return;
    }

    if (typeof vNode !== 'object') {
      if (typeof vNode === 'string' || typeof vNode === 'number') {
        result.push(createTextVNode(String(vNode)));
      }

      return;
    }

    if (Array.isArray(vNode)) {
      flatten(vNode, filterCommentNode, result);
      return;
    }

    if (vNode.type === Fragment) {
      if (vNode.children === null) {
        return;
      }

      if (Array.isArray(vNode.children)) {
        flatten(vNode.children, filterCommentNode, result);
      }
      // rawSlot
    } else if (vNode.type !== Comment) {
      result.push(vNode);
    }
  });

  return result;
}
