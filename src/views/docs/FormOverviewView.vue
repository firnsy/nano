<!--
  This file is part of nano.

  Author(s):
   - Ian Firns <firnsy@gmail.com>

  License: MIT
-->
<script setup>
import CodeBlock from '@/components/CodeBlock.vue';
</script>

<template>
  <hgroup>
    <p class="chapter">
      Forms
    </p>
    <h1>Forms Overview</h1>
    <p>
      All form elements are fully responsive with pure semantic HTML, enabling forms to scale gracefully across devices
      and viewports.
    </p>
  </hgroup>
  <aside id="table-of-contents">
    <nav>
      <details open>
        <summary>Content</summary>
        <ul>
          <li>
            <a
              class="secondary"
              href="/docs/forms"
            >Introduction</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/forms#helper-text"
            >Helper text</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/forms#usage-with-grid"
            >Usage with grid</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/forms#usage-with-group"
            >Usage with group</a>
          </li>
        </ul>
      </details>
    </nav>
  </aside>
  <div role="document">
    <section aria-label="Introduction">
      <p>
        Inputs are <code>width: 100%;</code> by default and are the same size as the buttons to build consitent forms.
      </p>
      <article
        aria-label="Form example"
        class="component"
      >
        <form>
          <fieldset>
            <label>First name<input
              name="first_name"
              placeholder="First name"
            ></label><label>Email<input
              type="email"
              name="email"
              placeholder="Email"
              autoComplete="email"
            ></label>
          </fieldset>
          <input
            type="submit"
            value="Subscribe"
          >
        </form>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;form&gt;
  &lt;fieldset&gt;
    &lt;label&gt;
      First name
      &lt;input
        name="first_name"
        placeholder="First name"
      /&gt;
    &lt;/label&gt;
    &lt;label&gt;
      Email
      &lt;input
        type="email"
        name="email"
        placeholder="Email"
        autocomplete="email"
      /&gt;
    &lt;/label&gt;
  &lt;/fieldset&gt;

  &lt;input
    type="submit"
    value="Subscribe"
  /&gt;
&lt;/form&gt;</pre>
          </code-block>
        </footer>
      </article>
      <p><code>&lt;input&gt;</code> can be inside or outside <code>&lt;label&gt;</code>.</p>
      <article
        aria-label="Label and input syntax"
        class="component"
      >
        <form>
          <label>First name<input
            name="first_name"
            placeholder="First name"
          ></label><label for="email">Email</label><input
            type="email"
            name="email"
            placeholder="Email"
            autocomplete="email"
          >
        </form>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;form&gt;
  &lt;!-- Input inside label --&gt;
  &lt;label&gt;
    First name
    &lt;input
      name="first_name"
      placeholder="First name"
    /&gt;
  &lt;/label&gt;

  &lt;!-- Input outside label --&gt;
  &lt;label for="email"&gt;Email&lt;/label&gt;
  &lt;input
    type="email"
    name="email"
    placeholder="Email"
    autocomplete="email"
  /&gt;

&lt;/form&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>

    <section>
      <h2>
        Helper text<a
          id="helper-text"
          href="#helper-text"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p><code>&lt;small&gt;</code> below form elements are muted and act as helper texts.</p>
      <article
        aria-label="Form helpers example"
        class="component"
      >
        <input
          type="email"
          name="email"
          placeholder="Email"
          autoComplete="email"
          aria-label="Email"
          aria-describedby="email-helper"
        ><small id="email-helper">We&#x27;ll never share your email with anyone else.</small>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;input
  type="email"
  name="email"
  placeholder="Email"
  autoComplete="email"
  aria-label="Email"
  aria-describedby="email-helper"
/&gt;
&lt;small id="email-helper"&gt;
  We'll never share your email with anyone else.
&lt;/small&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>
    <section>
      <h2>
        Usage with grid<a
          id="usage-with-grid"
          href="#usage-with-grid"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p>You can use <code><span class>.grid</span></code> inside a form. See <a href="/docs/grid">Grid</a>.</p>
      <article
        aria-label="Form and grid example"
        class="component"
      >
        <form>
          <fieldset class="grid">
            <input
              name="login"
              placeholder="Login"
              aria-label="Login"
              autoComplete="nickname"
            ><input
              type="password"
              name="password"
              placeholder="Password"
              aria-label="Password"
              autoComplete="current-password"
            ><input
              type="submit"
              value="Log in"
            >
          </fieldset>
        </form>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;form&gt;
  &lt;fieldset class="grid"&gt;
    &lt;input
      name="login"
      placeholder="Login"
      aria-label="Login"
      autocomplete="nickname"
    /&gt;
    &lt;input
      type="password"
      name="password"
      placeholder="Password"
      aria-label="Password"
      autocomplete="current-password"
    /&gt;
    &lt;input
      type="submit"
      value="Log in"
    /&gt;
  &lt;/fieldset&gt;
&lt;/form&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>
    <section>
      <h2>
        Usage with group<a
          id="usage-with-group"
          href="#usage-with-group"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p>
        You can use <code>role=&quot;group&quot;</code> with form elements. See <a href="/docs/group">Group</a>.
      </p>
      <article
        class="component"
        aria-label="Form group example"
      >
        <form role="group">
          <input
            type="email"
            placeholder="Enter your email"
          ><input
            type="submit"
            value="Subscribe"
          >
        </form>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;form role="group"&gt;
  &lt;input type="email" placeholder="Enter your email" /&gt;
  &lt;input type="submit" value="Subscribe" /&gt;
&lt;/form&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>
  </div>
</template>
