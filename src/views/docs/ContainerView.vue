<!--
  This file is part of nano.

  Author(s):
   - Ian Firns <firnsy@gmail.com>

  License: MIT
-->
<script setup>
import CodeBlock from '@/components/CodeBlock.vue';
</script>

<template>
  <hgroup>
    <p class="chapter">
      Layout
    </p>
    <h1>Container</h1>
    <p>Use <code>.container</code> for a centered viewport or <code>.container-fluid</code> for a full-width layout.</p>
  </hgroup>
  <aside id="table-of-contents">
    <nav>
      <details open>
        <summary>Content</summary>
        <ul>
          <li>
            <a
              class="secondary"
              href="/docs/container"
            >Breakpoints</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/container#fixed-width"
            >Fixed Width</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/container#fluid-width"
            >Fluid Width</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/container#auto-layout-columns"
            >Auto-layout Columns</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/container#equal-width"
            >Equal-width</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/container#setting-one-column-width"
            >Setting one column width</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/container#responsive-classes"
            >Responsive Classes</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/container#all-breakpoints"
            >All Breakpoints</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/container#stacked-to-horizontal"
            >Stacked to Horizontal</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/container#mix-and-match"
            >Mix and Match</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/container#row-columns"
            >Row Columns</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/container#nesting"
            >Nesting</a>
          </li>
        </ul>
      </details>
    </nav>
  </aside>
  <div role="document">
    <section>
      <p>
        Nano's grid system is largely compatible with
        <a href="https://getbootstrap.com/docs/4.1/layout/grid/">Bootstrap</a>. It uses a series of containers, rows,
        and columns to layout and align content. It’s built with flexbox and is fully responsive. Below is an example
        and an in-depth look at how the grid comes together.
      </p>
      <article aria-label="Grid example">
        <div class="container-example">
          <div class="container">
            <div class="row">
              <div class="col-sm">
                1 of 3 columns
              </div>
              <div class="col-sm">
                1 of 3 columns
              </div>
              <div class="col-sm">
                1 of 3 columns
              </div>
            </div>
          </div>
        </div>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;div class="container"&gt;
  &lt;div class="row"&gt;
    &lt;div class="col-sm"&gt;
      1 of 3 columns
    &lt;/div&gt;
    &lt;div class="col-sm"&gt;
      1 of 3 columns
    &lt;/div&gt;
    &lt;div class="col-sm"&gt;
      1 of 3 columns
    &lt;/div&gt;
  &lt;/div&gt;
&lt;/div&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>
    <section>
      <p>
        Nano includes six default breakpoints. These breakpoints can be customised with <a href="/docs/sass">Sass</a>.
      </p>
      <table class="striped">
        <thead>
          <tr>
            <th>Device</th>
            <th>Breakpoint</th>
            <th>Viewport</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Extra small</td>
            <td>&lt;576px</td>
            <td>100%</td>
          </tr>
          <tr>
            <td>Small</td>
            <td>≥576px</td>
            <td>540px</td>
          </tr>
          <tr>
            <td>Medium</td>
            <td>≥768px</td>
            <td>720px</td>
          </tr>
          <tr>
            <td>Large</td>
            <td>≥992px</td>
            <td>960px</td>
          </tr>
          <tr>
            <td>Extra large</td>
            <td>≥1280px</td>
            <td>1200px</td>
          </tr>
          <tr>
            <td>Extra extra large</td>
            <td>≥1536px</td>
            <td>1440px</td>
          </tr>
        </tbody>
      </table>
    </section>
    <section>
      <h2>
        Fixed Width<a
          id="fixed-width"
          href="#fixed-width"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p><code>.container</code> provides a centered container with a fixed width.</p>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block>
          <pre>&lt;body&gt;
  &lt;main class="container"&gt;
    ...
  &lt;/main&gt;
&lt;/body&gt;</pre>
        </code-block>
      </div>
    </section>
    <section>
      <h2>
        Fluid Width<a
          id="fluid-width"
          href="#fluid-width"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p><code>.container-fluid</code> provides a full-width container.</p>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block>
          <pre>&lt;body&gt;
  &lt;main class="container-fluid"&gt;
    ...
  &lt;/main&gt;
&lt;/body&gt;</pre>
        </code-block>
      </div>
    </section>

    <section>
      <h2>
        Auto-layout Columns<a
          id="auto-layout-columns"
          href="#auto-layout-columns"
          class="secondar"
          tabindex="-1"
        >#</a>
      </h2>
      <p>
        Utilise breakpoint-specific column classes for easy column sizing without an explicit numbered class like
        <code>.col-sm-6</code>.
      </p>
      <h3>
        Equal-width<a
          id="equal-width"
          href="#equal-width"
          class="secondar"
          tabindex="-1"
        >#</a>
      </h3>
      <p>
        For example, here are two grid layouts that apply to every device and viewport, from <code>xs</code> to
        <code>xxl</code>. Add any number of unit-less classes for each breakpoint you need and every column will be the
        same width.
      </p>
      <article>
        <div class="container-example container-example-row">
          <div class="container">
            <div class="row">
              <div class="col">
                1 of 2
              </div>
              <div class="col">
                2 of 2
              </div>
            </div>
            <div class="row">
              <div class="col">
                1 of 3
              </div>
              <div class="col">
                2 of 3
              </div>
              <div class="col">
                3 of 3
              </div>
            </div>
          </div>
        </div>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;div class="container"&gt;
  &lt;div class="row"&gt;
    &lt;div class="col"&gt;
      1 of 2
    &lt;/div&gt;
    &lt;div class="col"&gt;
      2 of 2
    &lt;/div&gt;
  &lt;/div&gt;
  &lt;div class="row"&gt;
    &lt;div class="col"&gt;
      1 of 3
    &lt;/div&gt;
    &lt;div class="col"&gt;
      2 of 3
    &lt;/div&gt;
    &lt;div class="col"&gt;
      3 of 3
    &lt;/div&gt;
  &lt;/div&gt;
&lt;/div&gt;</pre>
          </code-block>
        </footer>
      </article>

      <h3>
        Setting one column width<a
          id="setting-one-column-width"
          href="#setting-one-column-width"
          class="secondar"
          tabindex="-1"
        >#</a>
      </h3>
      <p>
        Auto-layout for flexbox grid columns also means you can set the width of one column and have the sibling columns
        automatically resize around it. You may use predefined grid classes (as shown below), grid mixins, or inline
        widths. Note that the other columns will resize no matter the width of the center column.
      </p>
      <article>
        <div class="container-example container-example-row">
          <div class="container">
            <div class="row">
              <div class="col">
                1 of 3
              </div>
              <div class="col-6">
                2 of 3 (wider)
              </div>
              <div class="col">
                3 of 3
              </div>
            </div>
            <div class="row">
              <div class="col">
                1 of 3
              </div>
              <div class="col-5">
                2 of 3 (wider)
              </div>
              <div class="col">
                3 of 3
              </div>
            </div>
          </div>
        </div>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;div class="container"&gt;
  &lt;div class="row"&gt;
    &lt;div class="col"&gt;
      1 of 3
    &lt;/div&gt;
    &lt;div class="col-6"&gt;
      2 of 3 (wider)
    &lt;/div&gt;
    &lt;div class="col"&gt;
      3 of 3
    &lt;/div&gt;
  &lt;/div&gt;
  &lt;div class="row"&gt;
    &lt;div class="col"&gt;
      1 of 3
    &lt;/div&gt;
    &lt;div class="col-5"&gt;
      2 of 3 (wider)
    &lt;/div&gt;
    &lt;div class="col"&gt;
      3 of 3
    &lt;/div&gt;
  &lt;/div&gt;
&lt;/div&gt;</pre>
          </code-block>
        </footer>
      </article>
      <h3>
        Variable width content<a
          id="variable-width-content"
          href="#variable-width-content"
          class="secondar"
          tabindex="-1"
        >#</a>
      </h3>
      <p>Use <code>col-{breakpoint}-auto</code> classes to size columns based on the natural width of their content.</p>
      <article>
        <div class="container-example container-example-row">
          <div class="container">
            <div class="row justify-content-md-center">
              <div class="col col-lg-2">
                1 of 3
              </div>
              <div class="col-md-auto">
                Variable width content
              </div>
              <div class="col col-lg-2">
                3 of 3
              </div>
            </div>
            <div class="row">
              <div class="col">
                1 of 3
              </div>
              <div class="col-md-auto">
                Variable width content
              </div>
              <div class="col col-lg-2">
                3 of 3
              </div>
            </div>
          </div>
        </div>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;div class="container"&gt;
  &lt;div class="row justify-content-md-center"&gt;
    &lt;div class="col col-lg-2"&gt;
      1 of 3
    &lt;/div&gt;
    &lt;div class="col-md-auto"&gt;
      Variable width content
    &lt;/div&gt;
    &lt;div class="col col-lg-2"&gt;
      3 of 3
    &lt;/div&gt;
  &lt;/div&gt;
  &lt;div class="row"&gt;
    &lt;div class="col"&gt;
      1 of 3
    &lt;/div&gt;
    &lt;div class="col-md-auto"&gt;
      Variable width content
    &lt;/div&gt;
    &lt;div class="col col-lg-2"&gt;
      3 of 3
    &lt;/div&gt;
  &lt;/div&gt;
&lt;/div&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>
    <section>
      <h2>
        Responsive Classes<a
          id="responsive-classes"
          href="#responsive-classes"
          class="secondar"
          tabindex="-1"
        >#</a>
      </h2>
      <p>
        Bootstrap's grid includes six tiers of predefined classes for building complex responsive layouts. Customise the
        size of your columns on extra small, small, medium, large, or extra large devices however you see fit.
      </p>
      <h3>
        All Breakpoints<a
          id="all-breakpoints"
          href="#all-breakpoints"
          class="secondar"
          tabindex="-1"
        >#</a>
      </h3>
      <p>
        For grids that are the same from the smallest of devices to the largest, use the <code>.col</code> and
        <code>.col-*</code> classes. Specify a numbered class when you need a particularly sized column; otherwise, feel
        free to stick to <code>.col</code>.
      </p>
      <article>
        <div class="container-example container-example-row">
          <div class="container">
            <div class="row">
              <div class="col">
                col
              </div>
              <div class="col">
                col
              </div>
              <div class="col">
                col
              </div>
              <div class="col">
                col
              </div>
            </div>
            <div class="row">
              <div class="col-8">
                col-8
              </div>
              <div class="col-4">
                col-4
              </div>
            </div>
          </div>
        </div>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;div class=&#34;container&#34;&gt;
  &lt;div class=&#34;row&#34;&gt;
    &lt;div class=&#34;col&#34;&gt;col&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;col&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;col&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;col&lt;/div&gt;
  &lt;/div&gt;
  &lt;div class=&#34;row&#34;&gt;
    &lt;div class=&#34;col-8&#34;&gt;col-8&lt;/div&gt;
    &lt;div class=&#34;col-4&#34;&gt;col-4&lt;/div&gt;
  &lt;/div&gt;
&lt;/div&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>
    <section>
      <h3>
        Stacked to Horizontal<a
          id="stacked-to-horizontal"
          href="#stacked-to-horizontal"
          class="secondar"
          tabindex="-1"
        >#</a>
      </h3>
      <p>
        Using a single set of <code>.col-sm-*</code> classes, you can create a basic grid system that starts out stacked
        and becomes horizontal at the small breakpoint (<code>sm</code>).
      </p>
      <article>
        <div class="container-example container-example-row">
          <div class="container">
            <div class="row">
              <div class="col-sm-8">
                col-sm-8
              </div>
              <div class="col-sm-4">
                col-sm-4
              </div>
            </div>
            <div class="row">
              <div class="col-sm">
                col-sm
              </div>
              <div class="col-sm">
                col-sm
              </div>
              <div class="col-sm">
                col-sm
              </div>
            </div>
          </div>
        </div>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;div class=&#34;container&#34;&gt;
  &lt;div class=&#34;row&#34;&gt;
    &lt;div class=&#34;col-sm-8&#34;&gt;col-sm-8&lt;/div&gt;
    &lt;div class=&#34;col-sm-4&#34;&gt;col-sm-4&lt;/div&gt;
  &lt;/div&gt;
  &lt;div class=&#34;row&#34;&gt;
    &lt;div class=&#34;col-sm&#34;&gt;col-sm&lt;/div&gt;
    &lt;div class=&#34;col-sm&#34;&gt;col-sm&lt;/div&gt;
    &lt;div class=&#34;col-sm&#34;&gt;col-sm&lt;/div&gt;
  &lt;/div&gt;
&lt;/div&gt;</pre>
          </code-block>
        </footer>
      </article>
      <h3>
        Mix and Match<a
          id="mix-and-match"
          href="#mix-and-match"
          class="secondar"
          tabindex="-1"
        >#</a>
      </h3>
      <p>
        Don't want your columns to simply stack in some grid tiers? Use a combination of different classes for each tier
        as needed. See the example below for a better idea of how it all works.
      </p>
      <article>
        <div class="container-example container-example-row">
          <div class="container">
            <!-- Stack the columns on mobile by making one full-width and the other half-width -->
            <div class="row">
              <div class="col-md-8">
                .col-md-8
              </div>
              <div class="col-6 col-md-4">
                .col-6 .col-md-4
              </div>
            </div>
            <!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
            <div class="row">
              <div class="col-6 col-md-4">
                .col-6 .col-md-4
              </div>
              <div class="col-6 col-md-4">
                .col-6 .col-md-4
              </div>
              <div class="col-6 col-md-4">
                .col-6 .col-md-4
              </div>
            </div>
            <!-- Columns are always 50% wide, on mobile and desktop -->
            <div class="row">
              <div class="col-6">
                .col-6
              </div>
              <div class="col-6">
                .col-6
              </div>
            </div>
          </div>
        </div>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;div class=&#34;container&#34;&gt;
  &lt;!-- Stack the columns on mobile by making one full-width and the other half-width --&gt;
  &lt;div class=&#34;row&#34;&gt;
    &lt;div class=&#34;col-md-8&#34;&gt;.col-md-8&lt;/div&gt;
    &lt;div class=&#34;col-6 col-md-4&#34;&gt;.col-6 .col-md-4&lt;/div&gt;
  &lt;/div&gt;

  &lt;!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop --&gt;
  &lt;div class=&#34;row&#34;&gt;
    &lt;div class=&#34;col-6 col-md-4&#34;&gt;.col-6 .col-md-4&lt;/div&gt;
    &lt;div class=&#34;col-6 col-md-4&#34;&gt;.col-6 .col-md-4&lt;/div&gt;
    &lt;div class=&#34;col-6 col-md-4&#34;&gt;.col-6 .col-md-4&lt;/div&gt;
  &lt;/div&gt;

  &lt;!-- Columns are always 50% wide, on mobile and desktop --&gt;
  &lt;div class=&#34;row&#34;&gt;
    &lt;div class=&#34;col-6&#34;&gt;.col-6&lt;/div&gt;
    &lt;div class=&#34;col-6&#34;&gt;.col-6&lt;/div&gt;
  &lt;/div&gt;
&lt;/div&gt;</pre>
          </code-block>
        </footer>
      </article>
      <h3>
        Row Columns<a
          id="row-columns"
          href="#row-columns"
          class="secondar"
          tabindex="-1"
        >#</a>
      </h3>
      <p>
        Use the responsive <code>.row-cols-*</code> classes to quickly set the number of columns that best render your
        content and layout. Whereas normal <code>.col-*</code> classes apply to the individual columns (e.g.,
        <code>.col-md-4</code>), the row columns classes are set on the parent <code>.row</code> as a default for
        contained columns. With <code>.row-cols-auto</code> you can give the columns their natural width.
      </p>
      <p>
        Use these row columns classes to quickly create basic grid layouts or to control your card layouts and override
        when needed at the column level.
      </p>
      <article>
        <div class="container-example container-example-row">
          <div class="container">
            <div class="row row-cols-2">
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
            </div>
          </div>
        </div>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;div class=&#34;container&#34;&gt;
  &lt;div class=&#34;row row-cols-2&#34;&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
  &lt;/div&gt;
&lt;/div&gt;</pre>
          </code-block>
        </footer>
      </article>
      <article>
        <div class="container-example container-example-row">
          <div class="container">
            <div class="row row-cols-3">
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
            </div>
          </div>
        </div>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;div class=&#34;container&#34;&gt;
  &lt;div class=&#34;row row-cols-3&#34;&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
  &lt;/div&gt;
&lt;/div&gt;</pre>
          </code-block>
        </footer>
      </article>
      <article>
        <div class="container-example container-example-row">
          <div class="container">
            <div class="row row-cols-auto">
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
            </div>
          </div>
        </div>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;div class=&#34;container&#34;&gt;
  &lt;div class=&#34;row row-cols-auto&#34;&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
  &lt;/div&gt;
&lt;/div&gt;</pre>
          </code-block>
        </footer>
      </article>
      <article>
        <div class="container-example container-example-row">
          <div class="container">
            <div class="row row-cols-4">
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
            </div>
          </div>
        </div>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;div class=&#34;container&#34;&gt;
  &lt;div class=&#34;row row-cols-4&#34;&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
  &lt;/div&gt;
&lt;/div&gt;</pre>
          </code-block>
        </footer>
      </article>
      <article>
        <div class="container-example container-example-row">
          <div class="container">
            <div class="row row-cols-4">
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
              <div class="col-6">
                Column
              </div>
              <div class="col">
                Column
              </div>
            </div>
          </div>
        </div>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;div class=&#34;container&#34;&gt;
  &lt;div class=&#34;row row-cols-4&#34;&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col-6&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
  &lt;/div&gt;
&lt;/div&gt;</pre>
          </code-block>
        </footer>
      </article>
      <article>
        <div class="container-example container-example-row">
          <div class="container">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4">
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
            </div>
          </div>
        </div>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;div class=&#34;container&#34;&gt;
  &lt;div class=&#34;row row-cols-1 row-cols-sm-2 row-cols-md-4&#34;&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
  &lt;/div&gt;
&lt;/div&gt;</pre>
          </code-block>
        </footer>
      </article>
      <article>
        <div class="container-example container-example-row">
          <div class="container">
            <div class="row row-cols-2 row-cols-lg-3">
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
              <div class="col">
                Column
              </div>
              <div class="col-4 col-lg-2">
                Column
              </div>
              <div class="col-4 col-lg-2">
                Column
              </div>
              <div class="col-4 col-lg-2">
                Column
              </div>
              <div class="col-4 col-lg-2">
                Column
              </div>
              <div class="col-4 col-lg-2">
                Column
              </div>
              <div class="col-4 col-lg-2">
                Column
              </div>
            </div>
          </div>
        </div>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;div class=&#34;container&#34;&gt;
  &lt;div class=&#34;row row-cols-2 row-cols-lg-3&#34;&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col-4 col-lg-2&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col-4 col-lg-2&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col-4 col-lg-2&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col-4 col-lg-2&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col-4 col-lg-2&#34;&gt;Column&lt;/div&gt;
    &lt;div class=&#34;col-4 col-lg-2&#34;&gt;Column&lt;/div&gt;
  &lt;/div&gt;
&lt;/div&gt;</pre>
          </code-block>
        </footer>
      </article>
      <p>You can also use the accompanying Sass mixin, <code>row-cols()</code>:</p>
      <footer
        class="code"
        data-theme="dark"
      >
        <code-block>
          <pre>.element {
  // Three columns to start
  @include row-cols(3);

  // Five columns from medium breakpoint up
  @include media-breakpoint-up(md) {
    @include row-cols(5);
  }
}</pre>
        </code-block>
      </footer>
    </section>
    <section>
      <h2>
        Nesting<a
          id="nesting"
          href="#nesting"
          class="secondar"
          tabindex="-1"
        >#</a>
      </h2>
      <p>
        To nest your content with the default grid, add a new <code>.row</code> and set of <code>.col-sm-*</code>
        columns within an existing <code>.col-sm-*</code> column. Nested rows should include a set of columns that add
        up to 12 or fewer (it is not required that you use all 12 available columns).
      </p>
      <article>
        <div class="container-example container-example-row">
          <div class="container">
            <div class="row">
              <div class="col-sm-3">
                Level 1: .col-sm-3
              </div>
              <div class="col-sm-9">
                <div class="row">
                  <div class="col-8 col-sm-6">
                    Level 2: .col-8 .col-sm-6
                  </div>
                  <div class="col-4 col-sm-6">
                    Level 2: .col-4 .col-sm-6
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;div class=&#34;container&#34;&gt;
  &lt;div class=&#34;row&#34;&gt;
    &lt;div class=&#34;col-sm-3&#34;&gt;
      Level 1: .col-sm-3
    &lt;/div&gt;
    &lt;div class=&#34;col-sm-9&#34;&gt;
      &lt;div class=&#34;row&#34;&gt;
        &lt;div class=&#34;col-8 col-sm-6&#34;&gt;
          Level 2: .col-8 .col-sm-6
        &lt;/div&gt;
        &lt;div class=&#34;col-4 col-sm-6&#34;&gt;
          Level 2: .col-4 .col-sm-6
        &lt;/div&gt;
      &lt;/div&gt;
    &lt;/div&gt;
  &lt;/div&gt;
&lt;/div&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>
  </div>
</template>

<style lang="scss">
  .container-example .row {
    .col,
    [class^="col-"] {
      background-color: #eee;
      border: 1px solid #ccc;
      text-align: center;
      padding-bottom: 1rem;
      padding-top: 1rem;
    }
  }
</style>
