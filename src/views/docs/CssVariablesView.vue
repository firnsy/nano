<!--
  This file is part of nano.

  Author(s):
   - Ian Firns <firnsy@gmail.com>

  License: MIT
-->
<script setup>
import CodeBlock from '@/components/CodeBlock.vue';
</script>

<template>
  <hgroup>
    <p class="chapter">
      Customisation
    </p>
    <h1>CSS Variables</h1>
    <p>Customise Nano's design system with over 130 CSS variables to create a unique look and feel.</p>
  </hgroup>
  <aside id="table-of-contents">
    <nav>
      <details open>
        <summary>Content</summary>
        <ul>
          <li>
            <a
              class="secondary"
              href="/docs/css-variables"
            >Introduction</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/css-variables#example"
            >Example</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/css-variables#css-variables-for-color-schemes"
            >Color schemes</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/css-variables#all-css-variables"
            >All CSS variables</a>
          </li>
        </ul>
      </details>
    </nav>
  </aside>
  <div role="document">
    <section>
      <p>
        Nano includes many custom properties (variables) that allow easy access to frequently used values such as
        <code>font-family</code>, <code>font-size</code>,<code>border-radius</code>, <code>margin</code>,
        <code>padding</code>, and more.
      </p>
      <p>
        All CSS variables are prefixed with <code>nano-</code> to avoid collisions with other CSS frameworks or your own
        vars. You can remove or customise this prefix by recompiling the CSS files with <a href="/docs/sass">Sass</a>.
      </p>
      <p>
        You can define the CSS variables within the <code>:root</code> selector to apply the changes globally or
        overwrite the CSS variables on specific selectors to apply the changes locally.
      </p>
    </section>
    <section>
      <h2>
        Example<a
          id="example"
          href="#example"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <article
        id="css-var-example"
        aria-label="Button colors example"
        class="component"
      >
        <h1>Music fest mania</h1>
        <p>
          Get ready to dance and sing your heart out at our Music Fest Mania. Join the crowd, jam to your favorite band,
          and discover new artists.
        </p>
        <button>Let's rock out!</button>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;style&gt;
  :root {
    --nano-border-radius: 2rem;
    --nano-typography-spacing-vertical: 1.5rem;
    --nano-form-element-spacing-vertical: 1rem;
    --nano-form-element-spacing-horizontal: 1.25rem;
  }
  h1 {
    --nano-font-family: Pacifico, cursive;
    --nano-font-weight: 400;
    --nano-typography-spacing-vertical: 0.5rem;
  }
  button {
    --nano-font-weight: 700;
  }
&lt;/style&gt;

&lt;h1&gt;Music fest mania&lt;/h1&gt;
&lt;p&gt;
  Get ready to dance and sing your heart out at
  our Music Fest Mania. Join the crowd, jam to
  your favorite band, and discover new artists.
&lt;/p&gt;
&lt;button&gt;Let's rock out!&lt;/button&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>
    <section>
      <h2>
        CSS variables for color schemes<a
          id="css-variables-for-color-schemes"
          href="#css-variables-for-color-schemes"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p>To add or edit CSS variables for light mode only (the default mode), define them inside:</p>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block>
          <pre>/* light color scheme (default) */
/* can be forced with data-theme="light" */
[data-theme="light"],
:root:not([data-theme="dark"]) {
 ...
} </pre>
        </code-block>
      </div>
      <p>To add or edit CSS variables for dark mode, you need to define them twice.</p>
      <p>
        The first inclusion is in the <code>@media</code>query that checks if the user has dark mode enabled through
        their device settings with <code>prefers-color-scheme: dark</code>. In this case, the dark mode styling is
        applied to the <code>:root</code> element if there is no explicit <code>data-theme</code> attribute set.
      </p>
      <p>
        The second inclusion is when you force the dark mode with <code>data-theme="dark"</code>. This allows you to
        manually toggle between the light and dark themes regardless of the user's device settings.
      </p>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block>
          <pre>/* dark color scheme (auto) */
/* automatically enabled if user has dark mode enabled */
 @media only screen and (prefers-color-scheme: dark) {
  :root:not([data-theme]) {
    ...
  }
}

/* dark color scheme (forced) */
/* enabled if forced with data-theme="dark" */
[data-theme="dark"] {
  ...
}</pre>
        </code-block>
      </div>
      <p>Try our <a href="/docs/theme-generator">Minimal theme generator</a> for a detailed example.</p>
    </section>
    <section>
      <h2>
        All CSS variables<a
          id="all-css-variables"
          href="#all-css-variables"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p>There are two categories of CSS variables:</p>
      <ol>
        <li><strong>Style variables</strong>, which do not depend on the color scheme,</li>
        <li><strong>Color variables</strong>, which depend on the color scheme.</li>
      </ol>
      <p style="margin-bottom:2rem">
        Here is the list of all CSS variables used in Nano:
      </p>
      <details>
        <summary
          role="button"
          class="secondary"
        >
          Default styles CSS variables
        </summary>
        <div
          class="code"
          data-theme="dark"
        >
          <!-- eslint-disable max-len -->
          <code-block>
            <pre>:root {
  --nano-font-family-emoji: "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
  --nano-font-family-sans-serif: system-ui, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, var(--nano-font-family-emoji);
  --nano-font-family-monospace: ui-monospace, SFMono-Regular, "SF Mono", Menlo, Consolas, "Liberation Mono", monospace, var(--nano-font-family-emoji);
  --nano-font-family: var(--nano-font-family-sans-serif);
  --nano-line-height: 1.5;
  --nano-font-weight: 400;
  --nano-font-size: 100%;
  --nano-text-underline-offset: 0.1rem;
  --nano-border-radius: 0.25rem;
  --nano-border-width: 0.0625rem;
  --nano-outline-width: 0.1875rem;
  --nano-transition: 0.2s ease-in-out;
  --nano-spacing: 1rem;
  --nano-typography-spacing-vertical: 1rem;
  --nano-block-spacing-vertical: calc(var(--nano-spacing) * 2);
  --nano-block-spacing-horizontal: var(--nano-spacing);
  --nano-grid-column-gap: var(--nano-spacing);
  --nano-grid-row-gap: var(--nano-spacing);
  --nano-form-element-spacing-vertical: 0.75rem;
  --nano-form-element-spacing-horizontal: 1rem;
  --nano-group-box-shadow: 0 0 0 rgba(0, 0, 0, 0);
  --nano-group-box-shadow-focus-with-button: 0 0 0 var(--nano-outline-width) var(--nano-primary-focus);
  --nano-group-box-shadow-focus-with-input: 0 0 0 0.0625rem var(--nano-form-element-border-color);
  --nano-modal-overlay-backdrop-filter: blur(0.375rem);
  --nano-nav-element-spacing-vertical: 1rem;
  --nano-nav-element-spacing-horizontal: 0.5rem;
  --nano-nav-link-spacing-vertical: 0.5rem;
  --nano-nav-link-spacing-horizontal: 0.5rem;
  --nano-nav-breadcrumb-divider: "&gt;";
  --nano-icon-checkbox: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(255, 255, 255)' stroke-width='4' stroke-linecap='round' stroke-linejoin='round'%3E%3Cpolyline points='20 6 9 17 4 12'%3E%3C/polyline%3E%3C/svg%3E");
  --nano-icon-minus: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(255, 255, 255)' stroke-width='4' stroke-linecap='round' stroke-linejoin='round'%3E%3Cline x1='5' y1='12' x2='19' y2='12'%3E%3C/line%3E%3C/svg%3E");
  --nano-icon-chevron: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(136, 145, 164)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Cpolyline points='6 9 12 15 18 9'%3E%3C/polyline%3E%3C/svg%3E");
  --nano-icon-date: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(136, 145, 164)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Crect x='3' y='4' width='18' height='18' rx='2' ry='2'%3E%3C/rect%3E%3Cline x1='16' y1='2' x2='16' y2='6'%3E%3C/line%3E%3Cline x1='8' y1='2' x2='8' y2='6'%3E%3C/line%3E%3Cline x1='3' y1='10' x2='21' y2='10'%3E%3C/line%3E%3C/svg%3E");
  --nano-icon-time: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(136, 145, 164)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Ccircle cx='12' cy='12' r='10'%3E%3C/circle%3E%3Cpolyline points='12 6 12 12 16 14'%3E%3C/polyline%3E%3C/svg%3E");
  --nano-icon-search: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(136, 145, 164)' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round'%3E%3Ccircle cx='11' cy='11' r='8'%3E%3C/circle%3E%3Cline x1='21' y1='21' x2='16.65' y2='16.65'%3E%3C/line%3E%3C/svg%3E");
  --nano-icon-close: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(136, 145, 164)' stroke-width='3' stroke-linecap='round' stroke-linejoin='round'%3E%3Cline x1='18' y1='6' x2='6' y2='18'%3E%3C/line%3E%3Cline x1='6' y1='6' x2='18' y2='18'%3E%3C/line%3E%3C/svg%3E");
  --nano-icon-loading: url("data:image/svg+xml,%3Csvg fill='none' height='24' width='24' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg' %3E%3Cstyle%3E g %7B animation: rotate 2s linear infinite; transform-origin: center center; %7D circle %7B stroke-dasharray: 75,100; stroke-dashoffset: -5; animation: dash 1.5s ease-in-out infinite; stroke-linecap: round; %7D @keyframes rotate %7B 0%25 %7B transform: rotate(0deg); %7D 100%25 %7B transform: rotate(360deg); %7D %7D @keyframes dash %7B 0%25 %7B stroke-dasharray: 1,100; stroke-dashoffset: 0; %7D 50%25 %7B stroke-dasharray: 44.5,100; stroke-dashoffset: -17.5; %7D 100%25 %7B stroke-dasharray: 44.5,100; stroke-dashoffset: -62; %7D %7D %3C/style%3E%3Cg%3E%3Ccircle cx='12' cy='12' r='10' fill='none' stroke='rgb(136, 145, 164)' stroke-width='4' /%3E%3C/g%3E%3C/svg%3E");
}
@media (min-width: 576px) {
  :root {
    --nano-font-size: 106.25%;
  }
}
@media (min-width: 768px) {
  :root {
    --nano-font-size: 112.5%;
  }
}
@media (min-width: 1024px) {
  :root {
    --nano-font-size: 118.75%;
  }
}
@media (min-width: 1280px) {
  :root {
    --nano-font-size: 125%;
  }
}
@media (min-width: 1536px) {
  :root {
    --nano-font-size: 131.25%;
  }
}
:root details summary[role=button]:not(.outline)::after {
  filter: brightness(0) invert(1);
}
:root [aria-busy=true]:not(input, select, textarea):is(button, [type=submit], [type=button], [type=reset], [role=button]):not(.outline)::before {
  filter: brightness(0) invert(1);
}

@media (min-width: 576px) {
  body &gt; header,
  body &gt; main,
  body &gt; footer,
  section {
    --nano-block-spacing-vertical: calc(var(--nano-spacing) * 2.5);
  }
}
@media (min-width: 768px) {
  body &gt; header,
  body &gt; main,
  body &gt; footer,
  section {
    --nano-block-spacing-vertical: calc(var(--nano-spacing) * 3);
  }
}
@media (min-width: 1024px) {
  body &gt; header,
  body &gt; main,
  body &gt; footer,
  section {
    --nano-block-spacing-vertical: calc(var(--nano-spacing) * 3.5);
  }
}
@media (min-width: 1280px) {
  body &gt; header,
  body &gt; main,
  body &gt; footer,
  section {
    --nano-block-spacing-vertical: calc(var(--nano-spacing) * 4);
  }
}
@media (min-width: 1536px) {
  body &gt; header,
  body &gt; main,
  body &gt; footer,
  section {
    --nano-block-spacing-vertical: calc(var(--nano-spacing) * 4.5);
  }
}

@media (min-width: 576px) {
  article {
    --nano-block-spacing-horizontal: calc(var(--nano-spacing) * 1.25);
  }
}
@media (min-width: 768px) {
  article {
    --nano-block-spacing-horizontal: calc(var(--nano-spacing) * 1.5);
  }
}
@media (min-width: 1024px) {
  article {
    --nano-block-spacing-horizontal: calc(var(--nano-spacing) * 1.75);
  }
}
@media (min-width: 1280px) {
  article {
    --nano-block-spacing-horizontal: calc(var(--nano-spacing) * 2);
  }
}
@media (min-width: 1536px) {
  article {
    --nano-block-spacing-horizontal: calc(var(--nano-spacing) * 2.25);
  }
}

dialog &gt; article {
  --nano-block-spacing-vertical: calc(var(--nano-spacing) * 2);
  --nano-block-spacing-horizontal: var(--nano-spacing);
}
@media (min-width: 576px) {
  dialog &gt; article {
    --nano-block-spacing-vertical: calc(var(--nano-spacing) * 2.5);
    --nano-block-spacing-horizontal: calc(var(--nano-spacing) * 1.25);
  }
}
@media (min-width: 768px) {
  dialog &gt; article {
    --nano-block-spacing-vertical: calc(var(--nano-spacing) * 3);
    --nano-block-spacing-horizontal: calc(var(--nano-spacing) * 1.5);
  }
}

a {
  --nano-text-decoration: underline;
}
a.secondary, a.contrast {
  --nano-text-decoration: underline;
}

small {
  --nano-font-size: 0.875em;
}

h1,
h2,
h3,
h4,
h5,
h6 {
  --nano-font-weight: 700;
}

h1 {
  --nano-font-size: 2rem;
  --nano-line-height: 1.25;
  --nano-typography-spacing-top: 3rem;
}

h2 {
  --nano-font-size: 1.75rem;
  --nano-line-height: 1.3;
  --nano-typography-spacing-top: 2.625rem;
}

h3 {
  --nano-font-size: 1.5rem;
  --nano-line-height: 1.35;
  --nano-typography-spacing-top: 2.25rem;
}

h4 {
  --nano-font-size: 1.25rem;
  --nano-line-height: 1.4;
  --nano-typography-spacing-top: 1.874rem;
}

h5 {
  --nano-font-size: 1.125rem;
  --nano-line-height: 1.45;
  --nano-typography-spacing-top: 1.6875rem;
}

h6 {
  --nano-font-size: 1rem;
  --nano-typography-spacing-top: 1.5rem;
}

thead th,
thead td,
tfoot th,
tfoot td {
  --nano-font-weight: 600;
  --nano-border-width: 0.1875rem;
}

pre,
code,
kbd,
samp {
  --nano-font-family: var(--nano-font-family-monospace);
}

kbd {
  --nano-font-weight: bolder;
}

input:not([type=submit],
[type=button],
[type=reset],
[type=checkbox],
[type=radio],
[type=file]),
:where(select, textarea) {
  --nano-outline-width: 0.0625rem;
}

[type=search] {
  --nano-border-radius: 5rem;
}

[type=checkbox],
[type=radio] {
  --nano-border-width: 0.125rem;
}

[type=checkbox][role=switch] {
  --nano-border-width: 0.1875rem;
}

details.dropdown summary:not([role=button]) {
  --nano-outline-width: 0.0625rem;
}

nav details.dropdown summary:focus-visible {
  --nano-outline-width: 0.1875rem;
}

[role=search] {
  --nano-border-radius: 5rem;
}

[role=search]:has(button.secondary:focus,
[type=submit].secondary:focus,
[type=button].secondary:focus,
[role=button].secondary:focus),
[role=group]:has(button.secondary:focus,
[type=submit].secondary:focus,
[type=button].secondary:focus,
[role=button].secondary:focus) {
  --nano-group-box-shadow-focus-with-button: 0 0 0 var(--nano-outline-width) var(--nano-secondary-focus);
}
[role=search]:has(button.contrast:focus,
[type=submit].contrast:focus,
[type=button].contrast:focus,
[role=button].contrast:focus),
[role=group]:has(button.contrast:focus,
[type=submit].contrast:focus,
[type=button].contrast:focus,
[role=button].contrast:focus) {
  --nano-group-box-shadow-focus-with-button: 0 0 0 var(--nano-outline-width) var(--nano-contrast-focus);
}
[role=search] button,
[role=search] [type=submit],
[role=search] [type=button],
[role=search] [role=button],
[role=group] button,
[role=group] [type=submit],
[role=group] [type=button],
[role=group] [role=button] {
  --nano-form-element-spacing-horizontal: 2rem;
}</pre>
          </code-block>
          <!-- eslint-enable vue/max-len -->
        </div>
      </details>
      <details>
        <summary
          role="button"
          class="secondary"
        >
          Default colors CSS variables
        </summary>
        <div
          class="code"
          data-theme="dark"
        >
          <!-- eslint-disable vue/max-len -->
          <code-block>
            <pre>[data-theme=light],
:root:not([data-theme=dark]) {
  --nano-background-color: #fff;
  --nano-color: #373c44;
  --nano-text-selection-color: rgba(2, 154, 232, 0.25);
  --nano-muted-color: #646b79;
  --nano-muted-border-color: #e7eaf0;
  --nano-primary: #0172ad;
  --nano-primary-background: #0172ad;
  --nano-primary-border: var(--nano-primary-background);
  --nano-primary-underline: rgba(1, 114, 173, 0.5);
  --nano-primary-hover: #015887;
  --nano-primary-hover-background: #02659a;
  --nano-primary-hover-border: var(--nano-primary-hover-background);
  --nano-primary-hover-underline: var(--nano-primary-hover);
  --nano-primary-focus: rgba(2, 154, 232, 0.5);
  --nano-primary-inverse: #fff;
  --nano-secondary: #5d6b89;
  --nano-secondary-background: #525f7a;
  --nano-secondary-border: var(--nano-secondary-background);
  --nano-secondary-underline: rgba(93, 107, 137, 0.5);
  --nano-secondary-hover: #48536b;
  --nano-secondary-hover-background: #48536b;
  --nano-secondary-hover-border: var(--nano-secondary-hover-background);
  --nano-secondary-hover-underline: var(--nano-secondary-hover);
  --nano-secondary-focus: rgba(93, 107, 137, 0.25);
  --nano-secondary-inverse: #fff;
  --nano-contrast: #181c25;
  --nano-contrast-background: #181c25;
  --nano-contrast-border: var(--nano-contrast-background);
  --nano-contrast-underline: rgba(24, 28, 37, 0.5);
  --nano-contrast-hover: #000;
  --nano-contrast-hover-background: #000;
  --nano-contrast-hover-border: var(--nano-contrast-hover-background);
  --nano-contrast-hover-underline: var(--nano-secondary-hover);
  --nano-contrast-focus: rgba(93, 107, 137, 0.25);
  --nano-contrast-inverse: #fff;
  --nano-box-shadow: 0.0145rem 0.029rem 0.174rem rgba(104, 120, 153, 0.01698), 0.0335rem 0.067rem 0.402rem rgba(104, 120, 153, 0.024), 0.0625rem 0.125rem 0.75rem rgba(104, 120, 153, 0.03), 0.1125rem 0.225rem 1.35rem rgba(104, 120, 153, 0.036), 0.2085rem 0.417rem 2.502rem rgba(104, 120, 153, 0.04302), 0.5rem 1rem 6rem rgba(104, 120, 153, 0.06), 0 0 0 0.0625rem rgba(104, 120, 153, 0.015);
  --nano-h1-color: #2d3138;
  --nano-h2-color: #373c44;
  --nano-h3-color: #424751;
  --nano-h4-color: #4d535e;
  --nano-h5-color: #5c6370;
  --nano-h6-color: #646b79;
  --nano-mark-background-color: #fde7c0;
  --nano-mark-color: #0f1114;
  --nano-ins-color: #1d6a54;
  --nano-del-color: #883935;
  --nano-blockquote-border-color: var(--nano-muted-border-color);
  --nano-blockquote-footer-color: var(--nano-muted-color);
  --nano-button-box-shadow: 0 0 0 rgba(0, 0, 0, 0);
  --nano-button-hover-box-shadow: 0 0 0 rgba(0, 0, 0, 0);
  --nano-table-border-color: var(--nano-muted-border-color);
  --nano-table-row-stripped-background-color: rgba(111, 120, 135, 0.0375);
  --nano-code-background-color: #f3f5f7;
  --nano-code-color: #646b79;
  --nano-code-kbd-background-color: var(--nano-color);
  --nano-code-kbd-color: var(--nano-background-color);
  --nano-form-element-background-color: #fbfcfc;
  --nano-form-element-selected-background-color: #dfe3eb;
  --nano-form-element-border-color: #cfd5e2;
  --nano-form-element-color: #23262c;
  --nano-form-element-placeholder-color: var(--nano-muted-color);
  --nano-form-element-active-background-color: #fff;
  --nano-form-element-active-border-color: var(--nano-primary-border);
  --nano-form-element-focus-color: var(--nano-primary-border);
  --nano-form-element-disabled-background-color: var(--nano-form-element-background-color);
  --nano-form-element-disabled-border-color: var(--nano-form-element-border-color);
  --nano-form-element-disabled-opacity: 0.5;
  --nano-form-element-invalid-border-color: #b86a6b;
  --nano-form-element-invalid-active-border-color: #c84f48;
  --nano-form-element-invalid-focus-color: var(--nano-form-element-invalid-active-border-color);
  --nano-form-element-valid-border-color: #4c9b8a;
  --nano-form-element-valid-active-border-color: #279977;
  --nano-form-element-valid-focus-color: var(--nano-form-element-valid-active-border-color);
  --nano-switch-background-color: #bfc7d9;
  --nano-switch-color: var(--nano-primary-inverse);
  --nano-switch-checked-background-color: var(--nano-primary-background);
  --nano-range-border-color: #dfe3eb;
  --nano-range-active-border-color: #bfc7d9;
  --nano-range-thumb-border-color: var(--nano-background-color);
  --nano-range-thumb-color: var(--nano-secondary-background);
  --nano-range-thumb-active-color: var(--nano-primary-background);
  --nano-accordion-border-color: var(--nano-muted-border-color);
  --nano-accordion-active-summary-color: var(--nano-primary-hover);
  --nano-accordion-close-summary-color: var(--nano-color);
  --nano-accordion-open-summary-color: var(--nano-muted-color);
  --nano-card-background-color: var(--nano-background-color);
  --nano-card-border-color: var(--nano-muted-border-color);
  --nano-card-box-shadow: var(--nano-box-shadow);
  --nano-card-sectioning-background-color: #fbfcfc;
  --nano-dropdown-background-color: #fff;
  --nano-dropdown-border-color: #eff1f4;
  --nano-dropdown-box-shadow: var(--nano-box-shadow);
  --nano-dropdown-color: var(--nano-color);
  --nano-dropdown-hover-background-color: #eff1f4;
  --nano-loading-spinner-opacity: 0.5;
  --nano-modal-overlay-background-color: rgba(232, 234, 237, 0.75);
  --nano-progress-background-color: #dfe3eb;
  --nano-progress-color: var(--nano-primary-background);
  --nano-tooltip-background-color: var(--nano-contrast-background);
  --nano-tooltip-color: var(--nano-contrast-inverse);
  --nano-icon-valid: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(29, 106, 84)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Cpolyline points='20 6 9 17 4 12'%3E%3C/polyline%3E%3C/svg%3E");
  --nano-icon-invalid: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(136, 57, 53)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Ccircle cx='12' cy='12' r='10'%3E%3C/circle%3E%3Cline x1='12' y1='8' x2='12' y2='12'%3E%3C/line%3E%3Cline x1='12' y1='16' x2='12.01' y2='16'%3E%3C/line%3E%3C/svg%3E");
  color-scheme: light;
}
[data-theme=light] input:is([type=submit],
[type=button],
[type=reset],
[type=checkbox],
[type=radio],
[type=file]),
:root:not([data-theme=dark]) input:is([type=submit],
[type=button],
[type=reset],
[type=checkbox],
[type=radio],
[type=file]) {
  --nano-form-element-focus-color: var(--nano-primary-focus);
}

@media only screen and (prefers-color-scheme: dark) {
  :root:not([data-theme]) {
    --nano-background-color: #13171f;
    --nano-color: #c2c7d0;
    --nano-text-selection-color: rgba(1, 170, 255, 0.1875);
    --nano-muted-color: #7b8495;
    --nano-muted-border-color: #202632;
    --nano-primary: #01aaff;
    --nano-primary-background: #0172ad;
    --nano-primary-border: var(--nano-primary-background);
    --nano-primary-underline: rgba(1, 170, 255, 0.5);
    --nano-primary-hover: #79c0ff;
    --nano-primary-hover-background: #017fc0;
    --nano-primary-hover-border: var(--nano-primary-hover-background);
    --nano-primary-hover-underline: var(--nano-primary-hover);
    --nano-primary-focus: rgba(1, 170, 255, 0.375);
    --nano-primary-inverse: #fff;
    --nano-secondary: #969eaf;
    --nano-secondary-background: #525f7a;
    --nano-secondary-border: var(--nano-secondary-background);
    --nano-secondary-underline: rgba(150, 158, 175, 0.5);
    --nano-secondary-hover: #b3b9c5;
    --nano-secondary-hover-background: #5d6b89;
    --nano-secondary-hover-border: var(--nano-secondary-hover-background);
    --nano-secondary-hover-underline: var(--nano-secondary-hover);
    --nano-secondary-focus: rgba(144, 158, 190, 0.25);
    --nano-secondary-inverse: #fff;
    --nano-contrast: #dfe3eb;
    --nano-contrast-background: #eff1f4;
    --nano-contrast-border: var(--nano-contrast-background);
    --nano-contrast-underline: rgba(223, 227, 235, 0.5);
    --nano-contrast-hover: #fff;
    --nano-contrast-hover-background: #fff;
    --nano-contrast-hover-border: var(--nano-contrast-hover-background);
    --nano-contrast-hover-underline: var(--nano-contrast-hover);
    --nano-contrast-focus: rgba(207, 213, 226, 0.25);
    --nano-contrast-inverse: #000;
    --nano-box-shadow: 0.0145rem 0.029rem 0.174rem rgba(0, 0, 0, 0.01698), 0.0335rem 0.067rem 0.402rem rgba(0, 0, 0, 0.024), 0.0625rem 0.125rem 0.75rem rgba(0, 0, 0, 0.03), 0.1125rem 0.225rem 1.35rem rgba(0, 0, 0, 0.036), 0.2085rem 0.417rem 2.502rem rgba(0, 0, 0, 0.04302), 0.5rem 1rem 6rem rgba(0, 0, 0, 0.06), 0 0 0 0.0625rem rgba(0, 0, 0, 0.015);
    --nano-h1-color: #f0f1f3;
    --nano-h2-color: #e0e3e7;
    --nano-h3-color: #c2c7d0;
    --nano-h4-color: #b3b9c5;
    --nano-h5-color: #a4acba;
    --nano-h6-color: #8891a4;
    --nano-mark-background-color: #014063;
    --nano-mark-color: #fff;
    --nano-ins-color: #62af9a;
    --nano-del-color: #ce7e7b;
    --nano-blockquote-border-color: var(--nano-muted-border-color);
    --nano-blockquote-footer-color: var(--nano-muted-color);
    --nano-button-box-shadow: 0 0 0 rgba(0, 0, 0, 0);
    --nano-button-hover-box-shadow: 0 0 0 rgba(0, 0, 0, 0);
    --nano-table-border-color: var(--nano-muted-border-color);
    --nano-table-row-stripped-background-color: rgba(111, 120, 135, 0.0375);
    --nano-code-background-color: #1a1f28;
    --nano-code-color: #8891a4;
    --nano-code-kbd-background-color: var(--nano-color);
    --nano-code-kbd-color: var(--nano-background-color);
    --nano-form-element-background-color: #1c212c;
    --nano-form-element-selected-background-color: #2a3140;
    --nano-form-element-border-color: #2a3140;
    --nano-form-element-color: #e0e3e7;
    --nano-form-element-placeholder-color: #8891a4;
    --nano-form-element-active-background-color: #1a1f28;
    --nano-form-element-active-border-color: var(--nano-primary-border);
    --nano-form-element-focus-color: var(--nano-primary-border);
    --nano-form-element-disabled-background-color: var(--nano-form-element-background-color);
    --nano-form-element-disabled-border-color: var(--nano-form-element-border-color);
    --nano-form-element-disabled-opacity: 0.5;
    --nano-form-element-invalid-border-color: #964a50;
    --nano-form-element-invalid-active-border-color: #b7403b;
    --nano-form-element-invalid-focus-color: var(--nano-form-element-invalid-active-border-color);
    --nano-form-element-valid-border-color: #2a7b6f;
    --nano-form-element-valid-active-border-color: #16896a;
    --nano-form-element-valid-focus-color: var(--nano-form-element-valid-active-border-color);
    --nano-switch-background-color: #333c4e;
    --nano-switch-color: var(--nano-primary-inverse);
    --nano-switch-checked-background-color: var(--nano-primary-background);
    --nano-range-border-color: #202632;
    --nano-range-active-border-color: #2a3140;
    --nano-range-thumb-border-color: var(--nano-background-color);
    --nano-range-thumb-color: var(--nano-secondary-background);
    --nano-range-thumb-active-color: var(--nano-primary-background);
    --nano-accordion-border-color: var(--nano-muted-border-color);
    --nano-accordion-active-summary-color: var(--nano-primary-hover);
    --nano-accordion-close-summary-color: var(--nano-color);
    --nano-accordion-open-summary-color: var(--nano-muted-color);
    --nano-card-background-color: #181c25;
    --nano-card-border-color: var(--nano-card-background-color);
    --nano-card-box-shadow: var(--nano-box-shadow);
    --nano-card-sectioning-background-color: #1a1f28;
    --nano-dropdown-background-color: #181c25;
    --nano-dropdown-border-color: #202632;
    --nano-dropdown-box-shadow: var(--nano-box-shadow);
    --nano-dropdown-color: var(--nano-color);
    --nano-dropdown-hover-background-color: #202632;
    --nano-loading-spinner-opacity: 0.5;
    --nano-modal-overlay-background-color: rgba(8, 9, 10, 0.75);
    --nano-progress-background-color: #202632;
    --nano-progress-color: var(--nano-primary-background);
    --nano-tooltip-background-color: var(--nano-contrast-background);
    --nano-tooltip-color: var(--nano-contrast-inverse);
    --nano-icon-valid: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(98, 175, 154)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Cpolyline points='20 6 9 17 4 12'%3E%3C/polyline%3E%3C/svg%3E");
    --nano-icon-invalid: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(206, 126, 123)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Ccircle cx='12' cy='12' r='10'%3E%3C/circle%3E%3Cline x1='12' y1='8' x2='12' y2='12'%3E%3C/line%3E%3Cline x1='12' y1='16' x2='12.01' y2='16'%3E%3C/line%3E%3C/svg%3E");
    color-scheme: dark;
  }
  :root:not([data-theme]) input:is([type=submit],
  [type=button],
  [type=reset],
  [type=checkbox],
  [type=radio],
  [type=file]) {
    --nano-form-element-focus-color: var(--nano-primary-focus);
  }
  :root:not([data-theme]) details summary[role=button].contrast:not(.outline)::after {
    filter: brightness(0);
  }
  :root:not([data-theme]) [aria-busy=true]:not(input, select, textarea).contrast:is(button,
  [type=submit],
  [type=button],
  [type=reset],
  [role=button]):not(.outline)::before {
    filter: brightness(0);
  }
}
[data-theme=dark] {
  --nano-background-color: #13171f;
  --nano-color: #c2c7d0;
  --nano-text-selection-color: rgba(1, 170, 255, 0.1875);
  --nano-muted-color: #7b8495;
  --nano-muted-border-color: #202632;
  --nano-primary: #01aaff;
  --nano-primary-background: #0172ad;
  --nano-primary-border: var(--nano-primary-background);
  --nano-primary-underline: rgba(1, 170, 255, 0.5);
  --nano-primary-hover: #79c0ff;
  --nano-primary-hover-background: #017fc0;
  --nano-primary-hover-border: var(--nano-primary-hover-background);
  --nano-primary-hover-underline: var(--nano-primary-hover);
  --nano-primary-focus: rgba(1, 170, 255, 0.375);
  --nano-primary-inverse: #fff;
  --nano-secondary: #969eaf;
  --nano-secondary-background: #525f7a;
  --nano-secondary-border: var(--nano-secondary-background);
  --nano-secondary-underline: rgba(150, 158, 175, 0.5);
  --nano-secondary-hover: #b3b9c5;
  --nano-secondary-hover-background: #5d6b89;
  --nano-secondary-hover-border: var(--nano-secondary-hover-background);
  --nano-secondary-hover-underline: var(--nano-secondary-hover);
  --nano-secondary-focus: rgba(144, 158, 190, 0.25);
  --nano-secondary-inverse: #fff;
  --nano-contrast: #dfe3eb;
  --nano-contrast-background: #eff1f4;
  --nano-contrast-border: var(--nano-contrast-background);
  --nano-contrast-underline: rgba(223, 227, 235, 0.5);
  --nano-contrast-hover: #fff;
  --nano-contrast-hover-background: #fff;
  --nano-contrast-hover-border: var(--nano-contrast-hover-background);
  --nano-contrast-hover-underline: var(--nano-contrast-hover);
  --nano-contrast-focus: rgba(207, 213, 226, 0.25);
  --nano-contrast-inverse: #000;
  --nano-box-shadow: 0.0145rem 0.029rem 0.174rem rgba(0, 0, 0, 0.01698), 0.0335rem 0.067rem 0.402rem rgba(0, 0, 0, 0.024), 0.0625rem 0.125rem 0.75rem rgba(0, 0, 0, 0.03), 0.1125rem 0.225rem 1.35rem rgba(0, 0, 0, 0.036), 0.2085rem 0.417rem 2.502rem rgba(0, 0, 0, 0.04302), 0.5rem 1rem 6rem rgba(0, 0, 0, 0.06), 0 0 0 0.0625rem rgba(0, 0, 0, 0.015);
  --nano-h1-color: #f0f1f3;
  --nano-h2-color: #e0e3e7;
  --nano-h3-color: #c2c7d0;
  --nano-h4-color: #b3b9c5;
  --nano-h5-color: #a4acba;
  --nano-h6-color: #8891a4;
  --nano-mark-background-color: #014063;
  --nano-mark-color: #fff;
  --nano-ins-color: #62af9a;
  --nano-del-color: #ce7e7b;
  --nano-blockquote-border-color: var(--nano-muted-border-color);
  --nano-blockquote-footer-color: var(--nano-muted-color);
  --nano-button-box-shadow: 0 0 0 rgba(0, 0, 0, 0);
  --nano-button-hover-box-shadow: 0 0 0 rgba(0, 0, 0, 0);
  --nano-table-border-color: var(--nano-muted-border-color);
  --nano-table-row-stripped-background-color: rgba(111, 120, 135, 0.0375);
  --nano-code-background-color: #1a1f28;
  --nano-code-color: #8891a4;
  --nano-code-kbd-background-color: var(--nano-color);
  --nano-code-kbd-color: var(--nano-background-color);
  --nano-form-element-background-color: #1c212c;
  --nano-form-element-selected-background-color: #2a3140;
  --nano-form-element-border-color: #2a3140;
  --nano-form-element-color: #e0e3e7;
  --nano-form-element-placeholder-color: #8891a4;
  --nano-form-element-active-background-color: #1a1f28;
  --nano-form-element-active-border-color: var(--nano-primary-border);
  --nano-form-element-focus-color: var(--nano-primary-border);
  --nano-form-element-disabled-background-color: var(--nano-form-element-background-color);
  --nano-form-element-disabled-border-color: var(--nano-form-element-border-color);
  --nano-form-element-disabled-opacity: 0.5;
  --nano-form-element-invalid-border-color: #964a50;
  --nano-form-element-invalid-active-border-color: #b7403b;
  --nano-form-element-invalid-focus-color: var(--nano-form-element-invalid-active-border-color);
  --nano-form-element-valid-border-color: #2a7b6f;
  --nano-form-element-valid-active-border-color: #16896a;
  --nano-form-element-valid-focus-color: var(--nano-form-element-valid-active-border-color);
  --nano-switch-background-color: #333c4e;
  --nano-switch-color: var(--nano-primary-inverse);
  --nano-switch-checked-background-color: var(--nano-primary-background);
  --nano-range-border-color: #202632;
  --nano-range-active-border-color: #2a3140;
  --nano-range-thumb-border-color: var(--nano-background-color);
  --nano-range-thumb-color: var(--nano-secondary-background);
  --nano-range-thumb-active-color: var(--nano-primary-background);
  --nano-accordion-border-color: var(--nano-muted-border-color);
  --nano-accordion-active-summary-color: var(--nano-primary-hover);
  --nano-accordion-close-summary-color: var(--nano-color);
  --nano-accordion-open-summary-color: var(--nano-muted-color);
  --nano-card-background-color: #181c25;
  --nano-card-border-color: var(--nano-card-background-color);
  --nano-card-box-shadow: var(--nano-box-shadow);
  --nano-card-sectioning-background-color: #1a1f28;
  --nano-dropdown-background-color: #181c25;
  --nano-dropdown-border-color: #202632;
  --nano-dropdown-box-shadow: var(--nano-box-shadow);
  --nano-dropdown-color: var(--nano-color);
  --nano-dropdown-hover-background-color: #202632;
  --nano-loading-spinner-opacity: 0.5;
  --nano-modal-overlay-background-color: rgba(8, 9, 10, 0.75);
  --nano-progress-background-color: #202632;
  --nano-progress-color: var(--nano-primary-background);
  --nano-tooltip-background-color: var(--nano-contrast-background);
  --nano-tooltip-color: var(--nano-contrast-inverse);
  --nano-icon-valid: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(98, 175, 154)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Cpolyline points='20 6 9 17 4 12'%3E%3C/polyline%3E%3C/svg%3E");
  --nano-icon-invalid: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='rgb(206, 126, 123)' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'%3E%3Ccircle cx='12' cy='12' r='10'%3E%3C/circle%3E%3Cline x1='12' y1='8' x2='12' y2='12'%3E%3C/line%3E%3Cline x1='12' y1='16' x2='12.01' y2='16'%3E%3C/line%3E%3C/svg%3E");
  color-scheme: dark;
}
[data-theme=dark] input:is([type=submit],
[type=button],
[type=reset],
[type=checkbox],
[type=radio],
[type=file]) {
  --nano-form-element-focus-color: var(--nano-primary-focus);
}
[data-theme=dark] details summary[role=button].contrast:not(.outline)::after {
  filter: brightness(0);
}
[data-theme=dark] [aria-busy=true]:not(input, select, textarea).contrast:is(button,
[type=submit],
[type=button],
[type=reset],
[role=button]):not(.outline)::before {
  filter: brightness(0);
}

progress,
[type=checkbox],
[type=radio],
[type=range] {
  accent-color: var(--nano-primary);
}</pre>
          </code-block>
          <!-- eslint-enable vue/max-len -->
        </div>
      </details>
    </section>
  </div>
</template>

<style>
#css-var-example h1,
#css-var-example p,
#css-var-example button {
    --nano-border-radius: 2rem;
    --nano-typography-spacing-vertical: 1.5rem;
    --nano-form-element-spacing-vertical: 1rem;
    --nano-form-element-spacing-horizontal: 1.5rem;
  }
#css-var-example h1 {
    --nano-font-family: Pacifico, cursive;
    --nano-font-weight: 400;
    --nano-typography-spacing-vertical: 0.5rem;
  }
#css-var-example button {
    --nano-font-weight: 700;
    margin-bottom: 0;
  }
</style>
