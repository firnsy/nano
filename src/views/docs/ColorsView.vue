<!--
  This file is part of nano.

  Author(s):
   - Ian Firns <firnsy@gmail.com>

  License: MIT
-->
<script setup>
import CodeBlock from '@/components/CodeBlock.vue';
import SwatchCard from '@/components/SwatchCard.vue';
import { getFamilies, getFamilyMap } from '@/utilities/colors-map';
</script>

<template>
  <hgroup>
    <p class="chapter">
      Customisation
    </p>
    <h1>Colors</h1>
    <p>Nano comes with 380 manually&nbsp;crafted colors to help you personalise your brand&nbsp;design&nbsp;system.</p>
  </hgroup>
  <aside id="table-of-contents">
    <nav>
      <details open="">
        <summary>Content</summary>
        <ul>
          <li>
            <a
              class="secondary"
              href="/docs/colors"
              aria-current="page"
            >Colors</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/colors#usage-with-css"
            >Usage with CSS</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/colors#usage-with-sass"
            >Usage with SASS</a>
          </li>
        </ul>
      </details>
    </nav>
  </aside>
  <div role="document">
    <section class="color-families">
      <swatch-card
        v-for="family in getFamilies()"
        :key="family"
        :family="family"
        :family-map="getFamilyMap(family)"
      />
    </section>
    <section>
      <h2>
        Usage with CSS<a
          id="usage-with-css"
          href="#usage-with-css"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p>No color utilities are in the main nano stylesheet.</p>
      <p>
        There is a separate stylesheet with all the color utilities that you can link in the
        <code v-highlight>&lt;head&gt;</code> of your website.
      </p>
      <div
        class="code small"
        data-theme="dark"
      >
        <code-block><pre>&lt;link rel="stylesheet" href="css/nano.colors.min.css" /&gt;</pre></code-block>
      </div>
      <p>This stylesheet is almost the same size as the entire nano library.</p>
      <p>
        We do not recommend including all colors on a production site. You should include only the color families
        and shades that you use.
      </p>
      <p>
        After linking the color utilities, you can style any element with the utility classes. Click on any color above
        to see details.
      </p>
      <article
        aria-label="Color example"
        class="component"
      >
        <h2 class="nano-color-pink-500">
          Pink title
        </h2>
        <footer
          class="code small"
          data-theme="dark"
        >
          <code-block><pre>&lt;h2 className="nano-color-pink-500"&gt;Pink title&lt;/h2&gt;</pre></code-block>
        </footer>
      </article>
      <article
        aria-label="Background color example"
        class="nano-background-pink-600"
      >
        Pink card
      </article>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block>
          <pre>&lt;article class="nano-background-pink-600"&gt;
  Pink card
&lt;/article&gt;</pre>
        </code-block>
      </div>
    </section>
    <section>
      <h2>
        Usage with SASS<a
          id="usage-with-sass"
          href="#usage-with-sass"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p>You can import all colors as SASS variables in any <code>.scss</code> file with:</p>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block><pre>@use "colors" as *;</pre></code-block>
      </div>
      <p>The colors can then be used like this:</p>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block>
          <pre>h2 {
  color: $pink-500;
}</pre>
        </code-block>
      </div>
      <p>
        You can also generate the utility classes with <a
          rel="noopener noreferrer"
          href="https://sass-lang.com/documentation/at-rules/use"
          target="_blank"
        >@use</a>:
      </p>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block><pre>@use "colors/utilities";</pre></code-block>
      </div>
      <p>There are many settings available.</p>
      <p>
        Here is, for example, how to generate only the color utilities (and not the background utilities) and only for
        red, pink, fuchsia, and purple color families.
      </p>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block>
          <pre>@use "colors/utilities" with (
  $palette: (
    "color-families": (
      red,
      pink,
      fuchsia,
      purple,
    ),
  ),
  $utilities: (
    "background-colors": false,
  )
);</pre>
        </code-block>
      </div>
      <details>
        <summary
          role="button"
          class="secondary"
        >
          All default settings
        </summary>
        <div
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>// prefix for css variables
$css-var-prefix: nano-settings.$css-var-prefix !default;
$css-class-prefix: nano-settings.$css-var-prefix !default;

// Palette
$palette: () !default;
$palette: map.merge(
  (
    // Color families
    "color-families":
      (
        red,
        pink,
        fuchsia,
        purple,
        violet,
        indigo,
        blue,
        azure,
        cyan,
        jade,
        green,
        lime,
        yellow,
        amber,
        pumpkin,
        orange,
        sand,
        grey,
        zinc,
        slate
      ),
    // Shades
    "shades":
      (
        50,
        100,
        150,
        200,
        250,
        300,
        350,
        400,
        450,
        500,
        550,
        600,
        650,
        700,
        750,
        800,
        850,
        900,
        950
      ),
    // Export main color for each family
    "enable-main-color": true,

    // Export shades for each family
    "enable-shades": true,

    // Export black and white
    "enable-black-and-white": false,

    // Light color used for dark backgrounds
    "light-color": #fff,

    // Dark color used for light backgrounds
    "dark-color": #000,

    // Export as HEX, RGB or HSL values
    "export-as": "hex" // hex | rgb | hsl
  ),
  $palette
);

// Properties names used for CSS variables and classes
// Useful if you want to shorten the names
$properties: () !default;
$properties: map.merge(
  (
    "color": "color",
    "background-color": "background",
  ),
  $properties
);

// Utilities to export
$utilities: () !default;
$utilities: map.merge(
  (
    // CSS Vars
    "css-vars": true,

    // Colors utility classes
    "colors": true,

    // Background color utility classes
    "background-colors": true,

    // Color value for background color utility classes
    "color-for-background-colors": true
  ),
  $utilities
);</pre>
          </code-block>
        </div>
      </details>
    </section>
  </div>
</template>

<style>
  .color-families {
    grid-column-gap: 2rem;
    grid-row-gap: 4rem;
    display: grid;
    grid-template-columns: 1fr
}

  @media(min-width: 768px) {
    .color-families {
      grid-template-columns:1fr 1fr
    }
  }

  .color-families .family {
    margin: 0;
    padding: 0;
    background-color: transparent;
    box-shadow: none
  }
</style>
