<!--
  This file is part of nano.

  Author(s):
   - Ian Firns <firnsy@gmail.com>

  License: MIT
-->
<script setup>
import { computed, ref } from 'vue';
import CodeBlock from '@/components/CodeBlock.vue';

const baseFontSize = '16px';
const breakpoints = [
  {
    name: 'xs',
    label: 'Extra Small < 576px',
    fontSize: 16,
    fontSizeRemPx: '16px',
    fontSizePercent: '100%'
  },
  {
    name: 'sm',
    label: 'Small ≥ 576px',
    fontSize: 17,
    fontSizeRemPx: '17px',
    fontSizePercent: '106.25%'
  },
  {
    name: 'md',
    label: 'Medium ≥ 768px',
    fontSize: 18,
    fontSizeRemPx: '18px',
    fontSizePercent: '112.5%'
  },
  {
    name: 'lg',
    label: 'Large ≥ 1024px',
    fontSize: 19,
    fontSizeRemPx: '19px',
    fontSizePercent: '118.75%'
  },
  {
    name: 'xl',
    label: 'Extra Large ≥ 1280px',
    fontSize: 20,
    fontSizeRemPx: '20px',
    fontSizePercent: '125%'
  },
  {
    name: 'xxl',
    label: 'Extra Extra Large ≥ 1536px',
    fontSize: 21,
    fontSizeRemPx: '21px',
    fontSizePercent: '131.25%'
  }
];

const multipliers = [
  {
    label: '<h1>',
    value: 2,
    unit: 'rem'
  },
  {
    label: '<h2>',
    value: 1.75,
    unit: 'rem'
  },
  {
    label: '<h3>',
    value: 1.5,
    unit: 'rem'
  },
  {
    label: '<h4>',
    value: 1.25,
    unit: 'rem'
  },
  {
    label: '<h5>',
    value: 1.125,
    unit: 'rem'
  },
  {
    label: '<h6>',
    value: 1,
    unit: 'rem'
  },
  {
    label: '<small>',
    value: 0.875,
    unit: 'em'
  }
];

// REFS
const unitsInRem = ref(false);

// COMPUTED
// eslint-disable-next-line arrow-body-style
const unitToggleLabel = computed(() => {
  return !unitsInRem.value ? 'Display font sizes in percent and rem.' : 'Display font sizes in pixels.';
});

// METHODS
function toggleUnits() {
  unitsInRem.value = !unitsInRem.value;
}

function renderBaseFontSize(breakpoint) {
  if (unitsInRem.value) {
    return `${breakpoint.fontSizePercent}`;
  }

  return `${breakpoint.fontSize}px`;
}

function renderUnits(breakpoint, multiplier) {
  if (unitsInRem.value) {
    return `${multiplier.value}${multiplier.unit}`;
  }

  return `${breakpoint.fontSize * multiplier.value}px`;
}

function renderBaseTooltip(breakpoint) {
  return `${breakpoint.fontSizePercent} * ${baseFontSize}`;
}

function renderTooltip(breakpoint, multiplier) {
  return `${breakpoint.fontSizePercent} * ${multiplier.value}${multiplier.unit} * ${breakpoint.fontSizeRemPx}`;
}
</script>

<template>
  <hgroup>
    <p class="chapter">
      Content
    </p>
    <h1>Typography</h1>
    <p>All typographic elements are responsive and scale gracefully across devices and viewports.</p>
  </hgroup>
  <aside id="table-of-contents">
    <nav>
      <details open>
        <summary>Content</summary>
        <ul>
          <li>
            <a
              class="secondary"
              href="/docs/typography"
            >Font sizes</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/typography#headings"
            >Headings</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/typography#heading-group"
            >Heading group</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/typography#inline-text-elements"
            >Inline text elements</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/typography#blockquote"
            >Blockquote</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/typography#horizontal-rule"
            >Horizontal rule</a>
          </li>
        </ul>
      </details>
    </nav>
  </aside>
  <div role="document">
    <section>
      <table
        id="responsive-font-sizes"
        class="striped"
      >
        <caption>Responsive Font Sizes</caption>
        <thead>
          <tr>
            <th>Breakpoint</th>
            <th
              v-for="breakpoint in breakpoints"
              :key="breakpoint.name"
            >
              <span
                :data-tooltip="breakpoint.label"
                data-placement="bottom"
              >{{ breakpoint.name }}</span>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Base</td>
            <td
              v-for="breakpoint in breakpoints"
              :key="breakpoint.name"
            >
              <span
                v-if="unitsInRem"
                data-placement="bottom"
              >{{ renderBaseFontSize(breakpoint) }}</span>
              <span
                v-else
                :data-tooltip="renderBaseTooltip(breakpoint)"
                data-placement="bottom"
              >{{ renderBaseFontSize(breakpoint) }}</span>
            </td>
          </tr>
          <tr
            v-for="multiplier in multipliers"
            :key="multiplier.label"
          >
            <td><code>{{ multiplier.label }}</code></td>
            <td
              v-for="breakpoint in breakpoints"
              :key="breakpoint.name"
            >
              <span
                v-if="unitsInRem"
                data-placement="bottom"
              >{{ renderUnits(breakpoint, multiplier) }}</span>
              <span
                v-else
                :data-tooltip="renderTooltip(breakpoint, multiplier)"
                data-placement="bottom"
              >{{ renderUnits(breakpoint, multiplier) }}</span>
            </td>
          </tr>
        </tbody>
      </table>
      <p><a href="#" @click="toggleUnits()">{{ unitToggleLabel }}</a></p>
      <p>
        To ensure that the user's default font size is followed, the base font size is defined as a percentage that
        grows with the user's screen size, while HTML elements are defined in <code>rem</code>.
      </p>
      <p>
        Since <code>rem</code> is a multiplier of the HTML document font size, all HTML element's font sizes grow
        proportionally with the size of the user's screen.
      </p>
    </section>
    <section>
      <h2>
        Headings<a
          id="headings"
          href="#headings"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <article
        aria-label="Headings example"
        class="component"
      >
        <h1>Heading 1</h1>
        <h2>Heading 2</h2>
        <h3>Heading 3</h3>
        <h4>Heading 4</h4>
        <h5>Heading 5</h5>
        <h6>Heading 6</h6>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;h1&gt;Heading 1&lt;/h1&gt;
&lt;h2&gt;Heading 2&lt;/h2&gt;
&lt;h3&gt;Heading 3&lt;/h3&gt;
&lt;h4&gt;Heading 4&lt;/h4&gt;
&lt;h5&gt;Heading 5&lt;/h5&gt;
&lt;h6&gt;Heading 6&lt;/h6&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>
    <section>
      <h2>
        Heading group<a
          id="heading-group"
          href="#heading-group"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p>Inside a <code>&lt;hgroup&gt;</code>, margins are collapsed, and the <code>:last-child</code> is muted.</p>
      <article
        aria-label="Headings example"
        class="component"
      >
        <hgroup>
          <h2>Get inspired with CSS</h2>
          <p>How to use CSS to add glam to your Website?</p>
        </hgroup>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block language="xml">
            <pre>&lt;hgroup&gt;
  &lt;h2&gt;Get inspired with CSS&lt;/h2&gt;
  &lt;p&gt;How to use CSS to add glam to your Website?&lt;/p&gt;
&lt;/hgroup&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>
    <section>
      <h2>
        Inline text elements<a
          id="inline-text-elements"
          href="#inline-text-elements"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <div class="container">
        <div class="row">
          <div class="col">
            <p><abbr title="Abbreviation">Abbr.</abbr> <code class="language-xml">&lt;abbr&gt;</code></p>
            <p>
              <strong>Bold</strong>
              <code class="language-xml">&lt;strong&gt;</code>
              <code class="language-xml">&lt;b&gt;</code>
            </p>
            <p>
              <em>Italic</em>
              <code class="language-xml">&lt;i&gt;</code>
              <code class="language-xml">&lt;em&gt;</code>
              <code class="language-xml">&lt;cite&gt;</code>
            </p>
            <p><del>Deleted</del> <code class="language-xml">&lt;del&gt;</code></p>
            <p><ins>Inserted</ins> <code class="language-xml">&lt;ins&gt;</code></p>
            <p><kbd>Ctrl + S</kbd> <code class="language-xml">&lt;kbd&gt;</code></p>
          </div>
          <div class="col">
            <p><mark>Highlighted</mark> <code class="language-xml">&lt;mark&gt;</code></p>
            <p><s>Strikethrough</s> <code class="language-xml">&lt;s&gt;</code></p>
            <p><small>Small </small><code class="language-xml">&lt;small&gt;</code></p>
            <p>Text <sub>Sub</sub> <code class="language-xml">&lt;sub&gt;</code></p>
            <p>Text <sup>Sup</sup> <code class="language-xml">&lt;sup&gt;</code></p>
            <p><u>Underline</u> <code class="language-xml">&lt;u&gt;</code></p>
          </div>
        </div>
      </div>
    </section>
    <section>
      <h2>
        Blockquote<a
          id="blockquote"
          href="#blockquote"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <blockquote>
        “Design is a funny word. Some people think design means how it looks. But of course, if you dig deeper, it's
        really how it works.”
        <footer><cite>— Steve Jobs</cite></footer>
      </blockquote>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block>
          <pre>&lt;blockquote&gt;
  “Design is a funny word. Some people think
  design means how it looks. But of course, if
  you dig deeper, it's really how it works.”
  &lt;footer&gt;
    &lt;cite&gt;— Steve Jobs&lt;/cite&gt;
  &lt;/footer&gt;
&lt;/blockquote&gt;</pre>
        </code-block>
      </div>
    </section>
    <section>
      <h2>
        Horizontal rule<a
          id="horizontal-rule"
          href="#horizontal-rule"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p>The <code>&lt;hr&gt;</code> tag renders a horizontal line.</p>
      <article
        aria-label="Horizontal rule example"
        class="component"
      >
        <p>Paragraph before the horizontal line.</p>
        <hr>
        <p>Paragraph after the horizontal line.</p>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;p&gt;Paragraph before the horizontal line.&lt;/p&gt;
&lt;hr /&gt;
&lt;p&gt;Paragraph after the horizontal line.&lt;/p&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>
  </div>
</template>

<style>
  #responsive-font-sizes {
    font-size: 16px;
  }
</style>
