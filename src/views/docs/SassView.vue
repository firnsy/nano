<!--
  This file is part of nano.

  Author(s):
   - Ian Firns <firnsy@gmail.com>

  License: MIT
-->
<script setup>
import CodeBlock from '@/components/CodeBlock.vue';
</script>

<template>
  <hgroup>
    <p class="chapter">
      Customisation
    </p>
    <h1>Sass</h1>
    <p>
      Build your own minimal design system by compiling a custom version of Nano's CSS framework with <a
        rel="noopener noreferrer"
        href="https://sass-lang.com/"
        target="_blank"
      >SASS</a>.
    </p>
  </hgroup>
  <aside id="table-of-contents">
    <nav>
      <details open>
        <summary>Content</summary>
        <ul>
          <li>
            <a
              class="secondary"
              href="/docs/sass"
            >Introduction</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/sass#settings"
            >Settings</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/sass#custom-theme"
            >Custom theme</a>
          </li>
        </ul>
      </details>
    </nav>
  </aside>
  <div role="document">
    <section>
      <p>
        To get the most out of Nano, we recommend compiling your own version with SASS. This way, you can include only
        the required modules and personalize the settings without overriding CSS styles.
      </p>
      <p>
        Avoid modifying Nano’s core files whenever possible. This approach allows you to keep Nano up to date without
        conflicts since the Nano code and your custom code are separated.
      </p>
      <p>
        You can import Nano into your SCSS file with <a
          rel="noopener noreferrer"
          href="https://sass-lang.com/documentation/at-rules/use"
          target="_blank"
        >@use</a>:
      </p>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block><pre>@use &quot;nano&quot;;</pre></code-block>
      </div>
      <p>
        If you are using<!-- --> <a
          rel="noopener noreferrer"
          href="https://sass-lang.com/documentation/cli/dart-sass"
          target="_blank"
        >Sass Command-Line Interface</a> to compile your <code>.scss</code> files, you can define the load path using
        <code>sass --load-path=node_modules/@nans/nano/scss/</code> to avoid using relative URLs like:
      </p>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block><pre>@use &quot;../../../node_modules/@nano/nano/scss/nano&quot;;</pre></code-block>
      </div>
    </section>
    <section>
      <h2>
        Settings<a
          id="settings"
          href="#settings"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p>
        You can set custom settings with <code>@use &quot;nano&quot; with ( ... );</code>. These custom values will
        override the default variables.
      </p>
      <p>Here is an example to generate the classless version:</p>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block>
          <pre>// nano classless version
@use &quot;nano&quot; with (
  $enable-semantic-container: true,
  $enable-classes: false
); </pre>
        </code-block>
      </div>
      <p>
        Example to generate a lightweight version without <code>.classes</code>, uncommon form elements, and components.
      </p>
      <p>This version reduces the weight of Nano by ~50%.</p>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block>
          <pre>// nano lightweight version
@use &quot;nano&quot; with (
  $enable-semantic-container: true,
  $enable-classes: false,
  $modules: (
    &quot;content/code&quot;: false,
    &quot;forms/input-color&quot;: false,
    &quot;forms/input-date&quot;: false,
    &quot;forms/input-file&quot;: false,
    &quot;forms/input-range&quot;: false,
    &quot;forms/input-search&quot;: false,
    &quot;components/accordion&quot;: false,
    &quot;components/card&quot;: false,
    &quot;components/dropdown&quot;: false,
    &quot;components/loading&quot;: false,
    &quot;components/modal&quot;: false,
    &quot;components/nav&quot;: false,
    &quot;components/progress&quot;: false,
    &quot;components/tooltip&quot;: false,
    &quot;utilities/accessibility&quot;: false,
    &quot;utilities/reduce-motion&quot;: false
  )
); </pre>
        </code-block>
      </div>
      <details>
        <summary
          role="button"
          class="secondary"
        >
          All default settings
        </summary>
        <div
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>// prefix for css variables
$css-var-prefix: &quot;nano-&quot; !default;

// define the root element used to target &lt;header&gt;, &lt;main&gt;, &lt;footer&gt;
// with $enable-semantic-container and $enable-responsive-spacings
$semantic-root-element: &quot;body&quot; !default;

// enable &lt;header&gt;, &lt;main&gt;, &lt;footer&gt; inside $semantic-root-element as containers
$enable-semantic-container: false !default;

// enable a centered viewport for &lt;header&gt;, &lt;main&gt;, &lt;footer&gt; inside $semantic-root-element
// fluid layout if disabled
$enable-viewport: true !default;

// enable responsive spacings for &lt;header&gt;, &lt;main&gt;, &lt;footer&gt;, &lt;section&gt;, &lt;article&gt;
// fixed spacings if disabled
$enable-responsive-spacings: true !default;

// enable responsive typography
// fixed root element size (rem) if disabled
$enable-responsive-typography: true !default;

// enable .classes
// .classless version if disabled
$enable-classes: true !default;

// enable transitions
$enable-transitions: true !default;

// enable overriding with !important
$enable-important: true !default;

$breakpoints: map.deep-merge(
  (
    // extra small (less than landscape phones)
    // font size: 16px
    xs: (
      breakpoint: 0,
      viewport: 100%,
      root-font-size: 100%,
    ),
    // small (landscape phones)
    // font size: 17px
    sm:
      (
        breakpoint: 576px,
        viewport: 540px,
        root-font-size: 106.25%,
      ),

    // medium (tablets)
    // font size: 18px
    md:
      (
        breakpoint: 768px,
        viewport: 720px,
        root-font-size: 112.5%,
      ),

    // large
    // font size: 19px
    lg:
      (
        breakpoint: 992px,
        viewport: 960px,
        root-font-size: 118.75%,
      ),

    // extra large
    // font size: 20px
    xl:
      (
        breakpoint: 1280px,
        viewport: 1200px,
        root-font-size: 125%,
      ),

    // extra extra large
    // font size: 21px
    xxl:
      (
        breakpoint: 1536px,
        viewport: 1440px,
        root-font-size: 131.25%,
      )
  ),
  $breakpoints
);

// modules to export
$modules: () !default;
$modules: map.merge(
  (
    // theme
    &quot;themes/default&quot;: true,

    // layout
    &quot;layout/document&quot;: true,
    &quot;layout/landmarks&quot;: true,
    &quot;layout/container&quot;: true,
    &quot;layout/section&quot;: true,
    &quot;layout/grid&quot;: true,
    &quot;layout/scroller&quot;: true,

    // content
    &quot;content/link&quot;: true,
    &quot;content/typography&quot;: true,
    &quot;content/embedded&quot;: true,
    &quot;content/button&quot;: true,
    &quot;content/table&quot;: true,
    &quot;content/code&quot;: true,
    &quot;content/miscs&quot;: true,

    // forms
    &quot;forms/basics&quot;: true,
    &quot;forms/checkbox-radio-switch&quot;: true,
    &quot;forms/input-color&quot;: true,
    &quot;forms/input-date&quot;: true,
    &quot;forms/input-file&quot;: true,
    &quot;forms/input-range&quot;: true,
    &quot;forms/input-search&quot;: true,

    // components
    &quot;components/accordion&quot;: true,
    &quot;components/card&quot;: true,
    &quot;components/dropdown&quot;: true,
    &quot;components/group&quot;: true,
    &quot;components/loading&quot;: true,
    &quot;components/modal&quot;: true,
    &quot;components/nav&quot;: true,
    &quot;components/progress&quot;: true,
    &quot;components/tooltip&quot;: true,

    // utilities
    &quot;utilities/accessibility&quot;: true,
    &quot;utilities/reduce-motion&quot;: true
  ),
  $modules
);

// Shortcut for CSS vars prefix
$p: --#{$css-var-prefix};</pre>
          </code-block>
        </div>
      </details>
    </section>
    <section>
      <h2>
        Custom theme<a
          id="custom-theme"
          href="#custom-theme"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p>To create a custom version of Nano with a fully custom theme that reflects your brand identity, you can:</p>
      <ol>
        <li>Exclude the default theme from compilation,</li>
        <li>
          Import your custom theme (you can duplicate <a
            rel="noopener noreferrer"
            href="https://github.com/nanocss/nano/tree/v2/scss/themes/"
            target="_blank"
          >Nano's default theme</a> as a starting point and customise it to match your brand's style).
        </li>
      </ol>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block>
          <pre>// your custom theme
@use &quot;path/custom-theme&quot;;

// nano without default theme
@use &quot;nano&quot; with (
  $modules: (
    &quot;themes/default&quot;: false
  )
);</pre>
        </code-block>
      </div>
    </section>
  </div>
</template>
