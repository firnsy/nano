<!--
  This file is part of nano.

  Author(s):
   - Ian Firns <firnsy@gmail.com>

  License: MIT
-->
<script setup>
import { computed, ref } from 'vue';
import CodeBlock from '@/components/CodeBlock.vue';
import NDataTable from '@/components/NDataTable.vue';
import NFilter from '@/components/NFilter.vue';
import NModal from '@/components/NModal.vue';
import NPager from '@/components/NPager.vue';

import { isDefined } from '@/utilities/is';
import {
  CriterionTypes,
  encodeFilter, useDataTableState
} from '@/composables/useDataTable';

const columnsDefinition = [
  {
    name: 'id',
    label: 'ID',
    sortable: false,
    filterable: true,
    type: 'number'
  },
  {
    name: 'first_name',
    label: 'First Name',
    sortable: true,
    filterable: true,
    type: 'string'
  },
  {
    name: 'last_name',
    label: 'Last Name',
    visible: false,
    sortable: true,
    filterable: true,
    type: 'string'
  },
  {
    name: 'phone_1',
    label: 'Phone 1',
    sortable: true,
    filterable: true,
    type: 'string'
  },
  {
    name: 'phone_2',
    label: 'Phone 2',
    visible: false,
    sortable: true,
    filterable: true,
    type: 'string'
  },
  {
    name: 'subscription_date',
    label: 'Subscription Date',
    sortable: true,
    filterable: true,
    type: 'date'
  },
  {
    name: 'dob',
    label: 'D.O.B.',
    visible: false,
    sortable: true,
    filterable: true,
    type: 'datetime'
  }
];

const fetchLocation = import.meta.env.PROD ? window.location.origin : 'http://localhost:3333/';

const {
  refresh, columns, rows,
  filterNew, filterUpdate, filterCount,
  pager, pagerUpdate,
  selection, selectionUpdate,
  sorter, sorterUpdate
} = useDataTableState({
  fetch: async (...args) => fetch(fetchLocation, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(args[0])
  }).then((res) => res.json()),
  columns: columnsDefinition,
  selectionColumn: 'id'
});

const filter = ref(filterNew());
const filterOptions = {
  criterionTypes: CriterionTypes
};

const modalOpen = ref(false);

const filterEncoded = ref([]);

const pagerEncoded = computed(() => {
  const { page, pageSize } = pager.value;

  return {
    page,
    pageSize
  };
});

const selectionEncoded = computed(() => {
  const { global, indexes } = selection.value;

  return {
    global,
    indexes: Object.keys(indexes).map((index) => parseInt(index, 10))
  };
});

// FUNCTIONS

function closeModal() {
  modalOpen.value = false;
}

function applyFilter() {
  filterUpdate(filter);
  filterEncoded.value = encodeFilter(filter.value);

  closeModal();
}

function clearFilter() {
  filter.value.filters.splice(0, filter.value.filters.length);
  filterUpdate(filter);

  closeModal();
}

function openModal() {
  modalOpen.value = true;
}

function tableUpdate(data) {
  const { selection: selectionData, sort: sortData } = data;

  if (isDefined(selectionData)) {
    selectionUpdate(selectionData.index, selectionData.state, selectionData.global);
  }

  if (isDefined(sortData)) {
    sorterUpdate(sortData);
  }
}

function updatePageSize(event) {
  pagerUpdate({
    pageSize: parseInt(event.target.value, 10)
  });
}
</script>

<template>
  <hgroup>
    <p class="chapter">
      Vue Components
    </p>
    <h1>Data Tables</h1>
    <p>
      Nano's <code>&lt;n-data-table&gt;</code>, <code>&lt;n-pager&gt;</code>, and
      <code>&lt;n-filter&gt;</code> provides a lean paginated, sortable and filterable
      connection to backend APIs.
    </p>
  </hgroup>
  <aside id="table-of-contents">
    <nav>
      <details open>
        <summary>Content</summary>
        <ul>
          <li>
            <a
              class="secondary"
              href="/docs/vue/data-table"
            >Syntax</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/vue/data-table/#paging"
            >Paging</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/vue/data-table/#filtering"
            >Filtering</a>
          </li>
        </ul>
      </details>
    </nav>
  </aside>
  <div role="document">
    <section>
      <article>
        <n-data-table
          :selection
          :columns
          :rows
          :sorter
          @update="tableUpdate"
        />
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block language="xml">
            <pre>&lt;n-data-table
  :selection
  :columns
  :rows
  :sorter
  @update="tableUpdate"
/&gt;</pre>
          </code-block>
        </footer>
      </article>
      <p>The available attributes for <code>&lt;n-data-table&gt;</code> are:</p>
      <ul>
        <li><code>selection</code> - The current selection state of the table.</li>
        <li><code>columns</code> - The columns to display in the table.</li>
        <li><code>rows</code> - The rows to display in the table.</li>
        <li><code>sorter</code> - The current sort state of the table.</li>
      </ul>
      <p>The available events for <code>&lt;n-data-table&gt;</code> are:</p>
      <ul>
        <li><code>selection-update</code> - Emitted when the selection state has changed.</li>
        <li><code>sorter-update</code> - Emitted when the sort state has changed.</li>
        <li><code>update</code> - Emitted when the table state has changed.</li>
      </ul>
    </section>

    <section>
      <h3>
        Selection<a
          id="selection"
          href="#selection"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h3>
      <p>
        The <code>selection</code> attribute is an object that contains the following properties:
      </p>
      <ul>
        <li><code>column</code> - The name of the column that contains the unique identifier.</li>
        <li><code>global</code> - The current global selection state of the table.</li>
        <li>
          <code>indexes</code> - An object whose keys represent the selected indicies relative to
          the global state. All values should be <code>true</code>.
        </li>
      </ul>
      <p>
        If the global state is <code>false</code>, then no rows are selected, any indexes specified
        have been selected. If the global state is <code>true</code>, then all rows are considered
        selected and therefore any indexes specified are considered as unselected.
      </p>
      <div
        class="code"
        data-theme="dark"
      >
        <pre><code>{{ selection }}</code></pre>
      </div>
    </section>

    <section>
      <h3>
        Selection API<a
          id="selection-api"
          href="#selection-api"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h3>
      <p>
        When communicating with a backend API, the <code>selection</code> attribute should be used
        to determine which rows are to be processed by the server. The format should either be the
        form described above, or the slightly condensed form described below.
      </p>
      <div
        class="code"
        data-theme="dark"
      >
        <pre><code>{{ selectionEncoded }}</code></pre>
      </div>
    </section>

    <section>
      <h3>
        Columns<a
          id="columns"
          href="#columns"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h3>
      <p>
        The <code>columns</code> attribute is an array of objects that represent the columns to
        display in the table. Each object should contain the following properties:
      </p>
      <ul>
        <li><code>name</code> - The name of the column.</li>
        <li><code>label</code> - The label to display in the table header.</li>
        <li><code>visible</code> - Whether or not the column is visible.</li>
        <li><code>sortable</code> - Whether or not the column is sortable.</li>
        <li><code>filterable</code> - Whether or not the column is filterable.</li>
        <li>
          <code>type</code> - The type of data in the column. This is used for sorting, filtering
          and rendering.
        </li>
      </ul>
      <p>The <code>type</code> attribute can be one of the following:</p>
      <ul>
        <li><code>string</code> - A string value.</li>
        <li><code>number</code> - A numeric value.</li>
        <li><code>date</code> - A date value.</li>
        <li><code>datetime</code> - A date and time value.</li>
      </ul>
      <p>The order of the columns in the array is the order they will be displayed in the table.</p>
      <article>
        <div class="overflow-auto">
          <table>
            <thead>
              <tr>
                <th>Column</th>
                <th>Label</th>
                <th class="text-center">
                  Visible
                </th>
                <th class="text-center">
                  Sortable
                </th>
                <th class="text-center">
                  Filterable
                </th>
                <th>Type</th>
              </tr>
            </thead>
            <tbody>
              <tr
                v-for="column in columns"
                :key="column.name"
              >
                <td>{{ column.name }}</td>
                <td>{{ column.label }}</td>
                <td class="text-center">
                  <input
                    v-model="column.visible"
                    type="checkbox"
                  >
                </td>
                <td class="text-center">
                  <input
                    v-model="column.sortable"
                    type="checkbox"
                  >
                </td>
                <td class="text-center">
                  <input
                    v-model="column.filterable"
                    type="checkbox"
                  >
                </td>
                <td>{{ column.type }}</td>
              </tr>
              <tr v-if="!columns.length">
                <td colspan="6">
                  No columns defined.
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </article>
    </section>

    <section>
      <h3>
        Rows<a
          id="rows"
          href="#rows"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h3>
      <p>
        The <code>rows</code> attribute is an array of objects that represent the rows to display in
        the table. Each object should contain the following properties:
      </p>
      <ul>
        <li><code>id</code> - The unique identifier for the row.</li>
        <li><code>...</code> - Any other properties that match the column names.</li>
      </ul>
    </section>

    <section>
      <h2>
        Paging<a
          id="paging"
          href="#paging"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <article>
        <n-pager
          :pager
          @update="pagerUpdate"
        />
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block language="xml">
            <pre>&lt;n-pager
  :pager
  @update="pagerUpdate"
/&gt;</pre>
          </code-block>
        </footer>
      </article>
      <p>The pager attribute is an object that contains the following properties:</p>
      <ul>
        <li><code>page</code> - The current page number.</li>
        <li><code>pageSize</code> - The number of rows to display per page.</li>
        <li><code>itemCount</code> - The total number of rows.</li>
        <li><code>pageCount</code> - The total number of pages.</li>
        <li><code>firstItem</code> - The 1-based index of the first row on the current page.</li>
        <li><code>lastItem</code> - The 1-based index of the last row on the current page.</li>
      </ul>
      <p>The available events for <code>&lt;n-pager&gt;</code> are:</p>
      <ul>
        <li><code>update</code> - Emitted when the pager state has changed.</li>
      </ul>
      <div
        class="code"
        data-theme="dark"
      >
        <pre><code>{{ pager }}</code></pre>
      </div>
    </section>
    <section>
      <h3>
        Paging API<a
          id="pager-api"
          href="#pager-api"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h3>
      <p>
        When communicating with a backend API, the <code>pager</code> attribute should be used to
        determine which rows are to be processed by the server. The format is described above with
        only the <code>page</code> and <code>pageSize</code> being required, all other attributes
        should be ignored by the server.
      </p>
      <div
        class="code"
        data-theme="dark"
      >
        <pre><code>{{ pagerEncoded }}</code></pre>
      </div>
    </section>

    <section>
      <h2>
        Filtering<a
          id="filtering"
          href="#filtering"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <article>
        <n-filter
          :filter
          :columns
          :filter-options
        />
        <button @click="applyFilter">
          Apply Filter
        </button>
        <button @click="clearFilter">
          Clear Filter
        </button>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block language="xml">
            <pre>&lt;n-filter
  :filter
  :columns
  :filter-options
  @filter-update="onFilterUpdate"
/&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>

    <section>
      <h3>
        Filter API<a
          id="filter-api"
          href="#filter-api"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h3>
      <p>
        When communicating with a backend API, the <code>filter</code> attribute should be used to
        determine which rows are to be processed by the server. The format is described below.
      </p>
      <div
        class="code"
        data-theme="dark"
      >
        <pre><code>{{ filterEncoded }}</code></pre>
      </div>
    </section>

    <section>
      <h2>
        Styling<a
          id="styling"
          href="#styling"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p>
        Nano's <code>&lt;n-data-table&gt;</code>, <code>&lt;n-pager&gt;</code>, and
        <code>&lt;n-filter&gt;</code> utilise the base content and components to provide the
        necessary DOM structures that aligning to a11y recommendations best practices with minimal
        opinion on style.
      </p>
      <p>
        Styling is left to the developer to ensure that the components align with the overall design
        of the application.
      </p>
      <article
        id="style-example"
        data-theme="light"
      >
        <div class="header-bar">
          <button
            data-tooltip="Refresh"
            @click="refresh"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              height="16"
              width="16"
              viewBox="0 0 512 512"
            >
              <path d="M483.5 28.5L431.4 80.7C386.5 35.8 324.5 8 256 8 123.2 8 14.8 112.3 8.3 243.5 8 250.3 13.5 256 20.3 256h28c6.4 0 11.6-5 12-11.3C66.2 141.6 151.5 60 256 60c54.2 0 103.2 21.9 138.6 57.4l-54.1 54.1c-7.6 7.6-2.2 20.5 8.5 20.5H492c6.6 0 12-5.4 12-12V37c0-10.7-12.9-16-20.5-8.5zM491.7 256h-28c-6.4 0-11.6 5-12 11.3C445.8 370.4 360.5 452 256 452c-54.2 0-103.2-21.9-138.6-57.4l54.1-54.1c7.6-7.6 2.2-20.5-8.5-20.5H20c-6.6 0-12 5.4-12 12v143c0 10.7 12.9 16 20.5 8.5L80.7 431.4C125.5 476.2 187.5 504 256 504c132.8 0 241.2-104.3 247.7-235.5 .3-6.8-5.2-12.5-12-12.5z" />
            </svg>
          </button>
          <button
            class="filter"
            data-tooltip="Filters"
            @click="openModal"
          >
            <span>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="16"
                width="16"
                viewBox="0 0 512 512"
              >
                <path d="M464 0H48.1C5.4 0-16.1 51.7 14.1 81.9L176 243.9V416c0 15.1 7.1 29.3 19.2 40l64 47.1c31.3 21.9 76.8 1.5 76.8-38.4V243.9L497.9 81.9C528 51.8 506.7 0 464 0zM288 224v240l-64-48V224L48 48h416L288 224z" />
              </svg>
            </span>
            <span class="filter-count">{{ filterCount }}</span>
          </button>
          <button
            class="columns"
            data-tooltip="Columns"
          >
            <span>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="16"
                width="16"
                viewBox="0 0 512 512"
              >
                <path d="M464 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zM232 432H54a6 6 0 0 1 -6-6V112h184v320zm226 0H280V112h184v314a6 6 0 0 1 -6 6z" />
              </svg>
            </span>
          </button>
        </div>
        <n-modal :open="modalOpen">
          <h2>Filter</h2>
          <n-filter
            :filter
            :columns
            :filter-options
          />
          <footer>
            <button
              class="secondary"
              @click="closeModal"
            >
              Cancel
            </button>
            <button @click="clearFilter">
              Clear Filter
            </button>
            <button
              class="primary"
              @click="applyFilter"
            >
              Apply Filter
            </button>
          </footer>
        </n-modal>

        <n-data-table
          :selection
          :columns
          :rows
          :sorter
          @update="tableUpdate"
        />

        <div class="footer-bar">
          <select
            id="page-size"
            name="page-size"
            @change="updatePageSize($event)"
          >
            <option
              value="10"
              selected
            >
              10
            </option>
            <option value="25">
              25
            </option>
            <option value="50">
              50
            </option>
            <option value="100">
              100
            </option>
          </select>

          <n-pager
            :pager
            @update="pagerUpdate"
          />
        </div>
      </article>
    </section>
  </div>
</template>

<style lang="scss">
  #style-example {
    font-size: .75rem;

    .header-bar {
      display: flex;
      justify-content: flex-end;

      margin-bottom: 1rem;

      button {
        background-color: #fff;
        border: 1px solid #e6e6e6;
        border-radius: .5rem;
        box-shadow: 1px 1px 2px 0px rgba(192,192,192,0.5);
        color: #000;
        font-size: .75rem;
        vertical-align: middle;

        &:not(:first-child) {
          margin-left: .25rem;
        }

        .filter-count {
          background-color: #3371e1;
          border-radius: 25%;
          color: #fff;
          display: inline-block;
          font-size: 0.625rem;
          font-weight: bold;
          height: 1rem;
          line-height: 1rem;
          margin-left: .25rem;
          padding: 0 .25rem;
          text-align: center;
        }
      }
    }

    .footer-bar {
      display: flex;
      justify-content: space-between;
      align-items: stretch;
    }

    #page-size {
      border: 1px solid #e6e6e6;
      border-radius: .5rem;
      box-shadow: 1px 1px 2px 0px rgba(192,192,192,0.5);
      color: #000;
      font-size: .75rem;
      margin-right: 1rem;
      padding: .125rem .5rem;
    }

    .n-data-table {
      margin-bottom: 1rem;

      [type=checkbox] {
        height: .875rem;
        width: .875rem;
      }

      tbody {
        tr td {
        }

        tr:hover td {
          background-color: var(--nano-color);
          color: var(--nano-background-color);
        }
      }
    }

    .n-filter-group,
    .n-filter-criterion {

      * {
        font-size: .75rem;
      }

      button {
        background: #fff;
        border: 1px solid #e6e6e6;
        border-radius: .5rem;
        box-shadow: 1px 1px 2px 0px rgba(192,192,192,0.5);
        color: #000;
        padding: .5rem;
        width: 3rem;
      }
    }

    footer {
      display: flex;
      justify-content: flex-end;

      button {
        background: #fff;
        background-color: #e14946;
        border: 1px solid #e6e6e6;
        border-radius: .5rem;
        color: #fff;
        box-shadow: 1px 1px 2px 0px rgba(192,192,192,0.5);
        font-size: .75rem;

        &.primary {
          background: #3371e1;
          color: #fff;
        }

        &.secondary {
          background: #e6e6e6;
          color: #000;
        }
      }
    }

    .n-pager {
      ul {
        border: 1px solid #e6e6e6;
        border-radius: .5rem;
        box-shadow: 1px 1px 2px 0px rgba(192,192,192,0.5);

        li {
          padding: 0;

          a {
            background: transparent;
            border-radius: .25rem;
            color: #000;
            font-size: .75rem;
            margin: .25rem;
            padding: .125rem .5rem;
            text-decoration: none;

            &[disabled] {
              color: #ccc;
            }

            &:hover {
              background-color: #e6e6e6;
            }

            &[aria-selected] {
              background-color: #3371e1;
              color: #fff;
              font-weight: bold;
            }

            svg {
              margin-top: -0.125rem;
            }
          }
        }
      }
    }
  }

  :root:not([data-theme=dark]), [data-theme=light] {
    #style-example {
      .n-data-table {
      }

      .n-pager {
      }
    }
  }
</style>
