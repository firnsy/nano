<!--
  This file is part of nano.

  Author(s):
   - Ian Firns <firnsy@gmail.com>

  License: MIT
-->
<script setup>
import CodeBlock from '@/components/CodeBlock.vue';
import NForm from '@/components/NForm.vue';
import NInput from '@/components/NInput.vue';
import NInputError from '@/components/NInputError.vue';
import NPassword from '@/components/NPassword.vue';
import { reactive } from 'vue';
import { useValidator } from '@/composables/useValidator';

const schema = {
  foo: {
    rules: {
      required: {},
      email: {},
    },
  },
  bar: {
    rules: {
      required: {},
    },
  },
  baz: {
    rules: {
      min: { value: 3 },
    },
  },
};

const source = reactive({foo: "", bar: null, baz: ""});

const data = reactive({foo: "", bar: null});
const { validity, blur } = useValidator(data, schema);

const d = reactive({foo: ""});
const { validity: v, blur: b } = useValidator(d, schema);

const rulesData = reactive({
  alpha: "",
  alphanum: "",
  digits: "",
  email: "",
  max: null,
  min: null,
  minmax: null,
  required: "",
  required_if: "",
});

const { validity: rulesValidity, blur: rulesBlur } = useValidator(rulesData, {
  alpha: {
    rules: {
      alpha: {}
    }
  },
  alphanum: {
    rules: {
      alphanum: {}
    }
  },
  digits: {
    rules: {
      digits: {}
    }
  },
  email: {
    rules: {
      email: {}
    }
  },
  max: {
    rules: {
      max: { value: 10 }
    }
  },
  min: {
    rules: {
      min: { value: 10 }
    }
  },
  minmax: {
    rules: {
      minmax: { min: 10, max: 20 }
    }
  },
  required: {
    rules: {
      required: {}
    }
  },
  required_if: {
    rules: {
      required_if: { target: "foo" }
    }
  },
});
</script>

<template>
  <hgroup>
    <p class="chapter">
      Vue Components
    </p>
    <h1>Validation</h1>
    <p>Validation composition for forms and other data.</p>
  </hgroup>
  <aside id="table-of-contents">
    <nav>
      <details open="">
        <summary>Content</summary>
        <ul>
          <li>
            <a
              class="secondary"
              href="/docs/vue/validation"
            >Syntax</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/vue/validation#flags"
            >Flags</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/vue/validation#add-rules"
            >Adding Rules</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/vue/validation#available-rules"
            >Available Rules</a>
          </li>
        </ul>
      </details>
    </nav>
  </aside>
  <div role="document">
    <section>
      <p>Validation is a key part of any form, and nano provides a simple and flexible way to validate form data using a set of rules and messages. The validation system is powered by a rule-based system, where each field in the form can have one or more rules that are applied to it.</p>
      <p>Nano provides a number of high-level components and composable functions that make it easy to validate form data and display error messages to the user. The <code>useValidator</code> composable function provides the core validation logic, while the <code>NForm</code>, <code>NInput</code>, and <code>NInputError</code> components provide the UI components to display the form and error messages.</p>

      <article aria-label="rules-alphanum">
        <n-form :source :schema>
          <n-input name="foo" type="text" />
          <n-input-error name="foo" />
          <n-input name="bar" type="number" />
          <n-input-error name="bar" />
          <n-password name="baz" />
          <n-input-error name="baz" />
        </n-form>

        <footer class="code" data-theme="dark">
          <code-block>
            <pre>&lt;script setup&gt; 
import { reactive } from "vue";
import NForm from "@/components/NForm.vue";
import NInput from "@/components/NInput.vue";
import NInputError from "@/components/NInputError.vue";
import NPassword from '@/components/NPassword.vue';

const schema = {
  foo: {
    rules: {
      alpha: {},
      minmax: { min: 3, max: 8 },
    },
  },
  bar: {
    rules: {
      required: {},
      minmax: { min: 2, max: 6 },
    },
  },
  bar: {
    rules: {
      required: {},
      email: {},
    },
  }
}
const source = reactive({foo: "", bar: null, baz: ""});
&lt;/script&gt;

&lt;template&gt;
  &lt;n-form :source :schema&gt;
    &lt;n-input name="foo" type="text" /&gt;
    &lt;n-input-error name="foo" /&gt;
    &lt;n-input name="bar" type="number" /&gt;
    &lt;n-input-error name="bar" /&gt;
    &lt;n-ipassword name="baz" /&gt;
    &lt;n-input-error name="baz" /&gt;
  &lt;/n-form&gt;</pre>
          </code-block>
        </footer>
      </article>

      <p>The <code>NForm</code> component is a high-level component that wraps the form and provides the core validation logic. The <code>NInput</code> component is a low-level component that represents a form field, and the <code>NInputError</code> component is a low-level component that displays the error messages for a form field.</p>
      <p>Not all forms are created equally, Nano deliberately seperates the error message from the input field, this allows for more flexibility in the layout and design of the form. As long as your <code>NInput</code> and <code>NInputError</code> components sit within the same <code>NForm</code> component, they will be linked together.</p>
    </section>
    <section>
      <h3>Composable<a id="usage" href="#usage" class="secondary" tabindex="-1" >#</a></h3>
      <p>The <code>useValidator</code> composable provides a simple way to validate form data using a set of rules and messages and provides the core validation to <code>NForm</code>, <code>NInput</code> and <code>NInputError</code> components.</p>
      <article aria-label="Composable Example">
        <input v-model="data.foo" type="text" aria-label="Text" :aria-invalid="validity.foo.ariaInv" aria-describedby="foo-error" @blur="blur('foo')" />
        <small id="foo-error" v-for="m in validity.foo.errors" role="alert" v-if="validity.foo.ariaInv">{{m}}</small>
        <input v-model="data.bar" type="number" aria-label="Number" :aria-invalid="validity.bar.ariaInv" @blur="blur('bar')" />
        <small v-for="m in validity.bar.errors" role="alert" v-if="validity.bar.ariaInv">{{m}}</small>
      </article>
    </section>
    <section>
      <h3>Usage<a id="usage" href="#usage" class="secondary" tabindex="-1" >#</a></h3>
      <p>The <code>useValidator</code> composable function takes two required arguments, and an optional third. The first is the data object that you want to validate, the second is the schema object that defines the rules for each field in the data object, and the third is the options object that allows you to customise the behavior of the validator.</p>

      <div class="code" data-theme="dark">
        <code-block>
          <pre>const {
  isFinished,
  pass,
  validity,
  blur,
  dirty,
  pristine,
  update,
  validate
} = useValidator(data, schema, options);</pre>
        </code-block>
      </div>

      <h4>Data</h4>
      <p>The data object is a reactive object that contains the form data that you want to validate. Each key in the data object represents a field in the form, and the value is the current value of that field.</p>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block>
          <pre>const data = reactive({foo: "", bar: null});</pre>    
        </code-block>
      </div>

      <h4>Schema</h4>
      <p>The schema object is a simple object that has the same keys as the data object, but the values are objects that define the rules for that field.</p>
      <p>Here is an example of a schema object:</p>
      <div class="code" data-theme="dark">
        <code-block>
          <pre>const schema = {
  foo: {
    rules: {
      alpha: {},
      minmax: { min: 3, max: 8 },
    },
  },
  bar: {
    rules: {
      required: {},
      minmax: { min: 2, max: 6 },
    },
  }
    
}</pre>
        </code-block>
      </div>

      <p>The schema object defines two fields, <code>foo</code> and <code>bar</code>, each with their own set of rules. The <code>foo</code> field has two rules, <code>alpha</code> and <code>minmax</code>, while the <code>bar</code> field has two rules, <code>required</code> and <code>minmax</code>.</p>
      <p>The <code>alpha</code> rule checks if the field value contains only alphabetic characters, while the <code>minmax</code> rule checks if the field value is between the specified min and max values. The <code>required</code> rule checks if the field value is not empty.</p>

      <h4>Options</h4>
      <p>The options object is an object that allows you to customise the behavior of the validator. It has the following properties:</p>
      <table>
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Type</th>
            <th scope="col">Default</th>
            <th scope="col">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">bail</th>
            <td>boolean</td>
            <td>false</td>
            <td>Stop validation on the first failed rule.</td>
          </tr>
          <tr>
            <th scope="row">debounce</th>
            <td>number</td>
            <td>0</td>
            <td>The debounce time in milliseconds before the validation is triggered.</td>
          </tr>
          <tr>
            <th scope="row">immediate</th>
            <td>boolean</td>
            <td>true</td>
            <td>The validation is triggered immediately when the function is called.</td>
          </tr>
          <tr>
            <th scope="row">manual</th>
            <td>boolean</td>
            <td>false</td>
            <td>Disable automatic validation and trigger validation manually.</td>
          </tr>
          <tr>
            <th scope="row">maxWait</th>
            <td>number</td>
            <td>0</td>
            <td>The maximum time in milliseconds to wait before the validation is triggered.</td>
          </tr>
          <tr>
            <th scope="row">validator</th>
            <td>object</td>
            <td>{}</td>
            <td>A custom validator object that contains custom rules.</td>
          </tr>
        </tbody>
      </table>

      <h4>Example</h4>
      <p>Here is an example of how to use the <code>useValidator</code> composable function:</p>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block>
          <pre>&lt;script setup&gt;
import { reactive } from "vue";
import { useValidator } from "@/composables/useValidator";

const schema = {
  foo: {
    rules: {
      alpha: {},
      minmax: { min: 3, max: 8 },
    },
  },
  bar: {
    rules: {
      required: {},
      minmax: { min: 2, max: 6 },
    },
  }
}
const data = reactive({foo: "", bar: null});
const { validity, blur } = useValidator(data, schema);
&lt;/script&gt;

&lt;template&gt;
  &lt;input
    v-model="data.foo"
    type="text"
    aria-label="Text"
    aria-describedby="foo-error"
    :aria-invalid="validity.foo.ariaInv"
    @blur="blur('foo')"
  /&gt;
  &lt;small
    id="foo-error"
    v-for="e in validity.foo.errors"
    role="alert"
    v-if="validity.foo.ariaInv"
  &gt;&#x7B;&#x7B;e&#x7D;&#x7D;&lt;/small&gt;
  &lt;input
    v-model="data.bar"
    type="number"
    aria-label="Number"
    aria-describedby="bar-error"
    :aria-invalid="validity.bar.ariaInv"
    @blur="blur('bar')"
  /&gt;
  &lt;small
    id="bar-error"
    v-for="e in validity.bar.errors"
    role="alert"
    v-if="validity.bar.ariaInv"
  &gt;&#x7B;&#x7B;e&#x7D;&#x7D;&lt;/small&gt;
&lt;/template&gt;</pre>
        </code-block>
      </div>





      <p>The <code>useValidator</code> composable function returns and object with a number of properties that you can use to manipulate and determine the validity of the form fields.</p>
      
      <h4>isFinished</h4>
      <p>The <code>isFinished</code> property is a reactive boolean value that indicates if the validation has been completed.</p>

      <h4>pass</h4>
      <p>The <code>pass</code> property is a reactive boolean value that indicates if the validation has passed.</p>

      <h4>validity</h4>
      <p>The <code>validity</code> property is a reactive object that contains the validity state of each field in the form. Each key in the object is the name of a field in the form, and the value is an object that contains the validity state of that field.</p>

      <p>The validation flags are a set of boolean values that gives you information about the field you are validating, for example you may want to know if the field is currently valid, or if it has been blurred by the user.</p>
      <p>This is a table of all the flags available that you can access per field:</p>
      <table id="flags">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Type</th>
            <th scope="col">Default</th>
            <th scope="col">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">valid</th>
            <td>boolean</td>
            <td>false</td>
            <td>The field is valid.</td>
          </tr>
          <tr>
            <th scope="row">invalid</th>
            <td>boolean</td>
            <td>false</td>
            <td>The field is invalid.</td>
          </tr>
          <tr>
            <th scope="row">changed</th>
            <td>boolean</td>
            <td>false</td>
            <td>The field value has been changed.</td>
          </tr>
          <tr>
            <th scope="row">dirty</th>
            <td>boolean</td>
            <td>false</td>
            <td>The field value has been manipulated.</td>
          </tr>
          <tr>
            <th scope="row">pristine</th>
            <td>boolean</td>
            <td>true</td>
            <td>The field value was not manipulated.</td>
          </tr>
          <tr>
            <th scope="row">required</th>
            <td>boolean</td>
            <td>false</td>
            <td>The field is required.</td>
          </tr>
          <tr>
            <th scope="row">touched</th>
            <td>boolean</td>
            <td>false</td>
            <td>The field has been blurred.</td>
          </tr>
          <tr>
            <th scope="row">untouched</th>
            <td>boolean</td>
            <td>true</td>
            <td>The field has not been blurred.</td>
          </tr>
          <tr>
            <th scope="row">validated</th>
            <td>boolean</td>
            <td>false</td>
            <td>The field has been validated at least once.</td>
          </tr>
          <tr>
            <th scope="row">passed</th>
            <td>boolean</td>
            <td>false</td>
            <td>The field has been validated and is valid.</td>
          </tr>
          <tr>
            <th scope="row">failed</th>
            <td>boolean</td>
            <td>false</td>
            <td>The field has been validated and is invalid.</td>
          </tr>
          <tr>
            <th scope="row">ariaInv</th>
            <td>string</td>
            <td>""</td>
            <td>The field invalidity should be announced to the user.</td>
          </tr>
        </tbody>
      </table>
      <article aria-label="Nav example">
        <input v-model="d.foo" type="text" aria-label="Text" aria-describedby="v-foo-error" :aria-invalid="v.foo.ariaInv" @blur="b('foo')" />
        <small id="v-foo-error" v-for="e in v.foo.errors" role="alert" v-if="v.foo.ariaInv">{{e}}</small>

        <div class="grid states">
          <div :class="[v.foo.valid ? 'on' : 'off']">valid: {{v.foo.valid}}</div>
          <div :class="[v.foo.invalid ? 'on' : 'off']">invalid: {{v.foo.invalid}}</div>
          <div :class="[v.foo.changed ? 'on' : 'off']">changed: {{v.foo.changed}}</div>
        </div>
        <div class="grid states">
          <div :class="[v.foo.dirty ? 'on' : 'off']">dirty: {{v.foo.dirty}}</div>
          <div :class="[v.foo.pristine ? 'on' : 'off']">pristine: {{v.foo.pristine}}</div>
          <div :class="[v.foo.required ? 'on' : 'off']">required: {{v.foo.required}}</div>
        </div>
        <div class="grid states">
          <div :class="[v.foo.touched ? 'on' : 'off']">touched: {{v.foo.touched}}</div>
          <div :class="[v.foo.untouched ? 'on' : 'off']">untouched: {{v.foo.untouched}}</div>
          <div :class="[v.foo.ariaInv ? 'on' : 'off']">ariaInv: {{v.foo.ariaInv}}</div>
        </div>
        <div class="grid states">
          <div :class="[v.foo.validated ? 'on' : 'off']">validated: {{v.foo.validated}}</div>
          <div :class="[v.foo.passed ? 'on' : 'off']">passed: {{v.foo.passed}}</div>
          <div :class="[v.foo.failed ? 'on' : 'off']">failed: {{v.foo.failed}}</div>
        </div>
      </article>

      <h4>blur()</h4>
      <p>The <code>blur</code> function is a function that you can call to manually blur a field. This is useful when you want to trigger the validation of a field manually or indicate that the field has been visited by the user.</p>

      <h4>dirty()</h4>
      <p>The <code>dirty</code> function is a function that you can call to manually mark a field as dirty. This is useful when you want to indicate that the field value has been manipulated.</p>

      <h4>pristine()</h4>
      <p>The <code>pristine</code> function is a function that you can call to manually mark a field as pristine. This is useful when you want to indicate that the field value has not been manipulated.</p>

      <h4>update()</h4>
      <p>The <code>update</code> function is a function that you can call to manually update the value of a field. This is useful when you want to update the value of a field without triggering the validation.</p>

      <h4>validate()</h4>
      <p>The <code>validate</code> function is a function that you can call to manually validate the form. This is useful when you want to trigger the validation of the entire form manually.</p>
    </section>



    <section>
      <h3>Adding Rules<a id="add-rules" href="#add-rules" class="secondary" tabindex="-1" >#</a></h3>
      <p>nano comes with a number of validation rules by default which work for most use cases. Additional rules can be added, or pre-existing rules modified via the extend function.</p>
      <p>Adding new rules with extend is straight forward, in its simplest form it looks like this:</p>
      <div
        class="code"
        data-theme="dark"
      >
        <code-block><pre>import { Validator } from 'nano';

validator = new Validator();
validator.rule('positive', value => {
return value >= 0;
});
        </pre></code-block>
      </div>

      <p>That last snippet can be placed any where in your app, typically you should define your rules before you use them in your template, so your entry file or a dedicated validation.js file is a great place to start.</p>
      <p>The extend function accepts the name of the rule and the validator function to use for that rule.</p>
      <p>You can use the newly defined positive rule in a schema definition like this:</p>

      <div class="code" data-theme="dark">
        <code-block>
          <pre>
const schema = {
  foo: {
    rules: {
      positive: {},
    },
  },
};</pre>
        </code-block>
      </div>

      <p>That's it! You have successfully added a new rule to your validation schema.</p>
    </section>
    <section>
      <h3>Avaliable Rules<a id="selection" href="#available-rules" class="secondary" tabindex="-1" >#</a></h3>
      <p>Nano's validation system is powered by a rule-based system, each field can have one or more rules that are applied to it. The rules are simple functions that return a boolean value, true if the field is valid, and false if it's not.</p>
      <p>Here is a list of the built-in rules that come with nano:</p>
      <table>
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">alpha</th>
            <td>The field value must contain only alphabetic characters.</td>
          </tr>
          <tr>
            <th scope="row">alphanum</th>
            <td>The field value must contain only alpha-numeric characters.</td>
          </tr>
          <tr>
            <th scope="row">email</th>
            <td>The field value must be a valid email address.</td>
          </tr>
          <tr>
            <th scope="row">max</th>
            <td>The field value must be less than or equal to the specified max value.</td>
          </tr>
          <tr>
            <th scope="row">min</th>
            <td>The field value must be greater than or equal to the specified min value.</td>
          </tr>
          <tr>
            <th scope="row">minmax</th>
            <td>The field value must be between the specified min and max values.</td>
          </tr>
          <tr>
            <th scope="row">required</th>
            <td>The field value must not be empty.</td>
          </tr>
          <tr>
            <th scope="row">required_if</th>
            <td>The field is required if the target field is not empty.</td>
          </tr>
        </tbody>
      </table>


      <h4>alpha</h4>
      <p>The <code>alpha</code> rule checks if the field value contains only alphabetic characters.</p>

      <article aria-label="rules-alpha">
        <input v-model="rulesData.alpha" type="text" aria-label="Text" aria-describedby="rules-alpha-error" :aria-invalid="rulesValidity.alpha.ariaInv" @blur="rulesBlur('alpha')" />
        <small id="rules-alpha-error" v-for="e in rulesValidity.alpha.errors" role="alert" v-if="rulesValidity.alpha.ariaInv">{{e}}</small>

        <footer class="code" data-theme="dark">
        <code-block>
          <pre>import { useValidator } from 'nano';
const data = reactive({ val: "" });
const { validity, blur } = useValidator(data, {
  val: {
    rules: {
      alpha: {}
    }
  }
});</pre>
        </code-block>
        </footer>
      </article>

      <p>There are no parameters for the alpha rule. When no parameters are required, you can simply pass an empty object to the rules property.</p>


      <h4>alphanum</h4>
      <p>The <code>alphanum</code> rule checks if the field value contains only alpha-numeric characters.</p>

      <article aria-label="rules-alphanum">
        <input v-model="rulesData.alphanum" type="text" aria-label="Text" aria-describedby="rules-alphanum-error" :aria-invalid="rulesValidity.alphanum.ariaInv" @blur="rulesBlur('alphanum')" />
        <small id="rules-alphanum-error" v-for="e in rulesValidity.alphanum.errors" role="alert" v-if="rulesValidity.alphanum.ariaInv">{{e}}</small>

        <footer class="code" data-theme="dark">
          <code-block>
            <pre>import { useValidator } from 'nano';
  const data = reactive({ val: "" });
  const { validity, blur } = useValidator(data, {
    val: {
      rules: {
        alphanum: {}
      }
    }
  });</pre>
          </code-block>
        </footer>
      </article>

      <h4>digits</h4>
      <p>The <code>digits</code> rule checks if the field value contains only digits.</p>

      <article aria-label="rules-digits">
        <input v-model="rulesData.digits" type="text" aria-label="Text" aria-describedby="rules-digits-error" :aria-invalid="rulesValidity.digits.ariaInv" @blur="rulesBlur('digits')" />
        <small id="rules-digits-error" v-for="e in rulesValidity.digits.errors" role="alert" v-if="rulesValidity.digits.ariaInv">{{e}}</small>

        <footer class="code" data-theme="dark">
          <code-block>
            <pre>import { useValidator } from 'nano';
  const data = reactive({ val: "" });
  const { validity, blur } = useValidator(data, {
    val: {
      rules: {
        digits: {}
      }
    }
  });</pre>
          </code-block>
        </footer>
      </article>

      <h4>email</h4>
      <p>The <code>email</code> rule checks if the field value is a valid email address.</p>

      <article aria-label="rules-email">
        <input v-model="rulesData.email" type="text" aria-label="Text" aria-describedby="rules-email-error" :aria-invalid="rulesValidity.email.ariaInv" @blur="rulesBlur('email')" />
        <small id="rules-email-error" v-for="e in rulesValidity.email.errors" role="alert" v-if="rulesValidity.email.ariaInv">{{e}}</small>

        <footer class="code" data-theme="dark">
          <code-block>
            <pre>import { useValidator } from 'nano';
  const data = reactive({ val: "" });
  const { validity, blur } = useValidator(data, {
    val: {
      rules: {
        email: {}
      }
    }
  });</pre>
          </code-block>
        </footer>
      </article>

      <h4>max</h4>
      <p>The <code>max</code> rule checks if the field value is less than or equal to the specified max value. This rule is type aware, for numbers it checks if the value is less than or equal to the specified max value, for strings it checks if the length of the string is less than or equal to the specified max value.</p>

      <article aria-label="rules-max">
        <input v-model="rulesData.max" type="number" aria-label="Number" aria-describedby="rules-max-error" :aria-invalid="rulesValidity.max.ariaInv" @blur="rulesBlur('max')" />
        <small id="rules-max-error" v-for="e in rulesValidity.max.errors" role="alert" v-if="rulesValidity.max.ariaInv">{{e}}</small>

        <footer class="code" data-theme="dark">
          <code-block>
            <pre>import { useValidator } from 'nano';
  const data = reactive({ val: "" });
  const { validity, blur } = useValidator(data, {
    val: {
      rules: {
        max: { value: 10 }
      }
    }
  });</pre>
          </code-block>
        </footer>
      </article>

      <table>
        <thead>
          <tr>
            <th scope="col">Param</th>
            <th scope="col">Required</th>
            <th scope="col">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>value</td>
            <td>Y</td>
            <td>The maximum value the field value must be less than or equal to.</td>
          </tr>
        </tbody>
      </table>

      <h4>min</h4>
      <p>The <code>min</code> rule checks if the field value is greater than or equal to the specified min value. This rule is type aware, for numbers it checks if the value is greater than or equal to the specified min value, for strings it checks if the length of the string is greater than or equal to the specified min value.</p>

      <article aria-label="rules-min">
        <input v-model="rulesData.min" type="number" aria-label="Number" aria-describedby="rules-min-error" :aria-invalid="rulesValidity.min.ariaInv" @blur="rulesBlur('min')" />
        <small id="rules-min-error" v-for="e in rulesValidity.min.errors" role="alert" v-if="rulesValidity.min.ariaInv">{{e}}</small>

        <footer class="code" data-theme="dark">
          <code-block>
            <pre>import { useValidator } from 'nano';
  const data = reactive({ val: "" });
  const { validity, blur } = useValidator(data, {
    val: {
      rules: {
        min: { value: 10 }
      }
    }
  });</pre>
          </code-block>
        </footer>
      </article>

      <table>
        <thead>
          <tr>
            <th scope="col">Param</th>
            <th scope="col">Required</th>
            <th scope="col">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>value</td>
            <td>Y</td>
            <td>The minimum value the field value must be greater than or equal to.</td>
          </tr>
        </tbody>
      </table>

      <h4>minmax</h4>
      <p>The <code>minmax</code> rule checks if the field value is between the specified min and max values. This rule is type aware, for numbers it checks if the value is between the specified min and max values, for strings it checks if the length of the string is between the specified min and max values.</p>

      <article aria-label="rules-minmax">
        <input v-model="rulesData.minmax" type="number" aria-label="Number" aria-describedby="rules-minmax-error" :aria-invalid="rulesValidity.minmax.ariaInv" @blur="rulesBlur('minmax')" />
        <small id="rules-minmax-error" v-for="e in rulesValidity.minmax.errors" role="alert" v-if="rulesValidity.minmax.ariaInv">{{e}}</small>

        <footer class="code" data-theme="dark">
          <code-block>
            <pre>import { useValidator } from 'nano';
  const data = reactive({ val: "" });
  const { validity, blur } = useValidator(data, {
    val: {
      rules: {
        minmax: { min: 10, max: 20 }
      }
    }
  });</pre>
          </code-block>
        </footer>
      </article>

      <table>
        <thead>
          <tr>
            <th scope="col">Param</th>
            <th scope="col">Required</th>
            <th scope="col">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>min</td>
            <td>Y</td>
            <td>The minimum value the field value must be greater than or equal to.</td>
          </tr>
          <tr>
            <td>max</td>
            <td>Y</td>
            <td>The maximum value the field value must be less than or equal to.</td>
          </tr>
        </tbody>
      </table>

      <h4>required</h4>
      <p>The <code>required</code> rule checks if the field value is not empty.</p>

      <article aria-label="rules-required">
        <input v-model="rulesData.required" type="text" aria-label="Text" aria-describedby="rules-required-error" :aria-invalid="rulesValidity.required.ariaInv" @blur="rulesBlur('required')" />
        <small id="rules-required-error" v-for="e in rulesValidity.required.errors" role="alert" v-if="rulesValidity.required.ariaInv">{{e}}</small>

        <footer class="code" data-theme="dark">
          <code-block>
            <pre>import { useValidator } from 'nano';
  const data = reactive({ val: "" });
  const { validity, blur } = useValidator(data, {
    val: {
      rules: {
        required: {}
      }
    }
  });</pre>
          </code-block>
        </footer>
      </article>

      <h4>required_if</h4>
      <p>The <code>required_if</code> rule checks if the field is required if the target field is not empty.</p>

      <article aria-label="rules-required_if">
        <input v-model="rulesData.required_if" type="text" aria-label="Text" aria-describedby="rules-required_if-error" :aria-invalid="rulesValidity.required_if.ariaInv" @blur="rulesBlur('required_if')" />
        <small id="rules-required_if-error" v-for="e in rulesValidity.required_if.errors" role="alert" v-if="rulesValidity.required_if.ariaInv">{{e}}</small>

        <footer class="code" data-theme="dark">
          <code-block>
            <pre>import { useValidator } from 'nano';
  const data = reactive({ val: "" });
  const { validity, blur } = useValidator(data, {
    val: {
      rules: {
        required_if: { target: 'foo' }
      }
    }
  });</pre>
          </code-block>
        </footer>
      </article>

      <table>
        <thead>
          <tr>
            <th scope="col">Param</th>
            <th scope="col">Required</th>
            <th scope="col">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>target</td>
            <td>Y</td>
            <td>The name of the target field that must not be empty for this field to be required.</td>
          </tr>
        </tbody>
      </table>

      <h3>Rules Object Expression</h3>
      <p>Rules can be expressed as objects instead of functions, this is useful when you want to add more metadata to your rules. Here is an example of the positive rule expressed as an object:</p>

      <div class="code" data-theme="dark">
        <code-block>
          <pre>import { Validator } from 'nano';
const validator = new Validator();
validator.rule('positive', {
  validate: value => {
    return value >= 0;
  }
});</pre>
        </code-block>
      </div>

      <p>Rules expressed as objects can have additional properties, one of them is the <code>message</code> property which allows you to define a custom error message for the rule. Here is an example of the positive rule with a custom error message:</p>

      <div class="code" data-theme="dark">
        <code-block>
          <pre>import { Validator } from 'nano';
const validator = new Validator();
validator.rule('positive', {
  validate: value => {
    return value >= 0;
  },
  message: 'The field must be a positive number.'
});</pre>
        </code-block>
      </div>

      <p>Rules expressed as objects can also have the <code>params</code> property which allows you to define the parameters that the rule requires. Here is an example of the min rule expressed as an object:</p>

      <div class="code" data-theme="dark">
        <code-block>
          <pre>import { Validator } from 'nano';
const validator = new Validator();
validator.rule('min', {
  validate: (value, args) => {
    return value.length >= args.length;
  },
  params: ['length']
});</pre>
        </code-block>
      </div>

      <h4>Transform</h4>
      <p>Sometimes it is necessary to transform a value before validation, possibly to coerce the value or to sanitize it in some way. To do this add a transform function to the validation rule. The property is transformed prior to validation and returned as promise result or callback result when pass validation. Here is an example of a rule that transforms the value before validation:</p>

      <div class="code" data-theme="dark">
        <code-block>
          <pre>import { Validator } from 'nano';
const validator = new Validator();
validator.rule('positive', {
  validate: value => {
    return value >= 0;
  },
  transform: value => {
    return parseInt(value);
  }
});</pre>
        </code-block>
      </div>


      <h4>Message</h4>
      <p>Nano generates error messages for your fields, the last examples had This field is invalid message which is the default message configured for all rules. You can change that by returning strings in the validation function itself:</p>>
      <div class="code" data-theme="dark">
        <code-block>
          <pre>import { Validator } from 'nano';
const validator = new Validator();
validator.rule('positive', {
  validate: value => {
    if (value >= 0) {
      return true;
    }

    return 'The field must be a positive number.';
  }
});</pre>
        </code-block>
      </div>

      <p>You can also leave out messages from the validator function and instead use the extended format to pass a dedicated message property:</p>

      <div class="code" data-theme="dark">
        <code-block>
          <pre>import { Validator } from 'nano';
const validator = new Validator();
validator.rule('positive', {
  validate: value => {
    reutrn value >= 0;
  },
  message: 'The field must be a positive number.'
});</pre>
        </code-block>
      </div>

      <p>This allows your validator functions to be much clearer.</p>

      <h5>Field Name Placeholders</h5>
      <p>Sometimes you want your messages to have the following format:</p>

      <div class="code" data-theme="dark">
        <code-block>
          <pre>The {_field_} must be positive.</pre>
        </code-block>
      </div>

      <p>The validator function doesn't accept the field name anywhere, but Nano offers simple interpolation mechanism for returned messages, you can use the <code>{_field_}</code> placeholder in your string and it will be replaced with your field name automatically</p>


      <h5>Argument Placeholders</h5>
      <p>You can't really have the min rule message to be "this field is invalid", this is not only confusing to the user, they will have no knowledge on how to fix them.</p>
      <p>Client-side validation is all about UX, so Nano interpolation can parse placeholders that match the rule parameters, so to define such a message for the min rule you can use a <code>{length}</code> placeholder in the error message like this:</p>

      <div class="code" data-theme="dark">
        <code-block>
          <pre>import { Validator } from 'nano';
const validator = new Validator();
validator.rule('min', {
  validate: (value, args) => {
    return value.length >= args.length;
  },
  params: ['length'],
  message: 'The field {_field_} must be at least {length} characters long.'
});</pre>
        </code-block>
      </div>

      <p>One thing to note is that the parameter placeholder doesn't have underscores <code>_</code> around it unlike the <code>{_field_}</code> placeholder. This is a convention of Nano as there are a special set of placeholders that have underscores around them. This is to prevent collisions and to make them distinct from rule parameters.</p>

      <h5>Messages as Functions</h5>
      <p>If using interpolated strings is not flexible enough for you, using functions is also allowed. When using a function as your message, it has to return a string. Function messages receive the field name and an object containing the placeholders mentioned earlier.</p>
      <p>This is the previous example but with a function as our message:</p>

      <div class="code" data-theme="dark">
        <code-block>
          <pre>import { Validator } from 'nano';
const validator = new Validator();
validator.rule('minmax', {
  validate: (value, { min, max }) => {
    return value.length >= min && value.length <= max;
  },
  params: ['min', 'max'],
  message: (fieldName, placeholders) => {
    return `The ${fieldName} field must have at least ${placeholders.min} characters and ${placeholders.max} characters at most`;
  }
});</pre>
        </code-block>
      </div>

      <p>This allows you to manually interpolate or generate messages depending on your needs, this will come in handy when implementing localization using popular plugins like vue-i18n.</p>
      <p>For reference these are the contents of the placeholders object:</p>

      <table>
        <thead>
          <tr>
            <th scope="col">Prop</th>
            <th scope="col">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><code>_field_</code></td>
            <td>The field name.</td>
          </tr>
          <tr>
            <td><code>_value_</code></td>
            <td>The field value that was validated.</td>
          </tr>
          <tr>
            <td><code>_rule_</code></td>
            <td>The rule name that triggered this message.</td>
          </tr>
        </tbody>
      </table>

      <p>Along side any parameters configured in the <code>params</code> array.</p>
    </section>
  </div>
</template>

<style lang="scss">
  #flags {
    thead {
      th:nth-child(2),
      th:nth-child(3) {
        text-align: center;
      }
    }
    tbody {
      tr {
        td:nth-child(2),
        td:nth-child(3) {
          text-align: center;
        }
      }
    }
  }

  .states {
    margin-bottom: 1rem;

    div {
      color: white;
      padding: 0.5rem;
      text-align: center;
    }

    .on {
      background-color: var(--nano-color-green-450)
    }
    .off {
      background-color: var(--nano-color-red-450);
    }
  }
</style>
