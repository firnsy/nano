<!--
  This file is part of nano.

  Author(s):
   - Ian Firns <firnsy@gmail.com>

  License: MIT
-->
<script setup>
import { RouterLink } from 'vue-router';
import CodeBlock from '@/components/CodeBlock.vue';
</script>

<template>
  <hgroup>
    <p class="chapter">
      Components
    </p>
    <h1>Dropdown</h1>
    <p>Create dropdown menus and custom selects with minimal and semantic HTML, without JavaScript.</p>
  </hgroup>
  <aside id="table-of-contents">
    <nav>
      <details open="">
        <summary>Content</summary>
        <ul>
          <li>
            <a
              class="secondary"
              href="/docs/dropdown"
              aria-current="page"
            >Syntax</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/dropdown#dropdowns-with-checkboxes-and-radios"
            >Checkboxes and radios</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/dropdown#button-variants"
            >Button variants</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/dropdown#validation-states"
            >Validation states</a>
          </li>
          <li>
            <a
              class="secondary"
              href="/docs/dropdown#usage-with-nav"
            >Usage with nav</a>
          </li>
        </ul>
      </details>
    </nav>
  </aside>
  <div role="document">
    <section>
      <p>
        Dropdowns are built with <code>&lt;details class="dropdown"&gt;</code> as a wrapper and
        <code>&lt;summary&gt;</code> and <code>&lt;ul&gt;</code> as direct childrens. Unless they are in a
        <RouterLink to="/docs/nav">
          Nav
        </RouterLink>, dropdowns are <code>width: 100%;</code> by default.
      </p>
      <p>For style consistency with the form elements, dropdowns are styled like a select by default.</p>
      <article
        aria-label="Dropdowns as selects"
        class="component"
      >
        <div class="grid">
          <details class="dropdown">
            <summary>Dropdown</summary>
            <ul>
              <li><a href="/docs/dropdown">Solid</a></li>
              <li><a href="/docs/dropdown">Liquid</a></li>
              <li><a href="/docs/dropdown">Gas</a></li>
              <li><a href="/docs/dropdown">Plasma</a></li>
            </ul>
          </details>
          <select required="">
            <option
              value=""
              disabled=""
              selected=""
            >
              Select
            </option>
            <option>Solid</option>
            <option>Liquid</option>
            <option>Gas</option>
            <option>Plasma</option>
          </select>
        </div>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;!-- Dropdown --&gt;
&lt;details class="dropdown"&gt;
  &lt;summary&gt;Dropdown&lt;/summary&gt;
  &lt;ul&gt;
    &lt;li&gt;&lt;a href="#"&gt;Solid&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Liquid&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Gas&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Plasma&lt;/a&gt;&lt;/li&gt;
  &lt;/ul&gt;
&lt;/details&gt;

&lt;!-- Select --&gt;
&lt;select required&gt;
  &lt;option selected disabled value=""&gt;Select&lt;/option&gt;
  &lt;option&gt;Solid&lt;/option&gt;
  &lt;option&gt;Liquid&lt;/option&gt;
  &lt;option&gt;Gas&lt;/option&gt;
  &lt;option&gt;Plasma&lt;/option&gt;false
&lt;/select&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>
    <section>
      <h2>
        Dropdowns with checkboxes and radios<a
          id="dropdowns-with-checkboxes-and-radios"
          href="#dropdowns-with-checkboxes-and-radios"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p>
        Dropdowns can be used as custom selects with <code>&lt;input type="radio"&gt;</code> or
        <code>&lt;input type="checkbox"&gt;</code>.
      </p>
      <article
        aria-label="Dropdowns with radio buttons or checkboxes"
        class="component"
      >
        <details class="dropdown">
          <summary>Select a phase of matter...</summary>
          <ul>
            <li>
              <label><input
                type="radio"
                name="phase"
                value="Solid"
              >Solid</label>
            </li>
            <li>
              <label><input
                type="radio"
                name="phase"
                value="Liquid"
              >Liquid</label>
            </li>
            <li>
              <label><input
                type="radio"
                name="phase"
                value="Gas"
              >Gas</label>
            </li>
            <li>
              <label><input
                type="radio"
                name="phase"
                value="Plasma"
              >Plasma</label>
            </li>
          </ul>
        </details>
        <details class="dropdown">
          <summary>Select phases of matter...</summary>
          <ul>
            <li>
              <label><input
                type="checkbox"
                name="solid"
                value="Solid"
              >Solid</label>
            </li>
            <li>
              <label><input
                type="checkbox"
                name="liquid"
                value="Liquid"
              >Liquid</label>
            </li>
            <li>
              <label><input
                type="checkbox"
                name="gas"
                value="Gas"
              >Gas</label>
            </li>
            <li>
              <label><input
                type="checkbox"
                name="plasma"
                value="Plasma"
              >Plasma</label>
            </li>
          </ul>
        </details>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;!-- Dropdown --&gt;
&lt;details class="dropdown"&gt;
  &lt;summary&gt;Dropdown&lt;/summary&gt;
  &lt;ul&gt;
    &lt;li&gt;&lt;a href="#"&gt;Solid&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Liquid&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Gas&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Plasma&lt;/a&gt;&lt;/li&gt;
  &lt;/ul&gt;
&lt;/details&gt;

&lt;!-- Select --&gt;
&lt;select required&gt;
  &lt;option selected disabled value=""&gt;Select&lt;/option&gt;
  &lt;option&gt;Solid&lt;/option&gt;
  &lt;option&gt;Liquid&lt;/option&gt;
  &lt;option&gt;Gas&lt;/option&gt;
  &lt;option&gt;Plasma&lt;/option&gt;false
&lt;/select&gt;</pre>
          </code-block>
        </footer>
      </article>
      <p>
        Pico does not include JavaScript code. You will probably need some JavaScript to interact with these custom
        dropdowns.
      </p>
    </section>
    <section>
      <h2>
        Button variants<a
          id="button-variants"
          href="#button-variants"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p><code>&lt;summary role="button"&gt;</code> transforms the dropdown into a button.</p>
      <article
        aria-label="Dropdowns as buttons"
        class="component"
      >
        <details class="dropdown">
          <summary role="button">
            Dropdown as a button
          </summary>
          <ul>
            <li><a href="/docs/dropdown">Solid</a></li>
            <li><a href="/docs/dropdown">Liquid</a></li>
            <li><a href="/docs/dropdown">Gas</a></li>
            <li><a href="/docs/dropdown">Plasma</a></li>
          </ul>
        </details>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;!-- Dropdown --&gt;
&lt;details class="dropdown"&gt;
  &lt;summary role="button"&gt;Dropdown as a button&lt;/summary&gt;
  &lt;ul&gt;
    &lt;li&gt;&lt;a href="#"&gt;Solid&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Liquid&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Gas&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Plasma&lt;/a&gt;&lt;/li&gt;
  &lt;/ul&gt;
&lt;/details&gt;

&lt;!-- Select --&gt;
&lt;select required&gt;
  &lt;option selected disabled value=""&gt;Select&lt;/option&gt;
  &lt;option&gt;Solid&lt;/option&gt;
  &lt;option&gt;Liquid&lt;/option&gt;
  &lt;option&gt;Gas&lt;/option&gt;
  &lt;option&gt;Plasma&lt;/option&gt;false
&lt;/select&gt;</pre>
          </code-block>
        </footer>
      </article>
      <p>
        Like regular buttons, they come with <code>.secondary</code>, <code>.contrast</code>, and <code>.outline</code>.
      </p>
      <article
        aria-label="Dropdowns as buttons"
        class="component"
      >
        <details class="dropdown">
          <summary role="button">
            Primary
          </summary>
          <ul>
            <li><a href="/docs/dropdown">Solid</a></li>
            <li><a href="/docs/dropdown">Liquid</a></li>
            <li><a href="/docs/dropdown">Gas</a></li>
            <li><a href="/docs/dropdown">Plasma</a></li>
          </ul>
        </details>
        <details class="dropdown">
          <summary
            role="button"
            class="secondary"
          >
            Secondary
          </summary>
          <ul>
            <li><a href="/docs/dropdown">Solid</a></li>
            <li><a href="/docs/dropdown">Liquid</a></li>
            <li><a href="/docs/dropdown">Gas</a></li>
            <li><a href="/docs/dropdown">Plasma</a></li>
          </ul>
        </details>
        <details class="dropdown">
          <summary
            role="button"
            class="contrast"
          >
            Contrast
          </summary>
          <ul>
            <li><a href="/docs/dropdown">Solid</a></li>
            <li><a href="/docs/dropdown">Liquid</a></li>
            <li><a href="/docs/dropdown">Gas</a></li>
            <li><a href="/docs/dropdown">Plasma</a></li>
          </ul>
        </details>
        <details class="dropdown">
          <summary
            role="button"
            class="outline"
          >
            Primary outline
          </summary>
          <ul>
            <li><a href="/docs/dropdown">Solid</a></li>
            <li><a href="/docs/dropdown">Liquid</a></li>
            <li><a href="/docs/dropdown">Gas</a></li>
            <li><a href="/docs/dropdown">Plasma</a></li>
          </ul>
        </details>
        <details class="dropdown">
          <summary
            role="button"
            class="outline secondary"
          >
            Secondary outline
          </summary>
          <ul>
            <li><a href="/docs/dropdown">Solid</a></li>
            <li><a href="/docs/dropdown">Liquid</a></li>
            <li><a href="/docs/dropdown">Gas</a></li>
            <li><a href="/docs/dropdown">Plasma</a></li>
          </ul>
        </details>
        <details class="dropdown">
          <summary
            role="button"
            class="outline contrast"
          >
            Contrast outline
          </summary>
          <ul>
            <li><a href="/docs/dropdown">Solid</a></li>
            <li><a href="/docs/dropdown">Liquid</a></li>
            <li><a href="/docs/dropdown">Gas</a></li>
            <li><a href="/docs/dropdown">Plasma</a></li>
          </ul>
        </details>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;!-- Dropdown --&gt;
&lt;details class="dropdown"&gt;
  &lt;summary&gt;Dropdown&lt;/summary&gt;
  &lt;ul&gt;
    &lt;li&gt;&lt;a href="#"&gt;Solid&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Liquid&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Gas&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Plasma&lt;/a&gt;&lt;/li&gt;
  &lt;/ul&gt;
&lt;/details&gt;

&lt;!-- Select --&gt;
&lt;select required&gt;
  &lt;option selected disabled value=""&gt;Select&lt;/option&gt;
  &lt;option&gt;Solid&lt;/option&gt;
  &lt;option&gt;Liquid&lt;/option&gt;
  &lt;option&gt;Gas&lt;/option&gt;
  &lt;option&gt;Plasma&lt;/option&gt;false
&lt;/select&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>
    <section>
      <h2>
        Validation states<a
          id="validation-states"
          href="#validation-states"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p>Just like any form elements, validation states are provided with <code>aria-invalid</code>.</p>
      <article
        aria-label="Dropdowns with validation states"
        class="component"
      >
        <details class="dropdown">
          <summary aria-invalid="false">
            Valid phase of matter: Solid
          </summary>
          <ul>
            <li><a href="/docs/dropdown">Solid</a></li>
            <li><a href="/docs/dropdown">Liquid</a></li>
            <li><a href="/docs/dropdown">Gas</a></li>
            <li><a href="/docs/dropdown">Plasma</a></li>
          </ul>
        </details>
        <details class="dropdown">
          <summary aria-invalid="true">
            Debated classification: Plasma
          </summary>
          <ul>
            <li><a href="/docs/dropdown">Solid</a></li>
            <li><a href="/docs/dropdown">Liquid</a></li>
            <li><a href="/docs/dropdown">Gas</a></li>
            <li><a href="/docs/dropdown">Plasma</a></li>
          </ul>
        </details>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;!-- Dropdown --&gt;
&lt;details class="dropdown"&gt;
  &lt;summary&gt;Dropdown&lt;/summary&gt;
  &lt;ul&gt;
    &lt;li&gt;&lt;a href="#"&gt;Solid&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Liquid&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Gas&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Plasma&lt;/a&gt;&lt;/li&gt;
  &lt;/ul&gt;
&lt;/details&gt;

&lt;!-- Select --&gt;
&lt;select required&gt;
  &lt;option selected disabled value=""&gt;Select&lt;/option&gt;
  &lt;option&gt;Solid&lt;/option&gt;
  &lt;option&gt;Liquid&lt;/option&gt;
  &lt;option&gt;Gas&lt;/option&gt;
  &lt;option&gt;Plasma&lt;/option&gt;false
&lt;/select&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>
    <section>
      <h2>
        Usage with <code>&lt;nav&gt;</code><a
          id="usage-with-nav"
          href="#usage-with-nav"
          class="secondary"
          tabindex="-1"
        >#</a>
      </h2>
      <p>You can use dropdowns inside <a href="/docs/nav">Nav</a>.</p>
      <p>To change the alignment of the submenu, simply use <code>&lt;ul dir="rtl"&gt;</code>.</p>
      <article
        aria-label="Dropdowns inside nav"
        class="component"
      >
        <nav>
          <ul>
            <li><strong>Acme Corp</strong></li>
          </ul>
          <ul>
            <li>
              <a
                class="secondary"
                href="/docs/dropdown"
              >Services</a>
            </li>
            <li>
              <details class="dropdown">
                <summary>Account</summary>
                <ul dir="rtl">
                  <li><a href="/docs/dropdown">Profile</a></li>
                  <li><a href="/docs/dropdown">Settings</a></li>
                  <li><a href="/docs/dropdown">Security</a></li>
                  <li><a href="/docs/dropdown">Logout</a></li>
                </ul>
              </details>
            </li>
          </ul>
        </nav>
        <footer
          class="code"
          data-theme="dark"
        >
          <code-block>
            <pre>&lt;!-- Dropdown --&gt;
&lt;details class="dropdown"&gt;
  &lt;summary&gt;Dropdown&lt;/summary&gt;
  &lt;ul&gt;
    &lt;li&gt;&lt;a href="#"&gt;Solid&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Liquid&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Gas&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="#"&gt;Plasma&lt;/a&gt;&lt;/li&gt;
  &lt;/ul&gt;
&lt;/details&gt;

&lt;!-- Select --&gt;
&lt;select required&gt;
  &lt;option selected disabled value=""&gt;Select&lt;/option&gt;
  &lt;option&gt;Solid&lt;/option&gt;
  &lt;option&gt;Liquid&lt;/option&gt;
  &lt;option&gt;Gas&lt;/option&gt;
  &lt;option&gt;Plasma&lt;/option&gt;false
&lt;/select&gt;</pre>
          </code-block>
        </footer>
      </article>
    </section>
  </div>
</template>
