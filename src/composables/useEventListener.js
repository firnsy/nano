/**
 * This file is part of nano.
 *
 * Author(s):
 *  - Ian Firns <firnsy@gmail.com>
 *
 * License: MIT
 */
import {
  getCurrentScope, onScopeDispose, toValue, watch
} from 'vue';

import { FN_NOOP } from '@/utilities/constants';
import { isObject } from '@/utilities/is';

/**
 * Call onScopeDispose() if it's inside an effect scope lifecycle, if not, do nothing
 *
 * @param fn
 */
export function tryOnScopeDispose(fn) {
  if (getCurrentScope()) {
    onScopeDispose(fn);
    return true;
  }

  return false;
}

/**
 * Get the dom element of a ref of element or Vue component instance
 *
 * @param {MaybeComputedElementRef<T>} elRef
 *
 * @returns {UnRefElementReturn<T>}
 */
export function unrefElement(elRef) {
  const plain = toValue(elRef);
  return plain?.$el ?? plain;
}

/**
 *
 * @param {MaybeRefOrGetter | undefined} target
 * @param {Arrayable<string>} events
 * @param {Arrayable<Function>} listeners
 * @param {MaybeRefOrGetter<boolean | AddEventListenerOptions>} options
 *
 * @returns {Function}
 */
export function useEventListener(...args) {
  let target;
  let events;
  let listeners;
  let options;

  if (typeof args[0] === 'string' || Array.isArray(args[0])) {
    [events, listeners, options] = args;
    target = window;
  } else {
    [target, events, listeners, options] = args;
  }

  if (!target) {
    return FN_NOOP;
  }

  if (!Array.isArray(events)) {
    events = [events];
  }

  if (!Array.isArray(listeners)) {
    listeners = [listeners];
  }

  const cleanups = [];
  const cleanup = () => {
    cleanups.forEach((fn) => fn());
    cleanups.length = 0;
  };

  const register = (el, event, listener, opts) => {
    el.addEventListener(event, listener, opts);
    return () => el.removeEventListener(event, listener, opts);
  };

  const stopWatch = watch(
    () => [unrefElement(target), toValue(options)],
    ([el, opts]) => {
      cleanup();
      if (!el) {
        return;
      }

      // create a clone of options, to avoid it being changed reactively on removal
      const optionsClone = isObject(opts) ? { ...opts } : opts;
      cleanups.push(
        ...(events).flatMap((event) => (listeners).map((listener) => register(el, event, listener, optionsClone)))
      );
    },
    { immediate: true, flush: 'post' }
  );

  const stop = () => {
    stopWatch();
    cleanup();
  };

  tryOnScopeDispose(stop);

  return stop;
}
