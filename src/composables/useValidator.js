/**
 * This file is part of nano.
 *
 * Author(s):
 *  - Ian Firns <firnsy@gmail.com>
 *
 * License: MIT
 */
import { computed, ref, shallowRef, toRef, toValue } from "vue";
import { debouncedWatch } from "@/utilities/watches";
import { FN_NOOP } from "@/utilities/constants";
import { isArray, isDefined, isFunction, isObject, isString } from "@/utilities/is";

import { rules } from "@/utilities/validation/rules.js";

export function useValidator(value, rules, options = {}) {
  const {
    bail = true,
    manual = false,
    immediate = true,
    debounce = 0,
    maxWait = undefined,
    validator = coreValidator,
  } = options;

  const valueRef = toRef(value);

  const _valueMap = shallowRef(buildMap(valueRef.value));

  const errors = shallowRef({});
  const isFinished = ref(true);
  const pass = ref(!immediate || manual);
  
  const schema = computed(() => {
    return validator.schema(toValue(rules));
  });

  const validity = computed(() => buildValidity(_valueMap.value, errors.value));

  /**
   * @desc blur the specified fields
   *
   * @param {string|array} fields - the field(s) to blur
   * @returns {void}
   */
  const blur = (fields = []) => {
    const vm = structuredClone(_valueMap.value);
    const _fields = isArray(fields) ? fields.length > 0 ? fields : Object.keys(vm) : [fields];

    _fields.forEach(field => {
      vm[field].touched = true;
      vm[field].untouched = false;
    })

    _valueMap.value = vm;
  }

  /**
   * @desc mark the specified fields as dirty
   *
   * @param {string|array} fields - the field(s) to mark as dirty
   * @returns {void}
   */
  const dirty = (fields) => {
    const vm = structuredClone(_valueMap.value);
    const _fields = isArray(fields) ? fields.length > 0 ? fields : Object.keys(vm) : [fields];

    _fields.forEach(field => {
      vm[field].pristine = false;
      vm[field].dirty = true;
    });

    _valueMap.value = vm;
  };

  /**
   * @desc mark the specified fields as pristine
   *
   * @param {string|array} fields - the field(s) to mark as pristine
   * @returns {void}
   */
  const pristine = (fields) => {
    const vm = structuredClone(_valueMap.value);
    const _fields = isArray(fields) ? fields.length > 0 ? fields : Object.keys(vm) : [fields];

    _fields.forEach(field => {
      vm[field].pristine = true;
      vm[field].dirty = false;
      vm[field].changed = false;
    });

    _valueMap.value = vm;
  };

  /**
   * @desc update the specified fields, marking them as:
   *  1. dirty and
   *  2. changed, if different from the initial value
   *
   * @param {string|array} fields - the field(s) to update
   * @returns {void}
   */
  const update = (fields) => {
    const vm = structuredClone(_valueMap.value);
    const _fields = isArray(fields) ? fields.length > 0 ? fields : Object.keys(vm) : [fields];

    _fields.forEach(field => {
        vm[field].dirty = true;
        vm[field].pristine = false;

        vm[field].changed = (valueRef.value[field] !== vm[field].initialValue);
    });

    _valueMap.value = vm;
  }

  /**
   * @desc validate the data object against the schema
   *
   * @param {object} n - the new data object
   * @param {object} o - the old data object
   * @returns {Promise}
   */
  const validate = async (n, o) => {
    isFinished.value = false;
    pass.value = false;

    try {
      const res = await schema.value.validate(valueRef.value, { bail });
      pass.value = true;
      errors.value = {};
    }
    catch (err) {
      if (err instanceof Error) {
        // TODO: better logging
        console.error('[n-validator] ', err);
      } else {
        errors.value = err;
      }
    }
    finally {
      isFinished.value = true
    }

    const vm = structuredClone(_valueMap.value);

    // process any changes/touches
    if (isDefined(o) && isDefined(n)) {
      Object.keys(n).forEach(field => {
        if (
          isDefined(n[field]) && 
          n[field] !== o[field]
        ) {
          vm[field].dirty = true;
          vm[field].pristine = false;
        }

        vm[field].changed = (n[field] !== vm[field].initialValue);
      });
    }

    // process requires/validated
    const requiredFields = isDefined(schema.value) ? schema.value.fieldsRequired() : [];

    Object.keys(vm).forEach(field => {
      vm[field].validated = true;
      vm[field].passed = true;
      vm[field].failed = vm[field].invalid;

      vm[field].required = requiredFields.includes(field);
    });

    _valueMap.value = vm;

    return {
      pass: pass.value,
      validity: validity.value,
    };
  }

  if (!manual) {
    debouncedWatch(
      () => { return {...valueRef.value}; },
      (n, o) => validate(n, o),
      {debounce, deep: true, immediate, maxWait},
    )
  }

  return {
    isFinished,
    pass,
    validity,

    blur,
    dirty,
    pristine,
    update,
    validate,
  };

  /**
   * @private
   * @desc create the initial valiue tracking map of supplied data object
   *
   * @param {object} value - the data object
   * @returns {object} - the value tracking map
   */
  function buildMap(value) {
    return Object.fromEntries(
      Object.keys(value).map(field => {
        return [field, {
          initialValue: value[field],
          changed: false,
          dirty: false,
          pristine: true,
          required: false,
          touched: false,
          untouched: true,
          validated: false,
          passed: false,
          failed: false,
          ariaInv: ""
        }];
      })
    );
  }

  /**
   * @private
   * @desc create the validity object
   *
   * @param {object} valueMap - the value tracking map
   * @param {object} errors - the errors object
   * @returns {object} - the validity object
   */
  function buildValidity(valueMap = {}, errors = {}) {
    return Object.fromEntries(
      Object.keys(valueRef.value).map(field => {
        const err = errors[field] || [];
        const vm = valueMap[field] || {};

        const ariaInv = (vm.dirty || (vm.touched && vm.required)) ? err.length > 0 ? "true" : "false" : "";

        const v = {
          errors: err,
          valid: err.length === 0,
          invalid: err.length > 0,
          changed: vm.changed,
          dirty: vm.dirty,
          pristine: vm.pristine,
          required: vm.required,
          touched: vm.touched,
          untouched: vm.untouched,
          validated: vm.validated,
          passed: vm.validated && (err.length === 0),
          failed: vm.validated && (err.length > 0),
          ariaInv
        };

        return [field, v];
      })
    );
  }
};

/**
 * @public
 * @class
 * @desc Validator class
 *
 * @example
 *   const validator = new Validator();
 *   validator.rule("required", value => !!value);
 *   validator.rule("min", (value, params) => value.length >= params.min);
 *   validator.schema({
 *     name: {
 *       rules: {
 *         required: true,
 *         min: { min: 3 }
 *       }
 *     }
 *   });
 *   validator.validate({ name: "Ian" });
 *   // returns a promise
 *   // resolves if the data is valid
 *   // rejects if the data is invalid
 *   // the reject object contains an errors array and a fields object
 *   // the errors array contains the error messages
 *   // the fields object contains the error messages grouped by field
 *
 */
export class Validator {
  constructor() {
    this._schema = {};
    this._rules = {};
  }

  fields() {
    return Object.keys(this._schema);
  }

  fieldsRequired() {
    return Object.keys(this._schema).filter(field => this._schema[field].required);
  }

  rule(name, validator) {
    let _newValidator = {
      message: "{_field_} is not valid",
      params: [],
      computesRequired: false,
      required: false,
      transform: (v) => v,
      ...this._rules[name] ?? {}
    };


    if (isFunction(validator)) {
      _newValidator.validate = validator;
    } else if (isObject(validator)) {
      _newValidator = {
        ..._newValidator,
        ...validator
      };
    } else {
      throw new Error("The validator must be a function or an object");
    }

    if (!isFunction(_newValidator.validate)) {
      throw new Error("A validate function must be defined");
    }

    this._rules[name] = _newValidator;

    return this;
  }

  schema(descriptor) {
    if (!isObject(descriptor)) {
      throw new Error("The schema must be an object");
    }

    Object.keys(descriptor).forEach(name => {
      let description = descriptor[name];
      let rules = description.rules ?? {};

      if (!isObject(rules)) {
        throw new Error("The rules must be an object");
      }

      this._schema[name] = description;

      Object.keys(rules).forEach(ruleName => {
        let rule = this._rules[ruleName];

        if (!rule) {
          throw new Error(`Cannot find the validator for rule: ${ruleName}`);
        }
      });
    });

    return this;
  }

  /**
   * @desc validate the data object against the schema
   *
   * @param {object} data - the data object to validate
   * @param {object} options - the options object
   * @returns {Promise}
   *
   * @throws {Error} - if the schema is not defined
   * @throws {Error} - if the data is not an object
   * @throws {Error} - if the validator for a rule is not found
   * @throws {Error} - if the validator does not return an object when computing required
   */
  async validate(data, options = {}) {
    if (!this._schema) {
      throw new Error("Cannot validate without a schema");
    }

    if (!isObject(data)) {
      throw new Error("The data must be an object");
    }

    const errors = {};

    // loop through each schema field and validate against the
    // data object in the same field against all the rules associated with it
    const schema_keys = Object.keys(this._schema);

    for (let i = 0; i < schema_keys.length; i++) {
      const name = schema_keys[i];
      const fieldSchema = this._schema[name];
      const rules = fieldSchema.rules;
      const value = data[name];


      const rules_keys = Object.keys(rules);

      for (let j = 0; j < rules_keys.length; j++) {
        const ruleName = rules_keys[j];
        const rule = this._rules[ruleName];
        const params = buildParams(rule, rules[ruleName]);

        if (!rule) {
          throw new Error(`Cannot find the validator for rule: ${rule.validator}`);
        }

        let result = await Promise.resolve(rule.validate(value, params));

        if (rule.computesRequired) {
          if (
            !isObject(result) ||
            !isDefined(result.required) ||
            !isDefined(result.valid)
          ) {
            throw new Error("The validator must return an object, with required and valid keys, when computing required");
          }

          fieldSchema.required = result.required;
          result = result.valid;
        } else if (rule.required === true) {
          fieldSchema.required = true;
        }

        if (
          isString(result) ||
          result === false
        ) {
          const message = formatMessage(isString(result) ? result : rule.message, rules[ruleName], name, value);

          errors[name] = errors[name] || [];
          errors[name].push(message);

          if (options.bail) {
            break;
          }
        }
      }
    }

    if (errors.length === 0) {
      return Promise.resolve();
    }

    return Promise.reject(errors);
  }
}

/**
 * @private
 * @desc build the params object to be passed to the validator function
 *
 * @param {object} rule - the rule object
 * @param {object} params - the params object
 * @returns {object}
 */

function buildParams(rule, params) {
  let _params = {};

  // normalise params to an object that maps to the order the params are defined in the rule
  if (isArray(params)) {
    params.forEach((param, index) => {
      _params[rule.params[index]] = param;
    });
  } else if (isObject(params)) {
    _params = params;
  // otherwise assume no defined params
  } else {
    return _params;
  }

  // only allow params that are defined in the rule
  Object.keys(_params).forEach(key => {
    if (!rule.params.includes(key)) {
      delete _params[key];
    }
  }); 

  return _params;
}

/**
 * @private
 * @desc format the error message substituting placeholders with the actual values
 *
 * @param {string} message - the message template
 * @param {object} params - the params object
 * @param {string} field - the field name
 * @param {string} value - the field value
 * @returns {string}
 */
function formatMessage(message, params, field, value) {
  if (isFunction(message)) {
    message = message(field, value, params);
  }

  return message.replace(/{([^}]+)}/g, (match, p1) => {
    if (p1 === "_field_") {
      return field;
    } else if (p1 === "_value_") {
      return value;
    }

    return params[p1] ?? "";
  });
}



// CORE VALIDATOR AND RULES

const coreValidator = new Validator();

Object.keys(rules).forEach(rule => {
  coreValidator.rule(rule, rules[rule]);
});

/** 
 * @desc extend the core validator with a new rule
 *
 * @param {string} name - the name of the rule
 * @param {object} rule - the rule object
 * @returns {void}
 */
export function extend(name, rule) {
  coreValidator.rule(name, rule);
}
