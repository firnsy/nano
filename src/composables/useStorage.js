import {
  getCurrentInstance, onMounted, nextTick, ref, shallowRef, toValue
} from 'vue';

import { pausableWatch } from '@/utilities/watches';
import { useEventListener } from '@/composables/useEventListener';

export function getLifeCycleTarget(target) {
  return target || getCurrentInstance();
}

/**
 * Call onMounted() if it's inside a component lifecycle, if not, just call the function
 *
 * @param fn
 * @param target
 * @param sync if set to false, it will run in the nextTick() of Vue
 */
export function tryOnMounted(fn, target, sync = true) {
  const instance = getLifeCycleTarget(target);

  if (instance) {
    onMounted(fn, instance);
  } else if (sync) {
    fn();
  } else {
    nextTick(fn);
  }
}

export const StorageSerializers = {
  boolean: {
    read: (v) => v === 'true',
    write: (v) => String(v)
  },
  object: {
    read: (v) => JSON.parse(v),
    write: (v) => JSON.stringify(v)
  },
  number: {
    read: (v) => Number.parseFloat(v),
    write: (v) => String(v)
  },
  any: {
    read: (v) => v,
    write: (v) => String(v)
  },
  string: {
    read: (v) => v,
    write: (v) => String(v)
  },
  map: {
    read: (v) => new Map(JSON.parse(v)),
    write: (v) => JSON.stringify(Array.from((v).entries()))
  },
  set: {
    read: (v) => new Set(JSON.parse(v)),
    write: (v) => JSON.stringify(Array.from(v))
  },
  date: {
    read: (v) => new Date(v),
    write: (v) => v.toISOString()
  }
};

export const customStorageEventName = 'ui-storage';

/**
 * Reactive LocalStorage/SessionStorage.
 *
 * @param key (string)
 * @param defaults (MaybeRefOrGetter)
 * @param storage (StorageLike | undefined)
 * @param options (UseStorageOptions)
 *
 * @returns {Ref}
 */

export function useStorage(key, defaults, storage, options = {}) {
  const {
    flush = 'pre',
    deep = true,
    listenToStorageChanges = true,
    writeDefaults = true,
    mergeDefaults = false,
    shallow,
    eventFilter,
    onError = () => {},
    initOnMounted
  } = options;

  const data = (shallow ? shallowRef : ref)(typeof defaults === 'function' ? defaults() : defaults);

  if (!storage) {
    try {
      // eslint-disable-next-line no-param-reassign
      storage = window?.localStorage;
    } catch (e) {
      onError(e);
    }
  }

  if (!storage) {
    return data;
  }

  const rawInit = toValue(defaults);
  const type = guessSerializerType(rawInit);
  const serializer = options.serializer ?? StorageSerializers[type];

  const { pause: pauseWatch, resume: resumeWatch } = pausableWatch(
    data,
    () => write(data.value),
    { flush, deep, eventFilter }
  );

  if (window && listenToStorageChanges) {
    tryOnMounted(() => {
      // this should be fine since we are in a mounted hook
      useEventListener(window, 'storage', update);
      useEventListener(window, customStorageEventName, updateFromCustomEvent);
      if (initOnMounted) {
        update();
      }
    });
  }

  // avoid reading immediately to avoid hydration mismatch when doing SSR
  if (!initOnMounted) update();

  return data;

  function write(v) {
    try {
      if (v == null) {
        storage.removeItem(key);
      } else {
        const serialized = serializer.write(v);
        const oldValue = storage.getItem(key);
        if (oldValue !== serialized) {
          storage.setItem(key, serialized);

          // send custom event to communicate within same page
          // importantly this should _not_ be a StorageEvent since those cannot
          // be constructed with a non-built-in storage area
          if (window) {
            window.dispatchEvent(new CustomEvent(customStorageEventName, {
              detail: {
                key,
                oldValue,
                newValue: serialized,
                storageArea: storage
              }
            }));
          }
        }
      }
    } catch (e) {
      onError(e);
    }
  }

  function read(event) {
    const rawValue = event
      ? event.newValue
      : storage.getItem(key);

    if (rawValue == null) {
      if (writeDefaults && rawInit != null) {
        storage.setItem(key, serializer.write(rawInit));
      }

      return rawInit;
    } if (!event && mergeDefaults) {
      const value = serializer.read(rawValue);

      if (typeof mergeDefaults === 'function') {
        return mergeDefaults(value, rawInit);
      } if (type === 'object' && !Array.isArray(value)) {
        return { ...rawInit, ...value };
      }

      return value;
    } if (typeof rawValue !== 'string') {
      return rawValue;
    }
    return serializer.read(rawValue);
  }

  function updateFromCustomEvent(event) {
    update(event.detail);
  }

  function update(event) {
    if (event && event.storageArea !== storage) {
      return;
    }

    if (event && event.key == null) {
      data.value = rawInit;
      return;
    }

    if (event && event.key !== key) {
      return;
    }

    pauseWatch();

    try {
      if (event?.newValue !== serializer.write(data.value)) {
        data.value = read(event);
      }
    } catch (e) {
      onError(e);
    } finally {
      // use nextTick to avoid infinite loop
      if (event) {
        nextTick(resumeWatch);
      } else {
        resumeWatch();
      }
    }
  }
}

function guessSerializerType(v) {
  if (v == null) {
    return 'any';
  } if (v instanceof Set) {
    return 'set';
  } if (v instanceof Map) {
    return 'map';
  } if (v instanceof Date) {
    return 'date';
  } if (typeof v === 'boolean') {
    return 'boolean';
  } if (typeof v === 'string') {
    return 'string';
  } if (typeof v === 'object') {
    return 'object';
  } if (!Number.isNaN(v)) {
    return 'number';
  }
  return 'any';
}
