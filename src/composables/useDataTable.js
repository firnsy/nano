/**
 * This file is part of nano.
 *
 * Author(s):
 *  - Ian Firns <firnsy@gmail.com>
 *
 * License: MIT
 */
import {
  ref, toRaw, toValue
} from 'vue';

import { FN_NOOP } from '@/utilities/constants';
import {
  isArray, isDefined, isEqual, isFunction, isObject, isObjectPlain, isString
} from '@/utilities/is';
import { merge } from '@/utilities/merge';

export const SORT_ORDER_NONE = 0;
export const SORT_ORDER_ASC = 1;
export const SORT_ORDER_DESC = 2;

export const FILTER_TYPE_GROUP = 1;
export const FILTER_TYPE_CRITERION = 2;

export const GROUP_TYPE_AND = 1;
export const GROUP_TYPE_NOT_AND = 2;
export const GROUP_TYPE_OR = 3;
export const GROUP_TYPE_NOT_OR = 4;

export const GroupType = {
  GROUP_TYPE_AND: {
    label: 'AND',
    shortCode: '$and'
  },
  GROUP_TYPE_NOT_AND: {
    label: 'NOT AND',
    shortCode: '$nand'
  },
  GROUP_TYPE_OR: {
    label: 'OR',
    shortCode: '$or'
  },
  GROUP_TYPE_NOT_OR: {
    label: 'NOT OR',
    shortCode: '$nor'
  }
};

export const CriterionTypes = [
  {
    label: 'Equals',
    shortCode: '$eq',
    types: ['string', 'number', 'date', 'datetime']
  },
  {
    label: 'Not Equals',
    shortCode: '$neq',
    types: ['string', 'number', 'date', 'datetime']
  },
  {
    label: 'Greater Than',
    shortCode: '$gt',
    types: ['number', 'date', 'datetime']
  },
  {
    label: 'Greater Than or Equal To',
    shortCode: '$gte',
    types: ['number', 'date', 'datetime']
  },
  {
    label: 'Less Than',
    shortCode: '$lt',
    types: ['number', 'date', 'datetime']
  },
  {
    label: 'Less Than or Equal To',
    shortCode: '$lte',
    types: ['number', 'date', 'datetime']
  },
  {
    label: 'Contains',
    shortCode: '$c',
    types: ['string']
  },
  {
    label: 'Does Not Contain',
    shortCode: '$nc',
    types: ['string']
  },
  {
    label: 'Starts With',
    shortCode: '$sw',
    types: ['string']
  },
  {
    label: 'Ends With',
    shortCode: '$ew',
    types: ['string']
  }
];

const DEFAULT_FILTER = {
  type: FILTER_TYPE_GROUP,
  groupType: GroupType.GROUP_TYPE_AND,
  filters: []
};

const DEFAULT_RENDERER = (data) => data;

const DEFAULT_RENDERERS = {
  string: DEFAULT_RENDERER,
  number: DEFAULT_RENDERER,
  date: DEFAULT_RENDERER,
  datetime: DEFAULT_RENDERER
};

const ShortCodeMap = {};

CriterionTypes.forEach((item) => {
  ShortCodeMap[item.shortCode] = {
    filterType: FILTER_TYPE_CRITERION,
    entry: item
  };
});

Object.values(GroupType).forEach((item) => {
  ShortCodeMap[item.shortCode] = {
    filterType: FILTER_TYPE_GROUP,
    entry: item
  };
});

export function useDataTableState(options = {}) {
  const fetchFn = options.fetch ?? FN_NOOP;
  const initialPageSize = options.initialPageSize ?? 10;
  const initialPage = options.initialPage ?? 1;

  // REACTIVE
  const isLoading = ref(false);
  const rows = ref([]);
  const columns = ref(normaliseColumns(options.columns ?? []));

  const selection = ref({
    column: options.selectionColumn ?? 'id',
    global: false,
    indexes: {}
  });

  const pager = ref({
    pageSize: initialPageSize,
    page: initialPage,
    pageCount: 0,
    itemCount: 0,
    firstItem: 0,
    lastItem: 0
  });

  const filter = ref(structuredClone(DEFAULT_FILTER));
  const filterCount = ref(0);

  const sorter = ref({});

  // FUNCTIONS

  function filterNew() {
    return structuredClone(DEFAULT_FILTER);
  }

  /**
   * @desc: Update the filter state
   *
   * @param {Object} newFilter - The new filter state, undefine or null clears the filter state.
   * @returns {void}
   *
   * @example
   *  filterUpdate();
   *  filterUpdate({type: FILTER_TYPE_GROUP, groupType: GroupType.GROUP_TYPE_AND, filters: []});
   *  filterUpdate({type: FILTER_TYPE_CRITERION, method: FilterMethod.EQ, columnName: 'foo', argument: 'bar'});
   *
   */
  function filterUpdate(newFilter) {
    const newEncodedFilter = encodeFilter(newFilter);
    const oldEncodedFilter = encodeFilter(filter);

    if (!isEqual(newEncodedFilter, oldEncodedFilter)) {
      refresh({ filter: newFilter.value });
    }
  }

  /**
   * @desc: Refresh the data
   * @param {Object} fsp - Filter, Sort, Pager object
   * @returns {void}
   * @example
   *   refresh();
   *   refresh({pager: {page: 2, pageSize: 10}});
   *   refresh({filter: ['$and', ['$eq', 'name', 'foo'], ['$eq', 'value', 'bar']]});
   *   refresh({sorter: ['+name', '-value']});
   *   refresh({filter: ['$neq', 'name', 'foo'], sorter: ['-foo'], pager: {page: 2, pageSize: 10}});
   *   refresh({sorter: ['+foo', '-bar'], pager: {page: 2, pageSize: 10}});
   */
  async function refresh(fsp, reset = false) {
    if (isLoading.value) {
      return;
    }

    isLoading.value = true;

    // merge doesn't play well with refs, so we need to ensure only proxy values or raw values
    let fspReq = {};

    if (!reset) {
      fspReq = merge(
        {
          sorter: toRaw(sorter.value),
          pager: {
            page: pager.value.page ?? initialPage,
            pageSize: pager.value.pageSize ?? initialPageSize
          }
        },
        fsp ?? {}
      );

      // filters are complex, we can't trust a merge so before encoding we manually inject
      // the existing filter if no new filter was supplied.
      fspReq.filter = encodeFilter(fspReq.filter ?? filter.value);

      // encode the sorter
      fspReq.sorter = encodeSorter(fspReq.sorter);
    }

    try {
      const data = await fetchFn(fspReq);

      rows.value = data.items;

      // update filter and filter count
      const decodedFilter = decodeFilter(data.filter ?? []);

      filter.value = decodedFilter;
      filterCount.value = calculateFilterCount(decodedFilter);

      // update sorter
      sorter.value = decodeSorter(data.sorter ?? []);

      // update pager
      pager.value.page = data.pager.page;
      pager.value.pageSize = data.pager.pageSize;
      pager.value.pageCount = data.pager.pageCount;
      pager.value.itemCount = data.pager.itemCount;
      pager.value.firstItem = data.pager.itemCount > 0 ? ((data.pager.page - 1) * data.pager.pageSize) + 1 : 0;
      pager.value.lastItem = data.pager.itemCount > 0 ? pager.value.firstItem + data.items.length - 1 : 0;
    } finally {
      isLoading.value = false;
    }
  }

  /**
   * @desc: Update the pager state
   *
   * @param {Object} newPager - The new pager state
   * @returns {void}
   *
   * @example
   *  pagerUpdate({page: 2, pageSize: 10});
   *  pagerUpdate({page: 2});
   *  pagerUpdate({pageSize: 10});
   *  pagerUpdate({});
   *  pagerUpdate();
   *  pagerUpdate({page: 2, pageSize: 10, foo: 'bar'}); // foo is ignored
   */
  function pagerUpdate(newPager) {
    const pagerReq = merge(
      {
        page: pager.value.page,
        pageSize: pager.value.pageSize
      },
      newPager
    );

    if (
      (pagerReq.page !== pager.value.page)
      || (pagerReq.pageSize !== pager.value.pageSize)
    ) {
      refresh({ pager: pagerReq });
    }
  }

  /**
   * @ddesc Update the selection state
   *
   * @param {string | null} reference - The reference to the item being selected
   * @param {boolean} state - The new state of the selection
   * @param {boolean} global - Whether this is a global selection or not
   * @returns {void}
   *
   * @example
   * selectionUpdate('1', true, false); // select item 1
   * selectionUpdate('1', false, false); // deselect item 1
   * selectionUpdate('1', true, true); // select all
   * selectionUpdate('1', false, true); // deselect all
   * selectionUpdate(null, false, true); // deselect all
   */
  function selectionUpdate(reference, state, global) {
    // eslint-disable-next-line no-bitwise
    if (global ^ selection.value.global) {
      Object.keys(selection.value.indexes).forEach((item) => {
        delete selection.value.indexes[item];
      });

      selection.value.global = global;
    } else if (isDefined(reference)) {
      // eslint-disable-next-line no-bitwise
      if (global ^ state) {
        selection.value.indexes[reference] = true;
      } else {
        delete selection.value.indexes[reference];
      }
    }
  }

  /**
   * @desc Update the sorter state
   *
   * @param {Object} new_sorter - The new sorter state, undefine or null clears the sorter state.
   * @returns {void}
   *
   * @example
   *   sorterUpdate();
   *   sorterUpdate({'foo', {direction: SORT_ORDER_ASC}});
   *   sorterUpdate({'foo', {direction: SORT_ORDER_DESC}});
   *   sorterUpdate({'foo', {direction: SORT_ORDER_ASC, 'bar', {direction: SORT_ORDER_DESC}}});
   */
  function sorterUpdate(newSorter) {
    let sorterReq = {};

    if (isObject(newSorter)) {
      const oldSorter = toRaw(toValue(sorter));

      // bump the priority of the old sorter (higher priority = lower number)
      Object.values(oldSorter).forEach((item) => {
        // eslint-disable-next-line no-param-reassign
        item.priority += 1;
      });

      sorterReq = merge(
        {},
        oldSorter,
        newSorter
      );
    }

    if (!isEqual(sorterReq, sorter.value)) {
      refresh({ sorter: sorterReq });
    }
  }

  // load data
  refresh();

  return {
    isLoading,
    refresh,
    columns,
    rows,
    filter,
    filterCount,
    filterNew,
    filterUpdate,
    pager,
    pagerUpdate,
    selection,
    selectionUpdate,
    sorter,
    sorterUpdate
  };
}

/**
 * @desc: Decode an nano-compliant filter array into more descriptive
 * structure.
 *
 * @param {string[]} encodedFilter - The encoded filter to decode
 *
 * @returns {Object} The decoded filter
 *
 * @example
 *  decodeFilter(['$and', ['$eq', 'name', 'foo'], ['$eq', 'value', 'bar']]);
 *  // {type: FILTER_TYPE_GROUP, groupType: GroupType.GROUP_TYPE_AND, filters: [
 *  //   {type: FILTER_TYPE_CRITERION, method: FilterMethod.EQ, columnName: 'name', argument: 'foo'},
 *  //   {type: FILTER_TYPE_CRITERION, method: FilterMethod.EQ, columnName: 'value', argument: 'bar'},
 *  // ]}
 */
export function decodeFilter(encodedFilter) {
  let decodedFilter = {};

  if (isArray(encodedFilter)) {
    if (encodedFilter.length === 0) {
      return decodedFilter;
    }
    const filterComponent = getFilterFromShortCode(encodedFilter[0]);

    if (filterComponent.filterType === FILTER_TYPE_GROUP) {
      decodedFilter = {
        type: FILTER_TYPE_GROUP,
        groupType: filterComponent.entry,
        filters: []
      };

      encodedFilter.slice(1).forEach((item) => {
        decodedFilter.filters.push(decodeFilter(item));
      });
    } else if (filterComponent.filterType === FILTER_TYPE_CRITERION) {
      if (encodedFilter.length !== 3) {
        throw Error('Invalid filter length');
      }

      decodedFilter = {
        type: FILTER_TYPE_CRITERION,
        method: filterComponent.entry,
        columnName: encodedFilter[1],
        argument: encodedFilter[2]
      };
    } else {
      throw Error('Unsupported filter type');
    }
  }

  return decodedFilter;
}

/**
 * @desc: Decode a sorter array into a sorter object
 *
 * @param {string[]} encodedSorter - The encoded sorter to decode
 *
 * @returns {Object} The decoded sorter
 *
 * @example
 *   decodeSorter(['+foo', '-bar']);
 *  // {foo: {direction: SORT_ORDER_ASC, priority: 0}, bar: {direction: SORT_ORDER_DESC, priority: 1}}
 */
export function decodeSorter(encodedSorter) {
  const decodedSorter = {};

  encodedSorter
    .forEach((item, priority) => {
      const direction = item[0] === '-' ? SORT_ORDER_DESC : SORT_ORDER_ASC;
      const column = item.substr(1);

      decodedSorter[column] = { direction, priority };
    });

  return decodedSorter;
}

/**
 * @desc: Encode a filter object into a string
 *
 * @param {MaybeRefOrGetter} decodedFilter - The filter object to encode
 */
export function encodeFilter(decodedFilter, level = 0) {
  let encoded = [];

  const decoded = toRaw(toValue(decodedFilter));

  if (decoded.type === FILTER_TYPE_GROUP) {
    const children = decoded.filters
      .map((filter) => encodeFilter(filter, level + 1))
      .filter((filter) => filter.length);

    if (children.length) {
      encoded = [decoded.groupType.shortCode].concat(children);
    }
  } else if (
    isObjectPlain(decoded) && (
      decoded.method
      && decoded.columnName
      && decoded.argument
    )
  ) {
    encoded = [
      decoded.method.shortCode,
      decoded.columnName,
      decoded.argument
    ];
  }

  return encoded;
}

/**
 * @desc: Encode a sorter object into a sort string
 *
 * @param {Object} data - The sorter object to encode
 * @returns {string[]} The encoded sorter
 *
 * @example
 *   encodeSorter({foo: {direction: SORT_ORDER_ASC, priority: 0}, bar: {direction: SORT_ORDER_DESC, priority: 1}});
 *   // ['+foo', '-bar']
 *
 *   encodeSorter({foo: {direction: SORT_ORDER_ASC, priority: 1}, bar: {direction: SORT_ORDER_DESC, priority: 0}});
 *   // ['-bar', '+foo']
 */
export function encodeSorter(data) {
  return Object.entries(data)
    .map(([column, props]) => merge(
      {
        column,
        priority: 0
      },
      props
    ))
    .filter((item) => item.direction !== SORT_ORDER_NONE)
    .sort((a, b) => a.priority - b.priority)
    .map((item) => (item.direction === SORT_ORDER_DESC ? '-' : '+') + item.column);
}

// NON-EXPORTED

function normaliseColumns(columns) {
  if (!isArray(columns)) {
    throw Error('Must specify an array');
  }

  const defaults = {
    visible: true,
    sortable: false,
    filterable: false,
    type: 'string',
    sort: {
      direction: SORT_ORDER_NONE,
      priority: 0
    }
  };

  const normalisedColumns = columns.map((item) => {
    let column = {};
    if (isString(item)) {
      column = {
        ...defaults,
        name: item,
        label: item
      };
    } else if (isObject(item)) {
      column = {
        ...defaults,
        ...item
      };
    } else {
      throw Error('Unsupported column definition');
    }

    if (!isFunction(column.renderer)) {
      column.renderer = DEFAULT_RENDERERS[column.type ?? 'string'];
    }

    return column;
  });

  return normalisedColumns;
}

/**
 * @desc: Calculate the number of filters in a filter object
 *
 * @param {Object} filter - The filter object to calculate the number of filters in
 * @returns {number} The number of filters in the filter object
 */
function calculateFilterCount(filter) {
  let stack = structuredClone(filter?.filters ?? []);
  let count = 0;

  while (stack.length > 0) {
    const item = stack.pop();

    if (isDefined(item)) {
      if (item.type === FILTER_TYPE_GROUP) {
        stack = stack.concat(structuredClone(item.filters));
      } else {
        count += 1;
      }
    }
  }

  return count;
}

function getFilterFromShortCode(shortCode) {
  // eslint-disable-next-line no-prototype-builtins
  if (!ShortCodeMap.hasOwnProperty(shortCode)) {
    throw Error('Unknown filter short code');
  }

  return ShortCodeMap[shortCode];
}
