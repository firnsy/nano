import { createApp } from 'vue';
import App from '@/App.vue';
import router from '@/router';
import vueHighlight from '@/directives/highlight';

import hljs from '@/vendor/highlight.min';

hljs.configure({
  cssSelector: 'code',
  ignoreUnescapedHTML: true
});

const app = createApp(App);

app.use(vueHighlight, { hljs });
app.use(router);
app.mount('#app');
