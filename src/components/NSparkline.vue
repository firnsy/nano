<!--
  This file is part of nano.

  Author(s):
   - Ian Firns <firnsy@gmail.com>

  License: MIT
-->

<script setup>
  import { defineComponent, ref, unref, h, computed, provide, nextTick } from 'vue'
  import { debounce } from '@/utilities/debounce';

  /* sparkline types: area, line, bar, column

  line options:
    options: {
      type: 'line',
      stroke: 'rgb(124, 255, 178)',
      strokeWidth: 2,
      marker: {
        size: 3,
        shape: 'diamond',
        fill: 'green',
        stroke: 'green',
        strokeWidth: 2
      },
      padding: {
          top: 5,
          bottom: 5,
      },
      highlightStyle: {
          size: 7,
          fill: 'rgb(124, 255, 178)',
          strokeWidth: 0
      },
    }

  area options:
    options: {
      type: 'area',
      fill: 'rgba(216, 204, 235, 0.3)',
      stroke: 'rgb(119,77,185)',
      highlightStyle: {
          fill: 'rgb(143,185,77)',
      },
      axis: {
          stroke: 'rgb(204, 204, 235)',
      }
    }

  bar options:
    options: {
      type: 'bar',
      fill: '#5470c6',
      stroke: '#91cc75',
      strokeWidth: 1,
      highlightStyle: {
          fill: '#fac858'
      },
      valueAxisDomain: [0, 1]
  },

  column options:
    options: {
      type: 'column',
      fill: '#5470c6',
      stroke: '#91cc75',
      strokeWidth: 1,
      highlightStyle: {
          fill: '#fac858'
      },
    }



  common options:
    options: {
      padding: {
        top: 5,
        bottom: 5,
        left: 5,
        right: 5
      },
      highlightStyle: {
          size: 7,
          fill: 'rgb(124, 255, 178)',
          strokeWidth: 0
      },
      axis: {
          stroke: 'rgb(204, 204, 235)',
      }
    }
  */

  // DEFAULT PROPS
  const OPTIONS_DEFAULT = {
    type: 'line',
    fill: 'var(--nano-primary)',
    stroke: 'var(--nano-secondary)',
    strokeWidth: 2,
    marker: {
      size: 3,
      shape: 'diamond',
      fill: 'green',
      stroke: 'green',
      strokeWidth: 2
    },
    padding: {
      top: 5,
      bottom: 5,
    },
    highlightStyle: {
      size: 7,
      fill: 'rgb(124, 255, 178)',
      strokeWidth: 0
    },
  };



  const props = defineProps({
    width: {
      type: Number,
      default: 200
    },
    height: {
      type: Number,
      default: 50
    },
    options: { 
      type: Object,
      default: () => ({})
    },
    smooth: {
      type: Boolean,
      default: true  
    },
    preserveAspectRatio: {
      type: String,
      default: 'none'
    },
    lineStyles: {
      type: Object,
      default: () => ({
        stroke: 'slategray',
        strokeWidth: 1,
        spotRadius: 3,
        fill: 'none'
      })
    },
    indicatorStyles: {
      type: [Object, Boolean],
      default: () => ({
        stroke: '#dd2c00'
      })
    },
    tooltipProps: {
      type: Object
    },
    tooltipStyles: {
      type: Object,
      default: () => ({
        display: 'none',
        background: 'rgba(0, 0, 0, 0.6)',
        borderRadius: '3px',
        minWidth: '30px',
        padding: '3px',
        color: '#fff',
        fontSize: '12px'
      })
    },
    tooltipDelay: {
      type: Number,
      default: 0
    },
    data: {
      type: Array
    },
    limit: {
      type: Number,
      default: 3
    },
    margin: {
      type: Number,
      default: 3
    },
    styles: {
      type: Object,
      default: () => ({})
    },
    max: Number,
    min: Number
  });

  // REACTIVE
  const indicator = ref(null);
  const tooltip = ref(null);

  const isFocusing = ref(false);
  const pointDatum = ref({});
  const svgMouseEvt = ref(true);

  const execMouseEvt = computed(() => {
    return (
      props.indicatorStyles &&
      indicator.value &&
      svgMouseEvt.value
    );
  });

  const styles = computed(() => {
    const styles = props.styles

    styles.width = props.width
    styles.height = props.height

    return styles
  });

  // VARIABLES
  let curEvt = {};

  // FUNCTIONS

  function getY(min, max, height, diff, value) {
    // return the y coordinate for the given value where min is the minimum value in the data set
    // and max is the maximum value in the data set. height is the height of the graph area and diff
    // is the size of the spot radius.

    return parseFloat((height - ((value - min) * height / (max - min)) + diff).toFixed(2));
  }

  function renderElements() { 
    const { fill, marker, stroke, strokeWidth } = { ...OPTIONS_DEFAULT, ...props.options };

    const spotRadius = marker.size;
    const spotDiameter = marker.size * 2;
    const lastItemIndex = props.data.length - 1;
    const offset = (props.width - spotDiameter*2) / lastItemIndex;
    const workHeight = props.height - spotDiameter - strokeWidth * 2;

    const min = Math.min(...props.data);
    const max = Math.max(...props.data);
    const range = max - min;

    const axisY = getY(min, max, workHeight, strokeWidth + spotRadius, 0);

    let elements = [];

    if (props.options.type === 'column') {
      elements = renderElementsColumn();
    } else if (props.options.type === 'line') {
      elements = renderElementsLine();
    } else {
      elements = renderElementsArea();
    }

    // add axis

    return elements;
  }

  function renderElementsArea() {
    const items = [];

    const { fill, marker, stroke, strokeWidth } = { ...OPTIONS_DEFAULT, ...props.options };

    const spotRadius = marker.size;
    const spotDiameter = marker.size * 2;
    const lastItemIndex = props.data.length - 1;
    const offset = (props.width - spotDiameter*2) / lastItemIndex;
    const workHeight = props.height - spotDiameter - strokeWidth * 2;

    const min = Math.min(...props.data);
    const max = Math.max(...props.data);
    const range = max - min;

    // hold the line coordinates.
    let axisY = getY(min, max, workHeight, strokeWidth + spotRadius, 0);
    let y = getY(min, max, workHeight, strokeWidth + spotRadius, props.data[0]);
    let x = 0;
    let points = [ `M${x},${axisY}` ];

    // straight lines
    if (!props.smooth) {
      for (let i = 0; i < props.data.length; i++) {
        const y = getY(min, max, workHeight, strokeWidth + spotRadius, props.data[i]);

        points.push(`L${x},${y}`);
        x += offset;
      }
    } else {
      // smooth lines by rounding the corners of lines using bezier curves
      for (let i = 0; i < props.data.length; i++) {
        const y = getY(min, max, workHeight, strokeWidth + spotRadius, props.data[i]);

        if (i === 0) {
          points.push(`L${x},${y}`);
        } else {
          const prev = props.data[i - 1];
          const py = getY(min, max, workHeight, strokeWidth + spotRadius, prev);
          const cp1x = x - offset / 2;
          const cp1y = py;
          const cp2x = x - offset / 2;
          const cp2y = y;

          points.push(`C${cp1x},${cp1y},${cp2x},${cp2y},${x},${y}`);
        }

        x += offset;
      }
    }

    points.push(`L${x-offset},${axisY} Z`);


    if (typeof props.indicatorStyles !== 'boolean') {
      props.indicatorStyles['shape-rendering'] = 'crispEdges'
      items.push(h('line', {
        display: 'none',
        style: props.indicatorStyles,
        x1: 0,
        y1: 0,
        x2: 0,
        y2: props.height,
        ref: indicator
      }))
    }

    items.push(h('path', {
      d: points.join(' '),
      stroke,
      'stroke-width': strokeWidth,
      'stroke-linejoin': 'round',
      'stroke-linecap': 'round',
      fill
    }));

    return items;
  }

  function renderElementsColumn() {
    const items = [];

    const { fill, marker, stroke, strokeWidth } = { ...OPTIONS_DEFAULT, ...props.options };

    const spotRadius = 0;
    const spotDiameter = 0;

    const padOuter = 0;
    const padInner = 10;

    const lastItemIndex = props.data.length - 1;
    const workHeight = props.height - strokeWidth * 2;

    const min = Math.min(...props.data);
    const max = Math.max(...props.data);

    // hold the line coordinates.
    const axisY = getY(min, max, workHeight, strokeWidth, 0);
    let x = padOuter;
    let y = getY(min, max, workHeight, strokeWidth, props.data[0]);

    const offset = (props.width - strokeWidth + 2 - padOuter * 2) / props.data.length;

    let barWidth = offset - padInner;

    let points = [];

    // straight lines
    for (let i = 0; i < props.data.length; i++) {
      y = getY(min, max, workHeight, strokeWidth + spotRadius, props.data[i]);

      points.push(`M${x},${axisY} L${x+ barWidth},${axisY} L${x + barWidth},${y} L${x},${y} Z`);
      x += offset;
    }

    if (typeof props.indicatorStyles !== 'boolean') {
      props.indicatorStyles['shape-rendering'] = 'crispEdges'
      items.push(h('line', {
        display: 'none',
        style: props.indicatorStyles,
        x1: 0,
        y1: 0,
        x2: 0,
        y2: props.height,
        ref: indicator
      }))
    }

    items.push(h('path', {
      d: points.join(' '),
      stroke,
      'stroke-width': strokeWidth,
      'stroke-linejoin': 'round',
      'stroke-linecap': 'round',
      fill
    }));

    return items;
  }

  function renderElementsLine() {
    const items = [];

    const { fill, marker, stroke, strokeWidth } = { ...OPTIONS_DEFAULT, ...props.options };

    const spotRadius = marker.size;
    const spotDiameter = marker.size * 2;
    const lastItemIndex = props.data.length - 1;
    const offset = (props.width - spotDiameter*2) / lastItemIndex;
    const workHeight = props.height - spotDiameter - strokeWidth * 2;

    const min = Math.min(...props.data);
    const max = Math.max(...props.data);
    const range = max - min;


    // hold the line coordinates.
    let x = 0;
    let points = [];

    // straight lines
    if (!props.smooth) {
      for (let i = 0; i < props.data.length; i++) {
        const y = getY(min, max, workHeight, strokeWidth + spotRadius, props.data[i]);

        if (i === 0) {
          points.push(`M${x},${y}`);
        } else {
          points.push(`L${x},${y}`);
        }

        x += offset;
      }
    } else {
      // smooth lines by rounding the corners of lines using bezier curves
      for (let i = 0; i < props.data.length; i++) {
        const y = getY(min, max, workHeight, strokeWidth + spotRadius, props.data[i]);

        if (i === 0) {
          points.push(`M${x},${y}`);
        } else {
          const prev = props.data[i - 1];
          const py = getY(min, max, workHeight, strokeWidth + spotRadius, prev);
          const cp1x = x - offset / 2;
          const cp1y = py;
          const cp2x = x - offset / 2;
          const cp2y = y;

          points.push(`C${cp1x},${cp1y},${cp2x},${cp2y},${x},${y}`);
        }

        x += offset;
      }
    }


    if (typeof props.indicatorStyles !== 'boolean') {
      props.indicatorStyles['shape-rendering'] = 'crispEdges'
      items.push(h('line', {
        display: 'none',
        style: props.indicatorStyles,
        x1: 0,
        y1: 0,
        x2: 0,
        y2: props.height,
        ref: indicator
      }))
    }

    items.push(h('path', {
      d: points.join(' '),
      stroke,
      'stroke-width': strokeWidth,
      'stroke-linejoin': 'round',
      'stroke-linecap': 'round',
      fill: 'none'
    }));

    return items;
  }

  function setStatus(status = true) {
    const display = status ? '' : 'none';

    if (tooltip.value) {
      tooltip.value.style.display = display;
    }

    if (indicator.value) {
      indicator.value.style.display = display;
    }
  }

  function updateData() {
    if (!isFocusing.value) {
      return false;
    }

    let rect;
    let curData = null;
    let tooltipContent = '';

    const pd = unref(pointDatum.value);

    /*
    for (const datum in pd) {
      curData = null;

      if (pd.hasOwnProperty(datum)) {
        setStatus(false);

        for (const [i, p] of pd[datum].points.entries()) {
          if (curEvt.ox < p.x && curData === null) {
            setStatus();
            rect = tooltip.value?.getBoundingClientRect?.();

            tooltipContent += `<span style="color: ${curData.color};">&bull;</span>&nbsp;${curData.value}<br />`
          }
        }
      }
    }
    */

    if (rect) {
      tooltip.value.style.left = `${curEvt.cx + rect.width / 2}px`
      tooltip.value.style.top = `${curEvt.cy - props.height - rect.height}px`

      try {
        tooltip.value.innerHTML = props.tooltipProps?.formatter?.(curData) || tooltipContent
      } catch (err) {
        tooltip.value.innerHTML = tooltipContent
      }
    }
  }

  const updateTooltip = debounce(updateData, props.tooltipDelay);

  function updateValue(value) {
    const { id, data, points, color, limit } = value;

    nextTick(() => {
      pointDatum.value[id] = {
        data: props.data.length >= limit ? data.slice(-limit) : data,
        points,
        color
      }

      Object.keys(curEvt).length && updateTooltip();
    });
  }

  /**
   * @desc render the component (referenced in the template as <render />)
   */
  function render() {
    return h('div', {
      'class': 'ui-sparkline',
    }, [
      h('svg',
        {
          style: styles,
          viewBox: `0 0 ${props.width} ${props.height}`,
          preserveAspectRatio: props.preserveAspectRatio,
          onMouseenter: () => {
            //setStatus(true);
          },
          onMousemove: (evt) => {
            if (!execMouseEvt) {
              return;
            }

            const ox = evt.offsetX;
            const oy = evt.offsetY;
            const cx = evt.clientX;
            const cy = evt.clientY;

            curEvt = {ox, oy, cx, cy, target: evt.target};

            isFocusing.value = true;
            updateTooltip();
          },
          onMouseleave: () => {
            isFocusing.value = false;
            setStatus(false);
          }
        },
        renderElements()
      ),
      h('div', {
        'class': 'sparkline__tooltip',
        display: 'none',
        style: {...props.tooltipStyles, position: 'fixed'},
        ref: tooltip
      })
    ])
  }
</script>

<template>
  <render />
</template>

<style lang="scss">
  .ui-sparkline--blue {
    stroke: #3d85de;

    &.ui-sparkline--filled {
      fill: rgba(61, 133, 222, 0.3);
    }
  }

  .ui-sparkline--green {
    stroke: #4c9b4c;
  }

  .ui-sparkline--green.ui-sparkline--filled {
    fill: rgba(76, 155, 76, 0.3);
  }

  .ui-sparkline--gray {
    stroke: #777;
  }

  .ui-sparkline--gray.ui-sparkline--filled {
    fill: rgba(119, 119, 119, 0.3);
  }

  .ui-sparkline--orange {
    stroke: #e36023;
  }

  .ui-sparkline--orange.ui-sparkline--filled {
    fill: rgba(227, 96, 35, 0.3);
  }

  .ui-sparkline--purple {
    stroke: #573585;
  }

  .ui-sparkline--purple.ui-sparkline--filled {
    fill: rgba(87, 53, 133, 0.3);
  }

  .ui-sparkline--yellow {
    stroke: #fdd700;
  }

  .ui-sparkline--yellow.ui-sparkline--filled {
    fill: rgba(253, 215, 0, 0.3);
  }

  .ui-sparkline--pink {
    stroke: #db3b9e;
  }

  .ui-sparkline--pink.ui-sparkline--filled {
    fill: rgba(219, 59, 158, 0.3);
  }
</style>
