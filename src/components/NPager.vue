<!--
  This file is part of nano.

  Author(s):
   - Ian Firns <firnsy@gmail.com>

  License: MIT
-->
<script setup>
import { computed, toRef } from 'vue';

// CONSTANTS
const maxEntries = 7;

// PROPS
const props = defineProps({
  pager: {
    type: Object,
    required: true
  }
});

// EMITS
const emit = defineEmits(['update']);

// REACTIVE
const pager = toRef(props, 'pager');

// returns true if we can perform a page next action
const canPageNext = computed(() => (
  pager.value.pageCount > 0
      && pager.value.page < pager.value.pageCount
));

// returns true if we can perform a page previous action
const canPagePrevious = computed(() => (
  pager.value.pageCount > 0
      && pager.value.page > 1
));

// only return a maximum of maxEntries pages left and right of current page
// if on page 1, show 1,2,3,...,last
// if on page 2, show 1,2,3,...,last
// if on page 3, show 1,2,3,4,...,last
// if on page 4, show 1,2,3,4,5,...,last
// if on page 5, show 1,...,3,4,5,6,7,...,last
// if on page 6, show 1,...,4,5,6,7,8,...,last
const pageList = computed(() => {
  const firstPage = 1;
  const lastPage = pager.value.pageCount;
  const list = [];

  if (pager.value.pageCount > 0) {
    const spread = Math.floor((maxEntries - 2) / 2);
    let start = pager.value.page - spread;
    let end = pager.value.page + spread;

    // push first page
    list.push({
      label: 1,
      page: firstPage,
      link: true,
      current: isActive(firstPage)
    });

    // push intermediate pages to a max of maxEntries pages - 2 (minus first and last which are always there)

    const maxLoop = maxEntries - 2;

    if (pager.value.pageCount < maxEntries) {
      if (pager.value.pageCount > 2) {
        // start at 2, because we already pushed page 1 and stop at pager.value.pageCount - 1
        // because we will push last page later
        end = pager.value.pageCount - 1;

        for (let i = 2; i <= end; i++) {
          list.push({
            label: i,
            page: i,
            link: true,
            current: isActive(i)
          });
        }
      }
    } else {
      if (start < firstPage + 1) {
        end += Math.abs(firstPage - start + 1);
        start = firstPage + 1;
      }

      if (end > lastPage - 1) {
        start -= Math.abs(lastPage - end - 1);
        end = lastPage - 1;
      }

      for (let i = start, c = 0; (i <= end) && (c < maxLoop); i++, c++) {
        list.push({
          label: i,
          page: i,
          link: true,
          current: isActive(i)
        });
      }
    }

    // push last page (if we have more than one page)
    if (pager.value.pageCount > 1) {
      list.push({
        label: lastPage,
        page: lastPage,
        link: true,
        current: isActive(lastPage)
      });
    }

    // calculate jumps
    if (lastPage > maxEntries) {
      if (list.at(1).page > (list.at(0).page + 1)) {
        list.splice(1, 1, {
          label: '&hellip;',
          link: false
        });
      }

      if (list.at(-1).page > (list.at(-2).page + 1)) {
        list.splice(list.length - 2, 1, {
          label: '&hellip;',
          link: false
        });
      }
    }
  }

  return list;
});

// FUNCTIONS
function isActive(page) {
  return page === pager.value.page;
}

function pageJump(page) {
  if (!isActive(page)) {
    emit('update', { page });
  }
}

function pageNext() {
  pageJump(pager.value.page + 1);
}

function pagePrevious() {
  pageJump(pager.value.page - 1);
}
</script>

<template>
  <div class="n-pager">
    <nav aria-label="pagination">
      <ul>
        <li>
          <a
            aria-label="Goto previous page"
            :disabled="!canPagePrevious ? '' : null"
            @click="pagePrevious()"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              height="16"
              width="10"
              viewBox="0 0 320 512"
            ><path d="M34.5 239L228.9 44.7c9.4-9.4 24.6-9.4 33.9 0l22.7 22.7c9.4 9.4 9.4 24.5 0 33.9L131.5 256l154 154.8c9.3 9.4 9.3 24.5 0 33.9l-22.7 22.7c-9.4 9.4-24.6 9.4-33.9 0L34.5 273c-9.4-9.4-9.4-24.6 0-33.9z" /></svg>
          </a>
        </li>
        <li
          v-for="page in pageList"
          :key="page"
        >
          <a
            v-if="page.link"
            :aria-label="`Goto page ${page.label}`"
            :aria-selected="page.current ? true : null"
            :aria-current="page.current ? 'page' : null"
            :disabled="page.current ? '' : null"
            @click="pageJump(page.page)"
          >{{ page.label }}</a>
          <span
            v-else
            class="separator"
          >&hellip;</span>
        </li>
        <li>
          <a
            aria-label="Goto next page"
            :disabled="!canPageNext ? '' : null"
            @click="pageNext()"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              height="16"
              width="10"
              viewBox="0 0 320 512"
            ><path d="M285.5 273L91.1 467.3c-9.4 9.4-24.6 9.4-33.9 0l-22.7-22.7c-9.4-9.4-9.4-24.5 0-33.9L188.5 256 34.5 101.3c-9.3-9.4-9.3-24.5 0-33.9l22.7-22.7c9.4-9.4 24.6-9.4 33.9 0L285.5 239c9.4 9.4 9.4 24.6 0 33.9z" /></svg>
          </a>
        </li>
      </ul>
    </nav>
  </div>
</template>

<style lang="scss">
  nav[aria-label="pagination"] {
    a {
      cursor: pointer;

      &[disabled],
      &[aria-disabled=true] {
        cursor: not-allowed;
      }
    }
  }
</style>
