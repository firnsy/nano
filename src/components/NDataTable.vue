<!--
  This file is part of nano.

  Author(s):
   - Ian Firns <firnsy@gmail.com>

  Based on research from:
    - https://pencilandpaper.io/articles/ux-pattern-analysis-enterprise-data-tables/
    - https://www.halo-lab.com/blog/the-ultimate-guide-to-designing-data-tables
  License: MIT
-->
<script setup>
import {
  computed, toRaw, toRefs
} from 'vue';

import { SORT_ORDER_NONE, SORT_ORDER_ASC, SORT_ORDER_DESC } from '@/composables/useDataTable';
import { merge } from '@/utilities/merge';

// CONSTANTS
const TYPE_CLASSES = {
  number: 'n-number',
  date: 'n-date',
  datetime: 'n-datetime',
  time: 'n-time',
  string: 'n-string'
};

const DEFAULT_TYPE_CLASS = 'n-string';

// PROPS
const props = defineProps({
  selection: {
    type: Object,
    default: null
  },
  rows: {
    type: Array,
    default() {
      return [];
    }
  },
  columns: {
    type: Array,
    default() {
      return [];
    }
  },
  sorter: {
    type: Object,
    default: null
  }
});

// EMITS
const emit = defineEmits(['update', 'selection-update', 'row-update', 'column-update']);

// REACTIVE
const draggable = true;

const { selection, sorter } = toRefs(props);

const hasRows = computed(() => props.rows.length > 0);

const hasSelection = computed(() => Object.keys(selection.value.indexes).length > 0);

const selectable = computed(() => props.selection !== null);

const visibleColumns = computed(() => props.columns
  .filter((column) => column.visible)
  .map((column) => merge(
    {
      filterable: true,
      sortable: true,
      sort: {
        direction: SORT_ORDER_NONE
      },
      type: 'string'
    },
    {
      ...column,
      classes: getClasses(column),
      sort: sorter.value[column.name] ?? {}
    }
  )));

const totalColumns = computed(() => visibleColumns.value.length + (selectable.value ? 1 : 0));

// FUNCTIONS

/**
 */
function getClasses(column) {
  const classes = [];

  if (column.sortable) {
    classes.push('sortable');
  }

  classes.push(TYPE_CLASSES[column.type] ?? DEFAULT_TYPE_CLASS);

  return classes;
}

/**
 * @desc Get the check state for a given item.
 *
 * @param {Object} item
 * @returns {Boolean}
 */
function getCheckState(item) {
  return !!(
    // eslint-disable-next-line no-bitwise
    selection.value.global
      ^ selection.value.indexes[item[props.selection.column]]
  );
}

/**
 * @desc Get the sort direction for a given column.
 *
 * @param {Object} column
 * @returns {Number}
 */
function globalClick() {
  emit('selection-update', {
    global: !selection.value.global
  });

  emit('update', {
    selection: {
      global: !selection.value.global
    }
  });
}

/**
 * @desc For sortable tables, handles the click on a row item's checkbox.
 *
 * @param {Object} item
 */
function itemSelectionClick(item) {
  emit('selection-update', {
    index: item[props.selection.column],
    state: !getCheckState(item),
    global: selection.value.global
  });

  emit('update', {
    selection: {
      index: item[props.selection.column],
      state: !getCheckState(item),
      global: selection.value.global
    }
  });
}

/**
 * @desc Handle a click on a column header.
 * For sortable columns this will update the sort state.
 *
 * @param {Object} column
 */
function columnHeaderClick(column) {
  if (column.sortable) {
    const sort = {
      ...toRaw(sorter.value)
    };

    sort[column.name] = {
      direction: ((column?.sort?.direction ?? 0) + 1) % 3,
      priority: 0
    };

    emit('update', { sort });
  }
}

function onColumnDragStart(event) {
  event.dataTransfer.setData('text/plain', event.target.dataset.index);
}

function onColumnDragEnter(event) {
  event.currentTarget.classList.add('hover');
}

function onColumnDragLeave(event) {
  event.currentTarget.classList.remove('hover');
}

function onColumnDragOver(event) {
  event.preventDefault();
}

function onColumnDrop(event) {
  event.preventDefault();

  const itemIndex = event.dataTransfer.getData('text/plain');
  const droppedIndex = event.target.parentNode.dataset.index;

  emit('column-update', {
    shift: {
      from: itemIndex,
      to: droppedIndex
    }
  });
}

function onRowDragStart(event) {
  event.dataTransfer.setData('text/plain', event.target.dataset.index);
}

function onRowDragEnter(event) {
  event.currentTarget.classList.add('hover');
}

function onRowDragLeave(event) {
  event.currentTarget.classList.remove('hover');
}

function onRowDragOver(event) {
  event.preventDefault();
}

function onRowDrop(event) {
  event.preventDefault();

  const itemIndex = event.dataTransfer.getData('text/plain');
  const droppedIndex = event.target.parentNode.dataset.index;

  emit('row-update', {
    shift: {
      from: itemIndex,
      to: droppedIndex
    }
  });
}

</script>

<template>
  <div class="n-data-table">
    <div class="overflow-auto">
      <table :class="{'selectable': selectable, 'draggable': draggable, 'empty': !hasRows}">
        <thead>
          <tr>
            <th v-if="selectable">
              <input
                type="checkbox"
                :checked="selection.global"
                :indeterminate.prop="hasSelection"
                @click="globalClick"
              >
            </th>
            <th
              v-for="(column, colIndex) in visibleColumns"
              :key="column.name"
              :class="column.classes"
              :data-index="colIndex"
              :draggable
              @click="columnHeaderClick(column)"
              @dragstart="onColumnDragStart"
              @dragenter="onColumnDragEnter"
              @dragleave="onColumnDragLeave"
              @dragover="onColumnDragOver"
              @drop="onColumnDrop"
            >
              <span>{{ column.label }}</span>
              <span v-if="column.sortable">
                <svg
                  v-if="column.sort.direction === SORT_ORDER_DESC"
                  xmlns="http://www.w3.org/2000/svg"
                  height="16"
                  width="10"
                  viewBox="0 0 320 512"
                ><path d="M41 288h238c21.4 0 32.1 25.9 17 41L177 448c-9.4 9.4-24.6 9.4-33.9 0L24 329c-15.1-15.1-4.4-41 17-41z" /></svg>
                <svg
                  v-else-if="column.sort.direction === SORT_ORDER_ASC"
                  xmlns="http://www.w3.org/2000/svg"
                  height="16"
                  width="10"
                  viewBox="0 0 320 512"
                ><path d="M279 224H41c-21.4 0-32.1-25.9-17-41L143 64c9.4-9.4 24.6-9.4 33.9 0l119 119c15.2 15.1 4.5 41-16.9 41z" /></svg>
                <svg
                  v-else
                  xmlns="http://www.w3.org/2000/svg"
                  height="16"
                  width="10"
                  viewBox="0 0 320 512"
                ><path d="M272 288H48.1c-42.6 0-64.2 51.7-33.9 81.9l111.9 112c18.7 18.7 49.1 18.7 67.9 0l112-112c30-30.1 8.7-81.9-34-81.9zM160 448L48 336h224L160 448zM48 224h223.9c42.6 0 64.2-51.7 33.9-81.9l-111.9-112c-18.7-18.7-49.1-18.7-67.9 0l-112 112C-16 172.2 5.3 224 48 224zM160 64l112 112H48L160 64z" /></svg>
              </span>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr v-if="!hasRows">
            <td
              class="no-data"
              :colspan="totalColumns"
            >
              No data to show.
            </td>
          </tr>
          <tr
            v-for="(item, rowIndex) in props.rows"
            :key="item.id"
            :data-index="rowIndex"
            :draggable
            @dragstart="onRowDragStart"
            @dragenter="onRowDragEnter"
            @dragleave="onRowDragLeave"
            @dragover="onRowDragOver"
            @drop="onRowDrop"
          >
            <td
              v-if="selectable"
              scope="row"
            >
              <input
                type="checkbox"
                :checked="getCheckState(item)"
                @click="itemSelectionClick(item)"
              >
            </td>
            <td
              v-for="column in visibleColumns"
              :key="column.name"
              :class="column.classes"
            >
              {{ column.renderer(item[column.name]) }}
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</template>

<style lang="scss">
  table {
    th {
      white-space: nowrap;
    }

    td,
    th {
      padding: .5rem;
      white-space: nowrap;
    }

    thead th {
      vertical-align: middle;

      &.sortable {
        cursor: pointer;
      }

      span {
        user-select: none
      }
    }

    .no-data {
      text-align: center;
    }

    &.selectable {
      thead th:first-child,
      tbody td:first-child {
        left: 0;
        position: sticky;
        padding-left: 0.5rem;
        width: 1.5rem;
        z-index: 2;
      }

      tbody td:not(:first-child) {
      }
    }

    &.draggable {
      tr {
        th:first-child,
        td:first-child {
          padding-left: 0.5rem;

          &:before {
            content: "";
            background-position: center;
            background-repeat: no-repeat;
            display: inline-block;
            height: 1rem;
            margin-right: 0.5rem;
            width: 0.625rem;
          }
        }
      }
    }

    &.draggable:not(.empty) {
      tr:hover {
        td:first-child {
          &:before {
            background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 320 512'%3E%3Cpath d='M96 32H32C14.3 32 0 46.3 0 64v64c0 17.7 14.3 32 32 32h64c17.7 0 32-14.3 32-32V64c0-17.7-14.3-32-32-32zm0 160H32c-17.7 0-32 14.3-32 32v64c0 17.7 14.3 32 32 32h64c17.7 0 32-14.3 32-32v-64c0-17.7-14.3-32-32-32zm0 160H32c-17.7 0-32 14.3-32 32v64c0 17.7 14.3 32 32 32h64c17.7 0 32-14.3 32-32v-64c0-17.7-14.3-32-32-32zM288 32h-64c-17.7 0-32 14.3-32 32v64c0 17.7 14.3 32 32 32h64c17.7 0 32-14.3 32-32V64c0-17.7-14.3-32-32-32zm0 160h-64c-17.7 0-32 14.3-32 32v64c0 17.7 14.3 32 32 32h64c17.7 0 32-14.3 32-32v-64c0-17.7-14.3-32-32-32zm0 160h-64c-17.7 0-32 14.3-32 32v64c0 17.7 14.3 32 32 32h64c17.7 0 32-14.3 32-32v-64c0-17.7-14.3-32-32-32z'/%3E%3C/svg%3E");
            cursor: grab;
            margin-top: -.125rem;
            opacity: 0.5;
          }
        }
      }
    }

    &.selectable.draggable {
      tr {
        th:first-child,
        td:first-child {
          padding-left: 0.5rem;
        }
      }
    }

    thead,
    tbody {
      .n-number,
      .n-datetime,
      .n-date,
      .n-time {
        text-align: right;
      }
    }
  }
</style>
