import { defineStore } from 'pinia';

const basicState = {
  component: null,
  props: {}
};

export default defineStore('modal-store', {
  state: () => ({
    modalState: basicState
  }),
  actions: {
    closeModal() {
      // Reset our state
      this.modalState = basicState;

      // Get the body element
      const { body } = document;

      // if its there, reset overflow style
      if (body) {
        body.style.overflow = 'auto';
      }
    },

    openModal(payload) {
      // Get props and component from payload passed to function
      const { props, component } = payload;

      // Get the body element
      const { body } = document;

      // if its there, prevent scroll from happening
      if (body) {
        body.style.overflow = 'hidden';
      }

      // Assign them to our state
      this.modalState = {
        component,
        props: props || {}
      };
    }
  },
  getters: {}
});
