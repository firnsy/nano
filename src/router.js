import { createRouter, createWebHistory } from 'vue-router';

import HomeView from '@/views/HomeView.vue';

import AccordionView from '@/views/docs/AccordionView.vue';
import AlertView from '@/views/docs/AlertView.vue';
import ButtonView from '@/views/docs/ButtonView.vue';
import CardView from '@/views/docs/CardView.vue';
import ColorSchemesView from '@/views/docs/ColorSchemesView.vue';
import ColorsView from '@/views/docs/ColorsView.vue';
import ContainerView from '@/views/docs/ContainerView.vue';
import CssVariablesView from '@/views/docs/CssVariablesView.vue';
import DropdownView from '@/views/docs/DropdownView.vue';
import FormCheckboxesView from '@/views/docs/FormCheckboxesView.vue';
import FormInputView from '@/views/docs/FormInputView.vue';
import FormOverviewView from '@/views/docs/FormOverviewView.vue';
import FormRadiosView from '@/views/docs/FormRadiosView.vue';
import FormRangeView from '@/views/docs/FormRangeView.vue';
import FormSelectView from '@/views/docs/FormSelectView.vue';
import FormSwitchView from '@/views/docs/FormSwitchView.vue';
import FormTextareaView from '@/views/docs/FormTextareaView.vue';
import GridView from '@/views/docs/GridView.vue';
import GroupView from '@/views/docs/GroupView.vue';
import LandmarkSectionView from '@/views/docs/LandmarkSectionView.vue';
import LinkView from '@/views/docs/LinkView.vue';
import LoadingView from '@/views/docs/LoadingView.vue';
import MissionView from '@/views/docs/MissionView.vue';
import ModalView from '@/views/docs/ModalView.vue';
import NavView from '@/views/docs/NavView.vue';
import NoteView from '@/views/docs/NoteView.vue';
import OverflowAutoView from '@/views/docs/OverflowAutoView.vue';
import ProgressView from '@/views/docs/ProgressView.vue';
import QuickStartView from '@/views/docs/QuickStartView.vue';
import SassView from '@/views/docs/SassView.vue';
import TableView from '@/views/docs/TableView.vue';
import TabsView from '@/views/docs/TabsView.vue';
import TooltipView from '@/views/docs/TooltipView.vue';
import TypographyView from '@/views/docs/TypographyView.vue';

import NanoModalView from '@/views/docs/vue-components/NanoModalView.vue';
import NanoDataTableView from '@/views/docs/vue-components/NanoDataTableView.vue';
import NanoSparklineView from '@/views/docs/vue-components/NanoSparklineView.vue';
import NanoTabsView from '@/views/docs/vue-components/NanoTabsView.vue';
import NanoPasswordView from '@/views/docs/vue-components/NanoPasswordView.vue';
import NanoValidationView from '@/views/docs/vue-components/NanoValidationView.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    // getting started
    {
      path: '/docs',
      name: 'docs',
      component: QuickStartView,
      meta: { section: 'getting-started' }
    },
    {
      path: '/docs/color-schemes',
      name: 'color-schemes',
      component: ColorSchemesView,
      meta: { section: 'getting-started' }
    },
    // customisation
    {
      path: '/docs/css-variables',
      name: 'css-variables',
      component: CssVariablesView,
      meta: { section: 'customisation' }
    },
    {
      path: '/docs/sass',
      name: 'sass',
      component: SassView,
      meta: { section: 'customisation' }
    },
    {
      path: '/docs/colors',
      name: 'colors',
      component: ColorsView,
      meta: { section: 'customisation' }
    },
    // layout
    {
      path: '/docs/container',
      name: 'container',
      component: ContainerView,
      meta: { section: 'layout' }
    },
    {
      path: '/docs/landmark-section',
      name: 'landmark-section',
      component: LandmarkSectionView,
      meta: { section: 'layout' }
    },
    {
      path: '/docs/grid',
      name: 'grid',
      component: GridView,
      meta: { section: 'layout' }
    },
    {
      path: '/docs/overflow-auto',
      name: 'overflow-auto',
      component: OverflowAutoView,
      meta: { section: 'layout' }
    },
    // content
    {
      path: '/docs/typography',
      name: 'typography',
      component: TypographyView,
      meta: { section: 'content' }
    },
    {
      path: '/docs/link',
      name: 'link',
      component: LinkView,
      meta: { section: 'content' }
    },
    {
      path: '/docs/button',
      name: 'button',
      component: ButtonView,
      meta: { section: 'content' }
    },
    {
      path: '/docs/table',
      name: 'table',
      component: TableView,
      meta: { section: 'content' }
    },
    // forms
    {
      path: '/docs/forms/overview',
      name: 'forms-overview',
      component: FormOverviewView,
      meta: { section: 'forms' }
    },
    {
      path: '/docs/forms/input',
      name: 'forms-input',
      component: FormInputView,
      meta: { section: 'forms' }
    },
    {
      path: '/docs/forms/textarea',
      name: 'forms-textarea',
      component: FormTextareaView,
      meta: { section: 'forms' }
    },
    {
      path: '/docs/forms/select',
      name: 'select',
      component: FormSelectView,
      meta: { section: 'forms' }
    },
    {
      path: '/docs/forms/checkboxes',
      name: 'forms-checkboxes',
      component: FormCheckboxesView,
      meta: { section: 'forms' }
    },
    {
      path: '/docs/forms/radios',
      name: 'forms-radios',
      component: FormRadiosView,
      meta: { section: 'forms' }
    },
    {
      path: '/docs/forms/switch',
      name: 'forms-switch',
      component: FormSwitchView,
      meta: { section: 'forms' }
    },
    {
      path: '/docs/forms/range',
      name: 'forms-range',
      component: FormRangeView,
      meta: { section: 'forms' }
    },
    // components
    {
      path: '/docs/accordion',
      name: 'accordion',
      component: AccordionView,
      meta: { section: 'components' }
    },
    {
      path: '/docs/alert',
      name: 'alert',
      component: AlertView,
      meta: { section: 'components' }
    },
    {
      path: '/docs/card',
      name: 'card',
      component: CardView,
      meta: { section: 'components' }
    },
    {
      path: '/docs/dropdown',
      name: 'dropdown',
      component: DropdownView,
      meta: { section: 'components' }
    },
    {
      path: '/docs/group',
      name: 'group',
      component: GroupView,
      meta: { section: 'components' }
    },
    {
      path: '/docs/loading',
      name: 'loading',
      component: LoadingView,
      meta: { section: 'components' }
    },
    {
      path: '/docs/modal',
      name: 'modal',
      component: ModalView,
      meta: { section: 'components' }
    },
    {
      path: '/docs/nav',
      name: 'nav',
      component: NavView,
      meta: { section: 'components' }
    },
    {
      path: '/docs/note',
      name: 'note',
      component: NoteView,
      meta: { section: 'components' }
    },
    {
      path: '/docs/progress',
      name: 'progress',
      component: ProgressView,
      meta: { section: 'components' }
    },
    {
      path: '/docs/tooltip',
      name: 'tooltip',
      component: TooltipView,
      meta: { section: 'components' }
    },
    {
      path: '/docs/tabs',
      name: 'tabs',
      component: TabsView,
      meta: { section: 'components' }
    },
    // vue-components
    {
      path: '/docs/vue/modal',
      name: 'n-modal',
      component: NanoModalView,
      meta: { section: 'vue-components' }
    },
    {
      path: '/docs/vue/data-table',
      name: 'n-data-table',
      component: NanoDataTableView,
      meta: { section: 'vue-components' }
    },
    {
      path: '/docs/vue/password',
      name: 'n-passwordj',
      component: NanoPasswordView,
      meta: { section: 'vue-components' }
    },
    {
      path: '/docs/vue/sparkline',
      name: 'n-sparkline',
      component: NanoSparklineView,
      meta: { section: 'vue-components' }
    },
    {
      path: '/docs/vue/tabs',
      name: 'n-tabs',
      component: NanoTabsView,
      meta: { section: 'vue-components' }
    },
    {
      path: '/docs/vue/validation',
      name: 'n-validation',
      component: NanoValidationView,
      meta: { section: 'vue-components' }
    },
    // about
    {
      path: '/docs/mission',
      name: 'mission',
      component: MissionView,
      meta: { section: 'about' }
    }
  ]
});

export default router;
