export default {
  install: (app, options) => {
    const { hljs } = options;
    app.directive('highlight', (el) => {
      if (!el.hasAttribute('data-highlighted')) {
        hljs.highlightElement(el);
      }
    });
  }
};
