import { fileURLToPath, URL } from 'node:url';
import path from 'node:path';

import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

export default defineConfig({
  define: {
    __VUE_PROD_HYDRATION_MISMATCH_DETAILS__: 'false',
  },
  plugins: [
    vue({
      template: {
        compilerOptions: {
          whitespace: 'preserve'
        }
      }
    }),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  build: {
    outDir: path.resolve(__dirname, './dist/lib'),
    lib: {
      entry: path.resolve(__dirname, 'src/nano-vue.js'),
      formats: ['es'],
      name: 'NanoVue',
      fileName: (format) => `nano-vue.js`
    },
    rollupOptions: {
      external: ['vue'],
      output: {
        assetFileNames: (assetInfo) => {
          if (assetInfo.name === 'style.css') {
            return 'nano-vue.css';
          }

          return assetInfo.name;
        },
        globals: {
          vue: 'Vue'
        },
      }
    },
  },
})
